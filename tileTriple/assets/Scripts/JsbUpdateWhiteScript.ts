import CCLog from "./CCLog";


import GameTileStaticData from "./GameStaticData";

import { JsbTileTrileTileCallMgr } from "./Common/JsbMgr";
import getEventEmiter from "./Libraries/EventEmitter";
import { CUSTOM_EVENT_NAME } from "./Common/Constant";
import ProgressController from "./Game/ProgressController";
import GameSceneController from "./Scenes/GameSceneController";
import GameTileStaticJsonData from "./GameStaticJsonData";
import { GameData, GetAccountSelfAdd, GetReceiveAward } from "./Common/GameData";
import { BizTypeEnum } from "./net/api";
import GameStaticData from "./GameStaticData";
import PropAreaController from "./Game/PropAreaController";



export default class JsbUpdateWhiteScript  {



    delegate = null;
    

    private static _instance:JsbUpdateWhiteScript = null
    public static get Instance():JsbUpdateWhiteScript  {
        if(JsbUpdateWhiteScript._instance == null){
            JsbUpdateWhiteScript._instance = new JsbUpdateWhiteScript();
        }

        return JsbUpdateWhiteScript._instance;
    }  

    private constructor(){
        this.delegate = {
            updateWhiteBao(){
                let isWhite =  JsbTileTrileTileCallMgr.handler("requestIsWhiteBao","()Ljava/lang/String;")
                if (isWhite == "true") {
                    GameTileStaticData.isShenHeModel = true
                }else{
                    GameTileStaticData.isShenHeModel = false
                }
                CCLog.log("++++++刷新白包",GameTileStaticData.isShenHeModel)
                
                if (!GameTileStaticData.isShenHeModel) {
                    //还原UI
                    
                    if (GameSceneController.I) {
                        GameSceneController.I.controleShenHeNode(!GameTileStaticData.isShenHeModel)
                    }
                    if (ProgressController.I) {
                        ProgressController.I.refresh()
                    }
                    
                    getEventEmiter().emit(CUSTOM_EVENT_NAME.updateRefreshWhiteBao);
                }
                //测试
                //JsbCocosCallMgr.handler('showWithdraw',"()V");
            },

            
            getH5AdTaskReward(){
                //cocos调安卓
                CCLog.log("++++++h5活动回传")
                let sendJson = {secTime:GameTileStaticJsonData.h5Activity.t+"",
                miniSecTime:GameTileStaticJsonData.h5Activity.mt+"",
                moneyrate:GameTileStaticJsonData.getMoneyChangeRate+"",
                bonus:GameTileStaticJsonData.getLevelPassRewardMoney(GameData.AccountData.moneytotal)+"",
                totalMoney:GameData.AccountData.moneytotal+""}
                JsbTileTrileTileCallMgr.handler("getH5AdTaskReward","(Ljava/lang/String;)v",JSON.stringify(sendJson))
            },
          
            h5RewardFromAndroid(rewardStr:string){
                CCLog.log("++++++h5接受奖励")
                //cocos 更新账户 上传服务器
                let reward = GameTileStaticJsonData.getTwoPointNum(Number(rewardStr))
                GetReceiveAward(BizTypeEnum.v2_h5Reward,0,reward)
                GetAccountSelfAdd(null,0,reward);
               
            },
            QueryFraudUser(FraudUser:string){
                if (FraudUser == "true") {
                    GameStaticData.isnotLiveuserH5 = FraudUser
                }else{
                    GameStaticData.isnotLiveuserH5 = "false"
                }
                if (PropAreaController.I) {
                    PropAreaController.I.updateBtnPos()
                }
            },
            tixianMoneyFromAndroid(rewardStr:string){
                CCLog.log("++++++安卓提现返回奖励",rewardStr)
                //cocos 更新账户 上传服务器
                let reward = GameTileStaticJsonData.getTwoPointNum(Number(rewardStr))
                GetAccountSelfAdd(null,0,reward,false);
               
            }
        }
    }
}
