class GlobalSystem {
    public initialize(): void {
        const manager = cc.director.getCollisionManager();
        manager.enabled = true;
        // manager.enabledDebugDraw = true;

        cc.macro.ENABLE_MULTI_TOUCH = false;
    }
}

export default new GlobalSystem();