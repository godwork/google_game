import { SCENES_NAME } from "../Common/Constant";
import { reportLevelEnter } from "./DataCollectSystem";
import SceneManagerSystem from "./SceneManagerSystem";

class GamingSystem {
    public mapInfo: IMapInfo = null;
    public gameOver: boolean = false;
    public gameComplete = false;
    public isPause: boolean = false;
    public mapLayersNum: number;
    public mapBlocksNum: number;
    public maxLevelNumber: number;
    public bundleLevels: cc.AssetManager.Bundle;
    public maps: { [index: number]: IMapInfo } = {};

    public async initialize() {
        this.bundleLevels = await new Promise((resolve: Function, reject: Function) => {
            cc.assetManager.loadBundle('bundle_levels', (err: Error, bundle: cc.AssetManager.Bundle) => {
                if (err) {
                  
                    //console.log("加载关卡信息=="+err);
                    
                    reject(err);
                } else {
                
                    //console.log("加载关卡成功=="+err);
                    resolve(bundle);
                }
            });
        });
        this.maxLevelNumber = this.bundleLevels.getDirWithPath("/").length;
    }

    public loadMap(level: number) {
        if (level in this.maps) {
            this.mapInfo = this.maps[level];
            this.mapInfo.level = level;
            const map = this.mapInfo.map;
            this.mapLayersNum = map.length;
            this.mapBlocksNum = map.reduce((num: number, v: IMapLayerInfo) => { return num + v.x.length }, 0);
            this.preLoadMap(level + 1);
            return;
        }
        return new Promise((resolve: Function, reject: Function) => {
            let relevel = level
            if (level > this.maxLevelNumber){
                relevel = Math.ceil(Math.random()*this.maxLevelNumber)
            }
            
            this.bundleLevels.load(`${relevel.toString()}`, (e: Error, res: cc.JsonAsset) => {
                if (e) {
                    this.mapInfo = null;
                    resolve();
                } else {
                    this.mapInfo = res.json;
                    this.mapInfo.level = level;
                    const map = this.mapInfo.map;
                    this.mapLayersNum = map.length;
                    this.mapBlocksNum = map.reduce((num: number, v: IMapLayerInfo) => { return num + v.x.length }, 0);
                        
                    this.preLoadMap(level + 1);
                    resolve();
                }
            });
        });
    }

    public preLoadMap(level: number): void {
        if (level in this.maps) return;
        let relevel = level
        if (level > this.maxLevelNumber){
            relevel = Math.ceil(Math.random()*this.maxLevelNumber)
        }
        this.bundleLevels.load(`${relevel.toString()}`, (e: Error, res: cc.JsonAsset) => {
            if (e) {
               
            } else {
                this.maps[level] = res.json;
              
            }
        });
    }

    public init(): void {
        this.gameOver = false;
        this.gameComplete = false;
        this.isPause = false;
    }


    public checkCurrentLevel(): boolean {
        if (this.mapInfo === null) {
            if (cc.sys.platform == cc.sys.WECHAT_GAME) {
                wx.showModal({
                    title: '通关了！',
                    content: '更多关卡正在路上！'
                });
            } else {
                cc.log('通关了,没有关卡可以玩了');
            }
            return false;
        }
        return true;
    }

    public realStartLevel(autoChangeScene: boolean = true): void {
        if (autoChangeScene) SceneManagerSystem.sceneChange(SCENES_NAME.GameScene);
        if (this.mapInfo){
            reportLevelEnter(this.mapInfo.level);
        }
    }
}

export default new GamingSystem();