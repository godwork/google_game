

export const LOG_KEY_GAME_PAGE = {
    timeBox: '',
    timeBoxSuccess: '',
    recall: '',
    recallVideo: '',
    recallVideoSuccess: '',
    refresh: '',
    refreshVideo: '',
    refreshVideoSuccess: '',
    hint: '',
    hintVideo: '',
    hintVideoSuccess: '',
};

export const LOG_KEY_DAILY_REWARD = {
    collect: '',
    boxVideo: '',
    boxVideoSuccess: '',
    collectDouble: '',
    collectDoubleSuccess: '',
};

export const TILI_REWARD = {
    buttonClick: '',
    getRewardSuccess: '',
};

export const LOADING_PAGE = {
    showLoadingSuccess: '',
    showNextScneSuccess: '',
}






export function reportUserBehaviorBranchAnalytics(id: string, dim: string = '0', evevtType: 1 | 2 = 1) {
   
}

export function reportLevelEnter(level: number): void {
    
}

export function reportLevelFail(level: number): void {
   
}

export function reportLevelComplete(level: number): void {

}
