// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
import ArchiveSystem from "../System/ArchiveSystem";
import CocosTileHelper from "../Common/CocosHelper"
import { showInterstitialAd } from "../Common/AdMethods";
import LanguagetileTile from "../Common/Language";
import Archive from "../Common/Archive";
const {ccclass, property} = cc._decorator;

@ccclass
export default class phoneTip extends cc.Component {

    @property(cc.Sprite)
    ProgressBar: cc.Sprite = null;
    @property(cc.Sprite)
    bg: cc.Sprite = null;

    @property(cc.Label)
    decribeLabel: cc.Label = null;

    @property(cc.Label)
    progressLabel: cc.Label = null;

    @property([cc.SpriteFrame])
    bgList: cc.SpriteFrame[] = [];
    // LIFE-CYCLE CALLBACKS:

    public async onLoad () {

         await this.initCurent();
     }

    start () {

    }
    public  async initCurent()
    {

        let curTaskId = ArchiveSystem.localData.curTaskId;


        let curTaskFinish = ArchiveSystem.localData.curTaskFinish;

        let taskInfo = Archive.getTask(curTaskId-1);

       
        if(taskInfo.type == 1)
        {
            let str = LanguagetileTile.getWord("CompleteMorelevels");
         
            str = str.replace("#", taskInfo.count.toString());
            this.decribeLabel.string = str;
        }
        else if(taskInfo.type == 2)
        {
            let str =LanguagetileTile.getWord("CompleteMoreVideos");
          
            str = str.replace("#", taskInfo.count.toString());
            this.decribeLabel.string = str;
        }

        this.ProgressBar.fillRange = curTaskFinish / taskInfo.count;
        this.progressLabel.string = curTaskFinish + "/" + taskInfo.count;


        // let url = "jsResourc/game2/game/bgIcon/s" + (curTaskId+1);
        
        // let res:any = await CocosHelper.loadAssetSync(url,cc.SpriteFrame)
        
        // this.bg.spriteFrame = res;

        this.bg.spriteFrame = this.bgList[curTaskId];

    }
    closeBtn()
    {
        ArchiveSystem.localData.lastOpenPhoneTaskId = ArchiveSystem.localData.curTaskId;
        this.node.destroy();
        showInterstitialAd();
    }
    

    
    // update (dt) {}
}
