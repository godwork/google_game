// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

import AudioTool from "../System/AudioSystem";
import { AUDIOS_PATH } from "../Common/AudioConfig";
import LanguagetileTile, { Country } from "../Common/Language";
const {ccclass, property} = cc._decorator;
const names: string[] = [
    "jusuf",
    "aisyah",
    "Carita",
    "Anissa",
    "renae",
    "slamet",
    "Daud",
    "Bima",
    "Utari",
    "Irfan",
    "muhammad",
    "surya",
    "Setiawan",
    "lita",
    "Habib",
    "Ridwan",
    "Wynita",
    "iskandar",
    "joakim",
    "Zakaria",
    "Anwar",
    "ibrahim",
    "Mega",
    "Osama",
    "Zakiah",
    "Camelia",
    "nike",
    "bambang",
    "gobha",
    "Walten",
    "Justus",
    "Ford",
    "Melanie",
    "Darrel",
    "nomy",
    "ishmael",
    "beals",
    "Freedom",
    "Nakia",
    "FIynn",
    "Clayton",
    "ferris",
    "Jozef",
    "Rufin",
    "Arsalan",
    "Nanna",
    "jukka",
    "Cavell",
    "fergus",
    "Peigi"
  ];
@ccclass
export default class winMoneyPop extends cc.Component {

    @property(cc.Label)
    nameLabel: cc.Label = null;

    //多语言描述文字1
    @property(cc.Label)
    decsLabel1: cc.Label = null;

    //多语言描述文字2
    @property(cc.Label)
    decsLabel2: cc.Label = null;

    //多语言描述文字3 赢得钱多语言
    @property(cc.Label)
    moneyLable: cc.Label = null;

    isInit:boolean = false;
    myNameList:any = [];
    

    onLoad () 
    {
      this.decsLabel1.string = LanguagetileTile.getWord("CongratulationsPlayer");
      this.decsLabel2.string = LanguagetileTile.getWord("receiving");
      //this.moneyLable.string = Language.getWord("winMoney");
    }
    init()
    { 
        if(this.isInit == false)
        {
            this.isInit = true;
            this.myNameList = this.shuffleArray(names);
        }

    }
    show()
    {

        AudioTool.play(AUDIOS_PATH.new_record)

        this.init();
        const randomIndex = Math.floor(Math.random() * this.myNameList.length);
     
        const randomName = this.myNameList[randomIndex];
        this.nameLabel.string = randomName;
        this.moneyLable.string = LanguagetileTile.MoneyChar + this.getMoney();
        this.scaleNode(this.node);
        this.closePopupAfter3Seconds(this.node);
    }
    getMoney()
    {
      let c = LanguagetileTile.getCountry
      if(c == Country.BR) //巴西
      {
          return this.getRandombr();
      }
      else if(c == Country.PK)//巴基斯坦
      {
        return this.getRandomEs();
          
      }else if(c == Country.ID)//印尼
      {
        return this.getRandomID();
          
      } 
      else
      {
        return this.getRandomEs();
      }
    }
    getRandomEs(): number {
      const min = 20;
      const max = 30;
      const random = Math.random() * (max - min) + min;
      return parseFloat(random.toFixed(2));
    }
    getRandombr(): number {
      const min = 100;
      const max = 150;
      const random = Math.random() * (max - min) + min;
      return parseFloat(random.toFixed(1));
    }
    getRandomID(): number {
      const min = 300000;
      const max = 700000;
      const random = Math.floor(Math.random() * (max - min + 1) + min);
      return random;
    }
    shuffleArray(array: any[]): any[] {
        // custom sorting function that generates a random number between -1 and 1
        function compare(a: any, b: any) {
          return Math.random() - 0.5;
        }
      
        return array.sort(compare);
      }


    scaleNode(node: cc.Node) {
        node.scale = 0;
    cc.tween(node)
      .to(0.2, { scale: 1.1 })
      .to(0.1, { scale: 0.9 })
      .to(0.1, { scale: 1 })
      .start();
    }
    closePopupAfter3Seconds(popup: cc.Node) {
        cc.tween(popup)
          .delay(3)
          .to(0.1, { scale: 1.1 })
          .to(0.2, { scale: 0 })
          .call(() => {
            popup.active = false;
          })
          .start();
      }

    start () {

    }

    // update (dt) {}
}
