import { JsbTileTrileTileCallMgr } from "../Common/JsbMgr";
import LanguagetileTile from "../Common/Language";

//import NetShowLoading  from "../fxTs/NetLoading"
const {ccclass, property} = cc._decorator;

@ccclass
export default class NetShowError extends cc.Component {
   
    callback:Function = null;
    btnIsClick:Boolean = false;
    @property(cc.Label)
    btnText: cc.Label = null;

    @property(cc.Label)
    descText: cc.Label = null;

     onLoad () 
     {
       
        //cc.game.addPersistRootNode(this.node);
        //this.node.active = false;
     }
    start () {
        
    }
    
    public showNetError(callBack: Function = null): void
    {
        //NetShowLoading.I.HideLoading();
        this.btnText.string = LanguagetileTile.getWord("retry")
        this.descText.string = LanguagetileTile.getWord("networkerro")
        this.node.zIndex = cc.macro.MAX_ZINDEX
        this.btnIsClick = false;
        this.callback = callBack;
        this.node.scale = 1;
        this.node.active = true;
    }
    public inVisibleNetError(): void
    {
        this.node.active = false;
        this.callback = null;
        //NetShowLoading.I.HideLoading();
    }

    onClickReconnet()
    {
        this.btnIsClick = true;
         let isNetConnect = JsbTileTrileTileCallMgr.handler("isNetworkConnect","()Ljava/lang/String;")
        if (isNetConnect == "false") {
            this.node.active = false;
            this.scheduleOnce(()=>{
                this.node.active = true;
            },0.1)
            return
        }
        //NetShowLoading.I.showLoading();
        if(this.callback)
        {
            this.callback();
            
        }
        this.node.active = false;
        this.callback = null;
    
    }
    // update (dt) {}
}
