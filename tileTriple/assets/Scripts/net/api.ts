import { HttpTileCrushServerMgr } from "../HttpMgr";
let isLocalData = CC_JSB ? false : true;

//游戏登陆
export const HttpLogin = () => HttpTileCrushServerMgr.post("J_V2_UL",isLocalData)
//账号信息
export const HttpAccount = () => HttpTileCrushServerMgr.post("J_V2_ACI",isLocalData)
//关卡信息
export const HttpLevelInfo = () => HttpTileCrushServerMgr.post("J_V2_PSI",isLocalData,{biztype:BizTypeEnum.v2_pass})
//领取奖励
export const HttpReceiveAward = (biztype: BizTypeEnum,passNum,boxNum,extData,getType) => HttpTileCrushServerMgr.post("J_V2_PR",isLocalData,{biztype,passNum,boxNum,extData,getType})
//领取奖励
export const HttpCloseWindowCount = () => HttpTileCrushServerMgr.post("J_V2_UL",isLocalData)

//主界面挖宝信息
export const HttpUserWaBaoInfo = () => HttpTileCrushServerMgr.post("J_V2_T_I",isLocalData)
//看视频增加挖宝次数
export const HttpSeeVIdeoAddWaBaoNum = () => HttpTileCrushServerMgr.post("J_V2_T_V",isLocalData)


//获取配置
export const HttpGetJsonFromServer = () => HttpTileCrushServerMgr.post("J_V2_GC",isLocalData)
//上报奖励
export const HttpPostServerAward = (biztype: BizTypeEnum,coin,money) => HttpTileCrushServerMgr.post("J_V2_RP",isLocalData,{biztype,coin,money})


export enum BizTypeEnum{
    /**第一关引导*/
    v2_novice = "v2_novice",
    /**第二关引导*/
    v2_novice2 = "v2_novice2",
    /**"通关奖励"*/
    v2_pass = "v2_pass",
    /**"翻倍金币"*/
    v2_morec = "v2_morec",
    /**"翻倍现金"*/
    v2_morem = "v2_morem",
    /**"宝箱奖励"*/
    v2_box = "v2_box",
    /**"合成金币奖励"*/
    v2_cgcoin = "v2_cgcoin",
    /**"合成现金奖励"*/
    v2_cgmoney = "v2_cgmoney",
    /**"飞行红包奖励"*/
    v2_fly = "v2_fly",
    /**"消除奖励"*/
    v2_clear = "v2_clear",
    /**"挖宝获取奖励"*/
    v2_digmoney = "v2_digmoney",
    /**"挖宝获取奖励"*/
    v2_h5Reward = "v2_hd_adv"
}