import GamingSystem from "../System/GamingSystem";

export const PauseController = (target: any, name: string, descriptor: PropertyDescriptor) => {
    const oldValue = descriptor.value;

    descriptor.value = function (...args: any[]) {
        if (GamingSystem.isPause || GamingSystem.gameOver || GamingSystem.gameComplete) return;
        return oldValue.apply(this, args);
    }
}