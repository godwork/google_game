const AUDIO_PATH_PRE: string = '';

export const AUDIOS_PATH = {
    block_click: `${AUDIO_PATH_PRE}block_click`,
    button_click_01: `${AUDIO_PATH_PRE}button_click_01`,
    button_click_02: `${AUDIO_PATH_PRE}button_click_02`,
    bg_music: `${AUDIO_PATH_PRE}bg_music`,
    block_eliminate: `${AUDIO_PATH_PRE}block_eliminate`,
    level_complete: `${AUDIO_PATH_PRE}level_complete`,
    level_failed: `${AUDIO_PATH_PRE}level_failed`,
    get_reward: `${AUDIO_PATH_PRE}get_reward`,
    revive: `${AUDIO_PATH_PRE}revive`,
    libaopanel: `${AUDIO_PATH_PRE}libaopanel`,
    coinandmoney: `${AUDIO_PATH_PRE}coinandmoney`,
    close_panel: `${AUDIO_PATH_PRE}close_panel`,
    flyprop: `${AUDIO_PATH_PRE}flyprop`,
    levelpanel: `${AUDIO_PATH_PRE}levelpanel`,
    block_destory: `${AUDIO_PATH_PRE}block_destory`,
    new_record: `${AUDIO_PATH_PRE}new_record`,
    wabao_bg: `${AUDIO_PATH_PRE}wabaoBg`,
    putgouzi: `${AUDIO_PATH_PRE}putgouzi`,
    gouziget: `${AUDIO_PATH_PRE}gouziget`,
    
};


export const AUDIOS_ENUM = cc.Enum((() => {
    return {
        block_click: 0,
        button_click_01: 1,
        button_click_02: 2,
        bg_music: 3,
        block_eliminate: 4,
        level_complete: 5,
        level_failed: 6,
        get_reward: 7,
        revive: 8,
        libaopanel: 9,
        coinandmoney: 10,
        close_panel: 11,
        flyprop:12,
        levelpanel: 13,
        block_destory: 14,
        wabao_bg:15,
        putgouzi:16,
        gouziget:17,
    }
})());