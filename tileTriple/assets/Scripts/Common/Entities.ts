export interface Login {
    "userId"?: string, //用户ID
    "registerTime"?: number, //注册时间毫秒
    "isAudit"?: boolean, //是否审核版
    "passNum"?: number, //当前关卡
    "extData"?: string //加密参数
}
export interface Account {
    "cointotal"?: number, /**金币 */
    "moneytotal"?: number, /**金钱 */
    /**现金达标值 */
    "moneyLimit"? : number 
}

export interface LevelInfo {
    "passNum"?: number,  //当前关卡
    "passCoin"?: number, //过关奖励
    "passMoney"?: number,//过关奖励
    "extData"?: string,//领取礼盒密钥
    "coinTile"?: number,//金币组数
    "moneyTile"?: number,//钱组数
    "novice"?: number, //第一关新手是否领取
    "novice2"?: number //第二关是否领取  0(没有) 1(领取了)
     /**本关领取了几个宝箱了 */
    "boxNum"?: number,
    "novice2Reward"?:number
    "noviceReward"?:number
}

export interface receiveInfo {
    "rewardCoin"?: number,
    "accountCoin"?: number,
    "rewardMoney"?: number,
    "accountMoney"?: number,
    "extData"?: string,
    "biztype"?: string
}


//游戏主要内容
export interface Game {
    LoginData: Login 
    AccountData: Account
    LevelData: LevelInfo
    receiveInfo: receiveInfo
    HttpCloseWindowCount:number
}