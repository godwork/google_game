import { BizTypeEnum, HttpAccount, HttpCloseWindowCount, HttpGetJsonFromServer, HttpLevelInfo, HttpLogin, HttpPostServerAward } from "../net/api";
import { Response } from "../HttpMgr";
import UITileToast from "./UIToast";
import { Game } from "./Entities";
import getEventEmiter from "../Libraries/EventEmitter";
import { CUSTOM_EVENT_NAME } from "./Constant";
import GameSceneController from "../Scenes/GameSceneController";
import CCLog from "../CCLog";
import GameTileStaticData, { rewardFlyType } from "../GameStaticData";

import GameTileStaticJsonData from "../GameStaticJsonData";
import LocalKey from "../LocalKey";
import LanguagetileTile, { Country } from "./Language";
import GameStaticData from "../GameStaticData";
import GameStaticJsonData from "../GameStaticJsonData";
import { wabaoEmit } from "../panels/wabao/wabaoMain";

export const GameData: Game = {
    LoginData: {},
    AccountData: {},
    LevelData: {},
    receiveInfo: {},
    HttpCloseWindowCount : 18
}


/**登陆 */
export const GetHttpLogin = async () => {
    let data:Response = await HttpLogin();
    if (data.resCode === 0) {
        GameData.LoginData = data.data;
       if (CC_JSB) {
        if (data.data.abTestInfo&&data.data.abTestInfo.ab_lists) {
            for (let index = 0; index < data.data.abTestInfo.ab_lists.length; index++) {
                if (data.data.abTestInfo.ab_lists[index].testId== 26||data.data.abTestInfo.ab_lists[index].testId== "26") {
                    if (data.data.abTestInfo.ab_lists[index].groupId== 1||data.data.abTestInfo.ab_lists[index].groupId== "1") {
                            GameTileStaticData.isShowFlyBox = true
                    }else{
                            GameTileStaticData.isShowFlyBox = false
                    }
                }
                    
            }
        }
            
       }
        
    }else{
        UITileToast.popUp(data.msg);
    }
    return data;
}


/**账号 */
export const GetAccountInfo = async (pos?: cc.Vec3) => {
    let data:Response = await HttpAccount();
    if (data.resCode === 0) {
        let coinAdd = 0;
        let moneyAdd = 0;
        GameData.AccountData.moneyLimit = data.data.moneyLimit
        data.data.aclist.forEach(element => {
            if (element.type == "coin"){
                if (GameData.AccountData){
                    if (GameData.AccountData.cointotal){
                        coinAdd =  element.total - GameData.AccountData.cointotal;
                    }
                }
                GameData.AccountData.cointotal = element.total
            }else if (element.type == "money"){
                if (GameData.AccountData){
                    if (GameData.AccountData.moneytotal){
                        moneyAdd =  element.total - GameData.AccountData.moneytotal;
                    }
                }
                GameData.AccountData.moneytotal = element.total
            }
        });
        if (coinAdd > 0){
            //GameSceneController.I && GameSceneController.I.showMoneyFlyAnim(pos);
            GameTileStaticData.showWithNewFlyAnimation(rewardFlyType.rewardType_coin)
        }
        getEventEmiter().emit(CUSTOM_EVENT_NAME.onCoinChange,coinAdd);
        if (moneyAdd > 0){
            GameTileStaticData.showWithNewFlyAnimation(rewardFlyType.rewardType_moeny)
            //GameSceneController.I && GameSceneController.I.showRedbagFlyAnim(pos);
        }
        getEventEmiter().emit(CUSTOM_EVENT_NAME.onMoneyChange,moneyAdd);
    }else{
        UITileToast.popUp(data.msg);
    }
    return data;
}
/**账号 */
export const GetAccountInfo_WithoutAniml = async (addNum) => {
    let data:Response = await HttpAccount();
    if (data.resCode === 0) {
        let coinAdd = 0;
        let moneyAdd = 0;
       
        data.data.aclist.forEach(element => {
            if (element.type == "coin"){
                if (GameData.AccountData){
                    if (GameData.AccountData.moneytotal){
                        coinAdd =  element.total - GameData.AccountData.cointotal;
                    }
                }
                GameData.AccountData.cointotal = element.total
            }else if (element.type == "money"){
                if (GameData.AccountData){
                    if (GameData.AccountData.moneytotal){
                        moneyAdd =  element.total - GameData.AccountData.moneytotal;
                        
                    }
                }
                GameData.AccountData.moneytotal = element.total
            }
        });
        
        getEventEmiter().emit(CUSTOM_EVENT_NAME.onMoneyChange,addNum);
    }else{
        UITileToast.popUp(data.msg);
    }
    return data;
}
/**账号 */
export const GetAccountSelfAdd = async (pos?: cc.Vec3,coinAdd?:number,moneyAdd?:number,playAni = true) => {
        if (coinAdd > 0){
            //GameSceneController.I && GameSceneController.I.showMoneyFlyAnim(pos);
            GameData.AccountData.cointotal =  GameData.AccountData.cointotal+coinAdd
            GameTileStaticJsonData.saveLocal_coinMoney(LocalKey.localCoin,GameData.AccountData.cointotal)
            GameTileStaticData.showWithNewFlyAnimation(rewardFlyType.rewardType_coin)
            getEventEmiter().emit(CUSTOM_EVENT_NAME.onCoinChange,coinAdd);
        }
        
        if (moneyAdd!=0){
            GameData.AccountData.moneytotal =  GameTileStaticJsonData.getTwoPointNum(GameData.AccountData.moneytotal+moneyAdd) 
            GameTileStaticJsonData.saveLocal_coinMoney(LocalKey.localMoney,GameData.AccountData.moneytotal)
            if (moneyAdd>0&&playAni==true) {
                GameTileStaticData.showWithNewFlyAnimation(rewardFlyType.rewardType_moeny)
            }
            //GameSceneController.I && GameSceneController.I.showRedbagFlyAnim(pos);
            getEventEmiter().emit(CUSTOM_EVENT_NAME.onMoneyChange,moneyAdd,playAni);
        }
        
}

/**根据关卡书计算出信息 passNum为当前关*/ 
export const GetLevelInfo = async (passNum?:number) => {
    if (!passNum) {
        passNum =  GameTileStaticJsonData.getLocalSavePassNum()
    }
    GameData.LevelData.passNum = passNum
    GameData.LevelData.passCoin = GameTileStaticJsonData.getLevelPassRewardCoin(GameData.AccountData.cointotal)
    GameData.LevelData.passMoney = GameTileStaticJsonData.getLevelPassRewardMoney(GameData.AccountData.moneytotal)
    GameData.LevelData.coinTile = GameTileStaticJsonData.getcoinTileAndMoneyTileNum(GameData.LevelData.passNum).coinTile
    GameData.LevelData.moneyTile = GameTileStaticJsonData.getcoinTileAndMoneyTileNum(GameData.LevelData.passNum).cashTile
    
    // let data:Response = await HttpLevelInfo();
    // if (data.resCode === 0) {
    //     GameData.LevelData = data.data;
    // }else{
    //     UIToast.popUp(data.msg);
    // }
    // return data;
}

/**等级 */
export const GetReceiveAward =  (biztype: BizTypeEnum,AddCoinNum,AddMoney) => {
    // let data:Response = await HttpReceiveAward(biztype,GameData.LevelData.passNum,boxNum,GameData.LevelData.extData,getTpe);
    // if (data.resCode === 0) {
    //     GameData.receiveInfo = data.data;
    // }else{
    //     UIToast.popUp(data.msg);
    // }
    // return data;
    //CCLog.log("++++++上传奖励",biztype,AddCoinNum,AddMoney)
    HttpPostServerAward(biztype,AddCoinNum,AddMoney)

}

/**游戏配置 */
export const GetGameJsonServer = async () => {
   
        if (LanguagetileTile.getCountry == Country.BR) {
            GameTileStaticJsonData.getMoneyChangeRate = 5.14
        }else if(LanguagetileTile.getCountry == Country.ID){
            GameTileStaticJsonData.getMoneyChangeRate = 15168
        }else if(LanguagetileTile.getCountry == Country.RU){
            GameTileStaticJsonData.getMoneyChangeRate = 90
        }else{
            GameTileStaticJsonData.getMoneyChangeRate = 1
        }
   
    if (CC_JSB) {
        let data:Response = await HttpGetJsonFromServer();
        if (data.resCode === 0) {
           
           
           if (data.data.nc) {
               GameTileStaticJsonData.newUserAward = data.data.nc
           }
           if (data.data.cc) {
               GameTileStaticJsonData.levelCoinRule = data.data.cc
           }
           if (data.data.crc) {
               GameTileStaticJsonData.coinFanbeiRule = data.data.crc
           }
           if (data.data.mc) {
              GameTileStaticJsonData.levelMoneyRule = data.data.mc
           }
           if (data.data.mrc) {
              GameTileStaticJsonData.moneyFanBeiRate = data.data.mrc
           }
           if (data.data.psc) {
              GameTileStaticJsonData.coinTileMoneyTileRule = data.data.psc
           }
           if (data.data.pac) {
              GameTileStaticJsonData.forceVideoRule = data.data.pac
           }
           if (data.data.dgc) {
              GameTileStaticJsonData.wabaoRule = data.data.dgc
           }
           if (data.data.frc) {
              GameTileStaticJsonData.flyBoxRate = data.data.frc
           }
           if (data.data.hdc) {
               GameTileStaticJsonData.h5Activity = data.data.hdc
           }
           //CCLog.log("+++++++汇率",data.data.rm,data.data.rm.rate)
           if (data.data.rm&&data.data.rm.rate) {
               GameTileStaticJsonData.getMoneyChangeRate = GameTileStaticJsonData.getTwoPointNum(Number(data.data.rm.rate)) 
           }
           if (data.data.acc) {
              GameTileStaticJsonData.InterstitialAd = data.data.acc
           }
           if (data.data.wdc) {
              GameTileStaticJsonData.tixianParm = data.data.wdc
           }
          




           /*
           CCLog.log("++++++相关的配置1 newUserAward",GameStaticJsonData.newUserAward.coin,GameStaticJsonData.newUserAward.money)
           for (let index = 0; index < GameStaticJsonData.levelCoinRule.length; index++) {
                
                let _a = GameStaticJsonData.levelCoinRule[index]
                CCLog.log("++++++++相关的配置2 levelCoinRule",_a.startCoin,_a.endCoin)
            
           }
           for (let index = 0; index < GameStaticJsonData.coinFanbeiRule.length; index++) {
                let _a = GameStaticJsonData.coinFanbeiRule[index]
                CCLog.log("++++++++相关的配置3 coinFanbeiRule",_a.multiplemin,_a.multiplemaxn)
           }
           for (let index = 0; index < GameStaticJsonData.levelMoneyRule.length; index++) {
            let _a = GameStaticJsonData.levelMoneyRule[index]
            CCLog.log("++++++++相关的配置4 levelMoneyRule",_a.startMoney,_a.endMoney)
           }
           for (let index = 0; index < GameStaticJsonData.moneyFanBeiRate.length; index++) {
            let _a = GameStaticJsonData.moneyFanBeiRate[index]
            CCLog.log("++++++++相关的配置5 moneyFanBeiRate",_a.multiplemin,_a.multiplemaxn)
           }
           for (let index = 0; index < GameStaticJsonData.coinTileMoneyTileRule.length; index++) {
            let _a = GameStaticJsonData.coinTileMoneyTileRule[index]
            CCLog.log("++++++++相关的配置6 coinTileMoneyTileRule",_a.minLevel,_a.maxLevel)
           }
           for (let index = 0; index < GameStaticJsonData.forceVideoRule.length; index++) {
            let _a = GameStaticJsonData.forceVideoRule[index]
            CCLog.log("++++++++相关的配置7 forceVideoRule",_a.lvNum)
           }
           
            CCLog.log("++++++++相关的配置8 wabaoRule",GameStaticJsonData.wabaoRule.startLv,JSON.stringify(GameStaticJsonData.wabaoRule))
            CCLog.log("++++++++相关的配置9 flyBoxRate",GameStaticJsonData.flyBoxRate.coin)
            */
          

        }else{
            UITileToast.popUp(data.msg);
        }
    }
   
    
}
/**获取挖宝信息 */
export const GetWaBaoInfo = async () => {
    if (true) {
        // let data:Response = await HttpUserWaBaoInfo();
        // if (data.resCode == 0) {
        //     GameStaticData.isUnLockwabao = data.data.unlock
        //     GameStaticData.lockwabaoLV = data.data.startLv
        //     GameStaticData.wabaoLeftTime = data.data.userCount;
        //     GameStaticData.wabaoTodaySeeVideoLeftTime = data.data.showVideo;
        //     GameStaticData.wabaoSeeVideoAddNum = data.data.showVideoCount;

        //     CCLog.log("++++获取挖宝信息",GameStaticData.wabaoLeftTime,GameStaticData.wabaoTodaySeeVideoLeftTime,GameStaticData.wabaoSeeVideoAddNum)
        //     CCLog.log("++++获取挖宝信息解锁",GameStaticData.isUnLockwabao)
        //     CCLog.log("++++获取挖宝信息开始通关关数",GameStaticData.lockwabaoLV)
        // }else{
        //     CCLog.log("++++获取挖宝信息报错",data.msg)
        //     UIToast.popUp(data.msg);
        // }
        // return data;

            GameStaticData.lockwabaoLV = GameStaticJsonData.wabaoRule.startLv
            if (GameData.LevelData.passNum>GameStaticData.lockwabaoLV) {
                GameStaticData.isUnLockwabao = true
            }else{
                GameStaticData.isUnLockwabao = false
            }
            
            
            GameStaticData.wabaoLeftTime = GameStaticJsonData.getLocalWabaoSaveNum(LocalKey.wabaoleft_time)
            GameStaticData.wabaoTodaySeeVideoLeftTime = GameStaticJsonData.getLocalWabaoSaveNum(LocalKey.wabaoleft_seeVideo_time)
           
            GameStaticData.wabaoSeeVideoAddNum =  GameStaticJsonData.wabaoRule.videoCount
            CCLog.log("++++剩余的次数",GameStaticData.wabaoTodaySeeVideoLeftTime)
    }else{
        return null
    }
    
}
/**看视频增加挖宝次数*/
export const addWaBaoNum = async (callBack) => {
    //挖宝次数增加  看视频次数减少
    GameStaticData.wabaoLeftTime+=GameStaticJsonData.wabaoRule.videoCount
    GameStaticJsonData.saveWaBaoLocal_Info(LocalKey.wabaoleft_time,GameStaticData.wabaoLeftTime)
    if (GameStaticData.wabaoTodaySeeVideoLeftTime>=1) {
        GameStaticData.wabaoTodaySeeVideoLeftTime--
        GameStaticJsonData.saveWaBaoLocal_Info(LocalKey.wabaoleft_seeVideo_time,GameStaticData.wabaoTodaySeeVideoLeftTime)
    }
    getEventEmiter().emit(wabaoEmit.wabao_refresh_left_time,{})
    CCLog.log("++++看视频增加挖宝信息",GameStaticData.wabaoLeftTime,GameStaticData.wabaoTodaySeeVideoLeftTime)
    if (callBack) {
        callBack()
    }
    // if (CC_JSB) {
    //     let data:Response = await HttpSeeVIdeoAddWaBaoNum();
    //     if (data.resCode === 0) {
        
    //         GameStaticData.wabaoLeftTime = data.data.userCount;
    //         GameStaticData.wabaoTodaySeeVideoLeftTime = data.data.showVideo;
    //         getEventEmiter().emit(wabaoEmit.wabao_refresh_left_time,{})
    //         CCLog.log("++++看视频增加挖宝信息",GameStaticData.wabaoLeftTime,GameStaticData.wabaoTodaySeeVideoLeftTime)
    //         if (callBack) {
    //             callBack()
    //         }
    //     }else{
    //         UIToast.popUp(data.msg);
    //     }
    //     return data;
    // }else{
    //     return null
    // }
}