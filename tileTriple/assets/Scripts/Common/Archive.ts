import { LIFE_MAX, SKIN_GROUP_DEFAULT } from "./Constant";
import { PROJECT_CONFIG } from "./ProjectConfig";
import ArchiveSystem from "../System/ArchiveSystem";

import getEventEmiter from "../Libraries/EventEmitter";
import CCLog from "../CCLog";
const LOCAL_DATA_DEFAULT: ILocalData = {
    skinGroup: SKIN_GROUP_DEFAULT,
    /**撤回次数 */
    recallNum: 2,
    /**刷新次数 */
    refreshNum: 2,
    /**提示次数 */
    hintNum: 2,
    music: true,
    sound: true,
    headIndex: Math.ceil(Math.random()*7),
    lastOpenPhoneTaskId:1,
    curTaskId:1,
    curTaskFinish:0,
    curSelectBg:0
};

class Archive {
    public createUserID(): string {
        let userID = cc.sys.localStorage.getItem(`${PROJECT_CONFIG.appID}_userID`);
        if (!userID) {
            let fullString: string = `0123456789abcdef`;
            userID = '';
            for (let i: number = 0; i < 16; i++) {
                userID += fullString[Math.floor(Math.random() * fullString.length)];
            }
            cc.sys.localStorage.setItem(`${PROJECT_CONFIG.appID}_userID`, userID);
        }
        return userID;
    }


    public createLocalData(userID: string): ILocalData {
        let localDataArchive: string = cc.sys.localStorage.getItem(`${PROJECT_CONFIG.appID}_${userID}_localData`);
        if (!localDataArchive /*|| !localDataArchive.includes('dataVersion')*/) {
            return LOCAL_DATA_DEFAULT;
        } else {
            return Object.assign(
                Object.assign({}, LOCAL_DATA_DEFAULT),
                JSON.parse(localDataArchive)
            );
        }
    }


    public saveLocalData(userID: string, data: ILocalData): void {
        cc.sys.localStorage.setItem(
            `${PROJECT_CONFIG.appID}_${userID}_localData`
            , JSON.stringify(data)
        );
    }

    //任务背景图
    public async loadTaskJson()
    {
        await new Promise((resolve, reject) => 
        {
            cc.resources.load('json/task', function (err, object:any) {
                if (err) {
                   
                    return;
                }
                
                let task = object.json;
                CCLog.log("加载task==length",task.length)
                
                ArchiveSystem.taskJson = task;
                //window.gameData.stageTable = stage;
                resolve(null);
                
            });
        });
    }
    public checkTask(type)
    {
        let curTaskId = ArchiveSystem.localData.curTaskId;
        if(curTaskId <= ArchiveSystem.taskJson.length)
        {
            let taskInfo = ArchiveSystem.taskJson[curTaskId-1];
            if(taskInfo.type == type)
            {
              
                ArchiveSystem.localData.curTaskFinish +=1;
                if(ArchiveSystem.localData.curTaskFinish >= taskInfo.count)
                {
                    ArchiveSystem.localData.curTaskId +=1;
                    ArchiveSystem.localData.curTaskFinish = 0;
                    getEventEmiter().emit("updateNewPhoneIcon");

                }
            }
        }
    }
    public getTask(id)
    {
        let index = id.toString();
        return ArchiveSystem.taskJson[index];
    }
}

export default new Archive();
