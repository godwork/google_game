import Helper from "../Common/Helper";

const { ccclass, property } = cc._decorator;

enum POSITION_TYPE {
    top,
    bottom,
}

@ccclass
export default class NewClass extends cc.Component {
    @property({ type: cc.Enum(POSITION_TYPE) })
    public positionType: POSITION_TYPE = POSITION_TYPE.top;

    @property(cc.Float)
    public normal: number = 0;

    @property(cc.Float)
    public bigger: number = 0;

    @property
    public auto: boolean = true;

    public start() {
        if (this.normal === 0 || this.bigger === 0) return;
        this.scheduleOnce(() => {
            const node = this.node;
            const parent = node.parent;
            const isNormal: boolean = Helper.isNormalScreen();

            switch (this.positionType) {
                case POSITION_TYPE.top:
                    node.y = parent.height * (1 - parent.anchorY) - (isNormal ? this.normal : this.bigger);
                    break;

                case POSITION_TYPE.bottom:
                    node.y = parent.height * (parent.anchorY - 1) + (isNormal ? this.normal : this.bigger);
                    break;
            }
        });
    }
}
