import CCLog from "../CCLog";
import { CUSTOM_EVENT_NAME, PROP_TIP_CHANGE_AFTER, PROP_TIP_SHOW_AFTER } from "../Common/Constant";
import { PauseController } from "../Common/Decorators";
import { JsbTileTrileTileCallMgr } from "../Common/JsbMgr";
import GameStaticData from "../GameStaticData";
import GameStaticJsonData from "../GameStaticJsonData";
import getEventEmiter from "../Libraries/EventEmitter";
import GamingSystem from "../System/GamingSystem";
import PropButtonController from "./PropButtonController";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PropAreaController extends cc.Component {
    public static I: PropAreaController;

    @property(PropButtonController)
    public propButtonControllers: PropButtonController[] = [];

    @property(cc.Node)
    public mask: cc.Node = null;
    @property(cc.Node)
    nodeBtns: cc.Node[] = [];

    private _guideIndex: number = 0;

    private _tipIndex: number = 0;

    public onLoad() {
        PropAreaController.I = this;
        this.mask.active = false;
        this.propButtonControllers.forEach((v: PropButtonController, i: number) => {
            v.setPropButtonType(i);
        });
    }
    protected start(): void {
        this.updateBtnPos()
    }
    updateBtnPos(){
        //CCLog.log("++++++h5显示条件",GameStaticData.isnotLiveuserH5,GameStaticJsonData.h5Activity.s,GameStaticData.isShenHeModel)
        // if (GameStaticData.isnotLiveuserH5 == "false"&&GameStaticJsonData.h5Activity.s == 1
        //    &&GameStaticData.isShenHeModel==false) {
        //     this.nodeBtns[2].x = -95
        //     this.nodeBtns[3].active = true
        // }else{
        //     this.nodeBtns[2].x = 0
        //     this.nodeBtns[3].active = false
        // }
    }
    public showTip(): void {
        this.propButtonControllers[this._tipIndex].showAnimation();
    }

    public hideTip(): void {
        this.propButtonControllers.forEach((v: PropButtonController, i: number) => {
            v.hideAnimation();
        });
    }

    public changeTip(): void {
        this.propButtonControllers[this._tipIndex].hideAnimation();
        this._tipIndex += 1;
        if (this._tipIndex === this.propButtonControllers.length) this._tipIndex = 0;
        this.propButtonControllers[this._tipIndex].showAnimation();
    }

    onClickH5Ad(){
        JsbTileTrileTileCallMgr.handler("showH5Ad")
        JsbTileTrileTileCallMgr.handler("sensorsBuryPoint","(Ljava/lang/String;Ljava/lang/String;)V","h5_click","")
        let jsonStr = {event:"h5_click",loglevel:2}
        JsbTileTrileTileCallMgr.handler("buryPoint","(Ljava/lang/String;)V",JSON.stringify(jsonStr))
    }
}
