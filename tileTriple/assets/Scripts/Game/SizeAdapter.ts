import Helper from "../Common/Helper";
import GamingSystem from "../System/GamingSystem";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SizeAdapter extends cc.Component {
    public start() {
        if (Helper.isNormalScreen()) {
            let mapMaxHeight = GamingSystem.mapInfo.map.reduce((n: number, v: IMapLayerInfo) => { return v.height > n ? v.height : n }, 0);
            switch (mapMaxHeight) {
                case 9:
                    this.node.scale = 0.94;
                    break;
                case 10:
                    this.node.scale = 0.9;
                    break;
                case 11:
                    this.node.scale = 0.88;
                    break;
            }
        }
    }
}
