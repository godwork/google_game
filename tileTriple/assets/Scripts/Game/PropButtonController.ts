import CCLog from "../CCLog";
import { CUSTOM_EVENT_NAME, PROP_BUTTON_TYPE } from "../Common/Constant";
import { GetAccountInfo } from "../Common/GameData";
import { JsbTileTrileTileCallMgr } from "../Common/JsbMgr";
import getEventEmiter from "../Libraries/EventEmitter";
import ArchiveSystem from "../System/ArchiveSystem";
import GamePlayController from "./GamePlayController";
import PropAreaController from "./PropAreaController";

const { ccclass, property } = cc._decorator;

export const BUTTON_TYPE: PROP_BUTTON_TYPE[] = [
    PROP_BUTTON_TYPE.recall,
    PROP_BUTTON_TYPE.refresh,
    PROP_BUTTON_TYPE.hint
];

@ccclass
export default class PropButtonController extends cc.Component {
    @property(cc.Node)
    public nodeVideoIcon: cc.Node = null;

    @property(cc.Node)
    public prop_num_bg: cc.Node = null;

    @property(cc.Label)
    public labelLeft: cc.Label = null;

    @property(cc.Label)
    public labelDesc: cc.Label = null;

    @property(cc.Sprite)
    public spriteButtonSkin: cc.Sprite = null;

    @property([cc.SpriteFrame])
    public frameButtonSkins: cc.SpriteFrame[] = [];

    private _propButtonType: PROP_BUTTON_TYPE;
    private _originScale: number;
    private _tipTween: cc.Tween = null;


    public onLoad() {
        this._originScale = this.node.scale;
    }

    public onDestroy() {
        getEventEmiter().off(CUSTOM_EVENT_NAME.localDataUpdate, this.updateLeftText, this);
    }

    public setPropButtonType(t: number): void {
        let node = this.node;
        this._propButtonType = t;
      
        //this.spriteButtonSkin.spriteFrame = this.frameButtonSkins[this._propButtonType];
        switch (this._propButtonType) {
            case PROP_BUTTON_TYPE.recall:
                
                node.on(cc.Node.EventType.TOUCH_END, () => {
                    // if (this.labelDesc.node.active) return;
                    GamePlayController.I.recall();
                }, this);
                this.labelDesc.string = 'Retract: Retract a step';
                break;

            case PROP_BUTTON_TYPE.refresh:
                node.on(cc.Node.EventType.TOUCH_END, () => {
                    // 左边的按钮
                    JsbTileTrileTileCallMgr.handler("sensorsBuryPoint","(Ljava/lang/String;Ljava/lang/String;)V","refresh_click","")
                    let jsonStr = {event:"refresh_click",loglevel:2}
                    JsbTileTrileTileCallMgr.handler("buryPoint","(Ljava/lang/String;)V",JSON.stringify(jsonStr))
                   
                    GamePlayController.I.refresh();
                }, this);
                this.labelDesc.string = 'Refresh: Rearrange';
                break;

            case PROP_BUTTON_TYPE.hint:
                node.on(cc.Node.EventType.TOUCH_END, () => {
                    // 右边边的按钮
                    JsbTileTrileTileCallMgr.handler("sensorsBuryPoint","(Ljava/lang/String;Ljava/lang/String;)V","prompt_click","")
                    let jsonStr = {event:"prompt_click",loglevel:2}
                    JsbTileTrileTileCallMgr.handler("buryPoint","(Ljava/lang/String;)V",JSON.stringify(jsonStr))
                    GamePlayController.I.hint();
                }, this);
                this.labelDesc.string = 'Tip: Automatically eliminate a group';
                break;
        }
        // node.on(cc.Node.EventType.TOUCH_END, PropAreaController.I.showPropGuide, PropAreaController.I);

        getEventEmiter().on(CUSTOM_EVENT_NAME.localDataUpdate, this.updateLeftText, this);
        this.updateLeftText();
        this.hideTip();
    }

    public updateLeftText(): void {
        const ld = ArchiveSystem.localData;
        switch (this._propButtonType) {
            case PROP_BUTTON_TYPE.recall:
                this.labelLeft.string = ld.recallNum > 0 ? ld.recallNum.toString() : '+';
                break;

            case PROP_BUTTON_TYPE.refresh:
                this.labelLeft.string = ld.refreshNum > 0 ? ld.refreshNum.toString() : '+';
                break;

            case PROP_BUTTON_TYPE.hint:
                this.labelLeft.string = ld.hintNum > 0 ? ld.hintNum.toString() : '+';
                break;
        }
        let isZero = this.labelLeft.string === '+';
        this.nodeVideoIcon.active = isZero;
        this.prop_num_bg.active = !isZero;
    }

    public showTip(): void {
        this.node.zIndex = 10;
        this.labelDesc.node.active = true;
    }

    public hideTip(): void {
        this.node.zIndex = 0;
        this.labelDesc.node.active = false;
    }

    public showAnimation(): void {
        // if (this.labelDesc.node.active) return;
        // let duration: number = 0.5;
        // let t = cc.tween().to(duration, { scale: this._originScale * 1.2 })
        //     .to(duration, { scale: this._originScale });
        // this._tipTween = cc.tween(this.node)
        //     .then(t)
        //     .repeatForever()
        //     .start();
    }

    public hideAnimation(): void {
        // if (this._tipTween) this._tipTween.stop();
        // this._tipTween = null;
        // this.node.scale = this._originScale;
    }
}
