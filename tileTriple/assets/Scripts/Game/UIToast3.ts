import CocosTileHelper from "../Common/CocosHelper";
import LanguagetileTile, { Country } from "../Common/Language";
import { forMattedMoney } from "../Common/ProjectConfig";
import GameTileStaticData from "../GameStaticData";

const { ccclass, property } = cc._decorator;

export interface HarvestRewardInfo {
    type: number, //1金币 2红包 3元宝
    num: number,
}

@ccclass
export default class UIToast3 extends cc.Component {

    static prefabPath = "panels/UIToast3";

    @property(cc.SpriteFrame)
    jinbisp:cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    moneysp:cc.SpriteFrame = null;


    static async popUp(harvestReward: HarvestRewardInfo,pos:cc.Vec3 = cc.v3(0,0),showParent?:cc.Node) {
        if (GameTileStaticData.isShenHeModel) {
            return
        }
        if (harvestReward) {
            let prefab = await CocosTileHelper.loadResSync<cc.Prefab>(UIToast3.prefabPath, cc.Prefab);
            let node = cc.instantiate(prefab);
            let parent = showParent || cc.find("Canvas");
            if (!parent) return;
            parent.addChild(node,cc.macro.MAX_ZINDEX);
            node.position = node.parent.convertToNodeSpaceAR(pos);
            const layout = node.getChildByName("layout");
            let reward = layout.getChildByName("reward").getComponent(cc.Label);
            let icon = layout.getChildByName("icon").getComponent(cc.Sprite);
            let uitoastts = node.getComponent(UIToast3)
            if (harvestReward.type == 1) {//金币
                reward.string = "+" + harvestReward.num;
                icon.spriteFrame = uitoastts.jinbisp;
            } else if (harvestReward.type == 2) {//红包
                LanguagetileTile.setOneMoneyIcon(icon)
                if (LanguagetileTile.getCountry == Country.ID||LanguagetileTile.getCountry == Country.RU) {
                    let str = "+" + forMattedMoney(harvestReward.num);
                    reward.string = str;
                }else{
                    let str = "+" + forMattedMoney(harvestReward.num);
                    let index = str.split(".");
                    if (index.length >= 2){
                        if (index[1].length >= 2){
                            index[1] = index[1].substring(0,2)
                        }
                        str = index[0] + "." + index[1]
                    }
                    reward.string = str;
                }
                

            }
            uitoastts.showAnim();
        }
    }

    public showAnim() {
        this.node.y -= 50;
        this.node.opacity = 255;

        cc.tween(this.node)
            .delay(0.4)
            .by(0.4, { position: cc.v3(0, 50, 0) })
            .to(1, { opacity: 0 })
            .call(() => {
                this.node.destroy();
                this.node.removeFromParent();
            }).start();
    }



}