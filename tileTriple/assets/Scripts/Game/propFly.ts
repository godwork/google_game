
import CCLog from "../CCLog";
import PropAreaController from "./PropAreaController";
import TopAreaTs from "./TopAreaTs";
const {ccclass, property} = cc._decorator;

@ccclass
export default class propFly extends cc.Component {
    static I:propFly
    @property(cc.Node)
    instacteTipNode: cc.Node = null;
    @property(cc.Node)
    instacteFreshNode: cc.Node = null;
    
    onLoad () {
        propFly.I = this;
        
    }
    //1
    flyFreshAction(callback =()=>{}){
        let _node = cc.instantiate(this.instacteFreshNode)
        _node.parent = this.node
        _node.position = cc.v3(0,0,0)
        let pos1 = PropAreaController.I.propButtonControllers[1].node.position
        let pos2 = PropAreaController.I.propButtonControllers[1].node.parent.convertToWorldSpaceAR(pos1)
        let endPos = this.node.convertToNodeSpaceAR(pos2)
        let delatT = cc.delayTime(0.3)
        let act_3 = cc.moveTo(0.7, endPos.x,endPos.y).easing(cc.easeIn(3.0))//越来越块 
 
        let callbackAction = cc.callFunc(()=>{
            callback()
            _node.destroy();
           })
        let seq = cc.sequence(delatT,act_3,callbackAction)
        _node.runAction(seq)
    }
    flyTipAction(callback =()=>{}){
        let _node = cc.instantiate(this.instacteTipNode)
        _node.parent = this.node
        _node.position = cc.v3(0,0,0)
        let pos1 = PropAreaController.I.propButtonControllers[2].node.position
        let pos2 = PropAreaController.I.propButtonControllers[2].node.parent.convertToWorldSpaceAR(pos1)
        let endPos = this.node.convertToNodeSpaceAR(pos2)
        let delatT = cc.delayTime(0.3)
        let act_3 = cc.moveTo(0.7, endPos.x,endPos.y).easing(cc.easeIn(3.0))//越来越块 
 
        let callbackAction = cc.callFunc(()=>{
            callback()
            _node.destroy();
           })
        let seq = cc.sequence(delatT,act_3,callbackAction)
        _node.runAction(seq)
    }
    
    protected start(): void {
        
    }
    
}
