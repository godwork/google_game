import CCLog from "../CCLog";
import LanguagetileTile from "../Common/Language";
import GameTileStaticData, { rewardFlyType } from "../GameStaticData";
import TopAreaTs from "./TopAreaTs";


const { ccclass, property } = cc._decorator;

@ccclass
export default class rewardAnimTs extends cc.Component {

    @property(cc.Node) 
    content:cc.Node = null;
    @property(cc.SpriteFrame) 
    coinSp:cc.SpriteFrame = null;

    @property([cc.Node])
    rpArray: Array<cc.Node> = [];

    targetPos: cc.Vec2 = null;
    lastScale = 1

    onLoad() {
       
        if (GameTileStaticData.rewardType == rewardFlyType.rewardType_moeny) {
            let targetNode = TopAreaTs.I.redbagSp
            let targetWPos = targetNode.parent.convertToWorldSpaceAR(cc.v2(targetNode.position.x,targetNode.position.y));
            let targetPos = this.content.convertToNodeSpaceAR(targetWPos);
            this.targetPos = targetPos;
            this.lastScale = 1.5//跟TopAreaTs的图标的大小一样
          
           
        }else{
            let targetNode = TopAreaTs.I.coinSp
            let targetWPos = targetNode.parent.convertToWorldSpaceAR(cc.v2(targetNode.position.x,targetNode.position.y));
            let targetPos = this.content.convertToNodeSpaceAR(targetWPos);
            this.targetPos = targetPos;
            this.lastScale = 1//跟TopAreaTs的图标的大小一样
        }

        for (let i = 0;i<this.content.childrenCount;i++){
            let sp = this.content.children[i].getComponent(cc.Sprite)
            if (GameTileStaticData.rewardType == rewardFlyType.rewardType_moeny) {
                LanguagetileTile.setOneMoneyIcon(sp)
            }else{
                sp.spriteFrame = this.coinSp
                
            }
            
        }
    }

    start() {
        this.movieToTarget();
    }

    // update (dt) {}

    movieToTarget() {

        let array = [
            {
                index:1,
                pos: cc.v2(-100, -2),
                delay: 0.25,
                movieTime: 0.20
            },
            {
                index:2,
                pos: cc.v2(75, -1),
                delay: 0.22,
                movieTime: 0.20
            },
            {
                index:3,
                pos: cc.v2(-130, 62),
                delay: 0.24,
                movieTime: 0.20
            },
            {
                index:4,
                pos: cc.v2(-170, -1),
                delay: 0.23,
                movieTime: 0.22
            },
            {
                index:5,
                pos: cc.v2(-39, 118),
                delay: 0.21,
                movieTime: 0.22
            },
            {
                index:6,
                pos: cc.v2(138, -1),
                delay: 0.23,
                movieTime: 0.22
            },
            {
                index:7,
                pos: cc.v2(-56, -2),
                delay: 0.22,
                movieTime: 0.22
            },
            {
                index:8,
                pos: cc.v2(20, -3),
                delay: 0.20,
                movieTime: 0.22
            },
            {
                index:9,
                pos: cc.v2(100, -1),
                delay: 0.21,
                movieTime: 0.22
            },
            {
                index:10,
                pos: cc.v2(105, 36),
                delay: 0.24,
                movieTime: 0.20
            },
            {
                index:11,
                pos: cc.v2(198, 87),
                delay: 0.24,
                movieTime: 0.21
            },
            {
                index:12,
                pos: cc.v2(60, -1),
                delay: 0.25,
                movieTime: 0.20
            },
            {
                index:13,
                pos: cc.v2(-119, -2),
                delay: 0.215,
                movieTime: 0.225
            },
            {
                index:14,
                pos: cc.v2(-27, -1),
                delay: 0.225,
                movieTime: 0.225
            },
            {
                index:15,
                pos: cc.v2(-150, -4),
                delay: 0.235,
                movieTime: 0.225
            }
           
        ]

        for (let i = 0; i < 15; i++) {
            this.nodeMoive(i, array[i]);
        }

        setTimeout(() => {
            if (cc.isValid(this.node)) {
                
                this.node.destroy();
            }
            
        }, 2000);
    }

    nodeMoive(index, model) {
        // setTimeout(() => {
        let node = this.rpArray[index];
        // let node = this.node.getChildByName(`rp${index+1}`)
        if (cc.isValid(node, true) == false) {
            return;
        }
        cc.tween(node)
            .delay(index * 0.02)
            .parallel(

                cc.tween().to(0.17 + (index % 2) * (index * 0.02), { position: cc.v3(model.pos.x * 1.03, model.pos.y * 1.03, 0) }),
                cc.tween().to(0.17 + ((index + 1) % 2) * (index * 0.02), { scale: this.lastScale+0.2 })
            )
            .parallel(
                cc.tween().to(0.12, { position: cc.v3(model.pos.x, model.pos.y, 0) }),
                cc.tween().to(0.14, { scale: this.lastScale+0.1 })
            )
            .delay(model.delay)
            .to(model.movieTime, { position: cc.v3(this.targetPos.x, this.targetPos.y, 0), scale: this.lastScale })
            .call(()=> {
                if(index == 0) {  
                    // music.playEffect(engineType.reward);
                    // let node = headRewardScrit.Instance.node;
                    // if (GameStaticData.rewardType == rewardFlyType.rewardType_moeny) {
                    //     node.getComponent(headRewardScrit).setRewardValue(true);
                    // }else{
                    //     node.getComponent(headRewardScrit).playLabelAnaimal(GameStaticData.rewardCoinNum)
                    // }
                    
                    
                }
            })
            .to(0.07, { scale: this.lastScale })
            .call(() => {
                node.destroy();
            })
            .start()
        // }, index * 300);    
    }
}
