const { ccclass, property } = cc._decorator;

@ccclass
export default class Navigation extends cc.Component {

    @property
    public appID: string = '';

    public onLoad() {
        switch (cc.sys.platform) {
            // 微信小游戏
            case cc.sys.WECHAT_GAME:
                this.node.on(cc.Node.EventType.TOUCH_END, () => {
                    wx.navigateToMiniProgram({
                        appId: this.appID,
                    });
                }, this);
                break;

            default:
                this.node.on(cc.Node.EventType.TOUCH_END, () => {
                    
                }, this);
                break;
        }
    }
}
