const { ccclass, property } = cc._decorator;

@ccclass
export default class ProgressStarController extends cc.Component {

    @property(cc.Node)
    public nodeLight: cc.Node = null;
    /**还没有领取 */
    public lightUp(): void {
        this.nodeLight.active = true;
    }
    /**量取过了 */
    public lightOff(): void {
        this.nodeLight.active = false;
    }
    
    public isLight(): boolean {
        return this.nodeLight.active;
    }
}
