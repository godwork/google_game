
import CCLog from "../CCLog";
import { GameData } from "../Common/GameData";
import { JsbTileTrileTileCallMgr } from "../Common/JsbMgr";
import LanguagetileTile, { Country } from "../Common/Language";
import UITileToast from "../Common/UIToast";
import GameTileStaticData from "../GameStaticData";
import GameTileStaticJsonData from "../GameStaticJsonData";
import GamingSystem from "../System/GamingSystem";
import GamePlayController from "./GamePlayController";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ProgressPassNum extends cc.Component {
    public static I: ProgressPassNum;

    @property(cc.Sprite)
    public spriteProgressBar: cc.Sprite = null;
    @property(cc.Label)
    public labelPass: cc.Label = null;
    @property(cc.Node)
    public liheGary: cc.Node = null;
    @property(sp.Skeleton)
    NPCLihe: sp.Skeleton = null;
    private hasChangedLiheSTatus = false
    

    public onLoad() {
        ProgressPassNum.I = this;
        this.unscheduleAllCallbacks()
        if (GameTileStaticData.isShenHeModel) {
            this.node.active = false
            return
        }
        if (GameData.LevelData.passNum>GameTileStaticJsonData.tixianParm.sl) {
            this.changeLiHeStatus()
        }
       
       
    }
    playAnimation(){
        this.NPCLihe.setAnimation(0,"giftbox_jump",false)
    }
    public refresh(): void {
        let passNum = GameData.LevelData.passNum-1
        if (passNum<0) {
            passNum = 0
        }
        let progress = passNum/GameTileStaticJsonData.tixianParm.sl
        this.labelPass.string = passNum+"/"+GameTileStaticJsonData.tixianParm.sl
        this.spriteProgressBar.fillRange = progress;
        
    }
    changeLiHeStatus(){
        if (this.hasChangedLiheSTatus) {
            
            return
        }
        this.hasChangedLiheSTatus = true
        this.liheGary.active = false
        this.NPCLihe.node.active = true
        this.schedule(()=>{
            this.playAnimation()
        },3,cc.macro.REPEAT_FOREVER,1)
    }
    onClickToDrawMoney(){
        if (GameData.LevelData.passNum<=GameTileStaticJsonData.tixianParm.sl) {
            UITileToast.popUp(LanguagetileTile.getWord("startTip2Center",GameTileStaticJsonData.tixianParm.sl));
            return
        }

        let passNumStr = GameData.LevelData.passNum+""

        let moneyNumStr = ""
        // let origin = GameMatchTileStaticJsonData.getTwoPointNum(GameData.AccountData.moneytotal*GameMatchTileStaticJsonData.getMoneyChangeRate) 
        // if (LanguageMatchTile.getCountry != Country.ID){
        //     moneyNumStr = origin+""
        // }else{
        //     moneyNumStr = Number(Math.floor(origin))+""
        // }
        moneyNumStr = GameData.AccountData.moneytotal+""
        let jsonStr = {passNum:passNumStr,
            coinNum:"0",
            moneyNum:moneyNumStr,
            moneyrate:GameTileStaticJsonData.getMoneyChangeRate+"",
            tixianstartlv:GameTileStaticJsonData.tixianParm.sl+"",
            needsignDays:GameTileStaticJsonData.tixianParm.r1+"",
            needvideo:GameTileStaticJsonData.tixianParm.r2+"",
            tixianshenlv:GameTileStaticJsonData.tixianParm.r3+""}
        JsbTileTrileTileCallMgr.handler("moneyDrawCash","(Ljava/lang/String;)V",JSON.stringify(jsonStr))
    }
   
}
