import { AREA_MOVE_DURATION, CUSTOM_EVENT_NAME } from "../Common/Constant";
import getEventEmiter from "../Libraries/EventEmitter";

const { ccclass, property } = cc._decorator;

enum DIRECTION {
    top,
    bottom,
}

@ccclass
export default class AreaMove extends cc.Component {
    @property({ type: cc.Enum(DIRECTION) })
    public moveFrom: DIRECTION = DIRECTION.top;

    @property
    public callBackType: string = '';

    private _offset: number = 600;

    public onLoad() {
        this.node.opacity = 0;
    }

    public start() {
        this.scheduleOnce(() => {
            switch (this.moveFrom) {
                case DIRECTION.top:
                    // this._offset = 500;
                    break;

                case DIRECTION.bottom:
                    this._offset = -this._offset;
                    break;
            }

            this.node.y += this._offset;
            this.node.opacity = 255;
            cc.tween(this.node)
                .to(AREA_MOVE_DURATION, { y: this.node.y - this._offset })
                .call(() => {
                    getEventEmiter().on(CUSTOM_EVENT_NAME.onSceneChanged, this.__onSceneChanged, this);
                    // this.scheduleOnce(() => {
                    //     getEventEmiter().emit(CUSTOM_EVENT_NAME.onSceneChangedComplete, this.callBackType);
                    // }, 0.05);
                })
                .start();
        });
    }

    public onDestroy() {
        getEventEmiter().off(CUSTOM_EVENT_NAME.onSceneChanged, this.__onSceneChanged, this);
    }

    private __onSceneChanged(): void {
        cc.tween(this.node)
            .to(AREA_MOVE_DURATION, { y: this.node.y + this._offset })
            .start();
    }
}
