import { AUDIOS_ENUM, AUDIOS_PATH } from "../Common/AudioConfig";
import AudioTool from "../System/AudioSystem";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ButtonAudio extends cc.Component {
    @property({ type: AUDIOS_ENUM })
    public clickAudioType = AUDIOS_ENUM.button_click_01;

    public onLoad() {
        let btn = this.node.getComponent(cc.Button);
        if (!btn) return;
        this.node.on(cc.Node.EventType.TOUCH_END, () => {
            AudioTool.play(AUDIOS_PATH[AUDIOS_ENUM[this.clickAudioType]]);
        });
    }
}
