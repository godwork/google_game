

//主页 循环弹幕


import { CUSTOM_EVENT_NAME } from "../Common/Constant";
import LanguagetileTile, { Country } from "../Common/Language";
import { forMattedMoney } from "../Common/ProjectConfig";
import GameTileStaticData from "../GameStaticData";
import getEventEmiter from "../Libraries/EventEmitter";



const { ccclass, property } = cc._decorator;

@ccclass
export default class hallTanMuUI extends cc.Component {

    @property(cc.Node)
    nodeAll:cc.Node= null
    @property(cc.Sprite)
    headPic:cc.Sprite= null
    @property(cc.Sprite)
    bankPic:cc.Sprite= null
    @property(cc.RichText)
    richText:cc.RichText= null
    private userJson = []
   
    onLoad(){
        getEventEmiter().on(CUSTOM_EVENT_NAME.updateRefreshWhiteBao,this.refreswhite,this);
        let english =[
            {index:1,name:"Jusuf ",money:"$15.94",way:"Amazon"},
            {index:2,name:"Aisyah ",money:"$19.55",way:"PayPal"},
            {index:3,name:"Carita ",money:"$16.94",way:"MasterCard"},
            {index:4,name:"Anissa ",money:"$15.62",way:"Amazon"},
            {index:5,name:"Renae ",money:"$15.43",way:"PayPal"},
            {index:6,name:"Slamet ",money:"$19.35",way:"Amazon"},
            {index:7,name:"Daud ",money:"$18.48",way:"PayPal"},
            {index:8,name:"Bima ",money:"$14.27",way:"MasterCard"},
            {index:9,name:"Utari ",money:"$14.14",way:"Amazon"},
            {index:10,name:"Lrfan ",money:"$17.37",way:"PayPal"},
            {index:11,name:"Muhammad ",money:"$13.58",way:"Amazon"},
            {index:12,name:"Surya ",money:"$15.77",way:"PayPal"},
            {index:13,name:"Setiawan ",money:"$15.39",way:"Amazon"},
            {index:14,name:"Lita ",money:"$17.43",way:"MasterCard"},
            {index:15,name:"Habib ",money:"$16.26",way:"Amazon"},
            {index:16,name:"Ridwan ",money:"$15.69",way:"PayPal"},
            {index:17,name:"Wynita ",money:"$19.22",way:"MasterCard"},
            {index:18,name:"Iskandar ",money:"$17.97",way:"Amazon"},
            {index:19,name:"Joakim ",money:"$18.53",way:"PayPal"},
            {index:20,name:"Zakaria ",money:"$16.26",way:"Amazon"},
            {index:21,name:"Anwar ",money:"$19.90",way:"MasterCard"},
            {index:22,name:"Ibrahim ",money:"$19.94",way:"Amazon"},
            {index:23,name:"Mega ",money:"$13.44",way:"MasterCard"},
            {index:24,name:"Osama ",money:"$19.92",way:"Amazon"},
            {index:25,name:"Zakiah ",money:"$16.11",way:"PayPal"},
            {index:26,name:"Camelia ",money:"$17.51",way:"Amazon"},
            {index:27,name:"Nike",money:"$19.44",way:"MasterCard"},
            {index:28,name:"Bambang ",money:"$15.55",way:"PayPal"},
            {index:29,name:"Gobha ",money:"$13.07",way:"Amazon"},
            {index:30,name:"Walten ",money:"$15.52",way:"MasterCard"},
        ]
        let yinni =[
            {index:1,name:"SUKIMAN",money:"$15.94",way:"dana"},
            {index:2,name:"FEI PHIN",money:"$19.55",way:"ovo"},
            {index:3,name:"DERNI TANTELA",money:"$16.94",way:"ShopeePay"},
            {index:4,name:"TJENG ROSLINA",money:"$15.62",way:"dana"},
            {index:5,name:"MINSAR",money:"$15.43",way:"ovo"},
            {index:6,name:"WUN TJIA SIAW",money:"$19.35",way:"dana"},
            {index:7,name:"POH SIM TIAN",money:"$18.48",way:"ovo"},
            {index:8,name:"BO LIE TJU",money:"$14.27",way:"ShopeePay"},
            {index:9,name:"ONG SARINAH",money:"$14.14",way:"dana"},
            {index:10,name:"SENG AUW KIM",money:"$17.37",way:"ovo"},
            {index:11,name:"RUMINI",money:"$13.58",way:"dana"},
            {index:12,name:"FUNG TJONG MIE",money:"$15.77",way:"ovo"},
            {index:13,name:"RAMIN",money:"$15.39",way:"ShopeePay"},
            {index:14,name:"SATIMIN KASIMAN",money:"$17.43",way:"dana"},
            {index:15,name:"KHIAU KHA BIE",money:"$16.26",way:"ovo"},
            {index:16,name:"ZAITUN",money:"$15.69",way:"dana"},
            {index:17,name:"TJIN SIU",money:"$19.22",way:"ovo"},
            {index:18,name:"HONG TJIE TJUN",money:"$17.97",way:"ShopeePay"},
            {index:19,name:"TJOA SUGINTA",money:"$18.53",way:"dana"},
            {index:20,name:"KIAT BUIE",money:"$16.26",way:"ovo"},
            {index:21,name:"RUSLI LINDA",money:"$19.90",way:"ShopeePay"},
            {index:22,name:"TAI TAN PI",money:"$19.94",way:"ovo"},
            {index:23,name:"KIN LIE",money:"$13.44",way:"dana"},
            {index:24,name:"ANITHA",money:"$19.92",way:"ovo"},
            {index:25,name:"SENG TAN PAEK",money:"$16.11",way:"ShopeePay"},
            {index:26,name:"KASMAN",money:"$17.51",way:"ovo"},
            {index:27,name:"JANTO",money:"$19.44",way:"ShopeePay"},
            {index:28,name:"LESMANA PRIBADI",money:"$15.55",way:"ovo"},
            {index:29,name:"SAIMAN",money:"$13.07",way:"ovo"},
            {index:30,name:"PO SIM SIAW",money:"$15.52",way:"dana"},
        ]
        let baxi =[
            {index:1,name:"Gjergj",money:"$15.94",way:"Pagbank"},
            {index:2,name:"Bertrand",money:"$19.55",way:"Boleto"},
            {index:3,name:"Arkady",money:"$16.94",way:"PIX"},
            {index:4,name:"Burghardt",money:"$15.62",way:"Pagbank"},
            {index:5,name:"Leibel",money:"$15.43",way:"Pagbank"},
            {index:6,name:"Gaspard",money:"$19.35",way:"PIX"},
            {index:7,name:"Austyn",money:"$18.48",way:"Pagbank"},
            {index:8,name:"Xander",money:"$14.27",way:"Boleto"},
            {index:9,name:"Bilsland",money:"$14.14",way:"PIX"},
            {index:10,name:"Scotty",money:"$17.37",way:"Pagbank"},
            {index:11,name:"Santos",money:"$13.58",way:"Boleto"},
            {index:12,name:"Ambrose",money:"$15.77",way:"PIX"},
            {index:13,name:"Shadi",money:"$15.39",way:"Pagbank"},
            {index:14,name:"Amund",money:"$17.43",way:"Boleto"},
            {index:15,name:"Guido",money:"$16.26",way:"PIX"},
            {index:16,name:"Tyron",money:"$15.69",way:"Boleto"},
            {index:17,name:"Maurus",money:"$19.22",way:"Pagbank"},
            {index:18,name:"Umar",money:"$17.97",way:"PIX"},
            {index:19,name:"Kamron",money:"$18.53",way:"PIX"},
            {index:20,name:"Cornelius",money:"$16.26",way:"Pagbank"},
            {index:21,name:"Isabella",money:"$19.90",way:"Pagbank"},
            {index:22,name:"Gabriela",money:"$19.94",way:"PIX"},
            {index:23,name:"Sofia",money:"$13.44",way:"Pagbank"},
            {index:24,name:"Beatriz",money:"$19.92",way:"Boleto"},
            {index:25,name:"Camila",money:"$16.11",way:"Pagbank"},
            {index:26,name:"Manuela",money:"$17.51",way:"Boleto"},
            {index:27,name:"Larissa",money:"$19.44",way:"Pagbank"},
            {index:28,name:"Juliana",money:"$15.55",way:"Boleto"},
            {index:29,name:"Rafaela",money:"$13.07",way:"Boleto"},
            {index:30,name:"Valentina",money:"$15.52",way:"PIX"},
        ]
        let ru =[
            {index:1,name:"Jusuf ",money:"$15.94",way:"ru_1"},
            {index:2,name:"Aisyah ",money:"$19.55",way:"ru_1"},
            {index:3,name:"Carita ",money:"$16.94",way:"ru_1"},
            {index:4,name:"Anissa ",money:"$15.62",way:"ru_1"},
            {index:5,name:"Renae ",money:"$15.43",way:"ru_1"},
            {index:6,name:"Slamet ",money:"$19.35",way:"ru_1"},
            {index:7,name:"Daud ",money:"$18.48",way:"ru_1"},
            {index:8,name:"Bima ",money:"$14.27",way:"ru_1"},
            {index:9,name:"Utari ",money:"$14.14",way:"ru_1"},
            {index:10,name:"Lrfan ",money:"$17.37",way:"ru_1"},
            {index:11,name:"Muhammad ",money:"$13.58",way:"ru_1"},
            {index:12,name:"Surya ",money:"$15.77",way:"ru_2"},
            {index:13,name:"Setiawan ",money:"$15.39",way:"ru_2"},
            {index:14,name:"Lita ",money:"$17.43",way:"ru_2"},
            {index:15,name:"Habib ",money:"$16.26",way:"ru_2"},
            {index:16,name:"Ridwan ",money:"$15.69",way:"ru_2"},
            {index:17,name:"Wynita ",money:"$19.22",way:"ru_2"},
            {index:18,name:"Iskandar ",money:"$17.97",way:"ru_2"},
            {index:19,name:"Joakim ",money:"$18.53",way:"ru_2"},
            {index:20,name:"Zakaria ",money:"$16.26",way:"ru_2"},
            {index:21,name:"Anwar ",money:"$19.90",way:"ru_2"},
            {index:22,name:"Ibrahim ",money:"$19.94",way:"ru_3"},
            {index:23,name:"Mega ",money:"$13.44",way:"ru_3"},
            {index:24,name:"Osama ",money:"$19.92",way:"ru_3"},
            {index:25,name:"Zakiah ",money:"$16.11",way:"ru_3"},
            {index:26,name:"Camelia ",money:"$17.51",way:"ru_3"},
            {index:27,name:"Nike",money:"$19.44",way:"ru_3"},
            {index:28,name:"Bambang ",money:"$15.55",way:"ru_3"},
            {index:29,name:"Gobha ",money:"$13.07",way:"ru_3"},
            {index:30,name:"Walten ",money:"$15.52",way:"ru_3"},
        ]
        if (LanguagetileTile.getCountry == Country.BR) {
            this.userJson = baxi
        }else if(LanguagetileTile.getCountry == Country.ID){
            this.userJson = yinni
        }else if(LanguagetileTile.getCountry == Country.RU){
            this.userJson = ru
        }else{
            this.userJson = english
        }
        this.node.zIndex = cc.macro.MAX_ZINDEX
        if (GameTileStaticData.isShenHeModel) {
            this.node.active = false
            return
        }
        this.startMoveAction()
    }
    start() {
       
    }
    refreswhite(){
        if (!GameTileStaticData.isShenHeModel) {
            this.node.active = true
            this.startMoveAction()
            
        }
    }
    startMoveAction(){
        let winsize = cc.view.getVisibleSize().width/2
        let startX = winsize+this.nodeAll.width/2+20
        let endX = -winsize-this.nodeAll.width/2-20
        this.nodeAll.x = startX
        let delayTime = this.getRandom(70,80)
        this.moveNodeAllAction()
        this.scheduleOnce(()=>{
            cc.tween(this.nodeAll)
            .to(7, { x:endX})
            .call(()=>{this.startMoveAction()})
            .start()
        },delayTime)
       
       
    }
    moveNodeAllAction(){
        let randomIndex = this.getRandom(1,29)
        let info = this.userJson[randomIndex]
        let headPath = ""
        let namehead = ""
        let moneyStr = ""
        if (LanguagetileTile.getCountry == Country.BR) {
            headPath = "danmu/tanmu_br/"
            namehead = "br_"
            moneyStr = LanguagetileTile.MoneyChar+this.getRandom(50,99)
        }else if(LanguagetileTile.getCountry == Country.ID){
            headPath = "danmu/tanmu_id/"
            namehead = "id_"
            moneyStr = LanguagetileTile.MoneyChar+forMattedMoney(this.getRandom(50000,99999))
        }else if(LanguagetileTile.getCountry == Country.RU){
            headPath = "danmu/tanmu_br/"
            namehead = "br_"
            moneyStr = LanguagetileTile.MoneyChar+forMattedMoney(this.getRandom(20,30))
        }else{
            headPath = "danmu/tanmu_br/"
            namehead = "br_"
            moneyStr = info.money
        }
        this.richText.string = LanguagetileTile.getWord("tanMustr",info.name,moneyStr)
        //头像
        let headpath = headPath + namehead+randomIndex
        cc.resources.load(headpath,cc.SpriteFrame,(err,asset:cc.SpriteFrame)=>{
           if (asset){
                 this.headPic.spriteFrame = asset;
           }
        })
        //银行
        let bankpath = "danmu/tanmu_platform/" + info.way
        cc.resources.load(bankpath,cc.SpriteFrame,(err,asset:cc.SpriteFrame)=>{
           if (asset){
                 this.bankPic.spriteFrame = asset;
           }
        })
    }
 
    onDestroy(){
        getEventEmiter().off(CUSTOM_EVENT_NAME.updateRefreshWhiteBao,this.refreswhite,this);
    }
    closeSelf() {
     
    }
    getRandom(min,max): number {
        // const min = 300000;
        // const max = 700000;
        const random = Math.floor(Math.random() * (max - min + 1) + min);
        return random;
      }
}