

//主页 挖宝

import CCLog from "../../CCLog";
import { GameData } from "../../Common/GameData";
import GameStaticData from "../../GameStaticData";
import Language, { Country } from "../../Common/Language";
import UIToast from "../../Common/UIToast";
import getEventEmiter from "../../Libraries/EventEmitter";
import SceneManagerSystem from "../../System/SceneManagerSystem";
import { wabaoEmit } from "./wabaoMain";



const { ccclass, property } = cc._decorator;

@ccclass
export default class hallWabaoControlUI extends cc.Component {

    @property(cc.Label)
    leftTime:cc.Label= null
    @property(cc.Node)
    nodeSpine_baxi:cc.Node= null
    @property(cc.Node)
    nodeSpine_en:cc.Node= null
    @property(cc.Node)
    nodeSpine_yinni:cc.Node= null
    @property(cc.Node)
    nodegray_baxi:cc.Node= null
    @property(cc.Node)
    nodegray_en:cc.Node= null
    @property(cc.Node)
    nodegray_yinni:cc.Node= null
    @property(cc.Node)
    tipleftNode:cc.Node= null
   
   
   
    onLoad(){
        getEventEmiter().on(wabaoEmit.wabao_refresh_left_time, this.refreshUI, this);
    }
    start() {
    }
    refreshUI(){
        this.nodeSpine_yinni.active = false
        this.nodeSpine_baxi.active = false
        this.nodeSpine_en.active = false
        this.nodegray_baxi.active = false
        this.nodegray_en.active = false
        this.nodegray_yinni.active = false

        this.leftTime.string = GameStaticData.wabaoLeftTime+""
        let country = Language.getCountry;
        
        if (GameStaticData.isUnLockwabao) {
            this.tipleftNode.active = true
            if (country == Country.ID){//印尼
                this.nodeSpine_yinni.active = true
            }else if (country == Country.BR){//巴西
                this.nodeSpine_baxi.active = true
            }else{
                this.nodeSpine_en.active = true
            }
        }else{
            this.tipleftNode.active = false
            if (country == Country.ID){//印尼
                this.nodegray_yinni.active = true
            }else if (country == Country.BR){//巴西
                this.nodegray_baxi.active = true
            }else{
                this.nodegray_en.active = true
            }
        }
    }
    onClickBtn(){
        CCLog.log("+++++点击主页挖宝按钮",GameStaticData.isUnLockwabao)
        if (!GameStaticData.isUnLockwabao) {
            UIToast.popUp(Language.getWord("WabaoNeeddLv",GameStaticData.lockwabaoLV))//需要达到5关卡数才可解锁
        }else{
           
            SceneManagerSystem.showWaBaoMainPanel()
           
        }
        
    }
    onDestroy(){
        getEventEmiter().off(wabaoEmit.wabao_refresh_left_time,this.refreshUI,this)
    }
    closeSelf() {
     
    }
}