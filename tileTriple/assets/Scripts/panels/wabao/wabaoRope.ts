import CCLog from "../../CCLog";
import { AUDIOS_PATH } from "../../Common/AudioConfig";
import GameStaticData from "../../GameStaticData";
import Language, { Country } from "../../Common/Language";
import getEventEmiter from "../../Libraries/EventEmitter";
import AudioSystem from "../../System/AudioSystem";
import wabaoCar from "./wabaoCar";
import  { ROPE_COLLIDER_TAG, wabaoBoxType, wabaoEmit} from "./wabaoMain";

const {ccclass, property} = cc._decorator;

@ccclass
export default class wabaoRope extends cc.Component {
    @property([cc.Node])
    getBoxNode_low: cc.Node[] = [];
    @property([cc.Node])
    getBoxNode_mid: cc.Node[] = [];
    @property([cc.Node])
    getBoxNode_hegh: cc.Node[] = [];
    @property(cc.Node)
    npcNode: cc.Node = null;
    @property(wabaoCar)
    carNode: wabaoCar = null;
    private isMovingRope = false
    private direction = -1// -1 向下  1 向上
    private downToUp = false //由下向上运动
    private ropeSpeed = 400
    private startY = -160
    private maxDownY = -160-1800/2
    onLoad () {
        this.node.getComponent(cc.Collider).tag = ROPE_COLLIDER_TAG;
    }

    start () {
         this.hideAllGetBox()
    }
    startRopeMove() {
        this.isMovingRope = true;
    }

    stopRopeMove() {
        this.isMovingRope = false;
    }
    isSetDownToUP(is){
        this.downToUp = is
    }
    hideAllGetBox(){
        for (let index = 0; index < this.getBoxNode_low.length; index++) {
            const element = this.getBoxNode_low[index];
            //element.zIndex = -100
            element.active = false  
        }
        for (let index = 0; index < this.getBoxNode_mid.length; index++) {
            const element = this.getBoxNode_mid[index];
            //element.zIndex = -100
            element.active = false  
        }
        for (let index = 0; index < this.getBoxNode_hegh.length; index++) {
            const element = this.getBoxNode_hegh[index];
            //element.zIndex = -100
            element.active = false  
        }
    }
    //显示碰撞的物体
    showTagRedBox(redBoxType){
       this.stopRopeMove()
       this.hideAllGetBox()
       let country = Language.getCountry;
    
       if (country == Country.ID){//印尼
            if (redBoxType == wabaoBoxType.wabao_BoxType_low) {
                    this.getBoxNode_low[2].active = true
                    this.getBoxNode_low[2].scaleX = this.npcNode.scaleX
                    GameStaticData.wabaoRewardType = "low"                 
            }else if(redBoxType == wabaoBoxType.wabao_BoxType_mid){
                    this.getBoxNode_mid[2].active = true
                    this.getBoxNode_mid[2].scaleX = this.npcNode.scaleX
                    GameStaticData.wabaoRewardType = "centre"
            }else if(redBoxType == wabaoBoxType.wabao_BoxType_heigh){
                    this.getBoxNode_hegh[2].active = true
                    this.getBoxNode_hegh[2].scaleX = this.npcNode.scaleX
                    GameStaticData.wabaoRewardType = "high"
            }
       }else if (country == Country.BR){//巴西
            if (redBoxType == wabaoBoxType.wabao_BoxType_low) {
                    this.getBoxNode_low[0].active = true
                    this.getBoxNode_low[0].scaleX = this.npcNode.scaleX
                    GameStaticData.wabaoRewardType = "low"
            }else if(redBoxType == wabaoBoxType.wabao_BoxType_mid){
                    this.getBoxNode_mid[0].active = true
                    this.getBoxNode_mid[0].scaleX = this.npcNode.scaleX
                    GameStaticData.wabaoRewardType = "centre"
            }else if(redBoxType == wabaoBoxType.wabao_BoxType_heigh){
                    this.getBoxNode_hegh[0].active = true
                    this.getBoxNode_hegh[0].scaleX = this.npcNode.scaleX
                    GameStaticData.wabaoRewardType = "high"
            }
       }else if (country == Country.RU){
        if (redBoxType == wabaoBoxType.wabao_BoxType_low) {
                this.getBoxNode_low[3].active = true
                this.getBoxNode_low[3].scaleX = this.npcNode.scaleX
                GameStaticData.wabaoRewardType = "low"
        }else if(redBoxType == wabaoBoxType.wabao_BoxType_mid){
                this.getBoxNode_mid[3].active = true
                this.getBoxNode_mid[3].scaleX = this.npcNode.scaleX
                GameStaticData.wabaoRewardType = "centre"
        }else if(redBoxType == wabaoBoxType.wabao_BoxType_heigh){
                this.getBoxNode_hegh[3].active = true
                this.getBoxNode_hegh[3].scaleX = this.npcNode.scaleX
                GameStaticData.wabaoRewardType = "high"
        }
       }else{
            if (redBoxType == wabaoBoxType.wabao_BoxType_low) {
                    //CCLog.log("+++++++旋转",this.getBoxNode_low[1].scaleX,this.getBoxNode_low[1].parent.parent.parent.scaleX,this.getBoxNode_low[1].parent.parent.scaleX)
                    this.getBoxNode_low[1].active = true
                    this.getBoxNode_low[1].scaleX = this.npcNode.scaleX
                    GameStaticData.wabaoRewardType = "low"
            }else if(redBoxType == wabaoBoxType.wabao_BoxType_mid){
                    this.getBoxNode_mid[1].active = true
                    this.getBoxNode_mid[1].scaleX = this.npcNode.scaleX
                    GameStaticData.wabaoRewardType = "centre"
            }else if(redBoxType == wabaoBoxType.wabao_BoxType_heigh){
                    this.getBoxNode_hegh[1].active = true
                    this.getBoxNode_hegh[1].scaleX = this.npcNode.scaleX
                    GameStaticData.wabaoRewardType = "high"
            }
       }
       this.scheduleOnce(()=>{
        this.direction = 1
        this.startRopeMove()
       },0.5)
    }
    public onCollisionEnter(other: cc.BoxCollider, self: cc.BoxCollider): void {
        if (other.tag == wabaoBoxType.wabao_BoxType_low||
            other.tag == wabaoBoxType.wabao_BoxType_mid||
            other.tag == wabaoBoxType.wabao_BoxType_heigh) {
            
           
            //向上的过程中 不在判断
            if (!GameStaticData.isWabaoRopeHasCollisioned&&this.direction == -1) {
                CCLog.log("++++++钩子开始碰撞",other.tag)
                //碰到的红包隐藏
                AudioSystem.play(AUDIOS_PATH.gouziget)
                this.showTagRedBox(other.tag)//
                other.node.active = false
                GameStaticData.isWabaoRopeHasCollisioned = true
            }
            
           
        }
        
    }

    public onCollisionExit(other: cc.BoxCollider, self: cc.BoxCollider): void {
        
    }
    update (dt) {
        if (this.isMovingRope) {
            //到达底部
            if (this.node.y < this.maxDownY) {
                this.direction = 1;
                this.downToUp = true
                
            } else if (this.node.y >= this.startY) {//到达顶部
               
                this.direction = -1;
                //已经挖到宝
                if (GameStaticData.isWabaoRopeHasCollisioned) {
                    CCLog.log("++++++重置挖宝状态")
                    this.isMovingRope = false
                    //碰撞的标识重置
                    GameStaticData.isWabaoRopeHasCollisioned = false
                    getEventEmiter().emit(wabaoEmit.wabao_open_reward,{});
                    return
                  
                }
                //由下往上
                if (this.downToUp) {
                    
                    //GameStaticData.isStartWabao = false//可以点击空白区域
                    this.isMovingRope = false
                    //this.carNode.startCarMove()
                    getEventEmiter().emit(wabaoEmit.wabao_open_reward,{});
                    return
                
                }
               
            }
            if (this.direction == 1) {
                this.node.y += this.direction * dt * this.ropeSpeed*2;
            }else{
                this.node.y += this.direction * dt * this.ropeSpeed;
            }
            
            
           
        }
    }
    
}
