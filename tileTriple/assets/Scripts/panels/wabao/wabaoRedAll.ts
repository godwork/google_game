import CCLog from "../../CCLog";

import {wabaoBoxType } from "./wabaoMain";
import wabaoRed from "./wabaoRed";

const {ccclass, property} = cc._decorator;

@ccclass
export default class wabaoRedAll extends cc.Component {
    @property([cc.Node])
    layoutAll: cc.Node[] = [];
    @property(cc.Prefab)
    prefabNode:cc.Prefab[] = [];
    private stayX  = 0;
    private canMove = false

    //第一行 向右
    private rowArr_1 = []
   
    private startrow1_x = 296
    private row1_Space = 148
    private layoutSpeed1 = 300;

    //第二行  向左
    private rowArr_2 = []
    private startrow2_x = -285
    private row2_Space = 190
    private layoutSpeed2 = 250;

     //第三行  向右
     private rowArr_3 = []
     private startrow3_x = 285
     private row3_Space = 285
     private layoutSpeed3 = 200;
    onLoad () {
       this.stayX = cc.view.getVisibleSize().width/2;
    }
    start () {
        this.createrow1()
        this.createrow2()
        this.createrow3()
        this.scheduleOnce(()=>{
            this.canMove = true
        })
       
    }
    createrow1(){
        this.layoutAll[0].removeAllChildren()
        for (let k = 0; k < 8; k++) {
            let _oneIns = cc.instantiate(this.prefabNode[0])
            _oneIns.parent = this.layoutAll[0]
            _oneIns.x = this.startrow1_x-this.row1_Space*k
            _oneIns.y = 0
            _oneIns.getComponent(wabaoRed).initWaBaoBoxInfo(wabaoBoxType.wabao_BoxType_low)
            this.rowArr_1.push(_oneIns) 
        }
    }
    createrow2(){
        this.layoutAll[1].removeAllChildren()
        for (let k = 0; k < 7; k++) {
            let _oneIns = cc.instantiate(this.prefabNode[1])
        
            _oneIns.parent = this.layoutAll[1]
            _oneIns.x = this.startrow2_x+this.row2_Space*k
            _oneIns.y = 0
            _oneIns.getComponent(wabaoRed).initWaBaoBoxInfo(wabaoBoxType.wabao_BoxType_mid)
            this.rowArr_2.push(_oneIns)
            
        }
    }
    createrow3(){
        this.layoutAll[2].removeAllChildren()
        for (let k = 0; k < 6; k++) {
            let _oneIns = cc.instantiate(this.prefabNode[2])
        
            _oneIns.parent = this.layoutAll[2]
            _oneIns.x = this.startrow3_x-this.row3_Space*k
            _oneIns.y = 0
            _oneIns.getComponent(wabaoRed).initWaBaoBoxInfo(wabaoBoxType.wabao_BoxType_heigh)
            this.rowArr_3.push(_oneIns)
        }
    }
    moverow1(dt){
        
        for (let index = 0; index < this.rowArr_1.length; index++) {
           let _a = this.rowArr_1[index]
           _a.x += dt * this.layoutSpeed1
       
           if (index == 0) {
                if (_a.x>=this.stayX+_a.width/2) {
                    _a.active = true
                    _a.x = this.rowArr_1[this.rowArr_1.length-1].x-this.row1_Space
                }
           }else{
                if (_a.x>=this.stayX+_a.width/2) {
                    _a.active = true
                    _a.x = this.rowArr_1[index-1].x-this.row1_Space
                }
           }
        }
    }
    moverow2(dt){

        for (let index = 0; index < this.rowArr_2.length; index++) {
            let _a = this.rowArr_2[index]
            _a.x -= dt * this.layoutSpeed2
        
            if (index == 0) {
                 if (_a.x<=-this.stayX-_a.width/2) {
                     _a.active = true
                     _a.x = this.rowArr_2[this.rowArr_2.length-1].x+this.row2_Space
                 }
            }else{
                if (_a.x<=-this.stayX-_a.width/2) {
                    _a.active = true
                    _a.x = this.rowArr_2[index-1].x+this.row2_Space
                }
            }
         }
    }
    moverow3(dt){
        for (let index = 0; index < this.rowArr_3.length; index++) {
            let _a = this.rowArr_3[index]
            _a.x += dt * this.layoutSpeed3
        
            if (index == 0) {
                 if (_a.x>=this.stayX+_a.width/2) {
                     _a.active = true
                     _a.x = this.rowArr_3[this.rowArr_3.length-1].x-this.row3_Space
                 }
            }else{
                 if (_a.x>=this.stayX+_a.width/2) {
                     _a.active = true
                     _a.x = this.rowArr_3[index-1].x-this.row3_Space
                 }
            }
         }
    }
    update (dt) {

        if (this.canMove) {
            this.moverow1(dt)
            this.moverow2(dt)
            this.moverow3(dt)
           
        }
    }
    
}
