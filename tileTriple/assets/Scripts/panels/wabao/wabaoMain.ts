import CCLog from "../../CCLog";
import { AUDIOS_PATH } from "../../Common/AudioConfig";
import { GameData} from "../../Common/GameData";
import GameStaticData from "../../GameStaticData";
import {JsbTileTrileTileCallMgr} from "../../Common/JsbMgr";
import Language, { Country } from "../../Common/Language";
import getEventEmiter from "../../Libraries/EventEmitter";
import AudioSystem from "../../System/AudioSystem";
import SceneManagerSystem from "../../System/SceneManagerSystem";
import wabaoCar from "./wabaoCar";
import wabaoRope from "./wabaoRope";
import GameStaticJsonData from "../../GameStaticJsonData";
import LocalKey from "../../LocalKey";
import GameTileStaticJsonData from "../../GameStaticJsonData";
export enum wabaoBoxType
{
    wabao_BoxType_low                      = 66,       
    wabao_BoxType_mid                      = 77,        
    wabao_BoxType_heigh                    = 88,        
}
export enum wabaoEmit
{
    wabao_open_reward                     = "wabao_open_reward",  
    wabao_refresh_left_time               = "wabao_refresh_left_time",//更新挖宝的剩余次数
    wabao_refresh_coin                    =  "wabao_refresh_coin",
    wabao_refresh_money                    =  "wabao_refresh_money"
       
}


export const ROPE_COLLIDER_TAG: number = 999;

const {ccclass, property} = cc._decorator;

@ccclass
export default class wabaoMain extends cc.Component {

    @property(cc.Node)
    nodeTopCoin: cc.Node = null;
    @property(cc.Node)
    nodeTopRed: cc.Node = null;
    @property(cc.Node)
    touchNode: cc.Node = null;
    @property(cc.Label)
    labelNum: cc.Label = null;
    @property(cc.Label)
    labelLeftStrNum: cc.Label = null;
    @property(cc.Label)
    labelTip: cc.Label = null;
    @property(wabaoCar)
    carNode: wabaoCar = null;
    @property(wabaoRope)
    ropeNode: wabaoRope = null;
  
   //更新金币 美元直接用 GetAccountInfo
    onLoad () {
        getEventEmiter().on(wabaoEmit.wabao_open_reward, this.getwabaoInfo, this);
        getEventEmiter().on(wabaoEmit.wabao_refresh_left_time, this.refreshLeftWaBaoTime, this);
        //let manager = cc.director.getCollisionManager(); 
       // manager.enabledDebugDraw = true
       // manager.enabledDrawBoundingBox = true
        this.touchNode.on(cc.Node.EventType.TOUCH_START, this.onTouchBegan, this);
        //this.touchNode.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.touchNode.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.refreshLeftWaBaoTime()
        
    }

    start () {
        AudioSystem.stopMusic();
        AudioSystem.playWaBaoMusic();
        this.labelLeftStrNum.string = Language.getWord("WabaoLeftTimeStr")
        this.labelTip.string = Language.getWord("topcashTip")

    }
    getwabaoInfo(){
       // setTimeout(async () => {
            let rewardMoney = 0
            let reward = GameStaticJsonData.getLevelPassRewardMoney(GameData.AccountData.moneytotal)
            if (GameStaticData.wabaoRewardType == "none") {
                rewardMoney = 0
            }else if(GameStaticData.wabaoRewardType == "low"){
                rewardMoney = GameStaticJsonData.getTwoPointNum(reward*GameStaticJsonData.wabaoRule.lowRate)
            }else if(GameStaticData.wabaoRewardType == "centre"){
                rewardMoney = GameStaticJsonData.getTwoPointNum(reward*GameStaticJsonData.wabaoRule.centreRate)
            }else if(GameStaticData.wabaoRewardType == "high"){
                rewardMoney = GameStaticJsonData.getTwoPointNum(reward*GameStaticJsonData.wabaoRule.highRate)
            }
            //至少0.01美元
            if (rewardMoney<=0.01&&GameStaticData.wabaoRewardType != "none") {
                rewardMoney = 0.01
            }
            GameStaticData.isStartWabao = false//可以点击空白区域了
            this.carNode.startCarMove()
            this.ropeNode.hideAllGetBox()
            GameStaticData.wabaoRewardMoneyNum = rewardMoney
            if (rewardMoney>0) {
                SceneManagerSystem.showWaBaoGetReward()
            }
           
            if (GameStaticData.wabaoLeftTime>=1) {
                GameStaticData.wabaoLeftTime--
                GameStaticJsonData.saveWaBaoLocal_Info(LocalKey.wabaoleft_time,GameStaticData.wabaoLeftTime)
                getEventEmiter().emit(wabaoEmit.wabao_refresh_left_time,{})
            }
            
            

            // //获取奖励
            // CCLog.log("++++++挖宝获取奖励传参",BizTypeEnum.v2_digmoney,GameData.LevelData.passNum,0,GameStaticData.wabaoRewardType)
            // let msg = await HttpReceiveAward(BizTypeEnum.v2_digmoney,GameData.LevelData.passNum,0,GameStaticData.wabaoRewardType,GetRewardReadOrGet.getTypeRead_none);
            // if (msg.resCode == 0){
            //     GameStaticData.isStartWabao = false//可以点击空白区域了
            //     this.carNode.startCarMove()
            //     this.ropeNode.hideAllGetBox()
               
                
            //     CCLog.log("++++++挖宝获取奖励",msg.data.rewardMoney,msg.data.rewardCoin,msg.data.digNum)
            //     if (msg.data.rewardMoney&&msg.data.rewardMoney>0) {
            //         GameStaticData.wabaoRewardMoneyNum = msg.data.rewardMoney
            //         SceneManagerSystem.showWaBaoGetReward()
            //     }
               
            //     if (msg.data.digNum>=0) {
            //         GameStaticData.wabaoLeftTime = msg.data.digNum
            //         getEventEmiter().emit(wabaoEmit.wabao_refresh_left_time,{})
            //     }
                
                
                
            // }else{
            //     GameStaticData.isStartWabao = false//可以点击空白区域了
            //     this.carNode.startCarMove()
            //     this.ropeNode.hideAllGetBox()
            // }
            
            
       // }, 0);
    }
    refreshLeftWaBaoTime(){
        this.labelNum.string = GameStaticData.wabaoLeftTime+""
    }
    onButtonClick(event,value = null){
        let passNumStr = GameData.LevelData.passNum+""

        let moneyNumStr = GameData.AccountData.moneytotal+""
        let jsonStr = {passNum:passNumStr,
            coinNum:"0",
            moneyNum:moneyNumStr,
            moneyrate:GameTileStaticJsonData.getMoneyChangeRate+"",
            tixianstartlv:GameTileStaticJsonData.tixianParm.sl+"",
            needsignDays:GameTileStaticJsonData.tixianParm.r1+"",
            needvideo:GameTileStaticJsonData.tixianParm.r2+"",
            tixianshenlv:GameTileStaticJsonData.tixianParm.r3+""}
        
        if (event.target.name == "coinbg"){
            JsbTileTrileTileCallMgr.handler("coinDrawCash","(Ljava/lang/String;)V",JSON.stringify(jsonStr))
        }else if (event.target.name == "redbg"||event.target.name == "tipBtn"){
            JsbTileTrileTileCallMgr.handler("moneyDrawCash","(Ljava/lang/String;)V",JSON.stringify(jsonStr))
        }
    }
    onTouchBegan(event){
       
    }
    async onTouchEnd(event){
        if (!GameStaticData.isStartWabao) {
            if (GameStaticData.wabaoLeftTime>0) {
                AudioSystem.play(AUDIOS_PATH.putgouzi)
                GameStaticData.isStartWabao = true
                GameStaticData.wabaoRewardType = "none"
                this.carNode.stopCarMove()
                this.ropeNode.startRopeMove()
                this.ropeNode.isSetDownToUP(false)
                
            }else{
                CCLog.log("+++++++挖宝次数不足 弹出看视频界面")
                await SceneManagerSystem.showWaBaoSeevideoPanel()
            }
           
        }
    }
    closeBtn()
    {     
        //状态重置
        GameStaticData.isWabaoRopeHasCollisioned = false
        GameStaticData.isStartWabao = false
        AudioSystem.stopMusic();
        AudioSystem.playMusic();
        this.touchNode.off(cc.Node.EventType.TOUCH_START, this.onTouchBegan, this);
        this.touchNode.off(cc.Node.EventType.TOUCH_START, this.onTouchEnd, this);
        this.node.destroy();
    }
    protected onDestroy(): void {
        getEventEmiter().off(wabaoEmit.wabao_open_reward,this.getwabaoInfo,this);
        getEventEmiter().off(wabaoEmit.wabao_refresh_left_time,this.refreshLeftWaBaoTime,this)
    }
}
