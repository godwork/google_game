

//主页 挖宝

import CCLog from "../../CCLog";
import { GameData } from "../../Common/GameData";
import GameStaticData from "../../GameStaticData";
import Language, { Country } from "../../Common/Language";
import UIToast from "../../Common/UIToast";
import getEventEmiter from "../../Libraries/EventEmitter";
import SceneManagerSystem from "../../System/SceneManagerSystem";
import { wabaoEmit } from "./wabaoMain";



const { ccclass, property } = cc._decorator;

@ccclass
export default class newHallWabaoControlUI extends cc.Component {

    @property(cc.Label)
    leftTime:cc.Label= null
    @property(cc.Node)
    tipleftNode:cc.Node= null

    @property(cc.Node)
    lightAllNode:cc.Node= null

    @property(cc.Node)
    lightNode1:cc.Node= null

    @property(cc.Node)
    lightNode2:cc.Node= null

    @property(cc.Node)
    picGray:cc.Node= null
  
    @property(cc.Node)
    picNodes:cc.Node[]= []
    private haseplaing = false
   
   
   
    onLoad(){
        if (GameStaticData.isShenHeModel) {
            this.node.active = false
            this.tipleftNode.active = false
            return
        }
        getEventEmiter().on(wabaoEmit.wabao_refresh_left_time, this.refreshUI, this);
        this.runActionLight()
        this.initPicZIndex()
        this.picGray.zIndex = cc.macro.MAX_ZINDEX
        
    }
    start() {
    }

    runActionLight(){
        //1 停留时间0.2   0.6秒后再次执行
        this.lightNode1.opacity = 0
        let t = cc.tween()
            .to(0.05, { opacity: 255})
            .delay(0.05)
            .to(0.05, { opacity: 0})
            .delay(0.3)
        cc.tween(this.lightNode1)
            .then(t)
            .repeatForever()
            .start();
            // this.lightNode2.opacity = 0
            // let t2 = cc.tween()
            // .delay(0.3)
            //     .to(0.1, { opacity: 255})
            //     .delay(0.1)
            //     .to(0.1, { opacity: 0})
                
            // cc.tween(this.lightNode2)
            //     .then(t2)
            //     .repeatForever()
            //     .start();
        
       
        
    }
    //顺序播放
    initPicZIndex(){
        for (let index = 0; index < this.picNodes.length; index++) {
            const element = this.picNodes[index];
            element.zIndex = cc.macro.MAX_ZINDEX -index-1
            
        }
    }
    playArrPic(index){
       
        let moveaction = cc.moveTo(1,cc.v2(0,130))
        let call = cc.callFunc(()=>{
            this.picNodes[index].y = 0
            let nextIndex = 0
            if(index == 0){
                this.picNodes[index].zIndex = this.picNodes[this.picNodes.length-1].zIndex-1
            }else{
                this.picNodes[index].zIndex = this.picNodes[index-1].zIndex-1
            }
            if (index == this.picNodes.length-1) {
                nextIndex = 0
            }else{
                nextIndex = index+1
            }

            this.playArrPic(nextIndex)
        })
        let delayT = cc.delayTime(20)
        let seq = cc.sequence(moveaction,delayT,call)
        this.picNodes[index].runAction(seq)
    }
    refreshUI(){
      

        this.leftTime.string = GameStaticData.wabaoLeftTime+""
       
        if (GameStaticData.isUnLockwabao) {
            this.tipleftNode.active = true
            this.picGray.active = false
            this.lightAllNode.active = true
            if (this.haseplaing) {
                return
            }
            this.unscheduleAllCallbacks()
            this.haseplaing = true
            for (let index = 0; index < this.picNodes.length; index++) {
                const element = this.picNodes[index];
                element.stopAllActions()
                
            }
            this.scheduleOnce(()=>{this.playArrPic(0)},20)
            
            
            
        }else{
            this.tipleftNode.active = false
            this.picGray.active = true
            this.lightAllNode.active = false
          
           
        }
    }
    onClickBtn(){
        CCLog.log("+++++点击主页挖宝按钮",GameStaticData.isUnLockwabao)
        if (!GameStaticData.isUnLockwabao) {
            UIToast.popUp(Language.getWord("WabaoNeeddLv",GameStaticData.lockwabaoLV))//需要达到5关卡数才可解锁
        }else{
           
            SceneManagerSystem.showWaBaoMainPanel()
           
        }
        
    }
    onDestroy(){
        getEventEmiter().off(wabaoEmit.wabao_refresh_left_time,this.refreshUI,this)
    }
    closeSelf() {
     
    }
}