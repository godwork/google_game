
const {ccclass, property} = cc._decorator;

@ccclass
export default class wabaoCar extends cc.Component {
    @property(sp.Skeleton)
    NPC: sp.Skeleton = null;
    
    private playerSpeed = 300

    // true: 车子来回摆动
    private runCar: boolean = false;
    private direction: number = -1; //-1向左, 1向右
    private stayX  = 0;
    onLoad () {
        this.stayX = cc.view.getVisibleSize().width/2;
    }
    start () {
         this.startCarMove()
    }
    startCarMove() {
        this.runCar = true;
        this.NPC.setAnimation(0,"kuanggong",true)
    }

    stopCarMove() {
        this.runCar = false;
        //this.NPC.setAnimation(0,"kuanggong",true)
    }

    update (dt) {
        if (this.runCar) {
            if (this.node.x < -this.stayX) {
                this.direction = 1;
            } else if (this.node.x > this.stayX) {
                this.direction = -1;
            }
            this.node.x += this.direction * dt * this.playerSpeed;
            this.NPC.node.scaleX = -this.direction;
        }
    }
}
