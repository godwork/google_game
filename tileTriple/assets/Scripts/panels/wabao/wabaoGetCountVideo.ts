import { showVideoAd } from "../../Common/AdMethods";
import { addWaBaoNum } from "../../Common/GameData";
import GameStaticData from "../../GameStaticData";
import Language, { Country } from "../../Common/Language";
import UIToast from "../../Common/UIToast";
import getEventEmiter from "../../Libraries/EventEmitter";
import { wabaoEmit } from "./wabaoMain";

const {ccclass, property} = cc._decorator;

@ccclass
export default class wabaoGetReward extends cc.Component {

    @property(cc.Label)
    titleStr: cc.Label = null;
    @property(cc.RichText)
    todayLeftTIme: cc.RichText = null;
    @property(cc.Node)
    tipBaXi: cc.Node = null;
    @property(cc.Node)
    tipYinnNi: cc.Node = null;
    @property(cc.Node)
    tipEn: cc.Node = null;
    @property(cc.Node)
    tipRu: cc.Node = null;
    @property(cc.Node)
    btnNodeLight: cc.Node = null;
    @property(cc.Node)
    btnNodeGray: cc.Node = null;
    @property(cc.Node)
    strTip1: cc.Node = null;
    @property(cc.Node)
    strTip2: cc.Node = null;
   
    onLoad () {
       
        
    }
    
    start () {
        this.titleStr.string = Language.getWord("NotEnught")
        this.strTip1.getComponent(cc.Label).string = Language.getWord("WabaoVideoStr1")
        this.strTip2.getComponent(cc.Label).string = Language.getWord("WabaoVideoStr2")

        this.todayLeftTIme.string = "<color=#A5572B>"+Language.getWord("WabaoTodaySeeLeftTime")+"</c>"+ 
        "<color=#FF0000>"+GameStaticData.wabaoTodaySeeVideoLeftTime+"</c>"

        this.refreshUI()
    }
    refreshUI(){
        this.strTip1.active = false
        this.strTip2.active = false
       if (GameStaticData.wabaoTodaySeeVideoLeftTime<=0) {
           this.btnNodeLight.active = false
           this.btnNodeGray.active = true
           this.strTip2.active = true
           let str1 = Language.getWord("WabaoSeeAddTimePart1")
           let str2 = Language.getWord("WabaoSeeAddTimePart2")
           this.btnNodeGray.getChildByName("newAddNum").getComponent(cc.RichText).string = "<color=#D1D5E5><outline color=#4B6D6E width=2>"+str1+"</outline></c>"+ 
                             "<color=#D1D5E5><outline color=#4B6D6E width=2>"+GameStaticData.wabaoSeeVideoAddNum+"</outline></c>"+
                             "<color=#D1D5E5><outline color=#4B6D6E width=2>"+str2+"</outline></c>"
       }else{
           this.btnNodeLight.active = true
           this.btnNodeGray.active = false
           this.strTip1.active = true
           let str1 = Language.getWord("WabaoSeeAddTimePart1")
           let str2 = Language.getWord("WabaoSeeAddTimePart2")
           this.btnNodeLight.getChildByName("newAddNum").getComponent(cc.RichText).string = "<color=#FFFFFF><outline color=#B67A00 width=2>"+str1+"</outline></c>"+ 
                             "<color=#FF0000><outline color=#B67A00 width=2>"+GameStaticData.wabaoSeeVideoAddNum+"</outline></c>"+
                             "<color=#FFFFFF><outline color=#B67A00 width=2>"+str2+"</outline></c>"
       }
       this.tipYinnNi.active = false
       this.tipBaXi.active = false
       this.tipEn.active = false
       this.tipRu.active = false
       let country = Language.getCountry;
    
       if (country == Country.ID){//印尼
            this.tipYinnNi.active = true
       }else if (country == Country.BR){//巴西
            this.tipBaXi.active = true
       }else if (country == Country.RU){
            this.tipRu.active = true
       }else{
            this.tipEn.active = true
       }
    }
    conClickSeeVideo(){
        if (GameStaticData.wabaoTodaySeeVideoLeftTime<=0) {
            UIToast.popUp(Language.getWord("WabaoVideoNone"))
        }else{
            let func = ()=>{
                addWaBaoNum(()=>{UIToast.popUp(Language.getWord("WabaoVideoSucc"));this.closeBtn()})
            }
            if (CC_JSB) {
                showVideoAd().then(()=>{
                    func()
                })
                    
            }else{
               //增加次数
               func()
            }
        }
       
    }

    closeBtn(){      
        this.node.destroy();
    }
    onDestroy() {
      
    }
}
