import { AUDIOS_PATH } from "../../Common/AudioConfig";
import { GameData, GetAccountInfo, GetAccountSelfAdd, GetReceiveAward } from "../../Common/GameData";

import Language, { Country } from "../../Common/Language";
import { forMattedMoney } from "../../Common/ProjectConfig";
import AnimationTs from "../../Game/AnimationTs";
import GameStaticData from "../../GameStaticData";
import AudioSystem from "../../System/AudioSystem";
import { BizTypeEnum, HttpAccount } from "../../net/api";

const {ccclass, property} = cc._decorator;

@ccclass
export default class wabaoGetReward extends cc.Component {

    @property(cc.Label)
    titleStr: cc.Label = null;
    @property(cc.Label)
    closeStr: cc.Label = null;
    @property(cc.Label)
    rewardNum: cc.Label = null;

    @property(sp.Skeleton)
    meichaospine: sp.Skeleton = null;
    @property(sp.Skeleton)
    idmeichaospine: sp.Skeleton = null;
    @property(sp.Skeleton)
    pkmeichaospine: sp.Skeleton = null;
    @property([AnimationTs])
    yanhuaTs: Array<AnimationTs>= []
    @property(cc.Node)
    nodePicBaXi: cc.Node = null
    @property(cc.Node)
    nodePicMei: cc.Node = null
    @property(cc.Node)
    nodePicYinNi: cc.Node = null
    @property(cc.Node)
    nodePicRU: cc.Node = null
   
    @property(cc.Node)
    BtnNode: cc.Node = null;
    onLoad () {
       
        
    }

    start () {
         this.titleStr.string = Language.getWord("CongrAtulations")
         this.closeStr.string = Language.getWord("WabaohappyGet")
         this.rewardNum.string =  Language.MoneyChar+forMattedMoney(GameStaticData.wabaoRewardMoneyNum)

         let country = Language.getCountry;
         this.meichaospine.node.active = false;
         this.idmeichaospine.node.active = false;
         this.pkmeichaospine.node.active = false;
         if (country == Country.PK){
             this.pkmeichaospine.node.active = true;
             this.pkmeichaospine.setAnimation(0,"huangmeichao",false)
             this.nodePicBaXi.active = true
         }else if (country == Country.ID){
             this.idmeichaospine.node.active = true;
             this.idmeichaospine.setAnimation(0,"hongmeichoa",false)
             this.nodePicYinNi.active = true
         }else if (country == Country.RU){
            this.idmeichaospine.node.active = true;
            this.idmeichaospine.setAnimation(0,"hongmeichoa",false)
            this.nodePicRU.active = true
        }else{
             this.meichaospine.node.active = true;
             this.meichaospine.setAnimation(0,"lvmeichao",false)
             this.nodePicMei.active = true
         }
         AudioSystem.play(AUDIOS_PATH.coinandmoney);
         //烟花
         this.playerYanHuaAni();
    }
    private playerybInterval1;
    playerYanHuaAni(index:number = 0){
        if (cc.isValid(this.yanhuaTs) && cc.isValid(this.yanhuaTs[index])){
            this.yanhuaTs[index].playAni(cc.WrapMode.Normal,(state)=>{
                if (state == cc.Animation.EventType.FINISHED){
                    if (index == 0 || index == 2){
                        this.playerYanHuaAni(index+1);
                    }else if (index == 3){
                        this.playerybInterval1 = setTimeout(()=>{
                            this.playerybInterval1 = null;
                            this.playerYanHuaAni();
                        },2000)
                    }
                }else if(state == AnimationTs.PlayCenterFrame){
                    if (index == 1){
                        this.playerYanHuaAni(index+1);
                    }
                }
            })
        }
    }
    closeBtn(){ 
          //上传挖宝奖励     
          GetReceiveAward(BizTypeEnum.v2_digmoney,0,GameStaticData.wabaoRewardMoneyNum)
          GetAccountSelfAdd(null,0,GameStaticData.wabaoRewardMoneyNum)
          this.node.destroy();
    }
    
    onDestroy() {
      
    }
}
