
import Language, { Country } from "../../Common/Language";
import { ROPE_COLLIDER_TAG, wabaoBoxType } from "./wabaoMain";


const {ccclass, property} = cc._decorator;

@ccclass
export default class wabaoRed extends cc.Component {
    @property([cc.Node])
    SpriteNode: cc.Node[] = [];
    private redType = wabaoBoxType.wabao_BoxType_low
   
    onLoad () {
        
     
    }
    initWaBaoBoxInfo(type:wabaoBoxType){
       this.redType = type
       this.node.getComponent(cc.Collider).tag = this.redType
       let country = Language.getCountry;
       this.SpriteNode[0].active = false;
       this.SpriteNode[1].active = false;
       this.SpriteNode[2].active = false;
       this.SpriteNode[3].active = false;
       if (country == Country.BR){
            this.SpriteNode[0].active = true;
       }else if (country == Country.ID){
            this.SpriteNode[2].active = true;
       }else if (country == Country.RU){
            this.SpriteNode[3].active = true;
       }else{
            this.SpriteNode[1].active = true;
       }
    }
    start () {
       
    }
   
   
    update (dt) {
       
    }
    
}
