
import { CUSTOM_EVENT_NAME } from "../../Common/Constant";
import { GameData } from "../../Common/GameData";
import Language from "../../Common/Language";
import { forMattedMoney } from "../../Common/ProjectConfig";
import MoneyFlySky from "../../Game/MoneyFlySky";
import RedbagFlySky from "../../Game/RedbagFlySky";

import getEventEmiter from "../../Libraries/EventEmitter";


const {ccclass, property} = cc._decorator;

@ccclass
export default class waBaoTopArea extends cc.Component {

    @property(cc.Label)
    label_coin: cc.Label = null;

    @property(cc.Label)
    label_money: cc.Label = null;
    @property(cc.Node)
    redbagDisLayout: cc.Node = null;
    @property(cc.Node)
    coinDisLayout: cc.Node = null;

    onLoad () {
        this.init();
    }

    start() {  
    }
    init(){
        this.addCoin(0)
        this.addRedBag(0);
      
        
        getEventEmiter().on(CUSTOM_EVENT_NAME.onCoinChange, this.addCoin, this)
        getEventEmiter().on(CUSTOM_EVENT_NAME.onMoneyChange, this.addRedBag, this)
        // getEventEmiter().on(CUSTOM_EVENT_NAME.addLevel, this.addLevel, this)
    }


   
    flyCoinAnimal(addnum){
        let targetNode = this.getCoinFlyTarget();
        if (targetNode) {
            MoneyFlySky.popUpCustom(cc.v3(cc.view.getVisibleSize().width / 2, cc.view.getVisibleSize().height / 2),
                targetNode.parent.convertToWorldSpaceAR(targetNode.position), targetNode, addnum);
        }
    }
    flyMoneyAnimal(addnum){
        let targetNode = this.getRedbagFlyTarget();
            if (targetNode) {
                RedbagFlySky.popUpCustom(
                    cc.v3(cc.view.getVisibleSize().width / 2, cc.view.getVisibleSize().height / 2),
                    targetNode.parent.convertToWorldSpaceAR(targetNode.position), targetNode,addnum);
            }
    }

    getCoinFlyTarget(){
        return this.coinDisLayout
    }

    getRedbagFlyTarget(){
        return this.redbagDisLayout
    }
    async addCoin(addNum) {
        if (addNum > 0 && this.coinDisLayout) {
            //播放个动画
            //this.flyCoinAnimal(addNum)
            // GameSceneController.I && GameSceneController.I.showMoneyFlyAnim();
            // await CocosHelper.sleepSync(1.5);
            // UIToast3.popUp({type:1,num:addNum},this.coinDisLayout.convertToWorldSpaceAR(cc.Vec3.ZERO));
        }
        if (this.label_coin && GameData.AccountData) {
            this.label_coin.string =  GameData.AccountData.cointotal + "";
        }
    }
    async addRedBag(addNum) {
        if (addNum > 0 && this.redbagDisLayout) {
            //播放个动画
            //this.flyMoneyAnimal(addNum)
            // GameSceneController.I && GameSceneController.I.showRedbagFlyAnim();
            // await CocosHelper.sleepSync(1.5);
            // UIToast3.popUp({type:2,num:addNum},this.redbagDisLayout.convertToWorldSpaceAR(cc.Vec3.ZERO));
        }

        if (this.label_money && GameData.AccountData) {
            this.label_money.string = Language.MoneyChar + forMattedMoney(GameData.AccountData.moneytotal) + "";
        }
    }

    protected onDestroy(): void {
        getEventEmiter().off(CUSTOM_EVENT_NAME.onCoinChange, this.addCoin, this)
        getEventEmiter().off(CUSTOM_EVENT_NAME.onMoneyChange, this.addRedBag, this)
    }
}
