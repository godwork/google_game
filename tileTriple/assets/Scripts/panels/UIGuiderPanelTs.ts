import CocosTileHelper from "../Common/CocosHelper";
import { BLOCK_ELIMINATE_LIMIT } from "../Common/Constant";
import GamePlayController from "../Game/GamePlayController";
import SingleBlockController from "../Game/SingleBlockController";
import TopAreaTs from "../Game/TopAreaTs";

const {ccclass, property} = cc._decorator;

@ccclass
export default class UIGuiderPanelTs extends cc.Component {
    static I:UIGuiderPanelTs 
    @property([cc.Node])
    GuiderArr:Array<cc.Node> = []

    protected onLoad(): void {
        UIGuiderPanelTs.I = this;
    }
        
    static async show(step:number){
        if (UIGuiderPanelTs.I){
            UIGuiderPanelTs.I.showStep(step)
        }else{
            let res:any = await CocosTileHelper.loadAssetSync("panels/UIGuiderPanel",cc.Prefab)
            let node = cc.instantiate(res)
            cc.find("Canvas").addChild(node)
            node.getComponent(UIGuiderPanelTs).showStep(step)
        }
    }

    showStep (step:number) {
        for (let i = 0; i < this.GuiderArr.length; i++) {
            this.GuiderArr[i].active = false;
        }
        console.log("引导",step)
        if (step == 1){
            this.GuiderArr[0].active = true;
            let same = this.findSame();
            console.log(same)
            let wopos = cc.v3(0,Number.MAX_VALUE)
            let playerNode = this.GuiderArr[0].getChildByName("playerNode")
            same.forEach(o=>{
                wopos.y = Math.min(wopos.y,o.node.y);
                let wpos = o.node.parent.convertToWorldSpaceAR(wopos);
                playerNode.y = playerNode.parent.convertToNodeSpaceAR(cc.v3(playerNode.x,wpos.y)).y
                o.moveToLayerMap(this.GuiderArr[0]);
            })
        }else if (step == 2){
            this.GuiderArr[0].active = true;
            let mask = this.GuiderArr[0].getChildByName("mask")
            let moneyicon = TopAreaTs.I.getRedbagFlyTarget();
            mask.setContentSize(cc.size(moneyicon.width+10,moneyicon.height+6))
            let wpos = moneyicon.convertToWorldSpaceAR(cc.Vec3.ZERO).sub(cc.v3(-5,0))
            mask.setPosition(mask.parent.convertToNodeSpaceAR(wpos))
            let playerNode = this.GuiderArr[0].getChildByName("playerNode")
            playerNode.x = 0 ;
            playerNode.y = playerNode.parent.convertToNodeSpaceAR(wpos).y-80
        }else if (step == 3){
            this.GuiderArr[1].active = true;
            let same = this.findJinBiSame();
            let wopos = cc.v3(0,Number.MAX_VALUE)
            let playerNode = this.GuiderArr[1].getChildByName("playerNode")
            same.forEach(o=>{
                wopos.y = Math.min(wopos.y,o.node.y);
                let wpos = o.node.parent.convertToWorldSpaceAR(wopos);
                playerNode.y = playerNode.parent.convertToNodeSpaceAR(cc.v3(playerNode.x,wpos.y)).y
                o.moveToLayerMap(this.GuiderArr[1]);
            })
        }else if (step == 4){
            this.GuiderArr[1].active = true;
            let mask = this.GuiderArr[1].getChildByName("mask")
            let moneyicon = TopAreaTs.I.getCoinFlyTarget();
            mask.setContentSize(cc.size(moneyicon.width+10,moneyicon.height+6))
            let wpos = moneyicon.convertToWorldSpaceAR(cc.Vec3.ZERO).sub(cc.v3(-5,0))
            mask.setPosition(mask.parent.convertToNodeSpaceAR(wpos))
            let playerNode = this.GuiderArr[1].getChildByName("playerNode")
            playerNode.x = 0 ;
            playerNode.y = playerNode.parent.convertToNodeSpaceAR(wpos).y-80
        }
    }

    public findSame() {
        let blocks = GamePlayController.I.getBlocksAvailable();
        let checked: SingleBlockController[] = [];
        for (let i: number = 0; i < blocks.length; i++) {
            let o = blocks[i];
            if (checked.some((v: SingleBlockController) => { return v.blockSkin.spriteFrame == o.blockSkin.spriteFrame })) continue;
            checked.push(o);
            let same = blocks.filter((v: SingleBlockController) => { return v.blockSkin.spriteFrame == o.blockSkin.spriteFrame });
            if (same.length >= BLOCK_ELIMINATE_LIMIT) {
                return same.slice(0, BLOCK_ELIMINATE_LIMIT);
            }
        }
    }

    public findJinBiSame() {
        let blocks = GamePlayController.I.getBlocksAvailable();
        let checked: SingleBlockController[] = [];
        for (let i: number = 0; i < blocks.length; i++) {
            let o = blocks[i];
            if (checked.some((v: SingleBlockController) => { return v.blockSkin.spriteFrame == o.blockSkin.spriteFrame })) continue;
            checked.push(o);
            let same = blocks.filter((v: SingleBlockController) => { return v.blockSkin.spriteFrame == o.blockSkin.spriteFrame });
            if (same.length >= BLOCK_ELIMINATE_LIMIT) {
                return same.slice(0, BLOCK_ELIMINATE_LIMIT);
            }
        }
    }

    public closeSelf(){
        this.node.destroy();
    }

    protected onDestroy(): void {
        UIGuiderPanelTs.I = null;
    }
}
