import { SCENES_NAME } from "../Common/Constant";
import { GameData, GetGameJsonServer, GetHttpLogin, GetLevelInfo, GetWaBaoInfo} from "../Common/GameData";
import LanguagetileTile, { Country } from "../Common/Language";
import PlatformSystem from "../Platform/PlatformSystem";
import ArchiveSystem from "../System/ArchiveSystem";
import AudioTool from "../System/AudioSystem";
import { LOADING_PAGE, reportUserBehaviorBranchAnalytics } from "../System/DataCollectSystem";
import GamingSystem from "../System/GamingSystem";
import GlobalSystem from "../System/GlobalSystem";
import SceneManagerSystem from "../System/SceneManagerSystem";
import SkinSystem from "../System/SkinSystem";
import Archive from "../Common/Archive";
import { JsbTileTrileTileCallMgr } from "../Common/JsbMgr";
import GameTileStaticData from "../GameStaticData";
import { HttpTileCrushServerMgr } from "../HttpMgr";
import UITileToast from "../Common/UIToast";
import JsbUpdateWhiteScript from "../JsbUpdateWhiteScript";
import CCLog from "../CCLog";
import GameTileStaticJsonData from "../GameStaticJsonData";
import LocalKey from "../LocalKey";
import GameStaticData from "../GameStaticData";
import GameStaticJsonData from "../GameStaticJsonData";
const { ccclass, property } = cc._decorator;

@ccclass
export default class LoadingSceneController extends cc.Component {
    @property(cc.Sprite)
    public spriteLoadingBar: cc.Sprite = null;

    @property(cc.Label)
    public labelLoadingTip: cc.Label = null;
    @property(cc.Label)
    public label_des: cc.Label = null;
    @property(cc.Sprite)
    public logo: cc.Sprite = null;

    @property(cc.Node)
    public netNode: cc.Node = null;

    @property(cc.SpriteFrame)
    public IDLogo: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    public BRLogo: cc.SpriteFrame = null;
  
    private _updating: boolean = false;
    private _loaded: boolean = false;

    public onLoad() {
        this.spriteLoadingBar.fillRange = 0;
        if (LanguagetileTile.getCountry == Country.ID){
            this.logo.spriteFrame = this.IDLogo;
        }else if (LanguagetileTile.getCountry == Country.BR){
            this.logo.spriteFrame = this.BRLogo;
        }
        // window.__errorHandler = function (file: any, line: any, error: any) {
        //     console.warn("错误日志抓取=======>", file, line, error)
        // }
    }

    public async start() {
        this.labelLoadingTip.string = LanguagetileTile.getWord("Loading") + "..."
        this.label_des.string = LanguagetileTile.getWord("LoadingDes")
        window["JstSDK"] = JsbUpdateWhiteScript.Instance;
        if (CC_JSB&&cc.sys.os == cc.sys.OS_ANDROID) {
            let isNetConnect = JsbTileTrileTileCallMgr.handler("isNetworkConnect","()Ljava/lang/String;")
            if (isNetConnect == "true") {
                this.netNode.active = false   
            }else{
                this.netNode.active = true
                return
            }
        }
        
        this.getShenHeParrm()
        //await GetHttpLogin()
        await GetGameJsonServer()
        //await GetAccountInfo();
        GameTileStaticJsonData.setMoneyLimit()
        GameData.AccountData.cointotal = GameTileStaticJsonData.getLocal_coinMoney(LocalKey.localCoin)
        GameData.AccountData.moneytotal = GameTileStaticJsonData.getLocal_coinMoney(LocalKey.localMoney)
        //
        await GetLevelInfo();
          //到了第二天 刷新挖宝看视频次数
          if (GameStaticJsonData.isNewday()) {
            GameStaticJsonData.saveWaBaoLocal_Info(LocalKey.wabaoleft_seeVideo_time,GameStaticJsonData.wabaoRule.video)
        }
        await GetWaBaoInfo();
        //测试
        //GameData.LevelData.passNum = 15
       
       
     
        // 初始化全局配置
    
        GlobalSystem.initialize();
      
        // 初始化本地存储
        ArchiveSystem.initialize();
      
        // 获取关卡总数
        // this.labelLoadingTip.string = "Loading level information...";
        await GamingSystem.initialize();
     
        await Archive.loadTaskJson();
     
        // 预加载最新的地图
        GamingSystem.preLoadMap(GameData.LevelData.passNum);
        // 加载使用的皮肤（背景图也在这里加载）
        await SkinSystem.initialize();
        // 初始化平台接口
        PlatformSystem.initialize();
        // 预加载一些Scene
        SceneManagerSystem.initialize();
        // 初始化音效系统
        AudioTool.initialize();

       

        // console.log(this._loadingScene)
        // if (ArchiveSystem.localData.lifeLeft <= 0) this._loadingScene = SCENES_NAME.MainScen;
        this._updating = true;
        SceneManagerSystem.preloadPrefabs();
        reportUserBehaviorBranchAnalytics(LOADING_PAGE.showLoadingSuccess);
    }

    public async update() {
        if (this._loaded) return;
        if (this._updating) {
            if (SceneManagerSystem.loadedProgress[SCENES_NAME.GameScene] > this.spriteLoadingBar.fillRange) {
                this.spriteLoadingBar.fillRange = SceneManagerSystem.loadedProgress[SCENES_NAME.GameScene];
            }
            if (this.spriteLoadingBar.fillRange === 1) {
                this._loaded = true;
                await GamingSystem.loadMap(GameData.LevelData.passNum);
                SceneManagerSystem.sceneChange(SCENES_NAME.GameScene, false);
                GamingSystem.realStartLevel(false);
                reportUserBehaviorBranchAnalytics(LOADING_PAGE.showNextScneSuccess);
            }
        } else {
            if (this.spriteLoadingBar.fillRange < 0.8) this.spriteLoadingBar.fillRange += 0.1;
        }
    }
    public getShenHeParrm(){
        let isshehe = JsbTileTrileTileCallMgr.handler("requestIsWhiteBao","()Ljava/lang/String;")
        if (isshehe =="true") {
            GameTileStaticData.isShenHeModel = true
        }else{
            GameTileStaticData.isShenHeModel = false
        }
        //GameStaticData.isnotLiveuserH5 = JsbSmashTileCallMgr.handler("requestIsFraudUser","()Ljava/lang/String;")
    }
}
