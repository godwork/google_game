import { _decorator, Component, instantiate, Node, Prefab } from 'cc';
import { AssetsHelper } from '../../Script/Common/AssetsHelper';
import { WabaoGameMain } from '../script/WabaoGameMain';
import { PREFABS_WABAO } from '../../Script/Common/Constant';
const { ccclass, property } = _decorator;

@ccclass('loading')
export class loading extends Component {
    async start() {
        await this.load();

        let panel = AssetsHelper.get<Prefab>(PREFABS_WABAO.Main, "wabao_game");
        if (panel) {
            let wabaoGameMain = instantiate(panel).getComponent(WabaoGameMain);
            wabaoGameMain.node.parent = this.node.parent;
        }
    }

    async load() {
        // 加载prefabs
        await AssetsHelper.load(Object.values(PREFABS_WABAO), Prefab, null, "wabao_game");
        AssetsHelper.logCache();
    }
}


