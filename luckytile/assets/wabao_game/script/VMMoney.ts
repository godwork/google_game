import { _decorator, CCInteger, Component, Node, tween, Vec3 } from 'cc';
const { ccclass, property } = _decorator;


@ccclass('VMMoney')
export class VMMoney extends Component {

    @property(Vec3)
    private origin: Vec3 = new Vec3();
    @property(CCInteger)
    private moveDis: number = 0;
    @property(CCInteger)
    private moveSpeed: number = 0;

    start() {
        this.move();
    }

    public move() {
        tween(this.node)
            .repeatForever(
                tween().to(Math.abs(this.moveDis / this.moveSpeed), { position: new Vec3(this.moveDis, this.origin.y, this.origin.z) })
                    .call(() => {
                        this.node.position = new Vec3(this.origin);
                        let children: Node[] = this.node.children;
                        for (let i = children.length - 1; i >= 0; i--) {
                            const child = children[i];
                            if (!child.active) {
                                child.active = true;
                                i + 1 < children.length ? children[i + 1].active = false : "";
                            }
                        }
                    })
            )
            .start();
    }
}


