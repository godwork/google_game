import { _decorator, BlockInputEvents, Component, instantiate, Label, Node, Prefab, Size, UITransform, Vec2, Vec3 } from 'cc';
import { PlayerData } from '../../Script/Model/PlayerData';
import { VMCar } from './VMCar';
import { VMMoney } from './VMMoney';
import { MONEY_TYPE } from './Config';
import { VMGetRewardPanel } from './VMGetRewardPanel';
import { AssetsHelper } from '../../Script/Common/AssetsHelper';
import { DATA_UPDATE, PREFABS_WABAO } from '../../Script/Common/Constant';
import { getEventEmiter } from '../../Script/Common/EventEmitter';
import LanguagetileTile from '../../Script/Common/Language';
import { Utils } from '../../Script/Common/Utils';
import { VMGetMoreTimesPanel } from './VMGetMoreTimesPanel';
import AudioTool from '../../Script/Common/AudioTool';
import { AUDIOS_PATH } from '../../Script/Common/AudioConfig';
const { ccclass, property } = _decorator;


@ccclass('WabaoGameMain')
export class WabaoGameMain extends Component {

    @property(Node)
    private startBtn: Node = null;
    @property(Label)
    private moneyLabel: Label = null;
    @property(Label)
    private gameCount: Label = null;

    @property(VMCar)
    private car: VMCar = null;
    @property(VMMoney)
    private smallMoney: VMMoney = null;
    @property(VMMoney)
    private middleMoney: VMMoney = null;
    @property(VMMoney)
    private largeMoney: VMMoney = null;
    @property(Node)
    private popup: Node = null;

    private gameGetRewardPanel: VMGetRewardPanel = null;
    private gameGetTimesPanel: VMGetMoreTimesPanel = null;

    start() {
        AudioTool.playWaBaoMusic();
        this.startBtn.on(Node.EventType.TOUCH_START, this.onTouch, this);
        this.updateWabaoTimes();
        this.updateMoneyNum();
    }

    protected onEnable(): void {
        getEventEmiter().on(DATA_UPDATE.WABAO_TIMES, this.onUpdateWabaoTimes, this);
        getEventEmiter().on(DATA_UPDATE.MONEY, this.onUpdateMoney, this);

    }

    protected onDisable(): void {
        getEventEmiter().off(DATA_UPDATE.WABAO_TIMES, this.onUpdateWabaoTimes, this);
        getEventEmiter().off(DATA_UPDATE.MONEY, this.onUpdateMoney, this);
    }

    onUpdateWabaoTimes() {
        this.updateWabaoTimes();
    }

    onUpdateMoney() {
        this.updateMoneyNum();
    }

    public updateWabaoTimes() {
        this.gameCount.string = LanguagetileTile.MoneyChar + PlayerData.ins.wabaoData.times.toString();
    }

    public updateMoneyNum() {
        this.moneyLabel.string = LanguagetileTile.MoneyChar + Utils.toDecimal(PlayerData.ins.moneyNum).toString();
    }

    onTouch() {
        console.log("touch");
        if (this.car.isHookRunning()) {
            return;
        }
        if (PlayerData.ins.wabaoData.times <= 0) {
            this.showGetTimesPanel();
            return;
        }
        // 次数减少
        PlayerData.ins.wabaoData.reduceTimes(1);;
        this.car.hookDrop((reward: MONEY_TYPE) => {
            if (reward) {
                this.showGetRewardPanel(reward);
            } else {
                console.log("无奖励");
            }
            this.car.move();
        });
        this.car.moveStop();
        AudioTool.sound(AUDIOS_PATH.putgouzi)
    }

    private async showGetTimesPanel() {
        if (!this.gameGetTimesPanel) {
            let parent = this.getOcclusion();
            let panel: Prefab = AssetsHelper.get<Prefab>(PREFABS_WABAO.GetMoreTimesPanel, "wabao_game");
            if (!panel) {
                await AssetsHelper.load([PREFABS_WABAO.GetMoreTimesPanel], Prefab, null, "wabao_game");
                panel = AssetsHelper.get<Prefab>(PREFABS_WABAO.GetMoreTimesPanel, "wabao_game");
            }
            this.gameGetTimesPanel = instantiate(panel).getComponent(VMGetMoreTimesPanel);
            this.gameGetTimesPanel.node.parent = parent;
        }
        this.gameGetTimesPanel.show();
    }

    public async showGetRewardPanel(reward: MONEY_TYPE) {
        if (!this.gameGetRewardPanel) {
            let parent = this.getOcclusion();
            let panel: Prefab = AssetsHelper.get<Prefab>(PREFABS_WABAO.GetRewardPanel, "wabao_game");
            if (!panel) {
                await AssetsHelper.load([PREFABS_WABAO.GetRewardPanel], Prefab, null, "wabao_game");
                panel = AssetsHelper.get<Prefab>(PREFABS_WABAO.GetRewardPanel, "wabao_game");
            }
            this.gameGetRewardPanel = instantiate(panel).getComponent(VMGetRewardPanel);
            this.gameGetRewardPanel.node.parent = parent;
        }
        this.gameGetRewardPanel.show(reward);
    }

    public getOcclusion() {
        let occlusion = new Node();
        occlusion.position = new Vec3(0, 0, 0);
        occlusion.addComponent(UITransform).contentSize = new Size(750, 1800);
        occlusion.addComponent(BlockInputEvents)
        occlusion.parent = this.popup;
        return occlusion;
    }

    public onCloseClick() {
        AudioTool.stop();
        AudioTool.playMusic();
        this.node.parent.active = false;
        this.node.destroy();
    }
}


