import { _decorator, CCInteger, Collider2D, Component, Contact2DType, Enum, Node, Sprite, Tween, tween, UITransform, Vec3 } from 'cc';
import { MONEY_TYPE } from './Config';
import AudioTool from '../../Script/Common/AudioTool';
import { AUDIOS_PATH } from '../../Script/Common/AudioConfig';
const { ccclass, property } = _decorator;

enum DIRECTION {
    LEFT = 1,
    RIGHT = 2,
}
Enum(DIRECTION);

const HOOK_MOVE_DIS: number = 930;
const HOOK_MOVE_SPEED: number = 306;

const MOVE_GO_BACK = 10;

const LEFT_POS_X = -375;
const RIGHT_POS_X = 375;

@ccclass('VMCar')
export class VMCar extends Component {

    @property(Vec3)
    private origin: Vec3 = new Vec3();
    @property({ type: DIRECTION })
    private direction: DIRECTION = DIRECTION.LEFT;
    @property(CCInteger)
    private moveSpeed: number = 0;

    @property(Node)
    private hook: Node = null;
    @property(Node)
    private person: Node = null;

    private isRuning: boolean = false;
    private hookDisable: boolean = false;
    private hookSp: Sprite = null;

    private hookMoneySmall: Node = null;
    private hookMoneyMiddle: Node = null;
    private hookMoneyLarge: Node = null;

    private hookMoney: MONEY_TYPE = null;
    private hookBackCallback: Function = null;

    protected onLoad(): void {
        this.hook.getComponentInChildren(Collider2D).on(Contact2DType.BEGIN_CONTACT, this.onBeginContact, this);

        this.hookSp = this.hook.getComponentInChildren(Sprite);
        this.hookMoneySmall = this.hook.getChildByName("Small");
        this.hookMoneyMiddle = this.hook.getChildByName("Middle");
        this.hookMoneyLarge = this.hook.getChildByName("Large");
    }

    start() {
        this.move();
    }

    onBeginContact(self: Collider2D, other: Collider2D) {
        if (this.hookDisable) {
            return;
        }
        this.hookMoney = other.tag;
        switch (other.tag) {
            case MONEY_TYPE.SMALL:
                other.node.active = false;
                this.hookMoneySmall.active = true;
                break;
            case MONEY_TYPE.MIDDLE:
                other.node.active = false;
                this.hookMoneyMiddle.active = true;
                break;
            case MONEY_TYPE.LARGE:
                other.node.active = false;
                this.hookMoneyLarge.active = true;
                break;
        }
        AudioTool.sound(AUDIOS_PATH.gouziget)
        this.hookStop();
        this.scheduleOnce(() => {
            this.hookBack(this.hookBackCallback);
        }, 0.8)
    }

    public isHookRunning() {
        return this.isRuning;
    }

    public hookDrop(callback?: Function) {
        if (this.isRuning) {
            return;
        }
        this.hookBackCallback = callback;
        this.isRuning = true;
        this.hookDisable = false;
        let rate = HOOK_MOVE_DIS / this.hookSp.getComponent(UITransform).contentSize.height;
        tween(this.hook)
            .tag(99)
            .to(3, { position: new Vec3(0, -HOOK_MOVE_DIS, 0) }, {
                onUpdate: (target: object, ratio: number) => {
                    this.hookSp.fillRange = 0.091 + rate * ratio;
                }
            })
            .call(() => {
                console.log("钩子到底")
                this.hookBack(callback);
            })
            .start();
    }

    private hookBack(callback?: Function) {
        this.hookDisable = true;
        let dis = Math.abs(this.hook.position.y);
        let rate = dis / this.hookSp.getComponent(UITransform).contentSize.height;
        tween(this.hook)
            .to(dis / HOOK_MOVE_SPEED, { position: new Vec3(0, 0, 0) }, {
                onUpdate: (target: object, ratio: number) => {
                    this.hookSp.fillRange = 0.091 + rate * (1 - ratio);
                }
            })
            .call(() => {
                console.log("钩子回位")
                this.isRuning = false;
                this.hookMoneySmall.active = false;
                this.hookMoneyMiddle.active = false;
                this.hookMoneyLarge.active = false;
                let hookMoney = this.hookMoney;
                this.hookMoney = null;
                this.hookBackCallback = null;
                callback && callback(hookMoney);
            })
            .start();
    }

    private hookStop() {
        this.hookDisable = true;
        Tween.stopAllByTag(99, this.hook);
    }

    public move() {
        this.node.position = new Vec3(this.origin.x, this.origin.y, this.origin.z);
        let dis = 0;
        let endPosX = 0;
        switch (this.direction) {
            case DIRECTION.LEFT:
                endPosX = LEFT_POS_X;
                dis = this.origin.x - endPosX;
                this.person.scale = new Vec3(1, 1, 1);
                break;
            case DIRECTION.RIGHT:
                endPosX = RIGHT_POS_X;
                dis = endPosX - this.origin.x;
                this.person.scale = new Vec3(-1, 1, 1);
                break;
        }


        tween(this.node)
            .tag(MOVE_GO_BACK)
            .to(Math.abs(dis / this.moveSpeed), { position: new Vec3(endPosX, this.origin.y, this.origin.z) })
            .call(() => {
                switch (this.direction) {
                    case DIRECTION.LEFT:
                        this.moveGoback(DIRECTION.RIGHT);
                        break;
                    case DIRECTION.RIGHT:
                        this.moveGoback(DIRECTION.LEFT);
                        break;
                }
            })
            .start();
    }

    private moveGoback(startDirection: DIRECTION) {
        switch (startDirection) {
            case DIRECTION.LEFT:
                this.direction = DIRECTION.LEFT;
                this.person.scale = new Vec3(1, 1, 1);
                this.node.position = new Vec3(RIGHT_POS_X, this.origin.y, this.origin.z);
                let tLeft: Tween<Node> = tween()
                    .to(750 / this.moveSpeed, { position: new Vec3(LEFT_POS_X, this.origin.y, this.origin.z) })
                    .call(() => {
                        this.direction = DIRECTION.RIGHT;
                        this.person.scale = new Vec3(-1, 1, 1);
                        this.node.position = new Vec3(LEFT_POS_X, this.origin.y, this.origin.z);
                    })
                    .to(750 / this.moveSpeed, { position: new Vec3(RIGHT_POS_X, this.origin.y, this.origin.z) })
                    .call(() => {
                        this.direction = DIRECTION.LEFT;
                        this.person.scale = new Vec3(1, 1, 1);
                        this.node.position = new Vec3(RIGHT_POS_X, this.origin.y, this.origin.z);
                    })
                tween(this.node).tag(MOVE_GO_BACK).repeatForever(tLeft).start()
                break;
            case DIRECTION.RIGHT:
                this.direction = DIRECTION.RIGHT;
                this.person.scale = new Vec3(-1, 1, 1);
                this.node.position = new Vec3(LEFT_POS_X, this.origin.y, this.origin.z);
                let tRight: Tween<Node> = tween()
                    .to(750 / this.moveSpeed, { position: new Vec3(RIGHT_POS_X, this.origin.y, this.origin.z) })
                    .call(() => {
                        this.direction = DIRECTION.LEFT;
                        this.person.scale = new Vec3(1, 1, 1);
                        this.node.position = new Vec3(RIGHT_POS_X, this.origin.y, this.origin.z);
                    })
                    .to(750 / this.moveSpeed, { position: new Vec3(LEFT_POS_X, this.origin.y, this.origin.z) })
                    .call(() => {
                        this.direction = DIRECTION.RIGHT;
                        this.person.scale = new Vec3(-1, 1, 1);
                        this.node.position = new Vec3(LEFT_POS_X, this.origin.y, this.origin.z);
                    })
                tween(this.node).tag(MOVE_GO_BACK).repeatForever(tRight).start()
                break;
        }

    }

    public moveStop() {
        Tween.stopAllByTag(MOVE_GO_BACK, this.node);
        this.origin.x = this.node.position.x;
        this.origin.y = this.node.position.y;
        this.origin.z = this.node.position.z;
    }
}


