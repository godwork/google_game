

export enum MONEY_TYPE {
    SMALL = 1,
    MIDDLE = 2,
    LARGE = 3,
}