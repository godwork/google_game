import { _decorator, Component, Label, Node, sp, Sprite, Animation, animation, macro, SpriteFrame } from 'cc';
import { Utils, Wait } from '../../Script/Common/Utils';
import LanguagetileTile from '../../Script/Common/Language';
import { PlayerData } from '../../Script/Model/PlayerData';
import { GameSceneController } from '../../Script/Scenes/GameSceneController';
import { MONEY_TYPE } from './Config';
import { GameInfoGetter } from '../../Script/Info/GameInfoGetter';
import { AUDIOS_PATH } from '../../Script/Common/AudioConfig';
import AudioTool from '../../Script/Common/AudioTool';
const { ccclass, property } = _decorator;

@ccclass('VMGetRewardPanel')
export class VMGetRewardPanel extends Component {

    @property(SpriteFrame)
    private yanhuaFirstFrame: SpriteFrame = null;
    @property(Label)
    private moneyNum: Label = null;
    @property([Node])
    private yanhuaAnim: Node[] = [];
    @property(sp.Skeleton)
    private moneyAnim: sp.Skeleton = null;

    private moneyAniReject: () => void = null;
    private wait: Wait = null;

    private rewardNum: number = 0;

    start() {

    }

    public close() {
        this.node.parent.active = false;
    }

    public show(type: MONEY_TYPE) {
        this.node.parent.active = true;
        AudioTool.sound(AUDIOS_PATH.coinandmoney);
        this.rewardNum = Utils.toDecimal(this.calculateWabaoReward(type), 2);
        console.log(this.rewardNum);
        
        // 奖励数
        this.moneyNum.string = LanguagetileTile.MoneyChar + this.rewardNum;
        this.playYanhuaAnim();
        this.playMoneyAnim();
    }

    public async playYanhuaAnim() {
        try {
            let len = this.yanhuaAnim.length;
            for (let i = 0; i < len; i++) {
                await this.playYanhuaByIndex(i);
            }
            this.wait = new Wait();
            await this.wait.second(2);
            this.playYanhuaAnim();
        } catch (error) {

        }
    }

    public async stopYanhuaAnim() {
        let len = this.yanhuaAnim.length;
        for (let i = 0; i < len; i++) {
            this.yanhuaAnim[i].getComponent(Animation).stop()
            this.yanhuaAnim[i].getComponent(Sprite).spriteFrame = this.yanhuaFirstFrame;
        }
        this.moneyAniReject && this.moneyAniReject();
        this.wait && this.wait.abort();
    }

    public playYanhuaByIndex(index: number): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.moneyAniReject = reject;
            let anim = this.yanhuaAnim[index].getComponent(Animation)
            anim.play();
            anim.on(Animation.EventType.FINISHED, () => {
                resolve();
            });
        })
    }

    public playMoneyAnim() {
        this.moneyAnim.node.active = true;
        this.moneyAnim.setAnimation(0, "lvmeichao", false);
    }

    public async onCloseClick() {
        this.stopYanhuaAnim();
        this.close();
    }

    public async onConfrimClick() {
        this.stopYanhuaAnim();
        this.close();
        // 播放飞钱动画
        // await GameSceneController.ins.showRewardPanel();
        // 加钱
        PlayerData.ins.addMoney(this.rewardNum);
    }

    private calculateWabaoReward(type: MONEY_TYPE): number {
        let result: number = 0;
        switch (type) {
            case MONEY_TYPE.SMALL:
                result = GameInfoGetter.getPassRewardMoney(PlayerData.ins.moneyNum) * GameInfoGetter.getWabaoInfo().lowRate;
                break;
            case MONEY_TYPE.MIDDLE:
                result = GameInfoGetter.getPassRewardMoney(PlayerData.ins.moneyNum) * GameInfoGetter.getWabaoInfo().centreRate;
                break;
            case MONEY_TYPE.LARGE:
                result = GameInfoGetter.getPassRewardMoney(PlayerData.ins.moneyNum) * GameInfoGetter.getWabaoInfo().highRate;
                break;
        }

        // 最少0.01
        if (result < 0.01) {
            result = 0.01;
        }

        return result;
    }
}


