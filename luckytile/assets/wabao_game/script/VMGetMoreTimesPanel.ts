import { _decorator, Component, Label, Node, sp, Sprite, Animation, animation, macro, SpriteFrame, RichText, Button } from 'cc';
import { Utils, Wait } from '../../Script/Common/Utils';
import LanguagetileTile from '../../Script/Common/Language';
import { PlayerData } from '../../Script/Model/PlayerData';
import { MONEY_TYPE } from './Config';
import { GameInfoGetter } from '../../Script/Info/GameInfoGetter';
const { ccclass, property } = _decorator;

@ccclass('VMGetMoreTimesPanel')
export class VMGetMoreTimesPanel extends Component {

    @property(Button)
    private addBtn: Button = null;
    @property(RichText)
    private addTips: RichText = null;

    start() {

    }

    public close() {
        this.node.parent.active = false;
    }

    public show() {
        this.node.parent.active = true;
        let btnSp = this.addBtn.getComponent(Sprite);
        let iconSp = this.addBtn.getComponentInChildren(Sprite);
        let text = this.addBtn.getComponentInChildren(RichText);

        this.addTips.string = `<color=#A5572B>Today we can add:</c><color=#FF0000>${PlayerData.ins.wabaoData.canSupplementTimes}</c>`;
        if (PlayerData.ins.wabaoData.canSupplementTimes <= 0) {
            // 看视频次数不足
            this.addBtn.interactable = false;
            btnSp.grayscale = true;
            iconSp.grayscale = true;
            text.string = `<color=#D1D5E5><outline color=#4B6D6E width=2>${"Add"}</outline></c><color=#D1D5E5><outline color=#4B6D6E width=2> ${GameInfoGetter.getWabaoInfo().videoCount} </outline></c><color=#D1D5E5><outline color=#4B6D6E width=2>${"times"}</outline></c>`;
        } else {
            this.addBtn.interactable = true;
            btnSp.grayscale = false;
            iconSp.grayscale = false;
            text.string = `<color=#FFFFFF><outline color=#B67A00 width=2>${"Add"}</outline></c><color=#FF0000><outline color=#B67A00 width=2> ${GameInfoGetter.getWabaoInfo().videoCount} </outline></c><color=#FFFFFF><outline color=#B67A00 width=2>${"times"}</outline></c>`;
        }
    }

    public async onConfrimClick() {
        this.close();
        PlayerData.ins.wabaoData.reduceCanSupplementTimes(1);
        PlayerData.ins.wabaoData.addTimes(GameInfoGetter.getWabaoInfo().videoCount);
    }

    public onCloseClick() {
        this.close();
    }
}


