import { _decorator, CCFloat, CCInteger, Component, instantiate, Label, math, Node, Prefab, Size, Sprite, Vec3 } from 'cc';
import { GameManager } from '../../Manager/GameManager';
import { PlayerData } from '../../Model/PlayerData';
import { GameSceneController } from '../../Scenes/GameSceneController';
import { ListViewHandler } from '../../Component/ListViewHandler';
import { ListView } from '../../Component/ListView';
import { TaskManager } from '../../Manager/TaskManager';
import { VMTaskItemPanel } from './VMTaskItemPanel';
import { PRELOAD_PREFABS } from '../../Common/Constant';
import { AssetsHelper } from '../../Common/AssetsHelper';
import { VMTaskDetailPanel } from './VMTaskDetailPanel';
import { UIToast } from '../../Component/UIToast';
const { ccclass, property } = _decorator;

const GRID_NUM: number = 3;

@ccclass('VMTaskPanel')
export class VMTaskPanel extends ListViewHandler {

    @property(Prefab)
    private itemTemplate: Prefab = null;
    @property(CCFloat)
    private itemW: number = 0;
    @property(CCFloat)
    private itemH: number = 0;
    @property(ListView)
    private list: ListView = null;
    @property(Sprite)
    private progress: Sprite = null;
    @property(Label)
    private progressText: Label = null;

    private detailPanel: VMTaskDetailPanel = null;

    private cellCount: number = 0;
    private gridCount: number = 0;

    start() {

    }

    public cellSize(): math.Size {
        return new Size(this.itemW, this.itemH);
    }
    public cellNum(): number {
        return this.cellCount;
    }
    public onCell(cellIndex: number, cell: Node): void {
        let children: Node[] = cell.children;
        for (let i = 0; i < GRID_NUM; i++) {
            let index = cellIndex * GRID_NUM + i;
            if (index < this.gridCount) {
                if (children[i]) {
                    // 显示刷新
                    children[i].active = true;
                    children[i].getComponent(VMTaskItemPanel).refresh(index);
                } else {
                    // 添加
                    let node = instantiate(this.itemTemplate);
                    node.name = `Ele_${cellIndex}_${i}`;
                    node.position = new Vec3(-206.6 + 206.6 * i, 0, 0);
                    node.parent = cell;
                    node.getComponent(VMTaskItemPanel).init(this);
                    node.getComponent(VMTaskItemPanel).refresh(index);
                }
            } else {
                if (children[i]) {
                    // 隐藏
                    children[i].active = false;
                } else {
                    // 删除
                }
            }
        }
    }

    public close() {
        this.node.destroy();
    }

    public show() {
        this.node.active = true;
        this.gridCount = TaskManager.ins.getTaskCount();
        this.cellCount = Math.floor(this.gridCount / GRID_NUM);
        this.progress.fillRange = (PlayerData.ins.currTaskId) / TaskManager.ins.getTaskCount();
        this.progressText.string = `${PlayerData.ins.currTaskId}/${TaskManager.ins.getTaskCount()}`
        this.list.refresh();
    }

    public refreshTaskItem(item: Node) {
        item.getChildByName("");
    }

    public async onCloseClick() {
        this.close();
    }

    public getTaskItem(gridIndex: number) {
        let cell: Node = this.list.getCell(Math.floor(gridIndex / GRID_NUM));
        if (cell) {
            let grid = cell.children[gridIndex % GRID_NUM];
            return grid?.getComponent(VMTaskItemPanel);
        }
        return null;
    }

    public async showDetailPanel(index: number) {
        let detailPanelPath = "prefabs/ui/TaskDetailPanel";
        if (!this.detailPanel) {
            let panel = AssetsHelper.get<Prefab>(detailPanelPath);
            if (!panel) {
                await AssetsHelper.load([detailPanelPath], Prefab);
                panel = AssetsHelper.get<Prefab>(detailPanelPath);
            }
            this.detailPanel = instantiate(panel).getComponent(VMTaskDetailPanel);
            this.detailPanel.node.parent = this.node;
        }
        this.detailPanel.show(index);
    }

    onRedeemClick() {
        UIToast.show("The task is not finished");
    }
}


