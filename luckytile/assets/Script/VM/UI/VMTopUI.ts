import { _decorator, Component, easing, Label, macro, Node, Skeleton, sp, Sprite, tween, UITransform, Vec3, Widget } from 'cc';
import { AREA_MOVE_DURATION, DATA_UPDATE } from '../../Common/Constant';
import LanguagetileTile from '../../Common/Language';
import { GameSceneController } from '../../Scenes/GameSceneController';
import { PlayerData } from '../../Model/PlayerData';
import { getEventEmiter } from '../../Common/EventEmitter';
import { GameInfoGetter } from '../../Info/GameInfoGetter';
import { Utils } from '../../Common/Utils';
const { ccclass, property } = _decorator;

@ccclass('VMTopUI')
export class VMTopUI extends Component {

    @property(Node)
    private ui: Node = null;
    @property(Node)
    private money: Node = null;
    @property(Node)
    private coins: Node = null;
    @property(Node)
    private passNum: Node = null;
    @property(Node)
    private passRewards: Node = null;

    private widget: Widget = null;
    private trans: UITransform = null;
    private passNumLabel: Label = null;
    private moneyIcon: Sprite = null;
    private coinIcon: Sprite = null;
    private moneyLabel: Label = null;
    private coinLabel: Label = null;
    private progress: Sprite = null;
    private progressLabel: Label = null;
    private giftBox: Node = null;


    private giftBoxAnim: Function = null;

    protected onLoad(): void {
        this.widget = this.getComponent(Widget);
        this.trans = this.getComponent(UITransform);
        this.passNumLabel = this.passNum.getChildByName("Num").getComponent(Label);
        this.moneyIcon = this.money.getComponentInChildren(Sprite);
        this.moneyLabel = this.money.getComponentInChildren(Label);
        this.coinIcon = this.coins.getComponentInChildren(Sprite);
        this.coinLabel = this.money.getComponentInChildren(Label);
        this.progress = this.passRewards.getChildByPath("Progress_Bg/Progress_Fg").getComponent(Sprite);
        this.progressLabel = this.passRewards.getComponentInChildren(Label);
        this.giftBox = this.passRewards.getChildByName("GiftBox");
    }

    protected start(): void {
        this.ui.position = new Vec3(0, this.trans.height + this.widget.top, 0);
        LanguagetileTile.setOneMoneyIcon(this.moneyIcon.getComponent(Sprite));
        this.updatePassNum();
        this.updateMoneyNum();
        this.updateCompletePassRewards();

        if (GameInfoGetter.easyVersion()) {
            this.money.parent.active = false;
        }
    }

    protected onEnable(): void {
        getEventEmiter().on(DATA_UPDATE.PASS, this.onUpdatePass, this);
        getEventEmiter().on(DATA_UPDATE.MONEY, this.onUpdateMoney, this);
    }

    protected onDisable(): void {
        getEventEmiter().off(DATA_UPDATE.PASS, this.onUpdatePass, this);
        getEventEmiter().off(DATA_UPDATE.MONEY, this.onUpdateMoney, this);
    }

    public begin() {
        if (this.trans) {
            this.ui.position = new Vec3(0, this.trans.height + this.widget.top, 0);
        }
        tween(this.ui).to(AREA_MOVE_DURATION, { position: new Vec3(0, 0, 0) }, { easing: easing.quadIn }).start();
    }

    public async reBegin() {
        await this.end();
        this.begin();
    }

    public async end(): Promise<void> {
        if (this.trans) {
            this.ui.position = new Vec3(0, 0, 0)
        }
        return new Promise<void>((resolve, reject) => {
            tween(this.ui).to(AREA_MOVE_DURATION, { position: new Vec3(0, this.trans.height + this.widget.top, 0) }, { easing: easing.quadOut })
                .call(() => {
                    resolve();
                }).start();
        })
    }

    public getMoneyIcon(): Sprite {
        return this.moneyIcon;
    }

    public getMoneyLabel(): Label {
        return this.moneyLabel;
    }

    public getCoinIcon(): Sprite {
        return this.coinIcon;
    }

    public getCoinLabel(): Label {
        return this.coinLabel;
    }

    public onUpdatePass() {
        this.updatePassNum();
        this.updateCompletePassRewards();
    }

    public onUpdateMoney() {
        this.updateMoneyNum();
    }

    public updatePassNum() {
        this.passNumLabel.string = PlayerData.ins.getPassData().passNum.toString();
    }

    public updateMoneyNum() {
        this.moneyLabel.string = LanguagetileTile.MoneyChar + Utils.toDecimal(PlayerData.ins.moneyNum).toString();
    }

    public updateCompletePassRewards() {
        let completePassNum = PlayerData.ins.getPassData().passNum - 1;
        let info = GameInfoGetter.getCompletePassRewardsInfo();
        this.progress.fillRange = completePassNum / info.sl;
        this.progressLabel.string = completePassNum + "/" + info.sl;
        this.updateGiftBox(completePassNum < info.sl);
    }

    private updateGiftBox(isGray: boolean) {
        let gray: Sprite = this.giftBox.getComponentInChildren(Sprite);
        let normal: sp.Skeleton = this.giftBox.getComponentInChildren(sp.Skeleton);
        if (isGray) {
            gray.node.active = true;
            normal.node.active = false;
        } else {
            gray.node.active = false;
            normal.node.active = true;
            this.unschedule(this.giftBoxAnim);
            this.giftBoxAnim = () => {
                normal.setAnimation(0, "giftbox_jump", false);
            };
            this.schedule(this.giftBoxAnim, 3, macro.REPEAT_FOREVER)
        }
    }

    onSettingClick() {
        GameSceneController.ins.showSettingPanel()
    }
}


