import { _decorator, Component, Label, Node } from 'cc';
import { PlayerData } from '../../Model/PlayerData';
import { GameManager } from '../../Manager/GameManager';
import { GameSceneController } from '../../Scenes/GameSceneController';
import LanguagetileTile from '../../Common/Language';
import { AdManager } from '../../Manager/AdManager';
import { AUDIOS_PATH } from '../../Common/AudioConfig';
import AudioTool from '../../Common/AudioTool';
const { ccclass, property } = _decorator;

@ccclass('VMSucessPanel')
export class VMSucessPanel extends Component {

    @property(Label)
    private title: Label = null;

    @property(Label)
    private moneyNum: Label = null;

    @property(Label)
    private btnText: Label = null;

    start() {

    }

    public close() {
        this.node.active = false;
    }

    public show() {
        this.node.active = true;
        AudioTool.sound(AUDIOS_PATH.level_complete);
        // 奖励数
        this.moneyNum.string = LanguagetileTile.MoneyChar + PlayerData.ins.getPassData().rewardMoney;
    }

    public async onCloseClick() {
        this.close();
        // 切换关卡
        await this.next();
    }

    public async onConfrimClick() {
        AdManager.ins.onResult(async (flag: boolean) => {
            this.close();
            if (flag) {
                // 播放飞钱动画后开始下一关
                await GameSceneController.ins.showRewardPanel();
                // 加钱
                PlayerData.ins.addMoney(PlayerData.ins.getPassData().rewardMoney);
                await this.next();
            }
        });
        AdManager.ins.open();
    }

    public async next() {
        // 切换关卡
        PlayerData.ins.nextPass();
        await GameManager.ins.loadMap(PlayerData.ins.getPassData().passNum);
        GameSceneController.ins.gameRestart();
    }
}


