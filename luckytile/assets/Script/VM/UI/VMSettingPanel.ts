import { _decorator, Component, Node, Toggle } from 'cc';
import { GameSceneController } from '../../Scenes/GameSceneController';
const { ccclass, property } = _decorator;

@ccclass('VMSettingPanel')
export class VMSettingPanel extends Component {

    start() {

    }

    public close() {
        this.node.active = false;
    }

    public show() {
        this.node.active = true;
    }

    onRestartClick() {
        this.close();
        GameSceneController.ins.gameRestart();
    }

    onHelpClick() {

    }

    onPrivacyClick() {

    }

    onMusicCheck(target: Toggle) {
        if (target.isChecked) {
            console.log("音乐关闭");
        } else {
            console.log("音乐开启");
        }
    }

    onCloseClick() {
        this.close();
    }
}


