import { _decorator, Component, instantiate, Label, Node, Prefab, Sprite } from 'cc';
import { AssetsHelper } from '../../Common/AssetsHelper';
import { CUSTOM_EVENT_NAME, PRELOAD_PREFABS } from '../../Common/Constant';
import { PropAnim } from '../../Anim/PropAnim';
import { GameSceneController } from '../../Scenes/GameSceneController';
import { PlayerData } from '../../Model/PlayerData';
import { getEventEmiter } from '../../Common/EventEmitter';
import { AdManager } from '../../Manager/AdManager';
const { ccclass, property } = _decorator;

export enum PROP_TYEP {
    REFRESH = 1,
    HINT = 2,
    BACK = 3,
}

@ccclass('VMPropGetPanel')
export class VMPropGetPanel extends Component {

    @property(Sprite)
    private refreshIcon: Sprite = null;
    @property(Sprite)
    private hintIcon: Sprite = null;
    @property(Sprite)
    private backIcon: Sprite = null;
    @property(Label)
    private num: Label = null;

    private type: PROP_TYEP = null;

    private propGetAnimResolve: () => void = null;

    protected onLoad(): void {
        this.refreshIcon.node.active = false;
        this.hintIcon.node.active = false;
        this.backIcon.node.active = false;
    }

    start() {

    }

    public close() {
        this.node.active = false;
    }

    public show(type: PROP_TYEP) {
        this.type = type;
        this.node.active = true;
        this.refreshIcon.node.active = false;
        this.hintIcon.node.active = false;
        this.backIcon.node.active = false;

        switch (type) {
            case PROP_TYEP.REFRESH:
                this.refreshIcon.node.active = true;
                break;
            case PROP_TYEP.HINT:
                this.hintIcon.node.active = true;
                break;
            case PROP_TYEP.BACK:
                this.backIcon.node.active = true;
                break;
        }
        this.num.string = "x3";
    }

    async onGetClick() {
        AdManager.ins.onResult(async (flag: boolean) => {
            if (flag) {
                this.close();
                await GameSceneController.ins.createPropGetAnim(this.type);
                PlayerData.ins.supplementProp(this.type);
                this.propGetAnimResolve && this.propGetAnimResolve();
            }
        });
        AdManager.ins.open();
    }

    onCloseClick() {
        this.close();

    }

    promiseGetAnim(): Promise<void> {
        return new Promise((resolve: () => void, reject: () => void) => {
            this.propGetAnimResolve = resolve;
        });
    }
}


