import { _decorator, Asset, Component, Node, Sprite, SpriteFrame, Tween, tween, UITransform, Vec3 } from 'cc';
import { GameConfig, RewardType } from '../../GameConfig';
import { GameSceneController } from '../../Scenes/GameSceneController';
import LanguagetileTile from '../../Common/Language';
import { AssetsHelper } from '../../Common/AssetsHelper';
import { PRELOAD_SP } from '../../Common/Constant';
const { ccclass, property } = _decorator;

@ccclass('VMRewardsPanel')
export class VMRewardsPanel extends Component {

    @property([Sprite])
    private moneyIcons: Sprite[] = [];

    private uitrans: UITransform = null;
    private targetPos: Vec3 = null;
    private scale: Vec3 = null;

    private resolve: () => void = null;

    protected onLoad(): void {
        this.uitrans = this.getComponent(UITransform);
        if (GameConfig.rewardType == RewardType.MONEY) {
            let targetNode: Node = GameSceneController.ins.topUI.getMoneyIcon().node;
            this.targetPos = this.uitrans.convertToNodeSpaceAR(targetNode.getWorldPosition());
            this.scale = new Vec3(1.5, 1.5, 1) //跟TopAreaTs的图标的大小一样
        } else {
            let targetNode: Node = GameSceneController.ins.topUI.getCoinIcon().node;
            this.targetPos = this.uitrans.convertToNodeSpaceAR(targetNode.getWorldPosition());
            this.scale = new Vec3(1.5, 1.5, 1)//跟TopAreaTs的图标的大小一样
        }
        for (let i = 0; i < this.moneyIcons.length; i++) {
            let sp = this.moneyIcons[i];
            if (GameConfig.rewardType == RewardType.MONEY) {
                LanguagetileTile.setOneMoneyIcon(sp)
            } else {
                sp.spriteFrame = AssetsHelper.get<SpriteFrame>(PRELOAD_SP.coinsIcon);
            }
        }
    }

    start() {
        this.startRewardAnim();
    }

    startRewardAnim() {
        let animArray = [
            {
                index: 1,
                pos: new Vec3(-100, -2),
                delay: 0.25,
                duration: 0.20
            },
            {
                index: 2,
                pos: new Vec3(75, -1),
                delay: 0.22,
                duration: 0.20
            },
            {
                index: 3,
                pos: new Vec3(-130, 62),
                delay: 0.24,
                duration: 0.20
            },
            {
                index: 4,
                pos: new Vec3(-170, -1),
                delay: 0.23,
                duration: 0.22
            },
            {
                index: 5,
                pos: new Vec3(-39, 118),
                delay: 0.21,
                duration: 0.22
            },
            {
                index: 6,
                pos: new Vec3(138, -1),
                delay: 0.23,
                duration: 0.22
            },
            {
                index: 7,
                pos: new Vec3(-56, -2),
                delay: 0.22,
                duration: 0.22
            },
            {
                index: 8,
                pos: new Vec3(20, -3),
                delay: 0.20,
                duration: 0.22
            },
            {
                index: 9,
                pos: new Vec3(100, -1),
                delay: 0.21,
                duration: 0.22
            },
            {
                index: 10,
                pos: new Vec3(105, 36),
                delay: 0.24,
                duration: 0.20
            },
            {
                index: 11,
                pos: new Vec3(198, 87),
                delay: 0.24,
                duration: 0.21
            },
            {
                index: 12,
                pos: new Vec3(60, -1),
                delay: 0.25,
                duration: 0.20
            },
            {
                index: 13,
                pos: new Vec3(-119, -2),
                delay: 0.215,
                duration: 0.225
            },
            {
                index: 14,
                pos: new Vec3(-27, -1),
                delay: 0.225,
                duration: 0.225
            },
            {
                index: 15,
                pos: new Vec3(-150, -4),
                delay: 0.235,
                duration: 0.225
            }
        ]
        let num = this.moneyIcons.length;
        for (let i = 0; i < this.moneyIcons.length; i++) {
            let t: Tween<Node> = this.getIconAnim(i, animArray[i]);
            t.target(this.moneyIcons[i].node)
                .call(() => {
                    num--;
                    if (num == 0) {
                        this.resolve && this.resolve();
                        this.resolve = null;
                        this.node.destroy();
                    }
                })
                .start();
        }
    }

    getIconAnim(idx: number, animInfo: any): Tween<Node> {
        let t: Tween<Node> = tween()
            .delay(idx * 0.02)
            .parallel(
                tween().to(0.17 + (idx % 2) * (idx * 0.02), { position: Vec3.multiplyScalar(new Vec3(), animInfo.pos, 1.03) }),
                tween().to(0.17 + ((idx + 1) % 2) * (idx * 0.02), { scale: Vec3.add(new Vec3(), this.scale, new Vec3(0.2, 0.2)) })
            )
            .parallel(
                tween().to(0.12, { position: new Vec3(animInfo.pos) }),
                tween().to(0.14, { scale: Vec3.add(new Vec3(), this.scale, new Vec3(0.1, 0.1)) })
            )
            .delay(animInfo.delay)
            .to(animInfo.duration, { position: new Vec3(this.targetPos.x, this.targetPos.y, 0), scale: this.scale })
            .to(0.07, { scale: this.scale })
            .removeSelf();
        return t;
    }

    public setResolveCallback(resolve: () => void) {
        this.resolve = resolve;
    }

    public async promise(): Promise<void> {
        return new Promise<void>((resolve: () => void, reject: () => void) => {
            this.resolve = resolve;
        });
    }
}


