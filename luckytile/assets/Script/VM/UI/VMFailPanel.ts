import { _decorator, Component, Node } from 'cc';
import { GameManager } from '../../Manager/GameManager';
import { PlayerData } from '../../Model/PlayerData';
import { GameSceneController } from '../../Scenes/GameSceneController';
import { AUDIOS_PATH } from '../../Common/AudioConfig';
import AudioTool from '../../Common/AudioTool';
const { ccclass, property } = _decorator;

@ccclass('VMFailPanel')
export class VMFailPanel extends Component {
    start() {

    }

    public close() {
        this.node.active = false;
    }

    public show() {
        this.node.active = true;
        AudioTool.sound(AUDIOS_PATH.level_failed);
    }

    public async onCloseClick() {
        this.close();
        GameSceneController.ins.gameRestart();
    }

    public async onContinueClick() {
        this.close();
        // 退回并继续
        GameSceneController.ins.onGameRecall();
    }

}


