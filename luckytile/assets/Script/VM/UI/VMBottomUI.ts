import { _decorator, Button, Component, easing, Label, Node, Sprite, tween, UITransform, Vec3, Widget } from 'cc';
import { AREA_MOVE_DURATION, CUSTOM_EVENT_NAME, DATA_UPDATE } from '../../Common/Constant';
import { PlayerData } from '../../Model/PlayerData';
import { getEventEmiter } from '../../Common/EventEmitter';
import { GameInfoGetter } from '../../Info/GameInfoGetter';
import { TaskManager } from '../../Manager/TaskManager';
import { GameSceneController } from '../../Scenes/GameSceneController';
import { UIToast } from '../../Component/UIToast';
const { ccclass, property } = _decorator;

@ccclass('VMBottomUI')
export class VMBottomUI extends Component {

    @property(Node)
    private ui: Node = null;
    @property(Node)
    public refreshBtn: Node = null;
    @property(Node)
    public hintBtn: Node = null;
    @property(Node)
    public module1: Node = null;
    @property(Node)
    public module2: Node = null;

    private widget: Widget = null;
    private trans: UITransform = null;

    private refreshPropNum: Label = null;
    private refreshPropAdd: Node = null;
    private hintPropNum: Label = null;
    private hintPropAdd: Node = null;
    private wabaoNum: Label = null;
    private wabaoAdd: Node = null;

    protected onLoad(): void {
        this.widget = this.getComponent(Widget);
        this.trans = this.getComponent(UITransform);
    }

    protected start(): void {
        this.ui.position = new Vec3(0, -(this.trans.height + this.widget.bottom), 0);
        this.refreshPropNum = this.refreshBtn.getChildByName("Num").getComponentInChildren(Label);
        this.refreshPropAdd = this.refreshBtn.getChildByName("AddIcon");
        this.hintPropNum = this.hintBtn.getChildByName("Num").getComponentInChildren(Label);
        this.hintPropAdd = this.hintBtn.getChildByName("AddIcon");
        this.wabaoNum = this.module2.getChildByName("Num").getComponentInChildren(Label);
        this.wabaoAdd = this.module2.getChildByName("AddIcon");
        if (GameInfoGetter.easyVersion()) {
            this.module1.active = false;
            this.module2.active = false;
        }

        this.updateWabaoLockState();
        this.updatePropNum();
        this.updateWabaoNum();
    }

    protected onEnable(): void {
        getEventEmiter().on(DATA_UPDATE.PASS, this.onUnlockWabao, this);
        getEventEmiter().on(DATA_UPDATE.PROP_NUM, this.onUpdatePropNum, this);
        getEventEmiter().on(DATA_UPDATE.WABAO_TIMES, this.onUpdateWabaoNum, this);
    }

    protected onDisable(): void {
        getEventEmiter().off(DATA_UPDATE.PASS, this.onUnlockWabao, this);
        getEventEmiter().off(DATA_UPDATE.PROP_NUM, this.onUpdatePropNum, this);
        getEventEmiter().off(DATA_UPDATE.WABAO_TIMES, this.onUpdateWabaoNum, this);
    }

    public begin() {
        if (this.trans) {
            this.ui.position = new Vec3(0, -(this.trans.height + this.widget.bottom), 0);
        }
        tween(this.ui).to(AREA_MOVE_DURATION, { position: new Vec3(0, 0, 0) }, { easing: easing.quadIn }).start();
    }

    public async reBegin() {
        await this.end();
        this.begin();
    }

    public async end(): Promise<void> {
        if (this.trans) {
            this.ui.position = new Vec3(0, 0, 0)
        }
        return new Promise<void>((resolve, reject) => {
            tween(this.ui).to(AREA_MOVE_DURATION, { position: new Vec3(0, -(this.trans.height + this.widget.bottom), 0) }, { easing: easing.quadOut })
                .call(() => {
                    resolve();
                }).start();
        })
    }

    public onUpdatePropNum() {
        this.updatePropNum();
    }

    public onUpdateWabaoNum() {
        this.updateWabaoNum();
    }

    public onUnlockWabao() {
        this.updateWabaoLockState();
    }

    private updatePropNum() {
        let refreshPropNum = PlayerData.ins.refreshPropNum;
        let hintPropNum = PlayerData.ins.hintPropNum;
        this.refreshPropAdd.active = !refreshPropNum;
        this.hintPropAdd.active = !hintPropNum;

        this.refreshPropNum.string = PlayerData.ins.refreshPropNum.toString();
        this.hintPropNum.string = PlayerData.ins.hintPropNum.toString();
    }

    private updateWabaoNum() {
        let wabaoNum = PlayerData.ins.wabaoData.times;
        this.wabaoAdd.active = !wabaoNum;
        this.wabaoNum.string = PlayerData.ins.wabaoData.times.toString();
    }

    private updateWabaoLockState() {
        if (PlayerData.ins.isWabaoUnLock()) {
            this.module2.getChildByName("IconAnim").active = true;
            this.module2.getChildByName("Icon").active = false;
        } else {
            this.module2.getChildByName("IconAnim").active = false;
            this.module2.getChildByName("Icon").active = true;
        }
    }

    onTaskClick() {
        // 加载任务页面
        GameSceneController.ins.showTaskPanel();
    }

    onWabaoGameClick() {
        if (PlayerData.ins.isWabaoUnLock()) {
            GameSceneController.ins.showWabaoGamePanel()
        } else {
            UIToast.show(`Complete the level ${GameInfoGetter.getWabaoInfo().startLv} to unlock`);
        }
    }
}


