import { _decorator, Component, Label, Node, sp, Sprite, Animation, animation, macro, SpriteFrame } from 'cc';
import { PlayerData } from '../../Model/PlayerData';
import { GameManager } from '../../Manager/GameManager';
import { GameSceneController } from '../../Scenes/GameSceneController';
import LanguagetileTile from '../../Common/Language';
import { Utils, Wait } from '../../Common/Utils';
import { AssetsHelper } from '../../Common/AssetsHelper';
import { PRELOAD_SP } from '../../Common/Constant';
const { ccclass, property } = _decorator;

@ccclass('VMGetMoneyPanel')
export class VMGetMoneyPanel extends Component {

    @property(SpriteFrame)
    private yanhuaFirstFrame: SpriteFrame = null;
    @property(Label)
    private moneyNum: Label = null;
    @property([Node])
    private yanhuaAnim: Node[] = [];
    @property(sp.Skeleton)
    private moneyAnim: sp.Skeleton = null;

    private moneyAniReject: () => void = null;
    private wait: Wait = null;

    start() {

    }

    public close() {
        this.node.active = false;
    }

    public show() {
        this.node.active = true;
        // 奖励数
        this.moneyNum.string = LanguagetileTile.MoneyChar + PlayerData.ins.getPassData().rewardMoney;
        this.playYanhuaAnim();
        this.playMoneyAnim();
    }

    public async playYanhuaAnim() {
        try {
            let len = this.yanhuaAnim.length;
            for (let i = 0; i < len; i++) {
                await this.playYanhuaByIndex(i);
            }
            this.wait = new Wait();
            await this.wait.second(2);
            this.playYanhuaAnim();
        } catch (error) {

        }
    }

    public async stopYanhuaAnim() {
        let len = this.yanhuaAnim.length;
        for (let i = 0; i < len; i++) {
            this.yanhuaAnim[i].getComponent(Animation).stop()
            this.yanhuaAnim[i].getComponent(Sprite).spriteFrame = this.yanhuaFirstFrame;
        }
        this.moneyAniReject && this.moneyAniReject();
        this.wait && this.wait.abort();
    }

    public playYanhuaByIndex(index: number): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.moneyAniReject = reject;
            let anim = this.yanhuaAnim[index].getComponent(Animation)
            anim.play();
            anim.on(Animation.EventType.FINISHED, () => {
                resolve();
            });
        })
    }

    public playMoneyAnim() {
        this.moneyAnim.node.active = true;
        this.moneyAnim.setAnimation(0, "lvmeichao", false);
    }

    public async onCloseClick() {
        this.stopYanhuaAnim();
        this.close();
    }

    public async onConfrimClick() {
        this.stopYanhuaAnim();
        this.close();
        // 播放飞钱动画
        await GameSceneController.ins.showRewardPanel();
        // 加钱
        PlayerData.ins.addMoney(PlayerData.ins.getPassData().rewardMoney);
    }
}


