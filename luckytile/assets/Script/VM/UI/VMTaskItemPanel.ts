import { _decorator, Component, Label, Node, Sprite, SpriteFrame } from 'cc';
import { TaskManager } from '../../Manager/TaskManager';
import { PlayerData } from '../../Model/PlayerData';
import { VMTaskPanel } from './VMTaskPanel';
import { AssetsHelper } from '../../Common/AssetsHelper';
import { CUSTOM_EVENT_NAME, GAMEBG_ICON_PATH_PRE, PRELOAD_SP } from '../../Common/Constant';
import { getEventEmiter } from '../../Common/EventEmitter';
const { ccclass, property } = _decorator;

@ccclass('VMTaskItemPanel')
export class VMTaskItemPanel extends Component {

    @property(Node)
    private progress: Node = null;
    @property(Node)
    private lock: Node = null;
    @property(Node)
    private checkbox: Node = null;
    @property(Sprite)
    private bgIcon: Sprite = null;

    private taskPanel: VMTaskPanel = null;

    private currIndex: number = 0;


    protected onLoad(): void {

    }

    start() {

    }

    public get defaultIcon(): SpriteFrame {
        return AssetsHelper.get<SpriteFrame>(PRELOAD_SP.defaultTaskIcon);
    }

    public init(panel: VMTaskPanel) {
        this.taskPanel = panel;
    }

    public async refresh(index: number) {
        this.currIndex = index;

        let currTaskId = PlayerData.ins.currTaskId;
        let info = TaskManager.ins.getTaskByIndex(index);
        if (this.currIndex < currTaskId) {
            // 已完成任务
            this.progress.active = false;
            this.lock.active = false;
            this.checkbox.active = true;
            let currSelected = PlayerData.ins.currSelectedBg;
            if (this.currIndex == currSelected) {
                this.checkbox.getComponentInChildren(Sprite).node.active = true;
            } else {
                this.checkbox.getComponentInChildren(Sprite).node.active = false;
            }
        } else if (index == currTaskId) {
            // 进行中任务
            this.progress.active = true;
            this.lock.active = false;
            this.checkbox.active = false;
            let taskPercent = PlayerData.ins.currTaskCompleteCount / info.count;
            this.progress.getComponentInChildren(Sprite).fillRange = taskPercent;
            this.progress.getComponentInChildren(Label).string = `${PlayerData.ins.currTaskCompleteCount}/${info.count}`;
        } else if (index > currTaskId) {
            // 未解锁任务
            this.progress.active = false;
            this.lock.active = true;
            this.checkbox.active = false;
        }

        // 刷新Icon
        this.bgIcon.spriteFrame = this.defaultIcon;
        if (this.currIndex <= currTaskId) {
            let path = `${GAMEBG_ICON_PATH_PRE}/bg_icon${index}/spriteFrame`;
            let icon = AssetsHelper.get<SpriteFrame>(path);
            if (!icon) {
                await AssetsHelper.load([path], SpriteFrame);
                icon = AssetsHelper.get<SpriteFrame>(path);
            }
            if (this.bgIcon && index == this.currIndex) {
                this.bgIcon.spriteFrame = icon;
            }
        }
    }

    public onItemClick() {
        let currTaskId = PlayerData.ins.currTaskId;
        if (this.currIndex < currTaskId) {
            // 已完成任务 选择背景
            let lastSelected = PlayerData.ins.currSelectedBg;
            PlayerData.ins.currSelectedBg = this.currIndex;
            // 刷新最后选中
            let selected: VMTaskItemPanel = this.taskPanel.getTaskItem(lastSelected);
            selected?.refresh(lastSelected);
            // 刷新当前选中
            this.refresh(this.currIndex);
            // 切换背景
            getEventEmiter().emit(CUSTOM_EVENT_NAME.GAME_CHANGE_BG, this.currIndex);
        } else if (this.currIndex == currTaskId) {
            // 进行中任务 弹窗任务详情
            this.taskPanel.showDetailPanel(currTaskId);
        } else if (this.currIndex > currTaskId) {
            // 未解锁任务 不做任何处理
        }
    }
}


