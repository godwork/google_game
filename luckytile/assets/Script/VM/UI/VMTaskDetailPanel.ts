import { _decorator, Component, Label, Node, Sprite, SpriteFrame } from 'cc';
import { PlayerData } from '../../Model/PlayerData';
import { TaskManager } from '../../Manager/TaskManager';
import { AssetsHelper } from '../../Common/AssetsHelper';
import { GAMEBG_ICON_PATH_PRE } from '../../Common/Constant';
const { ccclass, property } = _decorator;

@ccclass('VMTaskDetailPanel')
export class VMTaskDetailPanel extends Component {

    @property(Sprite)
    private percent: Sprite = null;
    @property(Label)
    private percentText: Label = null;
    @property(Sprite)
    private gameBg: Sprite = null;
    @property(Label)
    private tips: Label = null;

    start() {

    }

    public close() {
        // this.node.destroy();
        this.node.active = false;
    }

    public show(index: number) {
        this.node.active = true;

        let info = TaskManager.ins.getTaskByIndex(index);

        let taskPercent = PlayerData.ins.currTaskCompleteCount / info.count;
        this.percent.fillRange = taskPercent;
        this.percentText.string = `${PlayerData.ins.currTaskCompleteCount}/${info.count}`;
        switch (info.type) {
            case 1:
                this.tips.string = `Complete ${PlayerData.ins.currTaskCompleteCount} more levels`
                break;
            case 2:
                this.tips.string = `Watch ${PlayerData.ins.currTaskCompleteCount} videos`
                break;
        }

        let path = `${GAMEBG_ICON_PATH_PRE}/bg_icon${index}/spriteFrame`;
        let icon = AssetsHelper.get<SpriteFrame>(path);
        this.gameBg.spriteFrame = icon;
    }

    public async onCloseClick() {
        this.close();
    }
}


