import { _decorator, cclegacy, Color, Component, easing, EventTouch, Input, input, math, Node, Size, Sprite, SpriteFrame, Tween, tween, UITransform, Vec2, Vec3 } from 'cc';
import { BLOCK_COLLECT_SPEED, GRID_ACTION, GRID_HEIGHT, GRID_STATE, GRID_WIDTH, PRELOAD_SP } from '../../Common/Constant';
import CCLog from '../../CCLog';
import { GameManager } from '../../Manager/GameManager';
import { VMMapLayer } from './VMMapLayer';
import { Utils } from '../../Common/Utils';
import { AssetsHelper } from '../../Common/AssetsHelper';
import AudioTool from '../../Common/AudioTool';
import { AUDIOS_PATH } from '../../Common/AudioConfig';
const { ccclass, property } = _decorator;

@ccclass('VMGrid')
export default class VMGrid extends Component {

    @property(Sprite)
    public skin: Sprite = null;
    @property(UITransform)
    public skinTrans: UITransform = null;

    // 方格皮肤的原始大小
    private originalSkinSize: Size = null;
    // 方格的状态 0 正常状态 1 被覆盖状态
    private state: GRID_STATE = GRID_STATE.NORMAL;
    private isMoveing: boolean = false;
    // 所在层ID
    private _layerId: number = 0;
    public get layerId(): number {
        return this._layerId;
    }
    private set layerId(value: number) {
        this._layerId = value;
    }
    private _gridX: number = 0;
    public get gridX(): number {
        return this._gridX;
    }
    private set gridX(value: number) {
        this._gridX = value;
    }
    private _gridY: number = 0;
    public get gridY(): number {
        return this._gridY;
    }
    private set gridY(value: number) {
        this._gridY = value;
    }
    private siblingIdx: number = 0;

    // 移动效果
    private moveAnim: Tween<Node> = null;
    private collectedCallback: Function = null;
    private endPoint: Vec3 = null;

    onLoad() {
        this.node.on(Node.EventType.TOUCH_START, this.onTouch, this);
        this.node.on(Node.EventType.TOUCH_END, this.onTouch, this);
        this.node.on(Node.EventType.TOUCH_CANCEL, this.onTouch, this);
    }

    onDestroy() {
        this.node.off(Node.EventType.TOUCH_START, this.onTouch, this);
        this.node.off(Node.EventType.TOUCH_END, this.onTouch, this);
        this.node.off(Node.EventType.TOUCH_CANCEL, this.onTouch, this);
    }

    public init(layerId: number, gridX: number, gridY: number): VMGrid {
        this.layerId = layerId;
        this.gridX = gridX;
        this.gridY = gridY;
        this.node.name = `${layerId}_${gridX}_${gridY}`;
        this.originalSkinSize = this.skinTrans.contentSize;
        this.getComponent(UITransform).contentSize = new Size(GameManager.ins.gridW, GameManager.ins.gridH);
        this.skinTrans.contentSize = new Size(this.originalSkinSize.width * GameManager.ins.gridScale, this.originalSkinSize.height * GameManager.ins.gridScale);
        this.skin.node.position = new Vec3(0, (this.getComponent(UITransform).contentSize.height - this.skinTrans.contentSize.height) / 2, 0);
        return this;
    }

    public setSkin(skin: SpriteFrame) {
        this.skin.spriteFrame = skin;
    }

    public getSkin() {
        return this.skin.spriteFrame;
    }

    public refreshState() {
        if (this.hasCoverInUpperLayer()) {
            this.changeState(GRID_STATE.COVER);
        } else {
            this.changeState(GRID_STATE.NORMAL);
        }
    }

    public changeState(state: GRID_STATE) {
        if (this.state == state) {
            return;
        }
        switch (state) {
            case GRID_STATE.NORMAL:
                this.state2Normal();
                break;
            case GRID_STATE.COVER:
                this.state2Cover();
                break;
            case GRID_STATE.COLLECT:
                this.state2Collect();
                break;
            case GRID_STATE.ELIMINATE:
                this.state2Eliminate();
                break;
        }
    }

    private state2Normal() {
        this.state = GRID_STATE.NORMAL;
        this.skin.color = new Color(255, 255, 255, 255);
    }

    private state2Cover() {
        this.state = GRID_STATE.COVER;
        this.skin.color = new Color(100, 100, 100, 255);
    }

    private state2Collect() {
        this.state = GRID_STATE.COLLECT;
        this.skin.color = new Color(255, 255, 255, 255);
    }

    private state2Eliminate() {
        this.state = GRID_STATE.ELIMINATE;
        this.skin.color = new Color(255, 255, 255, 255);
    }

    private state2Back() {
        this.state = GRID_STATE.BACK;
        this.skin.color = new Color(255, 255, 255, 255);
    }

    public collectingMove(position: Vec3): Tween<Node> {
        this.isMoveing = true;
        Tween.stopAllByTag(GRID_ACTION.TAG_MOVE, this.node);
        this.moveAnim = tween(this.node).tag(GRID_ACTION.TAG_MOVE)
        this.endPoint = new Vec3(position);
        let duration = Vec3.subtract(new Vec3(), position, this.node.position).length() / BLOCK_COLLECT_SPEED;
        // if (delay) {
        //     this.moveAnim.delay(delay);
        // }
        this.moveAnim.to(duration, { position: position }, { easing: easing.quadOut }).call(() => {
            this.node.scale = Vec3.ONE;
            this.collectedCallback && this.collectedCallback();
            this.collectedCallback = null;
            this.moveAnim = null;
            this.endPoint = null;
            this.isMoveing = false;
        });
        return this.moveAnim;
    }

    public backMove(position: Vec3, callback?: (grid: VMGrid) => void): Tween<Node> {
        this.isMoveing = true;
        let duration = Vec3.subtract(new Vec3(), this.node.position, position).length() / BLOCK_COLLECT_SPEED;
        return tween(this.node).to(duration, { position: position }, { easing: easing.quadOut }).call(() => {
            this.isMoveing = false;
            callback && callback(this);
        })
    }

    public setCollectedCallback(callback: Function) {
        this.collectedCallback = callback;
    }

    public hasMoveAnim() {
        return !!this.moveAnim;
    }

    public getDis2EndPoint(): number {
        return this.endPoint ? Vec3.subtract(new Vec3(), this.endPoint, this.node.position).length() : 0;
    }

    public isEliminated() {
        return this.state == GRID_STATE.ELIMINATE;
    }

    public hasCoverInUpperLayer(): boolean {
        for (let i = this.layerId + 1; i < GameManager.ins.map.length; i++) {
            const layer: VMMapLayer = GameManager.ins.map[i];
            if (layer.hasCoverInLayer(this)) {
                return true;
            }
        }
        return false;
    }

    public getAroundGridInNextLayer(): VMGrid[] {
        let layer: VMMapLayer = GameManager.ins.map[this.layerId - 1];
        return layer.getAroundGridInLayer(this);
    }

    public collectReady() {
        this.siblingIdx = this.node.getSiblingIndex();
        CCLog.log("this.siblingIdx==", this.siblingIdx);
        this.node.scale = new Vec3(1.2, 1.2, 1);
        Utils.changeParent(this.node, GameManager.ins.collector.node);
    }

    public collectCancel() {
        this.node.scale = Vec3.ONE;
        Utils.changeParent(this.node, GameManager.ins.map[this.layerId].node);
        this.node.setSiblingIndex(this.siblingIdx);
    }

    public collecting() {
        // this.node.scale = Vec3.ONE;
        // 从队列中删除
        GameManager.ins.removeFromMap(this);
        // 刷新方格
        if (this.layerId > 0) {
            let grids: VMGrid[] = this.getAroundGridInNextLayer();
            for (let i = 0; i < grids.length; i++) {
                const grid = grids[i];
                grid.refreshState();
            }
        }
        GameManager.ins.collector.add(this);
    }

    onTouch(event: EventTouch) {
        if (this.state != GRID_STATE.NORMAL || this.isMoveing) {
            return;
        }
        switch (event.type) {
            case Node.EventType.TOUCH_START:
                this.onTouchStart(event);
                break;
            case Node.EventType.TOUCH_MOVE:
                break;
            case Node.EventType.TOUCH_END:
                this.onTouchEnd(event);
                break;
            case Node.EventType.TOUCH_CANCEL:
                this.onTouchCancel(event);
                break;
        }
    }

    onTouchStart(event: EventTouch) {
        this.collectReady();
        AudioTool.sound(AUDIOS_PATH.block_click);
    }

    onTouchEnd(event: EventTouch) {
        if (GameManager.ins.collector.isFull()) {
            this.collectCancel();
            return;
        }
        this.collecting();
    }

    onTouchCancel(event: EventTouch) {
        this.collectCancel();
    }

    delete() {
        tween(this.node)
            .delay(0.1)
            .to(0.2, { scale: new Vec3(0, 0, 1) }, { easing: easing.backIn })
            .call(() => {
                CCLog.log("delete==", this.node);
                this.node.destroy();
            })
            .start();
    }

    backInMap() {
        GameManager.ins.collector.removeByGrid(this);
        let mapLayer: VMMapLayer = GameManager.ins.addInMap(this);
        Utils.changeParent(this.node, mapLayer.node);
    }

    equals(grid: VMGrid) {
        if (!grid) {
            return false;
        }
        return grid.skin.spriteFrame == this.skin.spriteFrame;
    }

    isMoney() {
        return this.skin.spriteFrame == AssetsHelper.get<SpriteFrame>(PRELOAD_SP.moneySkinIcon);
    }
}


