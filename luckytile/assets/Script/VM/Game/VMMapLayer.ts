import { _decorator, assetManager, AssetManager, Component, easing, Game, instantiate, JsonAsset, math, Node, Prefab, Size, SpriteFrame, tween, UITransform, Vec2, Vec3 } from 'cc';
import { GameManager, IMapLayerInfo } from '../../Manager/GameManager';
import VMGrid from './VMGrid';
import CCLog from '../../CCLog';
import { Utils } from '../../Common/Utils';
import { GRID_STATE } from '../../Common/Constant';
const { ccclass, property } = _decorator;


@ccclass('VMMapLayer')
export class VMMapLayer extends Component {

    @property(Prefab)
    private gridPrefab: Prefab = null;

    private _layerId: number = 0;
    private _layerH: number = 0;
    private _layerW: number = 0;
    private uitrans: UITransform = null;
    private _layer: VMGrid[][] = null;


    public get layerId(): number {
        return this._layerId;
    }
    private set layerId(value: number) {
        this._layerId = value;
    }

    public get layerW(): number {
        return this._layerW;
    }
    private set layerW(value: number) {
        this._layerW = value;
    }

    public get layerH(): number {
        return this._layerH;
    }
    private set layerH(value: number) {
        this._layerH = value;
    }

    public get layer(): VMGrid[][] {
        return this._layer;
    }
    private set layer(value: VMGrid[][]) {
        this._layer = value;
    }

    public build(layerId: number, layerInfo: IMapLayerInfo, skins: SpriteFrame[]) {
        this.layerId = layerId;
        this.layerW = layerInfo.width;
        this.layerH = layerInfo.height;

        this.uitrans = this.getComponent(UITransform);
        this.node.name = `layer_${layerId}`;
        // 初始化层的位置
        this.node.position = new Vec3(0, 0, 0);
        // 初始化层的大小
        this.uitrans.contentSize = this.layerSize2ContentSize(this.layerW, this.layerH);

        // 生成方格
        this.layer = [];
        let gridNum = layerInfo.x.length;
        for (let i = 0; i < gridNum; i++) {
            let grid: VMGrid = this.createGrid(layerId, layerInfo.x[i], layerInfo.y[i], skins[i]);
            this.add(grid);
            grid.node.parent = this.node;
        }
    }

    public createGrid(layerId: number, gridX: number, gridY: number, skin: SpriteFrame): VMGrid {
        let instance = instantiate(this.gridPrefab);
        let grid = instance.getComponent(VMGrid);
        grid.init(layerId, gridX, gridY).setSkin(skin);
        instance.setPosition(this.coordinate2Position(gridX, gridY));
        return grid;
    }

    public add(grid: VMGrid): VMMapLayer {
        grid.changeState(GRID_STATE.NORMAL);
        let x = grid.gridX;
        let y = grid.gridY;
        if (!this.layer[x]) {
            this.layer[x] = [];
        }
        this.layer[x][y] = grid;
        return this;
    }

    /**
     * 根据地图坐标从地图层中方格
     *
     * @param {number} x x坐标
     * @param {number} y y坐标
     * @return {*}  {VMGrid} 
     * @memberof VMMapLayer 被删除的方格
     */
    public removeByCoordinate(x: number, y: number): VMGrid {
        let grid = null;
        if (this.layer[x]) {
            grid = this.layer[x][y];
            this.layer[x][y] = null;
        }
        return grid;
    }

    /**
     * 从地图层中删除方格
     *
     * @param {VMGrid} grid 需要删除的方格
     * @return {*}  {VMGrid}
     * @memberof VMMapLayer
     */
    public remove(grid: VMGrid): VMGrid {
        return this.removeByCoordinate(grid.gridX, grid.gridY);
    }

    /**
     * 从地图层中获取方格
     *
     * @param {number} x x坐标
     * @param {number} y y坐标
     * @return {*} 
     * @memberof VMMapLayer
     */
    public get(x: number, y: number): VMGrid {
        return this.layer[x] ? this.layer[x][y] : null;
    }

    /**
     * 地图层大小转实际节点大小
     *
     * @param {number} width
     * @param {number} height
     * @return {*}  {Size}
     * @memberof VMMapLayer
     */
    public layerSize2ContentSize(width: number, height: number): Size {
        return new Size(width * GameManager.ins.gridW, height * GameManager.ins.gridH);
    }

    /**
     * 地图坐标转节点位置
     *
     * @param {number} x
     * @param {number} y
     * @return {*}  {Vec3}
     * @memberof VMMapLayer
     */
    public coordinate2Position(x: number, y: number): Vec3 {
        let gridW = GameManager.ins.gridW;
        let gridH = GameManager.ins.gridH;
        let size: Size = this.uitrans.contentSize;
        return new Vec3(-size.width / 2 + x * gridW + gridW * 0.5, size.height / 2 - y * gridH - gridH * 0.5, 0);
    }

    /**
     * 节点坐标转地图坐标
     *
     * @param {Vec3} pos
     * @return {*} 
     * @memberof VMMapLayer
     */
    public position2Coordinate(pos: Vec3): Vec2 {
        let size: Size = this.uitrans.contentSize;
        let x = Math.floor((pos.x - (-size.width / 2)) / GameManager.ins.gridW);
        let y = Math.floor(((size.height / 2) - pos.y) / GameManager.ins.gridH);
        return new Vec2(x, y);
    }

    /**
     * 获取当前方格在此层中周围的方格
     * 
     * @param {VMGrid} grid
     * @memberof GameMapLayer
     */
    public getAroundGridInLayer(grid: VMGrid): VMGrid[] {
        let result: VMGrid[] = [];

        let pos = grid.node.getWorldPosition(new Vec3());
        let leftTop: Vec3 = new Vec3(pos.x - GameManager.ins.gridW * 0.25, pos.y + GameManager.ins.gridH * 0.25, pos.z);
        let leftBottom: Vec3 = new Vec3(pos.x - GameManager.ins.gridW * 0.25, pos.y - GameManager.ins.gridH * 0.25, pos.z);
        let rightTop: Vec3 = new Vec3(pos.x + GameManager.ins.gridW * 0.25, pos.y + GameManager.ins.gridH * 0.25, pos.z);
        let rightBottom: Vec3 = new Vec3(pos.x + GameManager.ins.gridW * 0.25, pos.y - GameManager.ins.gridH * 0.25, pos.z);

        Utils.convertToNodeSpaceAR(this.node, leftTop, leftTop);
        Utils.convertToNodeSpaceAR(this.node, leftBottom, leftBottom);
        Utils.convertToNodeSpaceAR(this.node, rightTop, rightTop);
        Utils.convertToNodeSpaceAR(this.node, rightBottom, rightBottom);

        let coordinate: Vec2 = null;
        let tmp: VMGrid = null;
        coordinate = this.position2Coordinate(leftTop);
        (tmp = this.get(coordinate.x, coordinate.y)) && result.push(tmp);
        coordinate = this.position2Coordinate(leftBottom);
        (tmp = this.get(coordinate.x, coordinate.y)) && result.push(tmp);
        coordinate = this.position2Coordinate(rightTop);
        (tmp = this.get(coordinate.x, coordinate.y)) && result.push(tmp);
        coordinate = this.position2Coordinate(rightBottom);
        (tmp = this.get(coordinate.x, coordinate.y)) && result.push(tmp);
        // 需要去重 防止获取到多个相同的方格
        return [...new Set(result)];
    }

    /**
     * 方格在当前层是否有遮挡
     *
     * @param {VMGrid} grid
     * @memberof GameMapLayer
     */
    public hasCoverInLayer(grid: VMGrid) {
        let grids: VMGrid[] = this.getAroundGridInLayer(grid);
        return grids.length > 0;
    }

    /**
     * 获取所有方格皮肤
     *
     * @return {*}  {SpriteFrame[]}
     * @memberof GameMapLayer
     */
    public getAllGridSkins(): SpriteFrame[] {
        let result: SpriteFrame[] = [];
        this.forEachGrid((value: VMGrid, x: number, y: number) => {
            result.push(value.getSkin());
        });
        return result;
    }

    /**
     * 设置所有方格皮肤
     *
     * @param {SpriteFrame[]} skins
     * @memberof GameMapLayer
     */
    public setAllGridSkins(skins: SpriteFrame[]) {
        let size = 0;
        this.forEachGrid((value: VMGrid, x: number, y: number, index: number) => {
            value.setSkin(skins[index]);
            size = index + 1;
        });
        skins.splice(0, size);
    }

    /**
     * 遍历当前地图层上所有皮肤数据
     *
     * @param {((value: VMGrid, x: number, y: number, index: number) => void | boolean)} callback 回调 如果回调函数返回true 则遍历停止 返回false或不返回则继续遍历
     * @return {*} 
     * @memberof GameMapLayer
     */
    public forEachGrid(callback: (value: VMGrid, x: number, y: number, index: number) => void | boolean): void {
        let index = 0;
        for (let x = 0; x < this.layer.length; x++) {
            if (this.layer[x]) {
                for (let y = 0; y < this.layer[x].length; y++) {
                    let ret = this.layer[x][y] && callback(this.layer[x][y], x, y, index++);
                    if (ret) {
                        return;
                    }
                }
            }
        }
    }

    /**
     * 地图层动画效果
     *
     * @memberof GameMapLayer
     */
    public runMapLayerFlyEffect(layerIdx: number, startPos: Vec3, callback?: Function) {
        this.node.position = startPos;
        let t = tween(this.node)
            .delay(layerIdx * 0.07)
            .to(0.1, { position: new Vec3(0, 0, 0) }, { easing: easing.quadOut })
        if (callback) {
            t.call(callback)
        }
        t.start();
    }

    /**
     * 获取当前层第一个Grid
     *
     * @memberof GameMapLayer
     */
    public getFirstGrid(): VMGrid {
        let first: VMGrid = null;
        this.forEachGrid((value: VMGrid, x: number, y: number, index: number) => {
            first = value;
            return true;
        });
        return first;
    }

    public findSameGrid(grid: VMGrid, out: VMGrid[], sameNum: number): VMGrid[] {
        this.forEachGrid((value: VMGrid, x: number, y: number, index: number) => {
            grid != value && grid.equals(value) && out.push(value);
            if (out.length >= sameNum) {
                return true;
            }
        });
        return out;
    }
}


