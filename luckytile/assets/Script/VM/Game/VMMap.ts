import { _decorator, assetManager, AssetManager, Component, easing, game, instantiate, JsonAsset, Light, Node, Prefab, size, Size, SpriteFrame, tween, UITransform, Vec3, view } from 'cc';
import VMGrid from './VMGrid';
import { GameManager, IMapInfo, IMapLayerInfo } from '../../Manager/GameManager';
import { VMMapLayer } from './VMMapLayer';
import { SkinManager } from '../../Manager/SkinManager';
import CCLog from '../../CCLog';
import { BLOCK_COLLECT_SPEED, BLOCK_ELIMINATE_LIMIT, GRID_HEIGHT, GRID_STATE, GRID_WIDTH } from '../../Common/Constant';
import { PlayerData } from '../../Model/PlayerData';
const { ccclass, property } = _decorator;

@ccclass('VMMap')
export class VMMap extends Component {

    @property(Prefab)
    private layerPrefab: Prefab = null;
    @property(SpriteFrame)
    private coinSp: SpriteFrame = null;
    @property(SpriteFrame)
    private moneySp: SpriteFrame = null;

    private map: VMMapLayer[] = null;
    protected onLoad(): void {
        this.map = [];
    }

    protected start(): void {
        // CCLog.log("Game is Start");
        // this.scheduleOnce(() => {
        //     // 生成地图并播放生成动画
        //     this.generateMap(GameManager.ins.mapInfo, 0, 0);
        // }, 1);
    }


    public begin() {
        // 生成地图并播放生成动画
        this.generateMap(GameManager.ins.mapInfo, 0, PlayerData.ins.getPassData().moneyGridNum);
    }

    /**
     * 地图生成
     *
     * @param {IMapInfo} mapInfo
     * @param {number} coinsNum
     * @param {number} moneyNum
     * @memberof GameMap
     */
    public generateMap(mapInfo: IMapInfo, coinsNum: number, moneyNum: number) {
        // 生成皮肤数据
        let skinSorted: SpriteFrame[] = this.generateSkin(mapInfo.gridTypeNum, coinsNum, moneyNum);
        // 初始化方格缩放比例
        GameManager.ins.gridScale = this.initGridScale(mapInfo.layerGroup);
        CCLog.log("moneyGridNum===", moneyNum);
        CCLog.log("gridScale===", GameManager.ins.gridScale);
        // 创建地图层
        for (let i = 0; i < mapInfo.layerGroup.length; i++) {
            let mapLayerInfo: IMapLayerInfo = mapInfo.layerGroup[i];
            this.build(i, mapLayerInfo, skinSorted.splice(0, mapLayerInfo.x.length));
        }
        GameManager.ins.map = this.map;
        CCLog.log("Map==", this.map);
        // 初始化每个grid状态
        this.initGridState();
        // 地图层动画效果
        this.runMapFlyEffect();
    }

    /**
     * 生成皮肤数据
     *
     * @private
     * @param {number} typeNum 皮肤类型数量
     * @param {number} coinsNum 金币类型皮肤数量
     * @param {number} moneyNum 现金类型皮肤数量
     * @return {*}  {SpriteFrame[]} 已生成的皮肤数据
     * @memberof GameMap
     */
    private generateSkin(typeNum: number, coinsNum: number, moneyNum: number): SpriteFrame[] {
        // 随机筛选方格皮肤
        const skinSelections: SpriteFrame[] = SkinManager.ins.getUsingSkin().sort(
            () => Math.random() < 0.5 ? 1 : -1
        ).slice(0, typeNum);

        CCLog.log("+++coinNum===", coinsNum);
        CCLog.log("redBagNum===", moneyNum);
        // 生成皮肤数据
        let skinSorted: SpriteFrame[] = [];
        for (let i: number = 0; i < GameManager.ins.mapBlocksNum / BLOCK_ELIMINATE_LIMIT; i++) {
            if (i < coinsNum) {
                for (let b: number = 0; b < BLOCK_ELIMINATE_LIMIT; b++) {
                    skinSorted.push(this.coinSp);
                }
            } else if (i < coinsNum + moneyNum) {
                for (let b: number = 0; b < BLOCK_ELIMINATE_LIMIT; b++) {
                    skinSorted.push(this.moneySp);
                }
            } else {
                let index = i % skinSelections.length;
                for (let b: number = 0; b < BLOCK_ELIMINATE_LIMIT; b++) {
                    skinSorted.push(skinSelections[index]);
                }
            }
        }
        // 所有方格皮肤乱序
        skinSorted.sort(() => Math.random() < 0.5 ? 1 : -1);
        return skinSorted;
    }

    /**
     * 构建地图层
     *
     * @param {IMapLayerInfo} layer 地图层数据
     * @param {SpriteFrame[]} skins 当前层使用的皮肤数据
     * @memberof GameMap
     */
    public build(layerId: number, layer: IMapLayerInfo, skins: SpriteFrame[]) {
        let layerNode: Node = instantiate(this.layerPrefab);
        layerNode.parent = this.node;
        let layerComp = layerNode.getComponent(VMMapLayer);
        layerComp.build(layerId, layer, skins);
        this.map.push(layerComp);
        return layerComp;
    }

    /**
     * 初始化方格缩放比例 地图层大于显示区域时 需要适当缩放方格大小 使其显示在区域内
     *
     * @private
     * @param {IMapLayerInfo[]} layerGroup
     * @return {*} 
     * @memberof GameMap
     */
    private initGridScale(layerGroup: IMapLayerInfo[]): number {
        let maxSizeH = 0;
        for (let i = 0; i < layerGroup.length; i++) {
            const layer = layerGroup[i];
            maxSizeH = Math.max(maxSizeH, layer.height);
        }

        let maxMapSize = this.node.parent.getComponent(UITransform).contentSize;
        let scale = maxMapSize.height / (maxSizeH * GRID_HEIGHT);
        return scale > 1 ? 1 : scale;
    }

    /**
     * 初始化所有方格的状态
     *
     * @private
     * @memberof GameMap
     */
    private initGridState() {
        // 遍历方格 初始化状态
        for (let i = 0; i < this.map.length; i++) {
            // 遍历所有地图层
            const mapLayer: VMMapLayer = this.map[i];
            for (let x = 0; x < mapLayer.layer.length; x++) {
                if (mapLayer.layer[x]) {
                    for (let y = 0; y < mapLayer.layer[x].length; y++) {
                        const gameGrid: VMGrid = mapLayer.layer[x][y];
                        gameGrid && gameGrid.refreshState();
                    }
                }
            }
        }
    }

    /**
     * 地图清除
     *
     * @memberof GameMap
     */
    public clearMap() {
        this.node.removeAllChildren();
        this.map = [];
    }

    /**
     * 地图动画
     *
     * @memberof GameMap
     */
    public runMapFlyEffect(): Promise<void> {
        const maxMapSize = this.node.parent.getComponent(UITransform).contentSize;
        return new Promise((resolve: () => void, reject: () => void) => {
            let num = this.map.length;
            for (let i = 0; i < this.map.length; i++) {
                let startPos: Vec3 = null;
                switch (i % 4) {
                    case 0:
                        startPos = new Vec3(-maxMapSize.width, maxMapSize.height, 0);
                        break;
                    case 1:
                        startPos = new Vec3(maxMapSize.width, maxMapSize.height, 0);
                        break;
                    case 2:
                        startPos = new Vec3(-maxMapSize.width, -maxMapSize.height, 0);
                        break;
                    case 3:
                        startPos = new Vec3(maxMapSize.width, - maxMapSize.height, 0);
                        break;
                }
                this.map[i].runMapLayerFlyEffect(i, startPos, () => {
                    num--;
                    if (num == 0) {
                        resolve();
                    }
                });
            }
        })
    }

    /**
     * 刷新地图
     *
     * @memberof GameMap
     */
    public async refresh(): Promise<void> {
        let allSkins: SpriteFrame[] = this.getAllSkins();
        allSkins.sort(() => Math.random() < 0.5 ? 1 : -1);
        this.setAllSkins(allSkins);
        return this.runMapFlyEffect();
    }

    /**
     * 提示
     *
     * @memberof VMMap
     */
    public hint(): boolean {
        let result = false;

        let sameGrids: VMGrid[] = null;
        let gridGroup: VMGrid[] = GameManager.ins.collector.getMaxGridGroup();
        let sameNum = BLOCK_ELIMINATE_LIMIT - gridGroup.length;
        CCLog.log("gridGroup=", gridGroup);

        if (sameNum > 0 && sameNum < BLOCK_ELIMINATE_LIMIT) {
            // 以收集器中已有方格组查找相同方格消除
            if (sameNum <= GameManager.ins.collector.freeSize()) {
                sameGrids = this.findSameGrid(gridGroup[0], sameNum);
            } else {
                CCLog.log(`收集器剩余空间${GameManager.ins.collector.freeSize()},不足${sameNum}`);
            }
        } else {
            CCLog.log("sameNum=", sameNum);
            // 未获取到收集器中的方格组 以地图中第一个方格获取相同的方格
            if (sameNum <= GameManager.ins.collector.freeSize()) {
                let frist = this.getFirstGrid();
                sameGrids = this.findSameGrid(frist, sameNum - 1);
                sameGrids && sameGrids.push(frist);
            } else {
                CCLog.log(`收集器剩余空间${GameManager.ins.collector.freeSize()},不足${sameNum}`);
            }
        }
        if (sameGrids) {
            CCLog.log("grids==", sameGrids);
            for (const value of sameGrids) {
                value.collectReady();
                value.collecting();
            }
            result = true;
        }
        return result;
    }

    /**
     * 返回
     *
     * @memberof VMMap
     */
    public recall() {
        let list = GameManager.ins.collector.getRecallGrids();
        if (!list.length) {
            console.log("不可回退");
        }
        GameManager.ins.collector.clearRecallList();
        // 先重新加入到地图层中
        for (const grid of list) {
            grid.backInMap();
        }
        console.log(...(list.map((value) => value.node.getSiblingIndex())));
        // 再刷新方格的状态 当前的方格可能会互相影响
        for (let i = 0; i < list.length; i++) {
            const grid = list[i];
            let tmpPos: Vec3 = new Vec3(grid.node.position);
            // 刷新方格状态
            let mapLayer: VMMapLayer = GameManager.ins.getMapLayer(grid.layerId);
            let pos: Vec3 = mapLayer.coordinate2Position(grid.gridX, grid.gridY);
            grid.node.position = pos;
            if (grid.layerId > 0) {
                let grids: VMGrid[] = grid.getAroundGridInNextLayer();
                for (let i = 0; i < grids.length; i++) {
                    grids[i].changeState(GRID_STATE.COVER);
                }
            }
            // 播放方格移动动画
            grid.node.position = tmpPos;
            grid.backMove(pos, (target: VMGrid) => {
                console.log("调整遮挡关系");
                // 调整顺序 防止遮挡关系不正确
                console.log("grid", grid.layerId, grid.gridX, grid.gridY);
                let nextGrid: VMGrid = null;
                (() => {
                    for (let y = target.gridY; y <= mapLayer.layerH; y++) {
                        for (let x = y == target.gridY ? target.gridX + 1 : 0; x < mapLayer.layerW; x++) {
                            nextGrid = mapLayer.get(x, y);
                            if (nextGrid && list.indexOf(nextGrid) == -1) {
                                console.log("nextGrid", nextGrid.layerId, nextGrid.gridX, nextGrid.gridY, nextGrid.node.getSiblingIndex());
                                target.node.setSiblingIndex(nextGrid.node.getSiblingIndex());
                                return;
                            }
                        }
                    }
                })();
                list.splice(i, 1);
            }).start();
        }
        GameManager.ins.collector.collectingMove();
    }

    public getFirstGrid(): VMGrid {
        let len = this.map.length;
        for (let i = len - 1; i >= 0; i--) {
            let firstGrid: VMGrid = this.map[i].getFirstGrid();
            if (firstGrid) {
                return firstGrid;
            }
        }
        return null;
    }

    public findSameGrid(grid: VMGrid, sameNum: number): VMGrid[] {
        let result: VMGrid[] = [];
        let len = this.map.length;
        for (let i = len - 1; i >= 0; i--) {
            result = this.map[i].findSameGrid(grid, result, sameNum);
            if (result.length >= sameNum) {
                return result;
            }
        }
        return null;
    }

    /**
     * 获取地图上所有方格的皮肤
     *
     * @return {*}  {SpriteFrame[]}
     * @memberof GameMap
     */
    public getAllSkins(): SpriteFrame[] {
        let result: SpriteFrame[] = [];
        // 获取所有皮肤数据
        for (let i = 0; i < this.map.length; i++) {
            result.push(...this.map[i].getAllGridSkins());
        }
        return result;
    }

    /**
     * 设置地图上所有方格的皮肤
     *
     * @param {SpriteFrame[]} skins
     * @memberof GameMap
     */
    public setAllSkins(skins: SpriteFrame[]) {
        for (let i = 0; i < this.map.length; i++) {
            this.map[i].setAllGridSkins(skins);
        }
    }


    public onMapRefresh() {
        this.refresh();
    }

    public onMapHint() {
        this.hint();
    }

    public onMapRecall() {
        this.recall();
    }
}


