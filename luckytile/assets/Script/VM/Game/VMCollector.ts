import { _decorator, Component, Node, SpriteFrame, Tween, tween, Vec3 } from 'cc';
import { GameManager } from '../../Manager/GameManager';
import VMGrid from './VMGrid';
import CCLog from '../../CCLog';
import { BLOCK_COLLECT_SPEED, BLOCK_ELIMINATE_LIMIT, CUSTOM_EVENT_NAME, GRID_ACTION, GRID_STATE } from '../../Common/Constant';
import { getEventEmiter } from '../../Common/EventEmitter';
import { AUDIOS_PATH } from '../../Common/AudioConfig';
import AudioTool from '../../Common/AudioTool';
const { ccclass, property } = _decorator;

@ccclass('VMCollector')
export class VMCollector extends Component {

    @property([Node])
    private collectorItem: Node[] = []; // 作用只是标记位置 无其它作用
    private collectGrid: VMGrid[] = []; // 收集到的所有方格
    private recallList: VMGrid[] = []; // 可返回方格的列表

    protected onLoad(): void {
        GameManager.ins.collector = this;
    }

    public add(grid: VMGrid) {
        // 收集器已满
        if (this.isFull()) {
            return;
        }
        grid.changeState(GRID_STATE.COLLECT);
        // 查找相同的方格下标
        let idx: number = this.findSameGridIndex(grid);
        if (idx >= 0 && idx < this.collectGrid.length - 1) {
            // 查询到 插入到此下标之后
            const insertIdx: number = idx + 1;
            // 插入到收集器列表中
            this.insert(insertIdx, grid);
            // 加入到返回列表中
            this.addInRecallList(grid);
            // 获取可以被消除的方格
            let eliminateGrids = this.getEliminateGrid(insertIdx);
            CCLog.log("insert eliminateGrids==", eliminateGrids);
            // 更新当前与之后所有方格的位置
            this.collectingMove(insertIdx);
            if (eliminateGrids.length > 0) {
                // 清空返回列表
                this.clearRecallList();
                // 最长Move时间的方格触发回调
                eliminateGrids.reduce((previousValue: VMGrid, currentValue: VMGrid) => {
                    return currentValue.getDis2EndPoint() > previousValue.getDis2EndPoint() ? currentValue : previousValue;
                }).setCollectedCallback(() => {
                    // 开始消除
                    this.eliminateGrids(eliminateGrids);
                });
            } else {
                // 没有可以消除 检测是否已满
                grid.setCollectedCallback(() => {
                    if (this.isFull() && !this.collectGrid.find((value: VMGrid) => { return value != grid && value.hasMoveAnim() })) {
                        // 收集器已满
                        getEventEmiter().emit(CUSTOM_EVENT_NAME.GAME_FAIL);
                    }
                });
            }

        } else {
            // 未查询到或者查询到为最后一个 则直接加入到队尾
            const currIdx = this.collectGrid.push(grid) - 1;
            CCLog.log("currIdx==", currIdx);
            // 加入到返回列表中
            this.addInRecallList(grid);
            // 获取可以被消除的方格
            let eliminateGrids = this.getEliminateGrid(currIdx);
            CCLog.log("getEliminateGrid==", eliminateGrids);
            // 更新当前方格的位置
            grid.collectingMove(this.collectorItem[currIdx].position).start();
            // 最长Move时间的方格触发回调
            if (eliminateGrids.length > 0) {
                // 清空返回列表
                this.clearRecallList();
                eliminateGrids.reduce((previousValue: VMGrid, currentValue: VMGrid) => {
                    return currentValue.getDis2EndPoint() > previousValue.getDis2EndPoint() ? currentValue : previousValue;
                }).setCollectedCallback(() => {
                    // 开始消除
                    this.eliminateGrids(eliminateGrids);
                });
            } else {
                // 没有可以消除 检测是否已满
                grid.setCollectedCallback(() => {
                    if (this.isFull() && !this.collectGrid.find((value: VMGrid) => { return value != grid && value.hasMoveAnim() })) {
                        // 收集器已满
                        getEventEmiter().emit(CUSTOM_EVENT_NAME.GAME_FAIL);
                    }
                });
            }
        }
    }

    private addInRecallList(grid: VMGrid) {
        if (this.recallList.length >= BLOCK_ELIMINATE_LIMIT) {
            this.recallList.shift();
        }
        this.recallList.push(grid);
    }

    public clearRecallList() {
        this.recallList.length = 0;
    }

    public getRecallGrids(): VMGrid[] {
        return Array.from(this.recallList);
    }

    public collectingMove(startIndex: number = 0) {
        // 更新方格的位置
        for (let i = startIndex; i < this.collectGrid.length; i++) {
            this.collectGrid[i].collectingMove(this.collectorItem[i].position).start();
        }
    }

    /**
     * 获取可消除方格
     *
     * @memberof GameGridCollector
     */
    public getEliminateGrid(idx: number): VMGrid[] {
        let result: VMGrid[] = [];

        let grid1 = this.collectGrid[idx - 0];
        let grid2 = this.collectGrid[idx - 1];
        let grid3 = this.collectGrid[idx - 2];

        if (!grid2 || !grid3) {
            return result;
        }
        if (grid1.equals(grid2) && grid1.equals(grid3) && !grid2.isEliminated() && !grid3.isEliminated()) {
            grid1.changeState(GRID_STATE.ELIMINATE);
            grid2.changeState(GRID_STATE.ELIMINATE);
            grid3.changeState(GRID_STATE.ELIMINATE);
            result.push(grid1);
            result.push(grid2);
            result.push(grid3);
        }
        return result;
    }

    /**
     * 消除方格
     *
     * @memberof GameGridCollector
     */
    public eliminateGrids(grids: VMGrid[]) {
        if (grids.length < BLOCK_ELIMINATE_LIMIT) {
            return;
        }
        // 有消除
        for (const grid of grids) {
            grid.delete();
            this.removeByGrid(grid);
        }

        // 当前关卡结束
        let gameEnd = GameManager.ins.isEnd();

        // 消除了现金方格
        let gridIsMoney = false;
        gridIsMoney = grids[0].isMoney();

        AudioTool.sound(AUDIOS_PATH.block_destory);

        this.scheduleOnce(() => {
            // 更新收集器中所有方格的位置
            this.collectingMove();
            // 当前关卡结束
            if (gameEnd) {
                getEventEmiter().emit(CUSTOM_EVENT_NAME.GAME_SUCESS);
            }
            // 消除了现金方格
            if (gridIsMoney) {
                getEventEmiter().emit(CUSTOM_EVENT_NAME.GAME_GET_MONEY);
            }
        }, 0.3)
    }

    public removeByGrid(grid: VMGrid) {
        this.removeByIndex(this.collectGrid.indexOf(grid));
    }

    public removeByIndex(index: number) {
        this.collectGrid.splice(index, 1);
    }

    public insert(index: number, grid: VMGrid) {
        this.collectGrid.splice(index, 0, grid);
    }

    /**
     * 查找收集器中相同皮肤的方格下标
     * 反向查询列表中最后一个
     *
     * @param {VMGrid} grid
     * @return {*}  {number}
     * @memberof GameGridCollector
     */
    public findSameGridIndex(grid: VMGrid): number {
        // 反向遍历 查找最后一个
        for (let i = this.collectGrid.length - 1; i >= 0; i--) {
            const itemGrid = this.collectGrid[i];
            if (!itemGrid.isEliminated() && itemGrid.equals(grid)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 收集器是否已满
     *
     * @return {*} 
     * @memberof GameGridCollector
     */
    public isFull(): boolean {
        return this.collectGrid.length >= this.collectorItem.length;
    }

    /**
     * 收集器空闲的位置
     *
     * @return {*} 
     * @memberof GameGridCollector
     */
    public freeSize(): number {
        return this.collectorItem.length - this.collectGrid.length;
    }

    /**
     * 全部位置是否空闲
     *
     * @return {*}  {number}
     * @memberof VMCollector
     */
    public isAllFree(): boolean {
        return this.collectGrid.length == 0;
    }

    /**
     * 获取收集器中最大数量一组皮肤
     *
     * @return {*}  {GameGrid[]}
     * @memberof GameGridCollector
     */
    public getMaxGridGroup(): VMGrid[] {
        let result: VMGrid[] = [];
        let currGrids: VMGrid[] = [];
        let len = this.collectGrid.length;
        for (let i = len - 1; i >= 0; i--) {
            const grid = this.collectGrid[i];
            const next = this.collectGrid[i - 1];
            if (!grid.isEliminated() && grid.equals(next)) {
                currGrids.push(grid);
            } else {
                if (!grid.isEliminated()) {
                    currGrids.push(grid);
                }
                result = currGrids.length > result.length ? currGrids : result;
                currGrids = [];
            }
        }
        return result;
    }

    public clear() {
        for (const grid of this.collectGrid) {
            grid.node.destroy();
        }
        this.collectGrid.length = 0;
    }
}


