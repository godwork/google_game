import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('LightRotationAnim')
export class LightRotationAnim extends Component {
    update(deltaTime: number) {
        this.node.angle += 1;
    }
}

