import { _decorator, Component, easing, Node, tween, Vec3 } from 'cc';
import { Utils } from '../Common/Utils';
const { ccclass, property } = _decorator;

@ccclass('PropAnim')
export class PropAnim extends Component {

    @property(Node)
    private refresh: Node = null;
    @property(Node)
    private hint: Node = null;

    protected onLoad(): void {
        this.refresh.active = false;
        this.hint.active = false;
    }

    start() {

    }

    /**
     * 刷新道具动画
     *
     * @memberof Anim
     */
    startRefreshPropAnim(target: Node, endCallback?: Function): Promise<void> {
        this.refresh.active = true;
        this.refresh.position = Vec3.ZERO;
        let pos = Utils.convertToNodeSpaceAR(this.node, target.worldPosition, new Vec3());
        return new Promise((resolve, reject) => {
            tween(this.refresh)
                .delay(0.3)
                .to(0.7, { position: pos }, { easing: easing.quadIn })
                .removeSelf()
                .call(() => {
                    endCallback && endCallback();
                    this.node.destroy();
                    resolve();
                })
                .start();
        })
    }

    /**
     * 提示道具动画
     *
     * @memberof Anim
     */
    startHintPropAnim(target: Node, endCallback?: Function): Promise<void> {
        this.hint.active = true;
        this.hint.position = Vec3.ZERO;
        let pos = Utils.convertToNodeSpaceAR(this.node, target.worldPosition, new Vec3());
        return new Promise((resolve, reject) => {
            tween(this.hint)
                .delay(0.3)
                .to(0.7, { position: pos }, { easing: easing.quadIn })
                .removeSelf()
                .call(() => {
                    endCallback && endCallback();
                    resolve();
                })
                .start();
        })
    }
}


