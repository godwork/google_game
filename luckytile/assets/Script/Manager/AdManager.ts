import { TASK_TYPE, TaskManager } from "./TaskManager";

type ResultCb = (isRewards: boolean) => void;


/**
 * 广告管理器
 *
 * @export
 * @class AdManager
 */
export class AdManager {
    private static _instance: AdManager = null;

    public static get ins(): AdManager {
        if (!this._instance) {
            this._instance = new AdManager();
        }
        return this._instance;
    }

    private resultCb: ResultCb = null;

    open() {
        this.result(true);
    }

    result(flag: boolean) {
        if (flag) {
            // 广告奖励发放
            TaskManager.ins.checkTask(TASK_TYPE.AD);
            this.resultCb && this.resultCb(flag);
            this.resultCb = null;
        } else {
            // 广告异常 不发放奖励
        }
    }

    onResult(adCallback: ResultCb) {
        this.resultCb = adCallback;
    }
}


