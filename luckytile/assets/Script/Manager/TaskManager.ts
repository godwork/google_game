import { AssetManager, JsonAsset, resources } from "cc";
import CCLog from "../CCLog";
import { CONFIG_PATH_PRE } from "../Common/Constant";
import { PlayerData } from "../Model/PlayerData";

type TaskInfoSingle = {
    id: number
    type: number
    count: number
}

export enum TASK_TYPE {
    PASS = 1,
    AD = 2,
}

export class TaskManager {
    private static _instance: TaskManager = null;

    private taskInfo: TaskInfoSingle[] = null;
    private taskCount: number = 0;

    public static get ins(): TaskManager {
        if (!this._instance) {
            this._instance = new TaskManager();
        }
        return this._instance;
    }

    public get taskConfigPath(): string {
        return `${CONFIG_PATH_PRE}/task`;
    }

    public async init(onProgress?: (finished: number, total: number, item: AssetManager.RequestItem) => void): Promise<TaskInfoSingle[]> {
        return new Promise<TaskInfoSingle[]>((resolve: Function, reject: Function) => {
            if (!this.taskInfo) {
                resources.load(this.taskConfigPath, JsonAsset, onProgress, (e: Error, res: JsonAsset) => {
                    if (e) {
                        CCLog.log("load task config error");
                    } else {
                        console.log("task json", res.json);
                        this.taskInfo = res.json as TaskInfoSingle[];
                        resolve(this.taskInfo);
                    }
                });
            } else {
                resolve(this.taskInfo);
            }
        });
    }

    getTaskInfo(): TaskInfoSingle[] {
        return this.taskInfo;
    }

    getTaskByIndex(index: number): TaskInfoSingle {
        return index < this.taskInfo.length ? this.taskInfo[index] : null;
    }

    getTaskCount(): number {
        return this.taskInfo.length;
    }

    checkTask(type: TASK_TYPE) {
        let taskInfo: TaskInfoSingle = this.getTaskByIndex(PlayerData.ins.currTaskId);
        if (taskInfo && type != taskInfo.type) {
            return;
        }
        switch (taskInfo.type) {
            case TASK_TYPE.PASS:
                // 通关类型任务
                PlayerData.ins.currTaskCompleteCount++;
                console.log("通关类型任务", PlayerData.ins.currTaskCompleteCount);
                if (PlayerData.ins.currTaskCompleteCount == taskInfo.count) {
                    // 任务完成
                    PlayerData.ins.currTaskId++;
                    PlayerData.ins.currTaskCompleteCount = 0;
                }
                break;
            case TASK_TYPE.AD:
                // 广告类型任务
                PlayerData.ins.currTaskCompleteCount++;
                if (PlayerData.ins.currTaskCompleteCount == taskInfo.count) {
                    // 任务完成
                    PlayerData.ins.currTaskId++;
                    PlayerData.ins.currTaskCompleteCount = 0;
                }
                break;
        }
    }
}