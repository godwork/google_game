import { AssetManager, SpriteFrame, resources } from "cc";
import CCLog from "../CCLog";
import { SKIN_GROUP_DEFAULT, SKIN_PATH_PRE } from "../Common/Constant";
import { PlayerData } from "../Model/PlayerData";

type SkinCache = { [index: string]: SpriteFrame[] }

export class SkinManager {
    private static _instance: SkinManager = null;

    public static get ins(): SkinManager {
        if (!this._instance) {
            this._instance = new SkinManager();
        }
        return this._instance;
    }

    public skinIds: string[] = null;
    public skinCache: SkinCache = {};

    public get skinPath(): string {
        return `${SKIN_PATH_PRE}/${PlayerData.ins.getSkinID()}`;
    }

    public async init(onProgress?: (finished: number, total: number, item: AssetManager.RequestItem) => void): Promise<SpriteFrame[]> {
        if (!this.skinIds) {
            let infos = resources.getDirWithPath(SKIN_PATH_PRE, SpriteFrame);
            let pathsTmp = infos.map(function (info) {
                let pathSlice = (info.path as string).split('/');
                return pathSlice[1];
            });
            this.skinIds = [...new Set(pathsTmp)].sort();
            if (!this.skinIds.includes(PlayerData.ins.getSkinID())) PlayerData.ins.setSkinID(SKIN_GROUP_DEFAULT);
        }
        return new Promise((resolve: Function, reject: Function) => {
            resources.loadDir(this.skinPath, SpriteFrame, onProgress, (e: Error, res: SpriteFrame[]) => {
                //log("error====",res);
                if (e || res.length === 0) {
                    CCLog.log("load skin error");
                } else {
                    this.skinCache[PlayerData.ins.getSkinID()] = res;
                    CCLog.log("this.skinPath=res1=", this.skinCache[PlayerData.ins.getSkinID()].length)
                    resolve(res);
                }
            });
        });
    }

    public getUsingSkin(): SpriteFrame[] {
        return this.skinCache[PlayerData.ins.getSkinID()];
    }

    public changSkin(num: string): void {
        // TODO 更换皮肤

    }
}