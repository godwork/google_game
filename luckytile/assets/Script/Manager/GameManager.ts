import { _decorator, assetManager, AssetManager, JsonAsset } from 'cc';
import { GRID_HEIGHT, GRID_WIDTH } from '../Common/Constant';
import { VMMapLayer } from '../VM/Game/VMMapLayer';
import { VMCollector } from '../VM/Game/VMCollector';
import VMGrid from '../VM/Game/VMGrid';
const { ccclass, property } = _decorator;

/**
 * 地图层信息结构
 */
export interface IMapLayerInfo {
    width: number; // 地图层宽
    height: number; // 地图层高
    x: number[]; // 地图层上所有grid的X坐标
    y: number[]; // 地图层上所有grid的Y坐标
}

/**
 * 地图信息结构
 */
export interface IMapInfo {
    passNum: number; // 关卡ID
    gridTypeNum: number; // 格子类型数量
    layerGroup: IMapLayerInfo[]; // 一组地图层信息（一组信息构成整个游戏地图）
}


@ccclass('GameManager')
export class GameManager {

    private static _instance: GameManager = null;

    public static get ins(): GameManager {
        if (!this._instance) {
            this._instance = new GameManager();
        }
        return this._instance;
    }

    public mapInfo: IMapInfo = null; // 地图信息
    public gameOver: boolean = false; // 游戏是否结束
    public gameComplete = false; // 游戏是否完成（通过当前关卡）
    public isPause: boolean = false; // 游戏是否暂停
    public mapLayersNum: number; // 当前关卡地图层数量
    public mapBlocksNum: number; // 当前关卡地图层上所有格子数量
    public maxPassNum: number; // 最大关卡数
    public bundle4Pass: AssetManager.Bundle; // 关卡数据bundle
    public cache4Map: { [index: number]: IMapInfo } = {}; // 地图信息缓存 key为关卡数
    public map: VMMapLayer[] = null; // 地图层数组
    public collector: VMCollector = null; // 收集器

    private _gridScale: number = 1;// 方格缩放比例

    public get gridScale(): number {
        return this._gridScale;
    }

    public set gridScale(value: number) {
        this._gridScale = value;
    }

    public get gridW(): number {
        return GRID_WIDTH * this.gridScale;
    }

    public get gridH() {
        return GRID_HEIGHT * this.gridScale;
    }

    /**
     * 加载地图数据Bundle
     *
     * @memberof GameManager
     */
    public async loadMapInfoBundle() {
        this.bundle4Pass = await new Promise((resolve: Function, reject: Function) => {
            assetManager.loadBundle('bundle_pass', (err: Error, bundle: AssetManager.Bundle) => {
                if (err) {
                    //console.log("加载关卡信息=="+err);
                    reject(err);
                } else {
                    //console.log("加载关卡成功=="+err);
                    resolve(bundle);
                }
            });
        });
        this.maxPassNum = this.bundle4Pass.getDirWithPath("/").length;
    }

    /**
     * 加载地图数据
     *
     * @param {number} passNum 关卡数
     * @return {*} 
     * @memberof GameManager
     */
    public loadMap(passNum: number): Promise<void> {
        if (passNum in this.cache4Map) {
            this.mapInfo = this.cache4Map[passNum];
            this.mapInfo.passNum = passNum;
            const layerGroup: IMapLayerInfo[] = this.mapInfo.layerGroup;
            this.mapLayersNum = layerGroup.length;
            this.mapBlocksNum = layerGroup.reduce((num: number, v: IMapLayerInfo) => { return num + v.x.length }, 0);
            // 预装配下一关数据
            this.preLoadMap(passNum + 1);
            return;
        }
        return new Promise((resolve: Function, reject: Function) => {
            let pass = passNum;
            if (passNum > this.maxPassNum) {
                pass = Math.ceil(Math.random() * this.maxPassNum)
            }
            this.bundle4Pass.load(`${pass}`, (e: Error, res: JsonAsset) => {
                if (e) {
                    this.mapInfo = null;
                    resolve();
                } else {
                    this.mapInfo = { passNum: 0, gridTypeNum: 0, layerGroup: [] };
                    this.mapInfo.passNum = passNum;
                    this.mapInfo.gridTypeNum = res.json.blockTypeNum;
                    this.mapInfo.layerGroup = res.json.map;
                    this.cache4Map[passNum] = this.mapInfo;
                    this.mapLayersNum = this.mapInfo.layerGroup.length;
                    this.mapBlocksNum = this.mapInfo.layerGroup.reduce((num: number, v: IMapLayerInfo) => { return num + v.x.length }, 0);
                    this.preLoadMap(passNum + 1);
                    resolve();
                }
            });
        });
    }

    /**
     * 预加载地图
     *
     * @param {number} passNum 关卡数
     * @return {*}  {void}
     * @memberof GameManager
     */
    public preLoadMap(passNum: number): void {
        if (passNum in this.cache4Map) return;
        let pass = passNum
        if (pass > this.maxPassNum) {
            pass = Math.ceil(Math.random() * this.maxPassNum)
        }
        this.bundle4Pass.load(`${pass}`, (e: Error, res: JsonAsset) => {
            if (e) {

            } else {
                this.cache4Map[pass] = { passNum: passNum, gridTypeNum: res.json.blockTypeNum, layerGroup: res.json.map };
            }
        });
    }

    /**
     * 初始化
     *
     * @memberof GameManager
     */
    public init(): void {
        this.gameOver = false;
        this.gameComplete = false;
        this.isPause = false;
    }

    public addInMap(grid: VMGrid): VMMapLayer {
        return this.getMapLayer(grid.layerId)?.add(grid);
    }

    /**
     * 从地图中删除方格
     *
     * @param {number} layerId
     * @param {number} x
     * @param {number} y
     * @memberof GameManager
     */
    public removeFromMap(grid: VMGrid): VMGrid {
        return GameManager.ins.getMapLayer(grid.layerId)?.remove(grid);
    }

    /**
     * 获取MapLayer
     *
     * @param {number} layerId
     * @return {*}  {VMMapLayer}
     * @memberof GameManager
     */
    public getMapLayer(layerId: number): VMMapLayer {
        return this.map[layerId];
    }

    /**
     * 地图中方格的数量
     *
     * @return {*}  {number}
     * @memberof GameManager
     */
    public gridNumInMap(): number {
        let size = 0;
        for (let i = 0; i < this.map.length; i++) {
            const mapLayer: VMMapLayer = this.map[i];
            size += mapLayer.node.children.length;
        }
        return size;
    }

    /**
     * 游戏是否已经结束
     *
     * @return {*}  {boolean}
     * @memberof GameManager
     */
    public isEnd(): boolean {
        // 检测地图上方格是否已经全部消除完毕
        return this.gridNumInMap() == 0 && this.collector.isAllFree();
    }
}


