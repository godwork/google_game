import { Component, director, easing, Node } from "cc";
import CCLog from "../CCLog";
import { AREA_MOVE_DURATION } from "../Common/Constant";

const offset: number = 1280;

class SceneManager extends Component {
    public loadedProgress: any = {};

    constructor() {
        super();
    }

    public init(): void {
        // 预加载场景
        // for (let scene of Object.values(SCENES_NAME)) {

        //     this.loadedProgress[scene] = 0;
        //     director.preloadScene(scene, (c: number, t: number) => {
        //         this.loadedProgress[scene] = c / t;
        //     }, () => { });
        // }
    }

    public preloadScene(sceneName: string) {
        // 预加载场景
        this.loadedProgress[sceneName] = 0;
        director.preloadScene(sceneName, (c: number, t: number) => {
            this.loadedProgress[sceneName] = c / t;
        }, () => { });
    }

    public sceneChange(nextSceneName: string, delay: boolean = true): void {
        if (delay) {
            this.scheduleOnce(() => {
                director.loadScene(nextSceneName);
            }, AREA_MOVE_DURATION);
        } else {
            director.loadScene(nextSceneName);
        }
    }
}

export default new SceneManager();