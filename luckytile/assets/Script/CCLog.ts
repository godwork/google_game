/**
 * @ Log information
 */



export default class CCLog {
    public static isShowLog = true
    public static log(message?: any, ...optionalParams: any[]) {
        // console.log("CCLog.isOutputLog()::", CCLog.isOutputLog());
        if (CCLog.isShowLog) {
            console.log(message, ...optionalParams);
           
        }
    }
   
    public static error(message?: any, ...optionalParams: any[]) {
        if (CCLog.isOutputLog()) {
        console.error(message, ...optionalParams);
        var logObject:any = {};
        logObject.message = message;
        logObject.logType = "error";
        logObject.logTime = this.getDateString();
        }
    }

    public static debug(message?: any, ...optionalParams: any[]) {
        if (CCLog.isOutputLog()) {
            console.debug(message, ...optionalParams);
        }
    }

    // 是否输出日志信息 true:输出  false:不输出
    public static isOutputLog(): boolean {
      
        return CCLog.isShowLog;
    }

    public static getDateString() {
        var d = new Date();
        var str = String(d.getHours());
        var timeStr = d.getFullYear()+"-";
        timeStr += (((d.getMonth()+1)+"").length == 1 ? ("0" + (d.getMonth()+1)) : ((d.getMonth())+1+"")) + "-";
        timeStr += ((d.getDate()+"").length == 1 ? ("0" + d.getDate()) : (d.getDate()+"")) + " ";

        timeStr += (str.length == 1 ? "0" + str : str) + ":";
        str = String(d.getMinutes());
        timeStr += (str.length == 1 ? "0" + str : str) + ":";
        str = String(d.getSeconds());
        timeStr += (str.length == 1 ? "0" + str : str) + ":";
        str = String(d.getMilliseconds());
        if (str.length == 1) str = "00" + str;
        if (str.length == 2) str = "0" + str;
        timeStr += str;

        timeStr = "[" + timeStr + "]";
        return timeStr;
    };

}





