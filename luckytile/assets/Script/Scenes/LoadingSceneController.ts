import LanguagetileTile, { Country } from "../Common/Language";
import CCLog from "../CCLog";
import { Component, Label, Sprite, SpriteFrame, _decorator, Prefab } from "cc";
import { PlayerData } from "../Model/PlayerData";
import { GameManager } from "../Manager/GameManager";
import SceneManager from "../Manager/SceneManager";
import { PRELOAD_PREFABS, PRELOAD_SP, SCENES_NAME } from "../Common/Constant";
import { SkinManager } from "../Manager/SkinManager";
import { AssetsHelper } from "../Common/AssetsHelper";
import { TaskManager } from "../Manager/TaskManager";
import { Utils } from "../Common/Utils";
import AudioTool from "../Common/AudioTool";
const { ccclass, property } = _decorator;

@ccclass('LoadingSceneController')
export default class LoadingSceneController extends Component {

    @property(Sprite)
    public spriteLoadingBar: Sprite = null;
    @property(Label)
    public loadingTitle: Label = null;
    @property(Label)
    public loadingTips: Label = null;
    @property(Sprite)
    public logo: Sprite = null;

    @property(SpriteFrame)
    public IDLogo: SpriteFrame = null;
    @property(SpriteFrame)
    public BRLogo: SpriteFrame = null;

    private _updating: boolean = false;
    private _loaded: boolean = false;

    // 加载任务数量
    private loadingTaskNum: number = -1;
    private loadedTaskNum: number = 0;

    public onLoad() {
        this.spriteLoadingBar.fillRange = 0;
        if (LanguagetileTile.getCountry == Country.ID) {
            this.logo.spriteFrame = this.IDLogo;
        } else if (LanguagetileTile.getCountry == Country.BR) {
            this.logo.spriteFrame = this.BRLogo;
        }
        // window.__errorHandler = function (file: any, line: any, error: any) {
        //     console.warn("错误日志抓取=======>", file, line, error)
        // }
    }

    public async start() {
        this.loadingTitle.string = LanguagetileTile.getWord("Loading") + "..."
        this.loadingTips.string = LanguagetileTile.getWord("LoadingDes")

        // 初始化PlayerData
        PlayerData.ins.init();
        // 初始化GameManager
        GameManager.ins.init();

        // 预加载音乐音效
        await AudioTool.initialize();
        CCLog.log("音频加载完毕");

        this.loadingTaskNum = 0;
        // 初始化皮肤管理器
        this.loadSkin();
        // 预加载地图数据
        this.loadMap();
        // 加载SpriteFrame
        this.loadPreSp();
        // 加载prefabs
        this.loadPrefabs();
        // 加载任务数据
        this.loadTask();

        SceneManager.preloadScene(SCENES_NAME.GameScene);
        // this._updating = true;
    }

    public async update() {
        if (this._loaded) return;
        if (this._updating) {
            if (SceneManager.loadedProgress[SCENES_NAME.GameScene] > this.spriteLoadingBar.fillRange) {
                this.spriteLoadingBar.fillRange = SceneManager.loadedProgress[SCENES_NAME.GameScene];
            }
            if (this.spriteLoadingBar.fillRange === 1) {
                CCLog.log("主场景加载完毕");
                this._loaded = true;
                await GameManager.ins.loadMap(PlayerData.ins.getPassData().passNum);
                SceneManager.sceneChange(SCENES_NAME.GameScene, false);
            }
        } else {
            if (this.loadingTaskNum == -1) {
                return
            }
            // if (this.spriteLoadingBar.fillRange < 0.8) {
            let num = Math.round(this.loadedTaskNum * 100) / 100;
            this.spriteLoadingBar.fillRange = num / this.loadingTaskNum * 0.8;
            // }
            if (num == this.loadingTaskNum) {
                this._updating = true;
            }
        }
    }

    public async loadSkin() {
        this.loadingTaskNum++;
        // 初始化皮肤管理器
        await SkinManager.ins.init(this.loadProgress());
        CCLog.log("皮肤加载完毕");
    }

    public async loadMap() {
        this.loadingTaskNum++;
        // 预加载地图数据
        await GameManager.ins.loadMapInfoBundle();
        GameManager.ins.preLoadMap(PlayerData.ins.getPassData().passNum);
        this.loadedTaskNum++;
        CCLog.log("地图加载完毕");
    }

    public async loadPreSp() {
        this.loadingTaskNum++;
        Utils.toPath(PRELOAD_SP, "gameBg", (PlayerData.ins.currSelectedBg + 1).toString());
        // 加载SpriteFrame
        await AssetsHelper.load(Object.values(PRELOAD_SP), SpriteFrame, this.loadProgress());
        CCLog.log("图片加载完毕");
    }

    public async loadPrefabs() {
        this.loadingTaskNum++;
        // 加载prefabs
        await AssetsHelper.load(Object.values(PRELOAD_PREFABS), Prefab, this.loadProgress());
        AssetsHelper.logCache();
        CCLog.log("预制加载完毕");
    }

    public async loadTask() {
        this.loadingTaskNum++;
        // 加载任务
        await TaskManager.ins.init(this.loadProgress());
        CCLog.log("任务加载完毕");
    }

    public loadProgress(): (finished: number, total: number) => void {
        let previous = 0;
        return (finished: number, total: number) => {
            if (finished / total > previous) {
                this.loadedTaskNum -= previous;
                // console.log("previous", this.loadedTaskNum, previous);
                previous = finished / total;
                this.loadedTaskNum += previous;
                // console.log("loadProgress", this.loadedTaskNum, previous);
            }
        }
    }
}
