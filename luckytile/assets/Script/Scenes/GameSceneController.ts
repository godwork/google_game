import { _decorator, Component, instantiate, Prefab, Sprite, SpriteFrame, Node } from 'cc';
import CCLog from '../CCLog';
import { VMMap } from '../VM/Game/VMMap';
import { VMTopUI } from '../VM/UI/VMTopUI';
import { VMBottomUI } from '../VM/UI/VMBottomUI';
import { getEventEmiter } from '../Common/EventEmitter';
import { AREA_MOVE_DURATION, CUSTOM_EVENT_NAME, GAMEBG_PATH_PRE, PREFABS_WABAO, PRELOAD_PREFABS } from '../Common/Constant';
import { AssetsHelper } from '../Common/AssetsHelper';
import { VMSucessPanel } from '../VM/UI/VMSucessPanel';
import { VMFailPanel } from '../VM/UI/VMFailPanel';
import { VMRewardsPanel } from '../VM/UI/VMRewardsPanel';
import { VMCollector } from '../VM/Game/VMCollector';
import { VMSettingPanel } from '../VM/UI/VMSettingPanel';
import { PropAnim } from '../Anim/PropAnim';
import { PROP_TYEP, VMPropGetPanel } from '../VM/UI/VMPropGetPanel';
import { PlayerData } from '../Model/PlayerData';
import { VMGetMoneyPanel } from '../VM/UI/VMGetMoneyPanel';
import { VMTaskPanel } from '../VM/UI/VMTaskPanel';
import { TASK_TYPE, TaskManager } from '../Manager/TaskManager';
import { AUDIOS_PATH } from '../Common/AudioConfig';
import AudioTool from '../Common/AudioTool';
const { ccclass, property } = _decorator;

@ccclass('GameSceneController')
export class GameSceneController extends Component {

    private static _ins: GameSceneController = null;
    public static get ins(): GameSceneController {
        return GameSceneController._ins;
    }
    public static set ins(value: GameSceneController) {
        GameSceneController._ins = value;
    }

    @property(Sprite)
    public gameBg: Sprite = null;
    @property(VMTopUI)
    public topUI: VMTopUI = null; // 顶部ui
    @property(VMBottomUI)
    public bottomUI: VMBottomUI = null; // 底部ui
    @property(VMMap)
    private gameMap: VMMap = null; // 地图
    @property(VMCollector)
    private collector: VMCollector = null; // 收集器
    @property(Node)
    private popup: Node = null;
    @property(Node)
    private wabao: Node = null;

    private gameSucessPanel: VMSucessPanel = null;
    private gameFailPanel: VMFailPanel = null;
    private gameSettingPanel: VMSettingPanel = null;
    private gamePropGetPanel: VMPropGetPanel = null;
    private gameGetMoneyPanel: VMGetMoneyPanel = null;

    private refreshLock: boolean = false;
    private hintLock: boolean = false;

    private currGameBgIndex: number = 0;

    protected onLoad(): void {
        GameSceneController.ins = this;
        this.changeGameBg(PlayerData.ins.currSelectedBg);
    }

    protected onEnable(): void {
        getEventEmiter().on(CUSTOM_EVENT_NAME.GAME_SUCESS, this.onGameSucess, this)
        getEventEmiter().on(CUSTOM_EVENT_NAME.GAME_FAIL, this.onGameFail, this)
        getEventEmiter().on(CUSTOM_EVENT_NAME.GAME_GET_MONEY, this.onGetMoney, this)
        getEventEmiter().on(CUSTOM_EVENT_NAME.GAME_CHANGE_BG, this.onChangeGameBg, this)
    }

    protected start(): void {
        this.gameStart();
    }

    protected onDisable(): void {
        getEventEmiter().off(CUSTOM_EVENT_NAME.GAME_SUCESS, this.onGameSucess, this)
        getEventEmiter().off(CUSTOM_EVENT_NAME.GAME_FAIL, this.onGameFail, this)
        getEventEmiter().off(CUSTOM_EVENT_NAME.GAME_GET_MONEY, this.onGetMoney, this)
        getEventEmiter().off(CUSTOM_EVENT_NAME.GAME_CHANGE_BG, this.onChangeGameBg, this)
    }

    public async changeGameBg(index: number) {
        this.currGameBgIndex = index;
        let path = `${GAMEBG_PATH_PRE}/bg_${index + 1}/spriteFrame`;
        let bg = AssetsHelper.get<SpriteFrame>(path);
        if (!bg) {
            await AssetsHelper.load([path], SpriteFrame);
            bg = AssetsHelper.get<SpriteFrame>(path);
        }
        if (this.gameBg && this.currGameBgIndex == index) {
            this.gameBg.spriteFrame = bg;
        }
    }

    /**
     * 游戏开始
     *
     * @memberof GameSceneController
     */
    public gameStart() {
        CCLog.log("Game is Start");
        this.topUI.begin();
        this.bottomUI.begin();
        this.scheduleOnce(() => {
            this.gameMap.begin();
        }, AREA_MOVE_DURATION);
    }

    public gameRestart() {
        CCLog.log("Game is Restart");
        this.topUI.reBegin();
        this.bottomUI.reBegin();
        this.collector.clear();
        this.gameMap.clearMap();
        this.scheduleOnce(() => {
            this.gameMap.begin();
        }, AREA_MOVE_DURATION * 2);
    }

    public onGameSucess() {
        console.log("onGameSucess");
        TaskManager.ins.checkTask(TASK_TYPE.PASS);
        this.showSucessPanel();
    }

    public onGameFail() {
        this.showFailPanel();
    }

    public onGetMoney() {
        this.showGetMoneyPanel();
    }

    public onChangeGameBg(index: number) {
        this.changeGameBg(index);
    }

    public onSettingClick() {

    }

    public showSucessPanel() {
        if (!this.gameSucessPanel) {
            let panel: Prefab = AssetsHelper.get<Prefab>(PRELOAD_PREFABS.SucessPanel);
            if (panel) {
                this.gameSucessPanel = instantiate(panel).getComponent(VMSucessPanel);
                this.gameSucessPanel.node.parent = this.popup;
            }
        }
        this.gameSucessPanel.show();
    }

    public showFailPanel() {
        if (!this.gameFailPanel) {
            let panel: Prefab = AssetsHelper.get<Prefab>(PRELOAD_PREFABS.FailPanel);
            if (panel) {
                this.gameFailPanel = instantiate(panel).getComponent(VMFailPanel);
                this.gameFailPanel.node.parent = this.popup;
            }
        }
        this.gameFailPanel.show();
    }

    public showGetMoneyPanel() {
        if (!this.gameGetMoneyPanel) {
            let panel: Prefab = AssetsHelper.get<Prefab>(PRELOAD_PREFABS.GetMoneyPanel);
            if (panel) {
                this.gameGetMoneyPanel = instantiate(panel).getComponent(VMGetMoneyPanel);
                this.gameGetMoneyPanel.node.parent = this.popup;
            }
        }
        this.gameGetMoneyPanel.show();
    }

    public showSettingPanel() {
        if (!this.gameSettingPanel) {
            let panel: Prefab = AssetsHelper.get<Prefab>(PRELOAD_PREFABS.SettingPanel);
            if (panel) {
                this.gameSettingPanel = instantiate(panel).getComponent(VMSettingPanel);
                this.gameSettingPanel.node.parent = this.popup;
            }
        }
        this.gameSettingPanel.show();
    }

    public async showRewardPanel() {
        let panel: Prefab = AssetsHelper.get<Prefab>(PRELOAD_PREFABS.RewardsPanel);
        let rewardsPanel = instantiate(panel).getComponent(VMRewardsPanel);
        rewardsPanel.node.parent = this.popup;
        AudioTool.sound(AUDIOS_PATH.flyprop)
        return rewardsPanel.promise();
    }

    public showPropGetPanel(type: PROP_TYEP): VMPropGetPanel {
        if (!this.gamePropGetPanel) {
            let panel: Prefab = AssetsHelper.get<Prefab>(PRELOAD_PREFABS.PropGetPanel);
            if (panel) {
                this.gamePropGetPanel = instantiate(panel).getComponent(VMPropGetPanel);
                this.gamePropGetPanel.node.parent = this.popup;
            }
        }
        this.gamePropGetPanel.show(type);
        return this.gamePropGetPanel;
    }

    public showTaskPanel() {
        let panel: Prefab = AssetsHelper.get<Prefab>(PRELOAD_PREFABS.TaskPanel);
        if (panel) {
            let taskPanel = instantiate(panel).getComponent(VMTaskPanel);
            taskPanel.node.parent = this.popup;
            taskPanel.show();
        }
    }

    public async showWabaoGamePanel() {
        this.wabao.active = true;
        let panel: Prefab = AssetsHelper.get<Prefab>(PREFABS_WABAO.Main);
        if (!panel) {
            await AssetsHelper.load([PREFABS_WABAO.Main], Prefab, null, "wabao_game");
            panel = AssetsHelper.get<Prefab>(PREFABS_WABAO.Main, "wabao_game");
        }
        let wabaoPanel = instantiate(panel)
        wabaoPanel.parent = this.wabao;
    }

    public async createPropGetAnim(type: PROP_TYEP) {
        let propAnim: PropAnim = null;
        let anim: Prefab = AssetsHelper.get<Prefab>(PRELOAD_PREFABS.PropAnim);
        if (anim) {
            propAnim = instantiate(anim).getComponent(PropAnim);
            propAnim.node.parent = this.popup;
        }
        switch (type) {
            case PROP_TYEP.REFRESH:
                if (propAnim) {
                    await propAnim.startRefreshPropAnim(this.bottomUI.refreshBtn);
                }
                break;
            case PROP_TYEP.HINT:
                if (propAnim) {
                    await propAnim.startHintPropAnim(this.bottomUI.hintBtn);
                }
                break;
        }
    }

    async onGameRefresh() {
        if (this.refreshLock) {
            return;
        }
        this.refreshLock = true;
        let isPropEnough: boolean = PlayerData.ins.isPropEnough(PROP_TYEP.REFRESH);
        if (isPropEnough) {
            // 减少数量
            PlayerData.ins.reducePropNum(PROP_TYEP.REFRESH, 1);
            await this.gameMap.refresh();
            this.refreshLock = false;
        } else {
            await this.showPropGetPanel(PROP_TYEP.REFRESH).promiseGetAnim();
            this.refreshLock = false;
        }
    }

    async onGameHint() {
        let isPropEnough: boolean = PlayerData.ins.isPropEnough(PROP_TYEP.HINT);
        if (isPropEnough) {
            // 减少数量
            if (this.gameMap.hint()) {
                PlayerData.ins.reducePropNum(PROP_TYEP.HINT, 1);
            }

        } else {
            if (this.hintLock) {
                return;
            }
            this.hintLock = true;
            await this.showPropGetPanel(PROP_TYEP.HINT).promiseGetAnim();
            this.hintLock = false;
        }
    }

    onGameRecall() {
        this.gameMap.recall();
    }
}


