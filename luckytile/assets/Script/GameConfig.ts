

export enum RewardType {
    COINS = 1,
    MONEY = 2,
}


export class GameConfig {
    // 挖宝
    public static rewardType: RewardType = RewardType.MONEY; // 奖励类型 金币&现金
}


