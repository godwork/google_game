import { Vec3, Node, UITransform, director } from "cc";


export class Wait {

    private reject: () => void = null;

    public second(sec: number) {
        return new Promise<void>((resolve, reject) => {
            this.reject = reject;
            setTimeout(() => {
                resolve();
            }, sec * 1000);
        });
    }

    public abort() {
        this.reject && this.reject();
    }
}


export class Utils {
    /**
     * 将世界坐标转到转节点坐标
     *
     * @static
     * @param {Node} target 目标节点
     * @param {Vec3} worldPosition 世界坐标
     * @param {Vec3} [out] 返回
     * @return {*} 
     * @memberof Utils
     */
    public static convertToNodeSpaceAR(target: Node, worldPosition: Vec3, out?: Vec3): Vec3 {
        return target.getComponent(UITransform).convertToNodeSpaceAR(worldPosition, out);
    }

    /**
     * 改变父节点
     *
     * @static
     * @param {Node} child 子节点
     * @param {Node} parent 新的父节点
     * @param {boolean} [notChangePosInWorld=true] 是否改变世界坐标
     * @memberof Utils
     */
    public static changeParent(child: Node, parent: Node, changePosInWorld: boolean = false) {
        if (changePosInWorld) {
            child.parent = parent;
        } else {
            let pos = child.worldPosition;
            child.parent = parent;
            child.worldPosition = pos;
        }
    }

    /**
     * 随机的整数
     *
     * @static
     * @param {number} min
     * @param {number} max
     * @return {*}  {number}
     * @memberof Utils
     */
    public static randomInt(min: number, max: number): number {
        const random = Math.floor(Math.random() * (max - min + 1) + min);
        return random;
    }

    /**
     * 保留两位小数 后面的去了
     *
     * @static
     * @param {number} num
     * @param {number} [bit=2]
     * @return {*} 
     * @memberof Utils
     */
    public static toDecimal(num: number, bit: number = 2) {
        let pow = Math.pow(10, bit)
        return Number((Math.floor(num * pow) / pow).toFixed(bit))
    }

    public static toPath(obj: Object, key: string, ...args: string[]) {
        for (let i = 0; i < args.length; i++) {
            obj[key] = obj[key].replace(`[${i}]`, args[i]);
        }
    }
}


