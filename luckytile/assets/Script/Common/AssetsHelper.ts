import { Asset, AssetManager, assetManager, error, resources } from "cc";
import { Wait } from "./Utils";

const defaultBundle = "resources"

export class AssetsHelper {
    // bundlehelper缓存
    private static assetsBundleCache: Map<string, BundleHelper> = new Map<string, BundleHelper>();

    /**
     * 从bundle中加载资源
     * 默认从resources中加载资源
     *
     * @static
     * @param {string[]} paths 资源路径列表
     * @param {typeof Asset} type 资源类型
     * @param {string} [bundlePath=defaultBundle] bundle路径 默认为resources
     * @memberof AssetsHelper 
     */
    public static async load(paths: string[], type: typeof Asset, onProgress?: (finished: number, total: number, item: AssetManager.RequestItem) => void, bundlePath: string = defaultBundle) {
        let bundleHelper: BundleHelper = this.assetsBundleCache.get(bundlePath);
        if (!bundleHelper) {
            this.assetsBundleCache.set(bundlePath, bundleHelper = new BundleHelper(bundlePath));
            await bundleHelper.init();
        } else {
            if (bundleHelper.isPromise()) {
                await bundleHelper.promise();
            }
        }
        await bundleHelper.loadAsset(paths, type, onProgress);
    }

    /**
     * 获取资源 
     * 此方法需要先加载资源 才可获取到
     *
     * @static
     * @template T
     * @param {string} path 资源路径
     * @param {string} [bundlePath=defaultBundle] bundle路径 默认为resources
     * @return {*}  {(T | null)}
     * @memberof AssetsHelper
     */
    public static get<T extends Asset>(path: string, bundlePath: string = defaultBundle): T | null {
        let bundle: BundleHelper = this.getBundle(bundlePath);
        if (!bundle) {
            return null;
        }
        return bundle.getAsset<T>(path);
    }

    public static getBundle(bundlePath: string) {
        return this.assetsBundleCache.get(bundlePath);
    }

    public static logCache() {
        console.log(this.assetsBundleCache);
    }
}

class BundleHelper {
    // 资源包路径
    private bundlePath: string = null;
    private bundleInstance: AssetManager.Bundle = null;
    // 包中资源缓存
    private assetsCache: Map<string, Asset> = new Map<string, Asset>();

    private _Promise: Promise<BundleHelper> = null;

    constructor(path: string) {
        this.bundlePath = path;
    }

    public isPromise(): boolean {
        return !!this._Promise;
    }

    public async promise(): Promise<BundleHelper> {
        return await this._Promise;
    }

    public async init(options: { [k: string]: any; version?: string; } | null = null): Promise<BundleHelper> {
        this._Promise = this._init(options);
        await this._Promise;
        this._Promise = null;
        return this;
    }

    /**
     * 初始化
     *
     * @param {({ [k: string]: any; version?: string; } | null)} [options=null]
     * @return {*}  {Promise<BundleHelper>}
     * @memberof BundleHelper
     */
    public async _init(options: { [k: string]: any; version?: string; } | null = null): Promise<BundleHelper> {
        switch (this.bundlePath) {
            case "resources":
                this.bundleInstance = resources;
                break;
            default:
                this.bundleInstance = await this.loadBundle(options);
                break;
        }
        return this;
    }

    /**
     * 加载bundle
     *
     * @private
     * @param {({ [k: string]: any; version?: string; } | null)} options
     * @return {*}  {Promise<AssetManager.Bundle>}
     * @memberof BundleHelper
     */
    private async loadBundle(options: { [k: string]: any; version?: string; } | null): Promise<AssetManager.Bundle> {
        return new Promise((resolve, reject) => {
            assetManager.loadBundle(this.bundlePath, options, (err: Error, bundle: AssetManager.Bundle) => {
                if (err) {
                    error(`加载bundle失败, url: ${this.bundlePath}, err:${err}`);
                    resolve(null);
                } else {
                    resolve(bundle);
                }
            });
        });
    }

    /**
     * 加载资源
     *
     * @param {string[]} paths 资源路径列表
     * @param {typeof Asset} type 资源类型
     * @memberof BundleHelper
     */
    public async loadAsset(paths: string[], type: typeof Asset, onProgress?: (finished: number, total: number, item: AssetManager.RequestItem) => void) {
        // 加载资源
        let assets: Asset[] = await new Promise((resolve, reject) => {
            this.bundleInstance.load(paths, type, onProgress, (err, assets: Asset[]) => {
                if (err) {
                    error(`加载asset失败, err: ${err}`);
                    resolve(null);
                } else {
                    this.addRef(assets);
                    resolve(assets);
                }
            });
        });
        // 缓存资源
        for (let i = 0; i < assets.length; i++) {
            this.assetsCache.set(paths[i], assets[i]);
        }
    }

    /**
     * 获取资源
     *
     * @template T
     * @param {string} path 资源路径
     * @return {*}  {(T | null)}
     * @memberof BundleHelper
     */
    public getAsset<T extends Asset>(path: string): T | null {
        let asset: Asset = this.assetsCache.get(path);
        if (!asset) {
            return null;
        }
        return asset as T;
    }

    /** 增加引用计数 */
    private addRef(assets: Asset | Asset[]) {
        if (assets instanceof Array) {
            for (const a of assets) {
                a.addRef();
            }
        } else {
            assets.addRef();
        }
    }
    /** 减少引用计数, 当引用计数减少到0时,会自动销毁 */
    private decRef(assets: Asset | Asset[]) {
        if (assets instanceof Array) {
            for (const a of assets) {
                a.decRef();
            }
        } else {
            assets.decRef();
        }
    }
}

