import { Vec3 } from "cc";

/**方块皮肤路径前缀 */
export const SKIN_PATH_PRE: string = 'grid_skin';

/**配置文件前缀 */
export const CONFIG_PATH_PRE: string = 'config';

/**游戏背景图标前缀 */
export const GAMEBG_ICON_PATH_PRE: string = 'bg_icon';

/**游戏背景前缀 */
export const GAMEBG_PATH_PRE: string = 'game_bg';

/**关卡配置文件前缀 */
export const LEVLE_PATH_PRE: string = 'Level';

/**默认皮肤配置组 */
export const SKIN_GROUP_DEFAULT: string = '1';

/**上层比下层碰撞体tag属性增加的值 */
export const BLOCK_TAG_ADDITION: number = 1000;

/**触摸体的碰撞体tag值 */
export const TOUCHER_COLLIDER_TAG: number = 7777;

/**引导层的层级 */
export const GUIDE_LAYER_ZINDEX: number = 100;

/**引导时块的层级 */
export const GUIDE_BLOCK_ZINDEX: number = 110;

/**收集块时的块的层级 */
export const BLOCK_COLLECT_ZINDEX: number = 120;

/**收集区域的层级 */
export const CATCHER_ZINDEX: number = 110;

/**收集块的缓动速度 */
export const BLOCK_COLLECT_SPEED: number = 1200;

/**相同块达到这么数量就消除 */
export const BLOCK_ELIMINATE_LIMIT: number = 3;

/**右上角宝箱奖励的倒计时长 */
export const TIME_BOX_DURATION: number = 300;

/**通关宝箱奖励星星累计数量 */
export const LEVEL_REWARD_SRARS_LIMIT: number = 10;

/**体力上限 */
export const LIFE_MAX: number = 5;

/**一次视频奖励的体力数量 */
export const LIFE_REWARD_ONCE_VIDEO: number = 3;

/**恢复体力时长 */
export const LIFT_RECOVER_DURATION = 600;

/**上下区域动画时间 */
export const AREA_MOVE_DURATION: number = 0.2;

/**没有操作超过这个时间就提示道具使用 */
export const PROP_TIP_SHOW_AFTER: number = 5;

/**没有操作超过这个时间就更换提示道具 */
export const PROP_TIP_CHANGE_AFTER: number = 2;

/**最大道具数量 */
export const MAX_PROP_NUM = 3;

/**已加载的皮肤的默认值 */
// export const SKIN_LOADED_DEFAULT: ISkinLoaded = { blockSkin: {} };

/**场景名称 */
export const SCENES_NAME = {
    /** 游戏页面 */
    GameScene: 'Scenes/GameScene',
}

export const PRELOAD_SP = {
    coinsIcon: "icon/coin/spriteFrame",
    moneyIcon: "icon/money/spriteFrame",
    coinsSkinIcon: "icon/100/spriteFrame",
    moneySkinIcon: "icon/Blockmoney/spriteFrame",
    IDMoneyIcon: "icon/IDmoney/spriteFrame",
    BRMoneyIcon: "icon/BRmoney/spriteFrame",
    PKMoneyIcon: "icon/BRmoney/spriteFrame",
    RUMoneyIcon: "icon/RUmoney/spriteFrame",
    defaultTaskIcon: "bg_icon/bg_icon0/spriteFrame",
    gameBg: "game_bg/bg_[0]/spriteFrame",
}

export const PRELOAD_PREFABS = {
    SucessPanel: "prefabs/ui/SucessPanel",
    FailPanel: "prefabs/ui/FailPanel",
    SettingPanel: "prefabs/ui/SettingPanel",
    RewardsPanel: "prefabs/ui/RewardsPanel",
    PropGetPanel: "prefabs/ui/PropGetPanel",
    GetMoneyPanel: "prefabs/ui/GetMoneyPanel",
    TaskPanel: "prefabs/ui/TaskPanel",
    TaskDetailPanel: "prefabs/ui/TaskDetailPanel",
    Toast: "prefabs/ui/toast/Toast",
    PropAnim: "prefabs/anim/PropAnim",
}

export const PREFABS_WABAO = {
    GetRewardPanel: "prefabs/GetRewardPanel",
    GetMoreTimesPanel: "prefabs/GetMoreTimesPanel",
    Main: "prefabs/Main",
}

/** 自定义事件名称 */
export const CUSTOM_EVENT_NAME = {
    GAME_START: 'GAME_START',
    GAME_SUCESS: 'GAME_SUCESS',
    GAME_FAIL: 'GAME_FAIL',
    GAME_GET_MONEY: 'GAME_GET_MONEY',
    GAME_CHANGE_BG: 'GAME_CHANGE_BG',
}

export const DATA_UPDATE = {
    PROP_NUM: "DATA_UPDATE_PROP_NUM", // 道具数量更新
    PASS: "DATA_UPDATE_PASS", // 关卡数据更新
    MONEY: "DATA_UPDATE_MONEY", // 现金数量更新
    WABAO_TIMES: "DATA_UPDATE_WABAO_TIMES", // 挖宝次数更新
    CAN_SUPPLEMEN_TIMES: "DATA_UPDATE_CAN_SUPPLEMENT_TIMES" // 可补充次数更新
}

/**道具/体力 的类型， */
export enum PROP_BUTTON_TYPE {
    recall = 0,
    refresh = 1,
    hint = 2,
    life = 3,
}

/**方格宽度 */
export const GRID_WIDTH: number = 85;
/**方格高度 */
export const GRID_HEIGHT: number = 84;

export enum GRID_STATE {
    NORMAL = 0,
    COVER = 1,
    COLLECT = 2,
    ELIMINATE = 3,
    BACK = 4,
}

export enum GRID_ACTION {
    TAG_MOVE = 100,
}