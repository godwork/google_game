import { Sprite, SpriteFrame, error, isValid, sys } from "cc"
import CocosTileHelper from "./CocosHelper"
import { JsbTileTrileTileCallMgr } from "./JsbMgr"
import { JSB } from "cc/env"
import { PRELOAD_SP } from "./Constant"
import { AssetsHelper } from "./AssetsHelper"
import { GameInfoGetter } from "../Info/GameInfoGetter"

export enum LanguageType {
    English = "English",
    /**印尼语 */
    IN = "IN",
    /**葡萄牙 */
    PT = "PT",
    /**葡萄牙 */
    RU = "RU"
}

export enum Country {
    /**印尼 */
    ID = "id",
    /**巴西 */
    BR = "br",
    /**巴基斯坦 */
    PK = "pk",
    /** 英语*/
    EN = "en",
    /** 俄罗斯*/
    RU = "ru",
}

const ENMoney = "icon/money/spriteFrame"
const IDMoney = "icon/IDmoney/spriteFrame"
const BRMoney = "icon/BRmoney/spriteFrame"
const PKMoney = "icon/BRmoney/spriteFrame"
const RUMoney = "icon/RUmoney/spriteFrame"

const ENMoreMoney = "icon/Moremoney/spriteFrame"
const IDMoreMoney = "icon/IDMoremoney/spriteFrame"
const BRMoreMoney = "icon/BRMoremoney/spriteFrame"
const PKMoreMoney = "icon/BRMoremoney/spriteFrame"
const RUMoreMoney = "icon/RUMoremoney/spriteFrame"

const ENBlockMoney = "icon/Blockmoney/spriteFrame"
const IDBlockMoney = "icon/IDBlockmoney/spriteFrame"
const BRBlockMoney = "icon/BRBlockmoney/spriteFrame"
const PKBlockMoney = "icon/BRBlockmoney/spriteFrame"
const RUBlockMoney = "icon/RUBlockmoney/spriteFrame"

const ENLargeMoney = "icon/pic_meiguo/spriteFrame"
const IDLargeMoney = "icon/pic_inni/spriteFrame"
const BRLargeMoney = "icon/picbaxi/spriteFrame"
const RULargeMoney = "icon/pic_eluso/spriteFrame"

export default class LanguagetileTile {
    private static _country = null;

    static getWord(key, parameter1?, parameter2?) {
        let LanguageArr: { [key: string]: { English: string, IN: string, PT: string, RU: string } } = {
            Loading: {
                English: "loading",
                IN: "memuat",
                PT: "Carregando",
                RU: "Загрузка"
            },
            SaveInWallet: {
                English: "Save in wallet",
                IN: "Simpan di dompet",
                PT: "Salvar na bolsa",
                RU: "Получить"
            },
            Open: {
                English: "Open",
                IN: "Buka",
                PT: "abrir",
                RU: "Открыть"
            },
            Restart: {
                English: "Restart",
                IN: "mengulang kembali",
                PT: "reiniciar",
                RU: "Перезапустить"
            },
            TermOfUse: {
                English: "Term Of Use",
                IN: "Jangka waktu penggunaan",
                PT: "Termo de utilização",
                RU: "Условияиспользования"
            },
            PrivacyPolicy: {
                English: "Privacy Policy",
                IN: "Kebijakan pribadi",
                PT: "Acordo de Privacidade",
                RU: "Политикаконфиденциальности"
            },
            LoadingDes: {
                English: "Google is not a sponsor nor is involved in any way with these contescs and sweepstakes.",
                IN: "Google bukan sponsor kontes dan undian ini, juga tidak terlibat dalamnya cara apa pun",
                PT: "Google não é um patrocinador nem está envolvido de forma alguma com esses concursos e sorteios.",
                RU: "Googleнеявляетсяспонсороминесвязанникакимобразомсэтимиконкурсамиирозыгрышами."
            },
            continue: {
                English: "continue",
                IN: "melanjutkan",
                PT: "Próximo nível",
                RU: "Продолжить"
            },
            Level: {
                English: "level",
                IN: "Tingkat",
                PT: "Nível",
                RU: "Уровень"
            },
            Props: {
                English: "Props",
                IN: "Perlengkapan",
                PT: "Itens",
                RU: "Предметы"
            },
            Free: {
                English: "FREE",
                IN: "Gratis",
                PT: "Grátis",
                RU: "Бесплатно"
            },
            GameFailed: {
                English: "Game failed",
                IN: "Game gagal",
                PT: "falhou o jogo",
                RU: "Играпровалена"
            },
            GameVictory: {
                English: "Game victory",
                IN: "Kemenangan game",
                PT: "Vitória do jogo",
                RU: "Победавигре"
            },
            LostRewards: {
                English: "Lost rewards",
                IN: "Kehilangan hadiah",
                PT: "perder recompensas",
                RU: "Потерянныенаграды"
            },
            NetReqTimeOut: {
                English: "request time out",
                IN: "Meminta waktu",
                PT: "Tempo limite do pedido",
                RU: "Тайм-аутзапроса"
            },
            NetFail: {
                English: "Net Fail",
                IN: "Kesalahan jaringan",
                PT: "erro de rede",
                RU: "Сбойсети"
            },
            GetRewards: {
                English: "Get rewards",
                IN: "Dapatkan hadiah",
                PT: "ganhar recompensas",
                RU: "Получитьнаграды"
            },
            ADNotLoadPlayerLaterRetry: {
                English: "AD is not ready,please try again later",
                IN: "Iklan telah gagal, coba lagi nanti",
                PT: "O carregamento da publicidade falhou",
                RU: "Рекламанеготова,пожалуйста,попробуйтепозже"
            },
            ADTooPFxSRetry: {
                English: `AD too frequently,Please try again in ${parameter1} seconds`,
                IN: `Iklannya sering terjadi, silakan coba lagi setelah ${parameter1}  detik`,
                PT: `Os anúncios são muito frequentes. Tente novamente em ${parameter1} segundos`,
                RU: `Рекламаслишкомчасто,пожалуйста,попробуйтесновачерез${parameter1}секунд`
            },
            Rewards: {
                English: "rewards",
                IN: "Hadiah",
                PT: "recompensas",
                RU: "Награды"
            },
            Next: {
                English: "NEXT",
                IN: "Berikutnya",
                PT: "Próximo nível",
                RU: "Далее"
            },
            Collect: {
                English: "Collect",
                IN: "Menerima",
                PT: "Receber",
                RU: "Собрать"
            },
            CongrAtulations: {
                English: "CONGR ATULATIONS!",
                IN: "SELAMAT!",
                PT: "Parabéns!",
                RU: "ПОЗДРАВЛЯЕМ!"
            },
            NewbieActivities: {
                English: "NEWBIE ACTIVITIES",
                IN: "AKTIVITAS PEMULA BARU",
                PT: "ACTIVIDADES PARA NOVATOS",
                RU: "НОВИЧОКАКТИВНОСТИ"
            },
            YCGAGRBETGC: {
                English: "You can get a gold reward by eliminating three gold coins",
                IN: "Hadiah emas bisa Anda dapatkan dengan tiga koin emas dihilangkan",
                PT: "Você pode ganhar uma recompensa de ouro através de eliminar três moedas de ouro",
                RU: "Выможетеполучитьнаградувзолоте,уничтоживтризолотыхмонеты"
            },
            TRYCWI: {
                English: `To reach ${LanguagetileTile.MoneyChar}${parameter1}, you can withdraw inmediately`,
                IN: `Anda dapat langsung menarik ketika ${LanguagetileTile.MoneyChar}${parameter1} dicapai`,
                PT: `Ao atingir a ${LanguagetileTile.MoneyChar}${parameter1} (dólar americano), você pode retirar dinheiro imediatamente`,
                RU: `Чтобыдостичь ${LanguagetileTile.MoneyChar}${parameter1}, выможетевывестиденьгинемедленно`
            },
            YHACTGCR: {
                English: "Claim your cash reward!",
                IN: "Tanyakan hadiah tunai Anda!",
                PT: "Reclamar sua recompensa em dinheiro!",
                RU: "Запроситесвоюденежнуюнаграду!"
            },
            YGACTIR: {
                English: "Claim your cash reward!",
                IN: "Tanyakan hadiah tunai Anda!",
                PT: "Reclamar sua recompensa em dinheiro!",
                RU: "Запроситесвоюденежнуюнаграду!"
            },
            Random: {
                English: "Random",
                IN: "Acak",
                PT: "aleatório",
                RU: "Случайно"
            },
            NoThanks: {
                English: "No Thanks",
                IN: "Tidak, terima kasih.",
                PT: "Não, obrigado.",
                RU: "Нет,спасибо"
            },
            CTTTSITVTTDA: {
                English: "Click to transfer the squares in the venue to the display bar, and the three identical squares will be eliminated.",
                IN: "Klik untuk mentransfer kotak di venue ke bilah tampilan, dan tiga kotak identik akan dihilangkan",
                PT: "Clique para transferir os blocos no local para a barra de exibição, e os três blocos idênticos serão eliminados.",
                RU: "Нажмите,чтобыперенестиквадратынаплощадкенапанельотображения,итриодинаковыхквадратабудутустранены."
            },
            CHTWM: {
                English: "click here to withdraw money",
                IN: "Klik di sini untuk menarik uang",
                PT: "Clique aqui para retirar dinheiro",
                RU: "Нажмитездесь,чтобывывестиденьги"
            },
            YCUGTC: {
                English: "You can use gold to cash!",
                IN: "Anda dapat menggunakan emas untuk uang tunai!",
                PT: "Podes cambiar moeda de ouro para dinheiro!",
                RU: "Выможетеиспользоватьзолотодляполученияденег!"
            },
            taskUnFinish: {
                English: "The task is not finished",
                IN: "Tugas belum selesai",
                PT: "A tarefa ainda não foi completada",
                RU: "Задачаневыполнена"
            },
            THEMES: {
                English: "THEMES",
                IN: "TEMA",
                PT: "TEMAS",
                RU: "ТЕМЫ"
            },
            PhoneTitle: {
                English: "Collect game themes and redeem Iphone14",
                IN: "Kumpulkan tema permainan dan menebus Iphone14",
                PT: "Colete o tema do jogo e resgate o Iphone14",
                RU: "СоберитеигровыетемыиобменяйтеихнаIphone14"
            },
            REDEEM: {
                English: "REDEEM",
                IN: "MENUKARKAN",
                PT: "CAMBIAR",
                RU: "ИСПОЛЬЗОВАТЬ"
            },
            NEW: {
                English: "NEW",
                IN: "NEW",
                PT: "NOVO",
                RU: "НОВЫЙ"
            },
            Progress: {
                English: "Progress:",
                IN: "Kemajuan:",
                PT: "Agendar:",
                RU: "Прогресс:"
            },
            CompleteMorelevels: {
                English: "Complete # more levels",
                IN: "Selesaikan # tingkat lagi",
                PT: "Completar mais # níveis",
                RU: "Завершите # уровней"
            },
            CompleteMoreVideos: {
                English: "Watch # videos",
                IN: "Menonton # video",
                PT: "Assistir # vídeos",
                RU: "Смотрите # видео"
            },
            receiving: {
                English: "on receiving",
                IN: "berhasil menarik",
                PT: "por receber",
                RU: "пополучению"
            },
            CongratulationsPlayer: {
                English: "Congratulations to player",
                IN: "Selamat kepada pemain",
                PT: "Parabéns ao jogador",
                RU: "Поздравляемигрока"
            },
            winMoney: {
                English: "$200",
                IN: "Rp3500k",
                PT: "R$2000",
                RU: "₽20000"
            },
            retry: {
                English: "Try again",
                IN: "Coba lagi",
                PT: "Novamente",
                RU: "Попробуйтеещераз"
            },
            networkerro: {
                English: "Network request timeout, please try again",
                IN: "Waktu kehabisan permintaan jaringan, silakan coba lagi",
                PT: "Tempo límite do pedido de rede, por favor tente novamente",
                RU: "Тайм-аутзапросасети,пожалуйста,попробуйтеещераз"
            },
            WabaoIcon: {
                English: "Treasure",
                IN: "Harta karun",
                PT: "Tesouro",
                RU: "Сокровище"
            },
            WabaoNeeddLv: {
                English: `Complete the  level ${parameter1} to unlock`,
                IN: `Selesaikan level ${parameter1} untuk membuka`,
                PT: `Complete o nível ${parameter1} para desbloquear`,
                RU: `Завершитеуровень ${parameter1} чтобыразблокировать`
            },
            WabaoLeftTimeStr: {
                English: "Number of excavations:",
                IN: "Jumlah penggalian:",
                PT: "Número de excavações:",
                RU: "Количествораскопок:"
            },
            NotEnught: {
                English: "More times",
                IN: "Lebih kali",
                PT: "Mais vezes",
                RU: "Большераз"
            },
            WabaoTodaySeeLeftTime: {
                English: "Today we can add:",
                IN: "Hari ini kita bisa menambahkan:",
                PT: "Hoje podemos acrescentar:",
                RU: "Сегоднямыможемдобавить:"
            },
            WabaoSeeAddTime: {
                English: `Add ${parameter1} times`,
                IN: `Tambah ${parameter1} kali`,
                PT: `Adicionar ${parameter1} vezes`,
                RU: `Добавьте ${parameter1} раз`
            },
            WabaoSeeAddTimePart1: {
                English: "Add",
                IN: "Tambah",
                PT: "Adicionar",
                RU: "Добавить"
            },
            WabaoSeeAddTimePart2: {
                English: " times",
                IN: " kali",
                PT: " vezes",
                RU: "раз"
            },
            WabaoVideoStr1: {
                English: "Watch videos or complete levels to supplement mining times",
                IN: "Lihat video atau tingkat lengkap untuk menambahkan waktu penambangan",
                PT: "Vejam vídeos ou níveis completos para suplementar tempos de mineração",
                RU: "Смотритевидеоилизавершитеуровни,чтобыдополнитьколичестворазработок"
            },
            WabaoVideoStr2: {
                English: "Complete levels to supplement excavation times",
                IN: "Tingkat lengkap untuk menambahkan waktu penggalian",
                PT: "Leves completos para suplementar os tempos de excavação",
                RU: "Завершитеуровни,чтобыдополнитьколичестворазработок"
            },
            WabaoVideoNone: {
                English: "We ran out of times today, please come back tomorrow",
                IN: "Kita kehabisan waktu hari ini, tolong kembali besok",
                PT: "Hoje acabamos os tempos, por favor volte amanhã",
                RU: "Сегодняунасзакончилисьразы,пожалуйста,вернитесьзавтра"
            },
            WabaoVideoSucc: {
                English: "Increased number of excavations",
                IN: "Jumlah penggalian meningkat",
                PT: "Aumento do número de excavações",
                RU: "Увеличеноколичествораскопок"
            },
            WabaohappyGet: {
                English: "Collect",
                IN: "Menerima",
                PT: "Receber",
                RU: "Собрать"
            },
            PicTip: {
                English: "98.2% of users can withdraw <outline color=#FFFFFF width=2><color=#FF0000>$10</color></outline><br/>within 5 edger minutes.",
                IN: "98.2% pengguna dapat menarik <outline color=#FFFFFF width=2><color=#FF0000>Rp100,000</color></outline><br/>dalam 5 menit.",
                PT: "98.2% dos usuários podem extrair <outline color=#FFFFFF width=2><color=#FF0000>R$50</color></outline><br/>dentro de 5 minutos",
                RU: "98.2%пользователеймогутвывестиденьгивтечение5крайнихминут."
            },
            PhoneBotomPic: {
                English: `<color=#FF0000><outline color=#FFFFFF width=2>${parameter1}</outline></color> users have already received IPhone14 today`,
                IN: `<color=#FF0000><outline color=#FFFFFF width=2>${parameter1}</outline></color> usuários já receberam o IPhone14 hoje`,
                PT: `<color=#FF0000><outline color=#FFFFFF width=2>${parameter1}</outline></color> pengguna telah menerima IPhone14 hari ini`,
                RU: `<color=#FF0000><outline color=#FFFFFF width=2>${parameter1}</outline></color>пользователейужеполучилиIPhone14сегодня.`
            },
            tanMustr: {
                English: `Congratulations to <color=#FF0000><outline color=#FFFFFF width=2>${parameter1}</outline></color> <br/>on receivingcash of <color=#FF0000><outline color=#FFFFFF width=2>${parameter2}</outline></color>`,
                IN: `Selamat <color=#FF0000><outline color=#FFFFFF width=2>${parameter1}</outline></color> untuk <br/>menarik <color=#FF0000><outline color=#FFFFFF width=2>${parameter2}</outline></color>`,
                PT: `Parabéns a <color=#FF0000><outline color=#FFFFFF width=2>${parameter1}</outline></color> por <br/>receber <color=#FF0000><outline color=#FFFFFF width=2>${parameter2}</outline></color>`,
                RU: `Поздравляем <color=#FF0000><outline color=#FFFFFF width=2>${parameter1}</outline></color> <br/>сполучением<color=#FF0000><outline color=#FFFFFF width=2>${parameter2}</outline></color>наличными.`
            },
            topcashTip: {
                English: `Withdraw cash at any time`,
                IN: `Menarik uang tunai kapan saja`,
                PT: `Retirar dinheiro a qualquer momento`,
                RU: `Взять наличные в любое время`
            },
            startTipBtn: {
                English: `Earn money`,
                IN: `Menghasilkan uang`,
                PT: `Ganhar dinheiro`,
                RU: `Зарабатывать деньги`
            },
            startTipCenter: {
                English: `Complete level <color=#FF0000><size=53>${parameter1}</size></color>\nwithdraw\nimmediately`,
                IN: `Selesaikan level <color=#FF0000><size=53>${parameter1}</size></color>\ntarik\nsegera`,
                PT: `Complete o nível <color=#FF0000><size=53>${parameter1}</size></color>\nsaque\nimediatamente`,
                RU: ` Завершите уровень \n<color=#FF0000><size=53>${parameter1}</size></color> сразу\nснимите`,
            },
            startTip2Center: {
                English: `Complete level ${parameter1} withdraw immediately`,
                IN: `Selesaikan level ${parameter1} tarik segera`,
                PT: `Complete o nível ${parameter1} saque imediatamente`,
                RU: `Завершите уровень ${parameter1} сразу снимите`
            },
            guideTixianTip: {
                English: `Click here to withdraw`,
                IN: `Clique aqui para retirar dinheiro`,
                PT: `Klik di sini untuk menarik uang`,
                RU: `Нажмите здесь, чтобы снять`
            },

        }
        let language = LanguagetileTile.localLanguage;

        if (LanguageArr[key]) {
            return LanguageArr[key][language];
        } else {
            error("[获取语言错误]", key)
            return ""
        }

    }

    static get localLanguage() {
        let type = "ru"
        if (sys.isNative) {
            type = JsbTileTrileTileCallMgr.handler("getLanguage", "()Ljava/lang/String;").toLowerCase()
        }

        if (type == "in") {
            return LanguageType.IN
        }
        else if (type == "pt") {
            return LanguageType.PT
        } else if (type == "ru") {
            return LanguageType.RU
        }
        else {
            return LanguageType.English
        }
    }

    static async setOneMoneyIcon(sp: Sprite) {
        try {
            let c = LanguagetileTile.getCountry
            let path = ENMoney;
            if (c == Country.BR) {
                path = BRMoney
            } else if (c == Country.PK) {
                path = PKMoney
            } else if (c == Country.RU) {
                path = RUMoney
            } else if (c == Country.ID) {
                path = IDMoney
            }
            let res: SpriteFrame = AssetsHelper.get<SpriteFrame>(path);
            if (isValid(sp)) {
                sp.spriteFrame = res;
            }
            // if (GameInfoGetter.easyVersion()) {
            //     sp.spriteFrame = AssetsHelper.get<SpriteFrame>("");
            // }
            return res
        } catch {
            return null;
        }
    }
    static async setLargeMoneyIcon(sp: Sprite) {
        try {
            let c = LanguagetileTile.getCountry
            let path = ENLargeMoney
            if (c == Country.BR) {
                path = BRLargeMoney
            } else if (c == Country.ID) {
                path = IDLargeMoney
            } else if (c == Country.RU) {
                path = RULargeMoney
            }
            let res: any = await CocosTileHelper.loadAssetSync(path, SpriteFrame)
            if (isValid(sp)) {
                sp.spriteFrame = res;
            }
        } catch { }
    }
    static async setMoreMoneyIcon(sp: Sprite) {
        try {
            let c = LanguagetileTile.getCountry
            let path = ENMoreMoney
            if (c == Country.BR) {
                path = BRMoreMoney
            } else if (c == Country.PK) {
                path = PKMoreMoney
            } else if (c == Country.ID) {
                path = IDMoreMoney
            } else if (c == Country.RU) {
                path = RUMoreMoney
            }
            let res: any = await CocosTileHelper.loadAssetSync(path, SpriteFrame)
            if (isValid(sp)) {
                sp.spriteFrame = res;
            }
        } catch { }
    }

    static async BlockMoneyIcon() {
        try {
            let c = LanguagetileTile.getCountry
            let path = ENBlockMoney
            if (c == Country.BR) {
                path = BRBlockMoney
            } else if (c == Country.PK) {
                path = PKBlockMoney
            } else if (c == Country.ID) {
                path = IDBlockMoney
            } else if (c == Country.RU) {
                path = RUBlockMoney
            }
            let res: SpriteFrame = await CocosTileHelper.loadAssetSync(path, SpriteFrame) as SpriteFrame
            return res
        } catch {

            return null;
        }
    }

    static get MoneyChar() {
        let c = LanguagetileTile.getCountry
        if (c == Country.BR) {
            return "R$"
        } else if (c == Country.PK) {
            return "P.Rs"
        } else if (c == Country.ID) {
            return "Rp"
        } else if (c == Country.RU) {
            return "₽"
        } else {
            return "$"
        }
    }

    static haveGetCountry = null
    static get getCountry() {
        if (!this._country) {
            let country = Country.EN;
            if (sys.isNative) {
                try {
                    if (!LanguagetileTile.haveGetCountry) {
                        let CommonParam = JsbTileTrileTileCallMgr.handler("getCommonParm", "()Ljava/lang/String;")
                        LanguagetileTile.haveGetCountry = JSON.parse(CommonParam).country.toLowerCase()
                    }
                    country = LanguagetileTile.haveGetCountry
                } catch {
                    country = Country.EN;
                }
            }
            let isfind = Object.values(Country).some((v) => v == country)
            if (isfind == false) {
                country = Country.EN
            }
            this._country = country;
        }
        return this._country;
    }
}