import { Asset, AssetManager, Canvas, Component, Node, Tween, assetManager, error, resources, tween } from "cc";

export class LoadProgress {
    public url: string;
    public completedCount: number;
    public totalCount: number;
    public item: any;
    public cb?: Function;
}

/** 一些cocos api 的封装, promise函数统一加上sync后缀 */
export default class CocosTileHelper {

    /** 加载进度 */
    public static loadProgress = new LoadProgress();

    /** 等待时间, 秒为单位 */
    public static sleepSync = function (target: Component, dur: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            target.scheduleOnce(() => {
                resolve(true);
            }, dur);
        });
    }

    /**
     * 
     * @param target 
     * @param repeat -1，表示永久执行
     * @param tweens 
     */
    public static async runRepeatTweenSync(target: any, repeat: number, ...tweens: Tween<Node>[]) {
        return new Promise((resolve, reject) => {
            let selfTween = tween(target);
            for (const tmpTween of tweens) {
                selfTween = selfTween.then(tmpTween);
            }
            if (repeat < 0) {
                tween(target).repeatForever(selfTween).start();
            } else {
                tween(target).repeat(repeat, selfTween).call(() => {
                    resolve(true);
                }).start();
            }
        });
    }
    /** 同步的tween */
    public static async runTweenSync(target: any, ...tweens: Tween<Node>[]): Promise<void> {
        return new Promise((resolve, reject) => {
            let selfTween = tween(target);
            for (const tmpTween of tweens) {
                selfTween = selfTween.then(tmpTween);
            }
            selfTween.call(() => {
                resolve();
            }).start();
        });
    }
    /** 停止tween */
    public stopTween(target: any) {
        Tween.stopAllByTarget(target);
    }
    public stopTweenByTag(tag: number) {
        Tween.stopAllByTag(tag);
    }
    // /** 同步的动作, 在2.4.x action已经被废弃了, 不建议使用 */
    // public static async runActionSync(node: Node, ...actions: FiniteTimeAction[]) {
    //     if (!actions || actions.length <= 0) return;
    //     return new Promise((resolve, reject) => {
    //         actions.push(callFunc(() => {
    //             resolve(true);
    //         }));
    //         node.runAction(sequence(actions));
    //     });
    // }

    // /** 同步的动画 */
    // public static async runAnimSync(node: Node, animName?: string | number) {
    //     let anim = node.getComponent(Animation);
    //     if (!anim) return;
    //     let clip: AnimationClip = null;
    //     if (!animName) clip = anim.defaultClip;
    //     else {
    //         let clips = anim.getClips();
    //         if (typeof (animName) === "number") {
    //             clip = clips[animName];
    //         } else if (typeof (animName) === "string") {
    //             for (let i = 0; i < clips.length; i++) {
    //                 if (clips[i].name === animName) {
    //                     clip = clips[i];
    //                     break;
    //                 }
    //             }
    //         }
    //     }
    //     if (!clip) return;
    //     await CocosTileHelper.sleepSync(clip.duration);
    // }

    /** 加载资源异常时抛出错误 */
    public static loadResThrowErrorSync<T>(url: string, type: typeof Asset, onProgress?: (completedCount: number, totalCount: number, item: any) => void): Promise<T> {
        return null;
    }

    private static _loadingMap: { [key: string]: Function[] } = {};
    public static loadRes<T>(url: string, type: typeof Asset, callback: Function) {
        if (this._loadingMap[url]) {
            this._loadingMap[url].push(callback);
            return;
        }
        this._loadingMap[url] = [callback];
        this.loadResSync<T>(url, type).then((data: any) => {
            let arr = this._loadingMap[url];
            for (const func of arr) {
                func(data);
            }
            this._loadingMap[url] = null;
            delete this._loadingMap[url];
        });
    }
    /** 加载资源 */
    public static loadResSync<T>(url: string, type: typeof Asset, onProgress?: (completedCount: number, totalCount: number, item: any) => void): Promise<T> {
        // log("loadResSync ==> url: " + url);
        return new Promise((resolve, reject) => {
            if (!onProgress) onProgress = this._onProgress;
            resources.load(url, type, onProgress, (err, asset: any) => {
                if (err) {
                    error(`${url} [资源加载] 错误 ${err}`);
                    resolve(null);
                } else {
                    resolve(asset);
                }
            });
        });
    }
    /** 
     * 加载进度
     * cb方法 其实目的是可以将loader方法的progress
     */
    private static _onProgress(completedCount: number, totalCount: number, item: any) {
        CocosTileHelper.loadProgress.completedCount = completedCount;
        CocosTileHelper.loadProgress.totalCount = totalCount;
        CocosTileHelper.loadProgress.item = item;
        CocosTileHelper.loadProgress.cb && CocosTileHelper.loadProgress.cb(completedCount, totalCount, item);
    }
    /**
     * 寻找子节点
     */
    public static findChildInNode(nodeName: string, rootNode: Node): Node {
        if (rootNode.name == nodeName) {
            return rootNode;
        }
        let rootChild: Node[] = rootNode.children;
        for (let i = 0; i < rootChild.length; i++) {
            let node = this.findChildInNode(nodeName, rootChild[i]);
            if (node) {
                return node;
            }
        }
        return null;
    }

    /** 获得Component的类名 */
    public static getComponentName(com: Function) {
        let arr = com.name.match(/<.*>$/);
        if (arr && arr.length > 0) {
            return arr[0].slice(1, -1);
        }
        return com.name;
    }
    /** 加载bundle */
    public static loadBundleSync(url: string, options: any): Promise<AssetManager.Bundle> {
        return new Promise((resolve, reject) => {
            assetManager.loadBundle(url, options, (err: Error, bundle: AssetManager.Bundle) => {
                if (!err) {
                    error(`加载bundle失败, url: ${url}, err:${err}`);
                    resolve(null);
                } else {
                    resolve(bundle);
                }
            });
        });
    }

    /** 路径是相对分包文件夹路径的相对路径 */
    public static loadAssetFromBundleSync(bundleName: string, url: string) {
        let bundle = assetManager.getBundle(bundleName);
        if (!bundle) {
            error(`加载bundle中的资源失败, 未找到bundle, bundleUrl:${bundleName}`);
            return null;
        }
        return new Promise((resolve, reject) => {
            bundle.load(url, (err, asset: Asset | Asset[]) => {
                if (err) {
                    error(`加载bundle中的资源失败, 未找到asset, url:${url}, err:${err}`);
                    resolve(null);
                } else {
                    resolve(asset);
                }
            });
        });
    }

    /** 通过路径加载资源, 如果这个资源在bundle内, 会先加载bundle, 在解开bundle获得对应的资源 */
    public static loadAssetSync(url: string, type: typeof Asset) {
        return new Promise((resolve, reject) => {
            resources.load(url, type, (err, assets: Asset | Asset[]) => {
                if (err) {
                    error(`加载asset失败, url:${url}, err: ${err}`);
                    resolve(null);
                } else {

                    this.addRef(assets);
                    resolve(assets);
                }
            });
        });
    }
    /** 释放资源 */
    public static releaseAsset(assets: Asset | Asset[]) {
        this.decRes(assets);
    }
    /** 增加引用计数 */
    private static addRef(assets: Asset | Asset[]) {
        if (assets instanceof Array) {
            for (const a of assets) {
                a.addRef();
            }
        } else {
            assets.addRef();
        }
    }
    /** 减少引用计数, 当引用计数减少到0时,会自动销毁 */
    private static decRes(assets: Asset | Asset[]) {
        if (assets instanceof Array) {
            for (const a of assets) {
                a.decRef();
            }
        } else {
            assets.decRef();
        }
    }
}

