import { AssetManager, AudioClip, AudioSource, assetManager, Node, director } from "cc";
import { AUDIOS_PATH } from "./AudioConfig";

class AudioTool {
    private openFlag: boolean = true;

    public audios: { [index: string]: AudioClip } = {};

    private AUDIO_SWITCH_KEY = "AUDIO_SWITCH_KEY"

    private _audioSource: AudioSource;

    public async initialize(): Promise<void> {
        let value: string = localStorage.getItem(this.AUDIO_SWITCH_KEY) || "1";
        this.openFlag = (value == "1");
        this.initAudioSource();
        await this.loadAudioRes();
        this.playMusic();
    }

    private initAudioSource() {
        let audioMgr = new Node();
        audioMgr.name = '__AudioMgr__';
        director.getScene().addChild(audioMgr);
        director.addPersistRootNode(audioMgr);
        this._audioSource = audioMgr.addComponent(AudioSource);
    }

    private async loadAudioRes() {
        const bundleAudios: AssetManager.Bundle = await new Promise((resolve: Function, reject: Function) => {
            assetManager.loadBundle('bundle_audios', (err: Error, bundle: AssetManager.Bundle) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(bundle);
                }
            });
        });

        for (let audioPath of Object.values(AUDIOS_PATH)) {
            this.audios[audioPath] = await new Promise((resolve: Function, reject: Function) => {
                bundleAudios.load(audioPath, AudioClip, (err: Error, res: AudioClip) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                });
            });
        }
    }

    public sound(path: string): void {
        if (this.isOpen() && path in this.audios) {
            this._audioSource.playOneShot(this.audios[path], 1);
        }
    }

    public music(path: string, loop: boolean = true) {
        if (this.isOpen() && path in this.audios) {
            this._audioSource.clip = this.audios[path];
            this._audioSource.loop = loop;
            this._audioSource.play();
        }
    }

    public closeVolume() {
        this._audioSource.volume = 0;
    }

    public openVolume() {
        this._audioSource.volume = 1;
    }

    public isOpenVolume() {
        return !!this._audioSource.volume;
    }

    public stop(): void {
        this._audioSource.stop();
    }

    public playMusic(): void {
        this.music(AUDIOS_PATH.bg_music);
    }

    public playWaBaoMusic(): void {
        this.music(AUDIOS_PATH.wabao_bg);
    }

    public open() {
        this.openFlag = true;
        this._audioSource.play();
        localStorage.setItem(this.AUDIO_SWITCH_KEY, "1");
    }

    public close() {
        this.openFlag = false;
        this._audioSource.stop();
        localStorage.setItem(this.AUDIO_SWITCH_KEY, "0");
    }

    public isOpen() {
        return this.openFlag;
    }
}

export default new AudioTool();