import { Clock } from '../Common/Clock';
import { DATA_UPDATE, MAX_PROP_NUM, SKIN_GROUP_DEFAULT } from '../Common/Constant';
import { getEventEmiter } from '../Common/EventEmitter';
import { GameInfoGetter } from '../Info/GameInfoGetter';
import { PROP_TYEP } from '../VM/UI/VMPropGetPanel';


/** 数据存储的KEY */
const LOCAL_CURR_MONEY = "C_MONEY"; // 当前现金数
const LOCAL_SKIN_ID = "C_SKIN_ID"; // 皮肤数据本地存储键值
const LOCAL_REF_PROP = "C_REF_PROP"; // 刷新道具数量
const LOCAL_HINT_PROP = "C_HINT_PROP"; // 提示道具数量
const LOCAL_CURR_PASS = "C_PASS"; // 当前关卡数
const LOCAL_CURR_TASK_ID = "C_TASK"; // 当前任务ID
const LOCAL_CURR_COMPLETE_COUNT = "C_COMPLETE_COUNT"; // 当前任务进度
const LOCAL_CURR_SELECT_BG = "C_SELECT_BG"; // 当前选中背景
const LOCAL_WABAO_TIMES = "C_WABAO_TIMES"; // 挖宝游戏次数
const LOCAL_WABAO_SUPPLEMENT_TIMES = "C_WABAO_SUPPLEMENT_TIMES"; // 挖宝游戏补充次数



export class PassData {
    // 关卡数
    private _passNum: number = 0;
    public get passNum() {
        return this._passNum;
    }
    public set passNum(value) {
        this._passNum = value;
        getEventEmiter().emit(DATA_UPDATE.PASS);
        PlayerData.ins.saveLocal(LOCAL_CURR_PASS, this._passNum.toString());
    }

    // // 奖励金币
    // private _rewardCoin: number = 0;
    // public get rewardCoin() {
    //     return this._rewardCoin;
    // }
    // public set rewardCoin(value) {
    //     this._rewardCoin = value;
    // }

    // 奖励现金
    private _rewardMoney: number = 0;
    public get rewardMoney() {
        return this._rewardMoney;
    }
    public set rewardMoney(value) {
        this._rewardMoney = value;
    }

    // 现金方格的组数量
    private _moneyGridNum: number = 0;
    public get moneyGridNum() {
        return this._moneyGridNum;
    }
    public set moneyGridNum(value) {
        this._moneyGridNum = value;
    }

    public init() {
        this._passNum = parseInt(PlayerData.ins.getLocal(LOCAL_CURR_PASS, "1"));
        this._rewardMoney = GameInfoGetter.getPassRewardMoney(PlayerData.ins.moneyNum);
        this._moneyGridNum = GameInfoGetter.getSpecialGrid(this._passNum).moneyGridNum;
    }
}

export class WabaoData {
    // 挖宝游戏次数
    private _times: number = 0;
    public get times(): number {
        return this._times;
    }
    public set times(value: number) {
        this._times = value;
        getEventEmiter().emit(DATA_UPDATE.WABAO_TIMES);
        PlayerData.ins.saveLocal(LOCAL_WABAO_TIMES, this._times.toString());
    }
    // 补充次数时间
    private supplementTimesDate: string = null;
    // 看视频补充的次数
    private _canSupplementTimes: number = 0;
    public get canSupplementTimes(): number {
        let currDateText = Clock.text(new Date());
        if (this.supplementTimesDate != currDateText) {
            this.supplementTimesDate = currDateText;
            this.canSupplementTimes = GameInfoGetter.getWabaoInfo().video;
        }
        return this._canSupplementTimes;
    }
    public set canSupplementTimes(value: number) {
        this._canSupplementTimes = value;
        getEventEmiter().emit(DATA_UPDATE.CAN_SUPPLEMEN_TIMES);
        PlayerData.ins.saveLocal(LOCAL_WABAO_SUPPLEMENT_TIMES, `${Clock.text(new Date())}=${this._canSupplementTimes}`);
    }

    init() {
        this._times = parseInt(PlayerData.ins.getLocal(LOCAL_WABAO_TIMES, "0"));

        let currDateText = Clock.text(new Date());
        let times = PlayerData.ins.getLocal(LOCAL_WABAO_SUPPLEMENT_TIMES, `${currDateText}=${GameInfoGetter.getWabaoInfo().video}`);
        let [dateText, value] = times.split("=");
        if (dateText == currDateText) {
            this.supplementTimesDate = currDateText;
            this._canSupplementTimes = parseInt(value);
        } else {
            this.supplementTimesDate = currDateText;
            this._canSupplementTimes = GameInfoGetter.getWabaoInfo().video;
        }
    }

    resetCanSupplementTimes() {
        this.canSupplementTimes = GameInfoGetter.getWabaoInfo().video;
    }

    reduceCanSupplementTimes(num: number) {
        this.canSupplementTimes -= num;
    }

    addTimes(num: number) {
        this.times += num;
    }

    reduceTimes(num: number) {
        this.times -= num;
    }
}

export class PlayerData {
    private static _instance: PlayerData = null;

    public static get ins(): PlayerData {
        if (!this._instance) {
            this._instance = new PlayerData();
        }
        return this._instance;
    }

    // // 金币数量
    // private coinNum: number = 0;
    // 现金数量
    private _moneyNum: number = 0;
    public get moneyNum(): number {
        return this._moneyNum;
    }
    public set moneyNum(value: number) {
        this._moneyNum = value;
        getEventEmiter().emit(DATA_UPDATE.MONEY);
        this.saveLocal(LOCAL_CURR_MONEY, this._moneyNum.toString());
    }
    // 方格皮肤ID
    private _skinId: string = null;
    // 刷新道具数量
    private _refreshPropNum: number = 0;
    public get refreshPropNum(): number {
        return this._refreshPropNum;
    }
    public set refreshPropNum(value: number) {
        this._refreshPropNum = value;
        getEventEmiter().emit(DATA_UPDATE.PROP_NUM);
        this.saveLocal(LOCAL_REF_PROP, this._refreshPropNum.toString());
    }
    // 提示道具数量
    private _hintPropNum: number = 0;
    public get hintPropNum(): number {
        return this._hintPropNum;
    }
    public set hintPropNum(value: number) {
        this._hintPropNum = value;
        getEventEmiter().emit(DATA_UPDATE.PROP_NUM);
        this.saveLocal(LOCAL_HINT_PROP, this._hintPropNum.toString());
    }
    // 当前关卡信息
    private passData: PassData = new PassData();
    // 当前进行中任务ID
    private _currTaskId: number = 1;
    public get currTaskId(): number {
        return this._currTaskId;
    }
    public set currTaskId(value: number) {
        this._currTaskId = value;
        this.saveLocal(LOCAL_CURR_TASK_ID, this._currTaskId.toString());
    }
    // 当前进行中任务进度
    private _currTaskCompleteCount: number = 0;
    public get currTaskCompleteCount(): number {
        return this._currTaskCompleteCount;
    }
    public set currTaskCompleteCount(value: number) {
        this._currTaskCompleteCount = value;
        this.saveLocal(LOCAL_CURR_COMPLETE_COUNT, this._currTaskCompleteCount.toString());
    }
    // 当前选中的背景
    private _currSelectedBg: number = 0;
    public get currSelectedBg(): number {
        return this._currSelectedBg;
    }
    public set currSelectedBg(value: number) {
        this._currSelectedBg = value;
        this.saveLocal(LOCAL_CURR_SELECT_BG, this._currSelectedBg.toString());
    }

    // 挖宝游戏数据
    public wabaoData: WabaoData = new WabaoData();

    public saveLocal(key: string, value: string): void {
        localStorage.setItem(key, value);
    }

    public getLocal(key: string, defaultValue?: string): string {
        let value = localStorage.getItem(key);
        return value == null ? defaultValue : value;
    }

    public init() {
        this.loadFromLocal();
    }

    /**
     * 加载本地数据
     *
     * @memberof PlayerData
     */
    public loadFromLocal() {
        this._refreshPropNum = parseInt(this.getLocal(LOCAL_REF_PROP, MAX_PROP_NUM.toString()));
        this._hintPropNum = parseInt(this.getLocal(LOCAL_HINT_PROP, MAX_PROP_NUM.toString()));
        this._skinId = this.getLocal(LOCAL_SKIN_ID, SKIN_GROUP_DEFAULT);
        this._moneyNum = parseFloat(this.getLocal(LOCAL_CURR_MONEY, "0"));
        this._currTaskId = parseInt(this.getLocal(LOCAL_CURR_TASK_ID, "1"));
        this._currTaskCompleteCount = parseInt(this.getLocal(LOCAL_CURR_COMPLETE_COUNT, "0"));
        this._currSelectedBg = parseInt(this.getLocal(LOCAL_CURR_SELECT_BG, "0"));

        this.passData.init();
        this.wabaoData.init();
    }

    /**
     * 加载服务器数据
     *
     * @memberof PlayerData
     */
    public loadFromServer() {

    }

    /**
     * 获取玩家当前关卡数据
     *
     * @return {*}  {PassData}
     * @memberof PlayerData
     */
    public getPassData(): PassData {
        return this.passData;
    }

    private setPassData(passNum: number) {
        this.passData.passNum = passNum;
        this.passData.rewardMoney = GameInfoGetter.getPassRewardMoney(this.moneyNum);
        this.passData.moneyGridNum = GameInfoGetter.getSpecialGrid(this.passData.passNum).moneyGridNum;
    }

    /**
     * 获取当前玩家使用的皮肤ID
     *
     * @return {*}  {number}
     * @memberof PlayerData
     */
    public getSkinID(): string {
        return this._skinId;
    }

    /**
     * 保存皮肤ID
     *
     * @param {number} id
     * @memberof PlayerData
     */
    public setSkinID(id: string): void {
        this._skinId = id;
        this.saveLocal(LOCAL_SKIN_ID, this._skinId);
    }

    public addMoney(num: number) {
        this.moneyNum += num;
    }

    public nextPass() {
        this.setPassData(this.passData.passNum + 1);
        getEventEmiter().emit(DATA_UPDATE.PASS);
        // if (this.isWabaoUnLock()) {
        this.wabaoData.addTimes(GameInfoGetter.getWabaoInfo().passCount);
        // }
    }

    public isWabaoUnLock() {
        return this.getPassData().passNum > GameInfoGetter.getWabaoInfo().startLv;
    }

    public isPropEnough(propType: PROP_TYEP): boolean {
        switch (propType) {
            case PROP_TYEP.REFRESH:
                return this.refreshPropNum > 0;
            case PROP_TYEP.HINT:
                return this.hintPropNum > 0;
        }
    }

    public supplementProp(propType: PROP_TYEP) {
        switch (propType) {
            case PROP_TYEP.REFRESH:
                this.refreshPropNum = MAX_PROP_NUM;
                break;
            case PROP_TYEP.HINT:
                this.hintPropNum = MAX_PROP_NUM;
                break;
        }
    }

    public reducePropNum(propType: PROP_TYEP, num: number) {
        switch (propType) {
            case PROP_TYEP.REFRESH:
                this.refreshPropNum -= num;
                break;
            case PROP_TYEP.HINT:
                this.hintPropNum -= num;
                break;
        }
    }
}


