import LanguagetileTile, { Country } from "../Common/Language";
import { Utils } from "../Common/Utils";
import GameInfo from "./GameInfo";


class GameInfoGetterClass {

    /**
     * 获取金币奖励数
     *
     * @param {number} currMoney
     * @return {*} 
     * @memberof GameInfoGetterClass
     */
    public getPassRewardCoins(currCoins: number): number {
        let info = GameInfo.levelCoinRule.find((value: any) => {
            return currCoins >= value.startCoin && currCoins <= value.endCoin;
        })
        if (!info) {
            info = GameInfo.levelCoinRule[GameInfo.levelCoinRule.length - 1];
        }
        return Utils.randomInt(info.minCoin, info.maxCoin);
    }

    /**
    * 获取现金奖励数
    *
    * @param {number} currMoney
    * @return {*} 
    * @memberof GameInfoGetterClass
    */
    public getPassRewardMoney(currMoney: number): number {
        let info = GameInfo.levelMoneyRule.find((value: any) => {
            return currMoney >= value.startMoney && currMoney <= value.endMoney;
        })
        if (!info) {
            info = GameInfo.levelMoneyRule[GameInfo.levelCoinRule.length - 1];
        }
        return Utils.randomInt(info.minMoney, info.maxMoney);
    }

    /**
     * 获取金币翻倍比例
     *
     * @param {number} currCoins
     * @return {*}  {number}
     * @memberof GameInfoGetterClass
     */
    public getPassrewardCoinsRate(currCoins: number): number {
        let info = GameInfo.coinFanbeiRule.find((value: any) => {
            return currCoins >= value.startCoin && currCoins <= value.endCoin;
        })
        if (!info) {
            info = GameInfo.coinFanbeiRule[GameInfo.coinFanbeiRule.length - 1];
        }
        return Utils.randomInt(info.multiplemin, info.multiplemaxn);
    }

    /**
     * 获取现金翻倍比例
     *
     * @param {number} currMoney
     * @return {*}  {number}
     * @memberof GameInfoGetterClass
     */
    public getPassrewardMoneyRate(currMoney: number): number {
        let info = GameInfo.moneyFanBeiRate.find((value: any) => {
            return currMoney >= value.startMoney && currMoney <= value.endMoney;
        })
        if (!info) {
            info = GameInfo.moneyFanBeiRate[GameInfo.moneyFanBeiRate.length - 1];
        }
        return Utils.randomInt(info.multiplemin, info.multiplemaxn);
    }

    /**
     * 获取特殊方格
     *
     * @param {number} currPass
     * @return {*}  {{ coinGrid: number, moneyGridNum: number }}
     * @memberof GameInfoGetterClass
     */
    public getSpecialGrid(currPass: number): { coinGridNum: number, moneyGridNum: number } {
        let info = GameInfo.coinTileMoneyTileRule.find((value: any) => {
            return currPass >= value.minLevel && currPass <= value.maxLevel;
        })
        if (!info) {
            info = GameInfo.coinTileMoneyTileRule[GameInfo.coinTileMoneyTileRule.length - 1];
        }
        return { coinGridNum: info.coinTile, moneyGridNum: info.cashTile };
    }

    /**
     * 通关奖励
     *
     * @return {*}  {{ sl: number, r1: number, r2: number, r3: number }}
     * @memberof GameInfoGetterClass
     */
    public getCompletePassRewardsInfo(): { sl: number, r1: number, r2: number, r3: number } {
        return GameInfo.tixianParm;
    }

    /**
     * 获取汇率
     *
     * @memberof GameInfoGetterClass
     */
    public getExchangeRate() {
        return GameInfo.exchangeRate[LanguagetileTile.getCountry];
    }

    /**
     *  保留num位小数位, 正负数皆可
     * 
     * @param origin 元数据, 正负数皆可
     * @param num 保留小数的位数, 需要>=1
     * @returns 
     */
    public formatMoney(origin: number): string {
        //本地计算了 origin都是美元 getMoneyChangeRate  汇率
        origin = Utils.toDecimal(origin * GameInfoGetter.getExchangeRate())
        if ((LanguagetileTile.getCountry != Country.ID) && (LanguagetileTile.getCountry != Country.RU)) {
            return origin + ""
        } else {
            //印尼的话 去尾取证
            origin = Number(Math.floor(origin))
            if (typeof origin == "number") {
                origin = Utils.toDecimal(origin, 2)
                let num = (origin + "").split(".");
                num[0] = num[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return num.join(".")
            } else {
                origin = Utils.toDecimal(parseFloat(origin), 2)
                return origin + ""
            }
        }
    }

    /**
     * 简易版
     *
     * @memberof GameInfoGetterClass
     */
    public easyVersion() {
        return GameInfo.easyVersion;
    }

    public getWabaoInfo() {
        return GameInfo.wabaoRule;
    }
}

export const GameInfoGetter = new GameInfoGetterClass();