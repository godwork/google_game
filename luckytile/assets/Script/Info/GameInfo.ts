import LanguagetileTile, { Country } from "../Common/Language";

export default class GameInfo {

    static easyVersion: boolean = false;

    //新手奖励
    static newUserAward = { "coin": 5000, "money": 50 }
    //飞行红包概率
    static flyBoxRate = { "coin": 0, "money": 1 }
    //h5活动
    static h5Activity = { "t": 0, "mt": 0, "s": 0 }
    //inster参数
    static InterstitialAd = { "startLevel": 1, "closeWindowCount": 20 }
    //提现起始关卡
    static tixianParm = { "sl": 10, "r1": 3, "r2": 20, "r3": 100 }

    //关卡金币奖励规则配置（endCoin等于-1代表无上限）
    static levelCoinRule = [
        {
            "startCoin": 0,
            "endCoin": 8000,
            "minCoin": 500,
            "maxCoin": 700
        },
        {
            "startCoin": 8001,
            "endCoin": 12000,
            "minCoin": 300,
            "maxCoin": 500
        },
        {
            "startCoin": 12001,
            "endCoin": 15000,
            "minCoin": 200,
            "maxCoin": 300
        },
        {
            "startCoin": 15001,
            "endCoin": 17000,
            "minCoin": 100,
            "maxCoin": 200
        },
        {
            "startCoin": 17001,
            "endCoin": 18000,
            "minCoin": 50,
            "maxCoin": 100
        },
        {
            "startCoin": 18001,
            "endCoin": 19000,
            "minCoin": 20,
            "maxCoin": 40
        },
        {
            "startCoin": 19001,
            "endCoin": 19500,
            "minCoin": 10,
            "maxCoin": 20
        },
        {
            "startCoin": 19501,
            "endCoin": -1,
            "minCoin": 10,
            "maxCoin": 20
        }
    ]
    //金币奖励倍率规则配置（endCoin等于-1代表无上限）
    static coinFanbeiRule = [
        {
            "startCoin": 0,
            "endCoin": 5000,
            "multiplemin": 1.6,
            "multiplemaxn": 1.9
        },
        {
            "startCoin": 5001,
            "endCoin": 8000,
            "multiplemin": 1.3,
            "multiplemaxn": 1.6
        },
        {
            "startCoin": 8001,
            "endCoin": 12000,
            "multiplemin": 0.8,
            "multiplemaxn": 1.2
        },
        {
            "startCoin": 12001,
            "endCoin": 15000,
            "multiplemin": 0.5,
            "multiplemaxn": 0.9
        },
        {
            "startCoin": 15001,
            "endCoin": 17000,
            "multiplemin": 0.4,
            "multiplemaxn": 0.7
        },
        {
            "startCoin": 17001,
            "endCoin": 18000,
            "multiplemin": 0.3,
            "multiplemaxn": 0.5
        },
        {
            "startCoin": 18001,
            "endCoin": -1,
            "multiplemin": 0.2,
            "multiplemaxn": 0.4
        }
    ]
    //关卡现金奖励规则配置（endMoney等于-1代表无上限 startMoney为大于等于，endMoney为小于)
    static levelMoneyRule = [
        {
            "startMoney": 0,
            "endMoney": 70,
            "minMoney": 6,
            "maxMoney": 8
        },
        {
            "startMoney": 70,
            "endMoney": 90,
            "minMoney": 4,
            "maxMoney": 6
        },
        {
            "startMoney": 90,
            "endMoney": 120,
            "minMoney": 2,
            "maxMoney": 4
        },
        {
            "startMoney": 120,
            "endMoney": 150,
            "minMoney": 1,
            "maxMoney": 2
        },
        {
            "startMoney": 150,
            "endMoney": 170,
            "minMoney": 0.3,
            "maxMoney": 0.5
        },
        {
            "startMoney": 170,
            "endMoney": 190,
            "minMoney": 0.2,
            "maxMoney": 0.3
        },
        {
            "startMoney": 190,
            "endMoney": 195,
            "minMoney": 0.15,
            "maxMoney": 0.2
        },
        {
            "startMoney": 195,
            "endMoney": -1,
            "minMoney": 0.01,
            "maxMoney": 0.02
        }
    ]
    //现金奖励倍率规则配置（endMoney等于-1代表无上限,startMoney为大于等于，endMoney为小于）
    static moneyFanBeiRate = [
        {
            "startMoney": 0,
            "endMoney": 80,
            "multiplemin": 2.6,
            "multiplemaxn": 2.9
        },
        {
            "startMoney": 80,
            "endMoney": 120,
            "multiplemin": 2.4,
            "multiplemaxn": 2.7
        },
        {
            "startMoney": 120,
            "endMoney": 150,
            "multiplemin": 2.3,
            "multiplemaxn": 2.5
        },
        {
            "startMoney": 150,
            "endMoney": 170,
            "multiplemin": 1.6,
            "multiplemaxn": 2
        },
        {
            "startMoney": 170,
            "endMoney": 180,
            "multiplemin": 1.3,
            "multiplemaxn": 1.5
        },
        {
            "startMoney": 180,
            "endMoney": 190,
            "multiplemin": 1.2,
            "multiplemaxn": 1.3
        },
        {
            "startMoney": 190,
            "endMoney": -1,
            "multiplemin": 1.1,
            "multiplemaxn": 1.2
        }
    ]
    //关卡特殊奖励方块（maxLevel等于-1代表无上限）
    static coinTileMoneyTileRule = [
        {
            "minLevel": 0,
            "maxLevel": 1,
            "coinTile": 0,
            "cashTile": 0
        },
        {
            "minLevel": 2,
            "maxLevel": 10,
            "coinTile": 0,
            "cashTile": 0
        },
        {
            "minLevel": 11,
            "maxLevel": 100,
            "coinTile": 0,
            "cashTile": 1
        },
        {
            "minLevel": 101,
            "maxLevel": -1,
            "coinTile": 0,
            "cashTile": 2
        }
    ]
    //关卡通关展示广告强制弹视频配置（maxLevel等于-1代表无上限）当前关卡取余 3 6 9 12 15.....   (n-1)%
    static forceVideoRule = [
        {
            "minLevel": 1,
            "maxLevel": 20,
            "lvNum": 3
        },
        {
            "minLevel": 21,
            "maxLevel": 100,
            "lvNum": 2
        },
        {
            "minLevel": 101,
            "maxLevel": -1,
            "lvNum": 1
        }
    ]
    //挖宝规则配置 (startLv:开始等级 passCount:通关增加挖宝次数 videoCount:看视频增加挖宝次数 video:每日最大看视频数量 lowRate:低价值宝箱奖励倍率 centreRate:中价值宝箱奖励倍率 highRate:高价值宝箱奖励倍率 )
    static wabaoRule = {
        "startLv": 3,
        "passCount": 2,
        "videoCount": 2,
        "video": 20,
        "lowRate": 0.8,
        "centreRate": 1.4,
        "highRate": 2
    }
    // 汇率
    static exchangeRate = { "id": 15168, "br": 5.14, "pk": 1, "en": 1, "ru": 90 }

    //胜利结算界面是否强制播放视频
    static isForcePlayVideo(passNum: number) {
        let find = null
        for (let index = 0; index < GameInfo.forceVideoRule.length; index++) {
            const _a = GameInfo.forceVideoRule[index];
            if (_a.minLevel <= passNum && _a.maxLevel > passNum) {
                find = _a
                break;
            }
        }
        if (!find) {
            find = GameInfo.forceVideoRule[GameInfo.forceVideoRule.length - 1]
        }
        if (passNum % (find.lvNum) == 0) {
            return true
        }
        return false
    }
}