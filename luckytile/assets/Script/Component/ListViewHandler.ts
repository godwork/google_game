import { _decorator, Component, Node, Size } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('ListViewHandler')
export abstract class ListViewHandler extends Component {
    public abstract cellSize(): Size;
    public abstract cellNum(): number;
    public abstract onCell(cellIndex: number, cell: Node): void;
}


