import { _decorator, CCFloat, CCInteger, Component, instantiate, Mask, Node, NodePool, Prefab, ScrollView, Size, UITransform, Vec3 } from 'cc';
import { ListViewHandler } from './ListViewHandler';
const { ccclass, property, requireComponent } = _decorator;

type ListItem = {
    itemId: number;
    node: Node;
}

@ccclass('ListView')
export class ListView extends ScrollView {

    @property(ListViewHandler)
    private handler: ListViewHandler = null;

    private itemW: number = 0;
    private itemH: number = 0;
    private size: number = 0;

    private list: ListItem[] = [];
    private viewport: Size = null;
    private itemPool: NodePool = new NodePool();

    protected onLoad(): void {
        this.viewport = this.getComponentInChildren(Mask).getComponent(UITransform).contentSize;
    }

    public refresh() {
        let size: Size = this.handler.cellSize();
        this.itemW = size.width;
        this.itemH = size.height;
        this.size = this.handler.cellNum();
        this.setSize(this.size);
    }

    start() {
        super.start();
        this.updateVirtualList();
    }

    public setSize(size: number) {
        this.size = size;
        if (this.vertical) {
            this.content.getComponent(UITransform).contentSize = new Size(this.itemW, this.itemH * this.size);
        } else if (this.horizontal) {
            this.content.getComponent(UITransform).contentSize = new Size(this.itemW * this.size, this.itemH);
        }
    }

    update(deltaTime: number) {
        super.update(deltaTime);
        if (this.isScrolling() || this.isAutoScrolling()) {
            this.updateVirtualList();
        }
    }

    private getListCellIndex(itemId: number): number {
        return this.list.findIndex((value: ListItem) => { return value.itemId == itemId; });
    }

    private updateVirtualList() {
        let offsetTop: number = this.getScrollOffset().y;
        let offsetBottom: number = offsetTop + this.viewport.height;
        let itemLength: number = this.itemH;
        let startIndex: number = Math.floor(offsetTop / itemLength);
        startIndex = startIndex > 0 ? startIndex : 0;
        let endIndex: number = Math.ceil(offsetBottom / itemLength) - 1;
        endIndex = endIndex < this.size ? endIndex : this.size - 1;

        this.syncList(startIndex, endIndex);
    }

    private syncList(startIndex: number, endIndex: number) {
        if (this.list.length > 0 && startIndex == this.list[0].itemId && endIndex == this.list[this.list.length - 1].itemId) {
            return;
        }
        // 处理需要回收的item
        for (let i = 0; i < this.list.length; i++) {
            const listItem = this.list[i];
            if (listItem.itemId < startIndex || listItem.itemId > endIndex) {
                // 回收
                this.itemPool.put(listItem.node);
                this.list.splice(i--, 1);
            }
        }

        // 再处理增加
        for (let i = startIndex; i <= endIndex; i++) {
            const listItemIndex = this.getListCellIndex(i);
            if (listItemIndex == -1) {
                let node = this.createCell(i);
                this.handler.onCell(i, node);
                this.addCell(i, node);
            }
        }
    }

    private addCell(itemId: number, itemNode: Node) {
        itemNode.parent = this.content;
        if (this.list.length > 0) {
            if (itemId <= this.list[0].itemId) {
                this.list.unshift({ itemId: itemId, node: itemNode });
            } else if (itemId >= this.list[this.list.length - 1].itemId) {
                this.list.push({ itemId: itemId, node: itemNode });
            }
        } else {
            this.list.push({ itemId: itemId, node: itemNode });
        }
    }

    private createCell(itemId: number): Node {
        let node = this.itemPool.get();
        if (!node) {
            node = new Node();
            let trans = node.addComponent(UITransform);
            trans.contentSize = new Size(this.itemW, this.itemH);
        }
        node.name = `Item_${itemId}`;
        node.position = new Vec3(0, -(this.itemH * itemId) - this.itemH * 0.5, 0);
        return node;
    }

    public getCell(index: number): Node {
        index = this.getListCellIndex(index);
        if (index != -1) {
            return this.list[index].node;    
        }
        return null;
    }
}


