import { _decorator, Button, Component, Node } from 'cc';
import { AUDIOS_ENUM, AUDIOS_PATH } from '../Common/AudioConfig';
import AudioTool from '../Common/AudioTool';
const { ccclass, property } = _decorator;

@ccclass('ButtonAudio')
export class ButtonAudio extends Component {
    @property({ type: AUDIOS_ENUM })
    public clickAudioType = AUDIOS_ENUM.button_click_01;

    public onLoad() {
        let btn = this.node.getComponent(Button);
        if (!btn) return;
        this.node.on(Node.EventType.TOUCH_END, () => {
            AudioTool.sound(AUDIOS_PATH[AUDIOS_ENUM[this.clickAudioType]]);
        });
    }
}


