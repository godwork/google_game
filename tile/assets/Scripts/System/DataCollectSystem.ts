export const LOG_KEY_VIDEO = {
    show: 'BCBgAAoXHx5d0yKBg9KpEF',
    success: 'BCBgAAoXHx5d0yKBg9KpEE',
};

export const LOG_KEY_REWARD_PAGE = {
    collect: 'BCBgAAoXHx5d0yKBg9KpE9',
    collectDouble: 'BCBgAAoXHx5d0yKBg9KpE8',
    collectDoubleSuccess: 'BCBgAAoXHx5d0yKBg9KpED',
};

export const LOG_KEY_FAIL_REVIVE_PAGE = {
    revive: 'BCBgAAoXHx5d0yKBg9KpE2',
    reviveSuccess: 'BCBgAAoXHx5d0yKBg9KpEC',
};

export const LOG_KEY_GAME_PAGE = {
    timeBox: 'BCBgAAoXHx5d0yKBg9KpEp',
    timeBoxSuccess: 'BCBgAAoXHx5d0yKBg9KpEB',
    recall: 'BCBgAAoXHx5d0yKBg9KpEo',
    recallVideo: 'BCBgAAoXHx5d0yKBg9KpEv',
    recallVideoSuccess: 'BCBgAAoXHx5d0yKBg9KpEK',
    refresh: 'BCBgAAoXHx5d0yKBg9KpEu',
    refreshVideo: 'BCBgAAoXHx5d0yKBg9KpEt',
    refreshVideoSuccess: 'BCBgAAoXHx5d0yKBg9KpEJ',
    hint: 'BCBgAAoXHx5d0yKBg9KpEs',
    hintVideo: 'BCBgAAoXHx5d0yKBg9KpEz',
    hintVideoSuccess: 'BCBgAAoXHx5d0yKBg9KpEL',
};

export const LOG_KEY_DAILY_REWARD = {
    collect: 'BCBgAAoXHx5d0yKBg9KpEg',
    boxVideo: 'BCBgAAoXHx5d0yKBg9KpEh',
    boxVideoSuccess: 'BCBgAAoXHx5d0yKBg9KpEG',
    collectDouble: 'BCBgAAoXHx5d0yKBg9KpEn',
    collectDoubleSuccess: 'BCBgAAoXHx5d0yKBg9KpEH',
};

export const TILI_REWARD = {
    buttonClick: 'BCBgAAoXHx5d0yKBg9KpEN',
    getRewardSuccess: 'BCBgAAoXHx5d0yKBg9KpEM',
};

export const LOADING_PAGE = {
    showLoadingSuccess: 'BCBgAAoXHx5d0yKBg9KpET',
    showNextScneSuccess: 'BCBgAAoXHx5d0yKBg9KpES',
}

const LOG_KEY_LEVEL_ENTER = {
    0: 'BCBgAAoXHx5d0yKBg9KpEI',
    1: 'BCBgAAoXHx5d0yKBg9KpEZ',
    2: 'BCBgAAoXHx5d0yKBg9KpEa',
    3: 'BCBgAAoXHx5d0yKBg9KpEY',
    4: 'BCBgAAoXHx5d0yKBg9KpEf',
    5: 'BCBgAAoXHx5d0yKBg9KpEe',
    6: 'BCBgAAoXHx5d0yKBg9KpEd',
    7: 'BCBgAAoXHx5d0yKBg9KpEb',
    8: 'BCBgAAoXHx5d0yKBg9KpEU',
    9: 'BCBgAAoXHx5d0yKBg9KpEV',
    10: 'BCBgAAoXHx5d0yKBg9KpEW',
    11: 'BCBgAAoXHx5d0yKBg9KpEX',
    12: 'BCBgAAoXHx5d0yKBg9KpEQ',
    13: 'BCBgAAoXHx5d0yKBg9KpER',
};

const LOG_KEY_LEVEL_FAIL = {
    0: 'BCBgAAoXHx5d0yKBg9KpEP',
    1: 'BCBgAAoXHx5d0yKBg9KpFk',
    2: 'BCBgAAoXHx5d0yKBg9KpFl',
    3: 'BCBgAAoXHx5d0yKBg9KpFr',
    4: 'BCBgAAoXHx5d0yKBg9KpFq',
    5: 'BCBgAAoXHx5d0yKBg9KpFp',
    6: 'BCBgAAoXHx5d0yKBg9KpFo',
    7: 'BCBgAAoXHx5d0yKBg9KpFm',
    8: 'BCBgAAoXHx5d0yKBg9KpFn',
    9: 'BCBgAAoXHx5d0yKBg9KpFg',
    10: 'BCBgAAoXHx5d0yKBg9KpFh',
    11: 'BCBgAAoXHx5d0yKBg9KpFi',
    12: 'BCBgAAoXHx5d0yKBg9KpFj',
    13: 'BCBgAAoXHx5d0yKBg9KpEc',
};

const LOG_KEY_LEVEL_COMPLETE = {
    0: 'BCBgAAoXHx5d0yKBg9KpEO',
    1: 'BCBgAAoXHx5d0yKBg9KpF3',
    2: 'BCBgAAoXHx5d0yKBg9KpFw',
    3: 'BCBgAAoXHx5d0yKBg9KpF2',
    4: 'BCBgAAoXHx5d0yKBg9KpF1',
    5: 'BCBgAAoXHx5d0yKBg9KpF0',
    6: 'BCBgAAoXHx5d0yKBg9KpF7',
    7: 'BCBgAAoXHx5d0yKBg9KpFx',
    8: 'BCBgAAoXHx5d0yKBg9KpFy',
    9: 'BCBgAAoXHx5d0yKBg9KpFz',
    10: 'BCBgAAoXHx5d0yKBg9KpFs',
    11: 'BCBgAAoXHx5d0yKBg9KpFt',
    12: 'BCBgAAoXHx5d0yKBg9KpFu',
    13: 'BCBgAAoXHx5d0yKBg9KpFv',
};



export function reportUserBehaviorBranchAnalytics(id: string, dim: string = '0', evevtType: 1 | 2 = 1) {
   
}

export function reportLevelEnter(level: number): void {
    // reportUserBehaviorBranchAnalytics(LOG_KEY_LEVEL_ENTER[getRange(level)], getDim(level).toString());
    reportUserBehaviorBranchAnalytics(LOG_KEY_LEVEL_ENTER[0], level.toString());
}

export function reportLevelFail(level: number): void {
    // reportUserBehaviorBranchAnalytics(LOG_KEY_LEVEL_FAIL[getRange(level)], getDim(level).toString());
    reportUserBehaviorBranchAnalytics(LOG_KEY_LEVEL_FAIL[0], level.toString());
}

export function reportLevelComplete(level: number): void {
    // reportUserBehaviorBranchAnalytics(LOG_KEY_LEVEL_COMPLETE[getRange(level)], getDim(level).toString());
    reportUserBehaviorBranchAnalytics(LOG_KEY_LEVEL_COMPLETE[0], level.toString());
}


function getRange(level: number): number {
    return Math.floor((level - 1) / 100);
}

function getDim(level: number): string {
    return ((level - 1) % 100 + 1).toString();
}