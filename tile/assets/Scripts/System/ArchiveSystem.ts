import Archive from "../Common/Archive";
import { CUSTOM_EVENT_NAME } from "../Common/Constant";
import getEventEmiter from "../Libraries/EventEmitter";

class ArchiveSystem extends cc.Component {
    private _localData: ILocalData;

    public userID: string;
    public localData: ILocalData;
    public taskJson: any;
    public initialize(): void {
        this.userID = Archive.createUserID();
        this._localData = Archive.createLocalData(this.userID);
        this.__saveLocalData();

        this.localData = new Proxy(this._localData, {
            get: (target: ILocalData, key: string) => {
                return target[key];
            },
            set: (target: ILocalData, key: string, value: any) => {
                target[key] = value;
                this.__saveLocalData();
                return true;
            }
        });
    }

    // public onCompleteLevel(level: number): void {
    //     if (level == this.localData.currentMaxLevel) this.localData.currentMaxLevel += 1;
    // }

    public getStarNum(level: number): number {
        let sAmount = this.localData.starsAmount;
        if (level in sAmount) return sAmount[level];
        return 0;
    }

    public setStarNum(level: number, stars: number): void {
        let sAmount = this.localData.starsAmount;
        if (stars > this.getStarNum(level)) sAmount[level] = stars;
        this.localData.starsAmount = sAmount;
    }

    public switchMusic(): void {
        this.localData.music = !this.localData.music;
    }

    public switchSound(): void {
        this.localData.sound = !this.localData.sound;
    }


    private __saveLocalData(): void {
        Archive.saveLocalData(this.userID, this._localData);
        getEventEmiter().emit(CUSTOM_EVENT_NAME.localDataUpdate);
    }
}

export default new ArchiveSystem();