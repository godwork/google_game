import CCLog from "../CCLog";
import { AREA_MOVE_DURATION, CUSTOM_EVENT_NAME, PREFABS_NAME, SCENES_NAME } from "../Common/Constant";
import { GetReceiveAward } from "../Common/GameData";

import SetPanelTs from "../Game/SetPanelTs";
import getEventEmiter from "../Libraries/EventEmitter";
import { BizTypeEnum } from "../net/api";
import GameFailPanelTs from "../panels/GameFailPanelTs";
import GameSucessPanelTs from "../panels/GameSucessPanelTs";
import GetCoinAndLiBaoPanel from "../panels/GetCoinAndLiBaoPanelTs";
import LevelPanelTs from "../panels/LevelPanelTs";
import PropUsePanelTs from "../panels/PropUsePanelTs";
import RedBagPanelTs from "../panels/RedBagPanelTs";

const offset: number = 1280;

class SceneManagerSystem extends cc.Component {
    public loadedProgress: any = {};

    constructor() {
        super();
    }

    public initialize(): void {
        // 预加载场景
        for (let scene of Object.values(SCENES_NAME)) {
            
            this.loadedProgress[scene] = 0;
            cc.director.preloadScene(scene, (c: number, t: number) => {
                this.loadedProgress[scene] = c / t;
            }, () => { });
        }
    }

    public preloadPrefabs(): void {
        // 预加载预制体
        for (let prefab of Object.values(PREFABS_NAME)) {
            CCLog.log('预加预制体： ', prefab);
            cc.resources.preload(<string>prefab);
        }
    }

    public sceneChange(nextSceneName: string, delay: boolean = true): void {
        getEventEmiter().emit(CUSTOM_EVENT_NAME.onSceneChanged);
        if (delay) {
            this.scheduleOnce(() => {
                cc.director.loadScene(nextSceneName);
            }, AREA_MOVE_DURATION);
        } else {
            cc.director.loadScene(nextSceneName);
        }
    }


    public dialogOnOpen(content: cc.Node, callBack: Function = null): void {
        content.y += offset;
        content.parent.active = true;
        cc.tween(content)
            .to(0.3, { y: content.y - offset }, { easing: cc.easing.backOut })
            .call(callBack)
            .start();
    }

    public dialogOnClose(content: cc.Node, callBack?: Function): void {
        cc.tween(content)
            .to(0.3, { y: content.y - offset }, { easing: cc.easing.backIn })
            .call(() => {
                content.parent.active = false;
                content.y += offset;
                if (callBack) callBack();
            })
            .start();
    }

    public dialogOnOpenX(content: cc.Node, callBack: Function = null): void {
        content.x += offset;
        content.parent.active = true;
        cc.tween(content)
            .to(0.6, { x: content.x - offset }, { easing: cc.easing.backOut })
            .call(callBack)
            .start();
    }

    public dialogOnCloseX(content: cc.Node, callBack?: Function): void {
        cc.tween(content)
            .to(0.5, { x: content.x - offset }, { easing: cc.easing.backIn })
            .call(() => {
                content.parent.active = false;
                content.x += offset;
                if (callBack) callBack();
            })
            .start();
    }

    public async showSetPanel() {
        if (!SetPanelTs.I) {
            await new Promise((res: Function, rej: Function) => {
                cc.resources.load(PREFABS_NAME.SetPanel, (e: Error, a: cc.Prefab) => {
                    let page = cc.instantiate(a);
                    page.zIndex = 1000;
                    this.__getCurentRoot().addChild(page);
                    res();
                });
            });
        }
        SetPanelTs.I.show();
    }

    public async showLevelPanels(callBack: Function = null) {
        if (!LevelPanelTs.I) {
            await new Promise((res: Function, rej: Function) => {
                cc.resources.load(PREFABS_NAME.LevelPanel, (e: Error, a: cc.Prefab) => {
                    let page = cc.instantiate(a);
                    page.zIndex = 1000;
                    this.__getCurentRoot().addChild(page);
                    page.active = false;
                    res();
                });
            });
        }
        LevelPanelTs.I.show(callBack);
    }

    public async showGetCoinOrLiBaoPanels(type:string,baxIndex = 0,callback,msgPack?) {
        let tag = BizTypeEnum.v2_cgcoin;
        if (type == "libao"){
            tag = BizTypeEnum.v2_box;
        }else if (type == "newplayerlibao"){
            tag = BizTypeEnum.v2_novice2
        }
     
        
        await new Promise((res: Function, rej: Function) => {
            cc.resources.load(PREFABS_NAME.GetCoinAndLiBaoPanel, (e: Error, a: cc.Prefab) => {
                let page = cc.instantiate(a);
                page.zIndex = 1000;
                this.__getCurentRoot().addChild(page);
                page.getComponent(GetCoinAndLiBaoPanel).show(type,callback);
                res();
            });
        });
        
    }

    public async showGetRedBagPanels(type="",callback:Function) {
        
        await new Promise((res: Function, rej: Function) => {
            cc.resources.load(PREFABS_NAME.RedBagPanel, (e: Error, a: cc.Prefab) => {
                let page = cc.instantiate(a);
                page.zIndex = 1000;
                this.__getCurentRoot().addChild(page);
                page.getComponent(RedBagPanelTs).show(type,callback);
                res();
            });
        });
       
    }

    public async showGetPanels(msg,callBack: Function = null) {
        // if (!LevelPanelTs.I) {
        //     await new Promise((res: Function, rej: Function) => {
        //         cc.resources.load(PREFABS_NAME.LevelPanel, (e: Error, a: cc.Prefab) => {
        //             let page = cc.instantiate(a);
        //             page.zIndex = 1000;
        //             this.__getCurentRoot().addChild(page);
        //             res();
        //         });
        //     });
        // }
        // LevelPanelTs.I.show(msg,callBack);
    }
    public  showPhonePanels() 
    {
        cc.resources.load(PREFABS_NAME.gamePhonePanel, (e: Error, a: cc.Prefab) => {
            let page = cc.instantiate(a);
            page.zIndex = 1000;
            this.__getCurentRoot().addChild(page);
            
        });
    }
    public  showGamephoneTipsPanels() 
    {
        cc.resources.load(PREFABS_NAME.gamephoneTip, (e: Error, a: cc.Prefab) => {
            let page = cc.instantiate(a);
            page.zIndex = 1000;
            this.__getCurentRoot().addChild(page);
            
        });
    }
    public async showSucessLayer() {
        // let msg = await GetReceiveAward(BizTypeEnum.v2_pass,0)
        // if (msg.resCode != 0) return;
        // if (!GameSucessPanelTs.I) {
        //     await new Promise((res: Function, rej: Function) => {
        //         cc.resources.load(PREFABS_NAME.gameSucessPanel, (e: Error, a: cc.Prefab) => {
        //             let page = cc.instantiate(a);
        //             this.__getCurentRoot().addChild(page);
        //             res();
        //         });
        //     });
        // }
        // GameSucessPanelTs.I.show(msg.data);

        if (!GameSucessPanelTs.I) {
            await new Promise((res: Function, rej: Function) => {
                cc.resources.load(PREFABS_NAME.gameSucessPanel, (e: Error, a: cc.Prefab) => {
                    let page = cc.instantiate(a);
                    this.__getCurentRoot().addChild(page);
                    res();
                });
            });
        }
        GameSucessPanelTs.I.show();
    }

    public async showFailLayer(callback) {
        if (!GameFailPanelTs.I) {
            await new Promise((res: Function, rej: Function) => {
                cc.resources.load(PREFABS_NAME.gameFailPanel, (e: Error, a: cc.Prefab) => {
                    let page = cc.instantiate(a);
                    this.__getCurentRoot().addChild(page);
                    res();
                });
            });
        }
        GameFailPanelTs.I.show(callback);
    }

    /** index 0刷新 1返回 2提示*/
    public async showPropUsePanel(index:number,callback:Function) {
        if (!PropUsePanelTs.I) {
            await new Promise((res: Function, rej: Function) => {
                cc.resources.load(PREFABS_NAME.propUsePanel, (e: Error, a: cc.Prefab) => {
                    let page = cc.instantiate(a);
                    this.__getCurentRoot().addChild(page);
                    res();
                });
            });
        }
        PropUsePanelTs.I.show(index,callback);
    }

    private __getCurentRoot(): cc.Node {
        return cc.director.getScene().getChildByName('Canvas');
    }
}

export default new SceneManagerSystem();