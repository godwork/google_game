import { AUDIOS_PATH } from "../Common/AudioConfig";
import ArchiveSystem from "./ArchiveSystem";

class AudioTool {
    public audios = {};
    public audiosID = {};

    private volume:number = 1;
    private _musicID: number = null;

    private AUDIO_SWITCH_KEY = "AUDIO_SWITCH_KEY"

    public async initialize(): Promise<void> {
        const bundleAudios: cc.AssetManager.Bundle = await new Promise((resolve: Function, reject: Function) => {
            cc.assetManager.loadBundle('bundle_audios', (err: Error, bundle: cc.AssetManager.Bundle) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(bundle);
                }
            });
        });

        for (let audioPath of Object.values(AUDIOS_PATH)) {
            this.audios[audioPath] = await new Promise((resolve: Function, reject: Function) => {
                bundleAudios.load(audioPath, cc.AudioClip, (err: Error, res: cc.AudioClip) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                });
            });
        }
        let bgswitch = cc.sys.localStorage.getItem(this.AUDIO_SWITCH_KEY);
        if (this.isOpenVolume()){
            this.openVolume();
        }else{
            this.closeVolume();
        }
        this.playMusic();
    }

    public play(path: string, loop: boolean = false): void {
        if (ArchiveSystem.localData.sound && path in this.audios) {
            const audioID: number = cc.audioEngine.play(this.audios[path], loop, this.volume);
            if (loop) {
                this.audiosID[path] = audioID;
            }
        }
    }

    public closeVolume(){
        this.volume = 0
        cc.sys.localStorage.setItem(this.AUDIO_SWITCH_KEY, 0);
        cc.audioEngine.setEffectsVolume(0.0)
        cc.audioEngine.setMusicVolume(0.0)
    }

    public openVolume(){
        this.volume = 1
        cc.sys.localStorage.setItem(this.AUDIO_SWITCH_KEY, 1);
        cc.audioEngine.setMusicVolume(1)
        cc.audioEngine.setEffectsVolume(1)
    }

    public isOpenVolume(){
        let bgswitch = cc.sys.localStorage.getItem(this.AUDIO_SWITCH_KEY);
        return bgswitch != 0;
    }

    public stop(path: string): void {
        if (path in this.audiosID) {
            cc.audioEngine.stop(this.audiosID[path]);
            delete this.audiosID[path];
        }
    }

    public playMusic(): void {
        if (this._musicID === null && ArchiveSystem.localData.music && this.audios.hasOwnProperty(AUDIOS_PATH.bg_music)) {
            this._musicID = cc.audioEngine.playMusic(this.audios[AUDIOS_PATH.bg_music], true);
        }
    }
  
    public stopMusic(): void {
        cc.audioEngine.stopMusic();
        this._musicID = null;
    }
}

export default new AudioTool();