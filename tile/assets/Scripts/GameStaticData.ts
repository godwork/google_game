import { AUDIOS_PATH } from "./Common/AudioConfig";
import NetError from "./fxTs/NetError"
import AudioTool from "./System/AudioSystem";

export enum rewardFlyType {
    rewardType_coin = 1,
    rewardType_moeny = 2,

}
//视频场景
export enum videoSceneType{
    videoScene_none = 0,
    videoScene_GameSucc_Btn_force = 1,

}
export default class GameSmashStaticData {
 
    
    static videoScene  = videoSceneType.videoScene_none
    static isShenHeModel = false//审核开关
    static isnotLiveuserH5 = "false"//无效的h5活动者  true:隐藏h5活动按钮
    static isUnLockwabao = true//是否解锁挖宝
    static lockwabaoLV = 0//通关哪一关解锁挖宝
    static wabaoLeftTime = 0//挖宝剩余的次数
    static wabaoTodaySeeVideoLeftTime = 10//今天挖宝看视频的剩余次数
    static wabaoSeeVideoAddNum = 0//看一次视频增加的挖宝次数
    
    static isStartWabao = false//点击开始一轮的挖宝
    static isWabaoRopeHasCollisioned = false//挖宝的钩子是否已经碰撞到红包了
    static wabaoRewardType:string = "none"//挖宝的类型 none:没勾到任何东西 low:低价值 centre:中价值 high:高价值

    static wabaoRewardMoneyNum = 0//挖宝的奖励钱数目

    static isShowFlyBox = true
    static rewardType  = rewardFlyType.rewardType_moeny
    static userGetiphone14Num = Math.floor(Math.random() * (15 - 10 + 1) + 10);//已经获取iphone14的人数



    static getNetErroScript(){
        let sc = cc.director.getScene().getChildByName("Canvas").getChildByName("NetError").getComponent(NetError)
        return sc
    }
    static showWithNewFlyAnimation(type) {
        if (GameSmashStaticData.isShenHeModel) {
            return
        }
        GameSmashStaticData.rewardType = type
        cc.resources.load('panels/rewardAnim', (e: Error, a: cc.Prefab) => {
            let page = cc.instantiate(a);
           
            let parent =  cc.find("Canvas");
            if (!parent) return;
            parent.addChild(page,cc.macro.MAX_ZINDEX);
            AudioTool.play(AUDIOS_PATH.flyprop)
        });
    }

}