

export default class LocalKey {
 
    static appName = "smashcrush_"
    static sign_today_in_game_time = LocalKey.appName+"c_time"//当前时间日期
    static currentLevel = LocalKey.appName+"c_lv"//当前关卡
    static wabaoleft_time = LocalKey.appName+"w_lf"//挖宝剩余的次数
    static wabaoleft_seeVideo_time = LocalKey.appName+"w_t_v_lf"//挖宝今天剩余测次数
    static localCoin = LocalKey.appName+"l_c"//存在本地金币
    static localMoney = LocalKey.appName+"l_m"//存在本地美元
    static localBg = LocalKey.appName+"bg"//背景图
    static localCashStartTip = LocalKey.appName+"l_c_t"
    static localTiXianStartTip = LocalKey.appName+"l_t_t"

  
   
}