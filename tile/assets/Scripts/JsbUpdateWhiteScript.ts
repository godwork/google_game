import CCLog from "./CCLog";


import GameSmashStaticData from "./GameStaticData";

import { JsbSmashTileCallMgr } from "./Common/JsbMgr";
import getEventEmiter from "./Libraries/EventEmitter";
import { CUSTOM_EVENT_NAME } from "./Common/Constant";
import ProgressController from "./Game/ProgressController";
import GameSceneController from "./Scenes/GameSceneController";
import GameSmashStaticJsonData from "./GameStaticJsonData";
import { GameData, GetAccountSelfAdd, GetReceiveAward } from "./Common/GameData";
import { BizTypeEnum } from "./net/api";
import GameStaticData from "./GameStaticData";
import PropAreaController from "./Game/PropAreaController";



export default class JsbUpdateWhiteScript  {



    delegate = null;
    

    private static _instance:JsbUpdateWhiteScript = null
    public static get Instance():JsbUpdateWhiteScript  {
        if(JsbUpdateWhiteScript._instance == null){
            JsbUpdateWhiteScript._instance = new JsbUpdateWhiteScript();
        }

        return JsbUpdateWhiteScript._instance;
    }  

    private constructor(){
        this.delegate = {
            updateWhiteBao(){
                let isWhite =  JsbSmashTileCallMgr.handler("requestIsWhiteBao","()Ljava/lang/String;")
                if (isWhite == "true") {
                    GameSmashStaticData.isShenHeModel = true
                }else{
                    GameSmashStaticData.isShenHeModel = false
                }
                CCLog.log("++++++刷新白包",GameSmashStaticData.isShenHeModel)
                
                if (!GameSmashStaticData.isShenHeModel) {
                    //还原UI
                    
                    if (GameSceneController.I) {
                        GameSceneController.I.controleShenHeNode(!GameSmashStaticData.isShenHeModel)
                    }
                    if (ProgressController.I) {
                        ProgressController.I.refresh()
                    }
                    
                    getEventEmiter().emit(CUSTOM_EVENT_NAME.updateRefreshWhiteBao);
                }
                //测试
                //JsbCocosCallMgr.handler('showWithdraw',"()V");
            },

            
            getH5AdTaskReward(){
                //cocos调安卓
                CCLog.log("++++++h5活动回传")
                let sendJson = {secTime:GameSmashStaticJsonData.h5Activity.t+"",
                miniSecTime:GameSmashStaticJsonData.h5Activity.mt+"",
                moneyrate:GameSmashStaticJsonData.getMoneyChangeRate+"",
                bonus:GameSmashStaticJsonData.getLevelPassRewardMoney(GameData.AccountData.moneytotal)+"",
                totalMoney:GameData.AccountData.moneytotal+""}
                JsbSmashTileCallMgr.handler("getH5AdTaskReward","(Ljava/lang/String;)v",JSON.stringify(sendJson))
            },
          
            h5RewardFromAndroid(rewardStr:string){
                CCLog.log("++++++h5接受奖励")
                //cocos 更新账户 上传服务器
                let reward = GameSmashStaticJsonData.getTwoPointNum(Number(rewardStr))
                GetReceiveAward(BizTypeEnum.v2_h5Reward,0,reward)
                GetAccountSelfAdd(null,0,reward);
               
            },
            QueryFraudUser(FraudUser:string){
                if (FraudUser == "true") {
                    GameStaticData.isnotLiveuserH5 = FraudUser
                }else{
                    GameStaticData.isnotLiveuserH5 = "false"
                }
                if (PropAreaController.I) {
                    PropAreaController.I.updateBtnPos()
                }
            },
            tixianMoneyFromAndroid(rewardStr:string){
                CCLog.log("++++++安卓提现返回奖励",rewardStr)
                //cocos 更新账户 上传服务器
                let reward = GameSmashStaticJsonData.getTwoPointNum(Number(rewardStr))
                GetAccountSelfAdd(null,0,reward,false);
               
            }
        }
    }
}
