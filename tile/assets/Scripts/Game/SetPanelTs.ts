// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { showInterstitialAd } from "../Common/AdMethods";
import { AUDIOS_PATH } from "../Common/AudioConfig";
import CocosSmachHelper from "../Common/CocosHelper";
import { GetLevelInfo } from "../Common/GameData";
import { JsbSmashTileCallMgr } from "../Common/JsbMgr";
import LanguageSmachTile from "../Common/Language";
import ArchiveSystem from "../System/ArchiveSystem";
import AudioTool from "../System/AudioSystem";
import GamingSystem from "../System/GamingSystem";
import SceneManagerSystem from "../System/SceneManagerSystem";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SetPanelTs extends cc.Component {
    static I:SetPanelTs 

    @property(cc.Node)
    content: cc.Node = null;

    @property(cc.Label)
    label_restart: cc.Label = null;

    @property(cc.Label)
    label_TermOfUse: cc.Label = null;

    @property(cc.Label)
    label_PrivacyPolicy: cc.Label = null;

    @property(cc.Sprite)
    head: cc.Sprite = null;

    @property(cc.Toggle)
    Toggle: cc.Toggle = null;

    onLoad(){
        SetPanelTs.I = this;
    }

    show(){
        this.node.active = true;
    }

    async start () {
        this.Toggle.isChecked = !AudioTool.isOpenVolume();
        this.label_restart.string = LanguageSmachTile.getWord("Restart")
        this.label_TermOfUse.string = LanguageSmachTile.getWord("TermOfUse")
        this.label_PrivacyPolicy.string = LanguageSmachTile.getWord("PrivacyPolicy")
        let res:any = await CocosSmachHelper.loadAssetSync("npc/" + ArchiveSystem.localData.headIndex,cc.SpriteFrame);
        this.head.spriteFrame = res;
    }

    onClickClose(){
        this.node.active = false;
        showInterstitialAd()
    }

    async onClickRestart(){
        this.node.active = false;
        showInterstitialAd()
        await GetLevelInfo();
        GamingSystem.realStartLevel();
    }

    onClickTermUser(){
       JsbSmashTileCallMgr.handler("TermsofUse")
    }

    onClickPrivacyPolicy(){
       JsbSmashTileCallMgr.handler("PrivacyPolicy")
    }

    onToggle(event){
       if( this.Toggle.isChecked){
            AudioTool.closeVolume();
       }else{
            AudioTool.openVolume();
       }
    }

    protected onDestroy(): void {
        SetPanelTs.I = null;
    }
}
