const { ccclass, property } = cc._decorator;

@ccclass
export default class ButtonScaleEffect extends cc.Component {
    public start() {
        let duration: number = 0.3;
        let t = cc.tween().to(duration, { scale: this.node.scale * 1.2 })
            .to(duration, { scale: this.node.scale })
            .to(duration, { scale: this.node.scale * 1.2 })
            .to(duration, { scale: this.node.scale })
            .delay(0.5);
        cc.tween(this.node)
            .then(t)
            .repeatForever()
            .start();
    }
}
