import LanguageSmachTile from "../Common/Language";

const {ccclass, property} = cc._decorator;

export const MoneyType = cc.Enum({
    More : 1,
    One : 2,
    large:3
})

@ccclass
export default class MoneyIcon extends cc.Component {
    @property({type:MoneyType})
    moneyType = MoneyType.One

    protected onLoad(): void {
        let sp = this.getComponent(cc.Sprite);
        if (sp){
            if (this.moneyType == MoneyType.One){
                LanguageSmachTile.setOneMoneyIcon(sp)
            }else if(this.moneyType == MoneyType.More){
                LanguageSmachTile.setMoreMoneyIcon(sp)
            }else if(this.moneyType == MoneyType.large){
                LanguageSmachTile.setMoreMoneyIcon(sp)
            }
        }
    }
}