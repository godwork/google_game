import { AUDIOS_PATH } from "../Common/AudioConfig";
import CocosSmachHelper from "../Common/CocosHelper";
import { CUSTOM_EVENT_NAME } from "../Common/Constant";
import getEventEmiter from "../Libraries/EventEmitter";
import AudioTool from "../System/AudioSystem";

const {ccclass, property} = cc._decorator;

@ccclass
export default class RedbagFlySky extends cc.Component {

    static prefabPath = "panels/redbagFlySky";

    static goldPool: cc.NodePool = new cc.NodePool();//金币池
    static async initGoldPool(count = 15) {
        let money = await CocosSmachHelper.loadResSync<cc.Prefab>('panels/redbag', cc.Prefab);
        for (let i = 0; i < count; i++) {
            this.goldPool.put(cc.instantiate(money));
        }
        return
    }

    static async popUpCustom(w_stPos: cc.Vec3=cc.v3(375, cc.view.getVisibleSize().height/2),w_stEndPos: cc.Vec3=cc.v3(375, cc.view.getVisibleSize().height/2), targetNode: cc.Node, count?: number, ShowParent?:cc.Node) {
        let randomScope = 60;
        count = count ? count : 8;
        // let count = 10

        const poolSize = this.goldPool.size();
        if(poolSize<count)await RedbagFlySky.initGoldPool();

        let w_edPos = w_stEndPos;
        let parent = cc.find("Canvas");
        if(!parent) return;
        let node = parent.getChildByName("RedbagFlySky");
        if(!node){
            let prefab = await CocosSmachHelper.loadResSync<cc.Prefab>(RedbagFlySky.prefabPath, cc.Prefab);
            node = cc.instantiate(prefab);
            node.name = "RedbagFlySky"    
            parent.addChild(node,100000000);
        }
       
        AudioTool.play(AUDIOS_PATH.flyprop)
        node.getComponent(RedbagFlySky).showAnim3(w_stPos,w_edPos,count, targetNode);
    }

    //花里胡哨
    public showAnim3(w_stPos: cc.Vec3,w_edPos: cc.Vec3,count:number,targetNode: cc.Node) {
        let edPos=this.node.convertToNodeSpaceAR(w_edPos);
        let stPos=this.node.convertToNodeSpaceAR(w_stPos);
        for (let index = 0; index < count; index++) {
            let gold = RedbagFlySky.goldPool.get();
            if (!gold) return;
            gold.setPosition(stPos);
            this.node.addChild(gold);

            this.addOne(gold,edPos,index,count);
        }
        // SoundMgr.inst.playEffect(ResConfig.SoundPath.getRedbagAndGold);
    }

    addOne(node:cc.Node,edPos:cc.Vec3,index:number,count:number){
        var xx = 5 + Math.random() * 80
        if (Math.random() > 0.5) {
            xx = xx * -1
        }

        var yy = 40 + Math.random() * 50
        var f_time = 0.3 + Math.random() * 0.4

        var act_0 = cc.delayTime(Math.random() * 0.05)
        var act_1 = cc.moveBy(f_time, xx, yy).easing(cc.easeCubicActionOut())
        var act_2 = cc.moveBy(f_time, 0, -yy + 10 - Math.random() * 40 ).easing(cc.easeBounceOut()) // 
        var act_3 = cc.moveTo(0.7, edPos.x,edPos.y).easing(cc.easeIn(3.0))//越来越块 
        var act_4 = cc.callFunc(()=> {
            RedbagFlySky.goldPool.put(node);
            if(index===count-1){
                getEventEmiter().emit(CUSTOM_EVENT_NAME.onMoneyChange)
            }
        })
        var end = cc.sequence(act_0,act_1, act_2, act_3,act_4)
        node.runAction(end);
    }






















    getCirclePoints(r: number, pos: cc.Vec3, count: number, randomScope: number): cc.Vec3[] {
        let points = [];
        //数学中的角度分弧度和角度两种，弧度的圆周是2π，而角度的圆周是360°。所以要将角度化为弧度就只需用角度乘以π/180，反之就除以(π/180)。
        let radians = (Math.PI / 180) * Math.round(360 / count);//角度化为弧度
        for (let i = 0; i < count; i++) {
            let x = pos.x + r * Math.sin(radians * i);
            let y = pos.y + r * Math.cos(radians * i);
            points.unshift(cc.v3(x + Math.random() * randomScope, y + Math.random() * randomScope, 0));
        }
        return points;
    }
}