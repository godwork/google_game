
import TopAreaTs from "./TopAreaTs";
const {ccclass, property} = cc._decorator;

@ccclass
export default class catchMoneyFly extends cc.Component {
    static I:catchMoneyFly
    @property(cc.Node)
    instacteNode: cc.Node = null;
    moneyNodePool:cc.NodePool = null
    

    //创建缓存
    onLoad () {
        catchMoneyFly.I = this;
        this.moneyNodePool = new cc.NodePool()
        for (let index = 0; index < 6; index++) {
            let starNode = cc.instantiate(this.instacteNode)
            this.moneyNodePool.put(starNode)
            
        }
    }
    getOneFromPool(){
        let one = null
        if (this.moneyNodePool.size()>0) {
            one = this.moneyNodePool.get()
        }else{
            one = cc.instantiate(this.instacteNode)
        }
        return one
    }
    flyMoney(worldPos,index){
        let one = this.getOneFromPool()
        one.parent = this.node
        one.position = this.node.convertToNodeSpaceAR(worldPos)
        one.active = true

        let targetNode = TopAreaTs.I.getRedbagFlyTarget();
        let delatT = cc.delayTime(0.05*index)
        let pos1 = targetNode.parent.convertToWorldSpaceAR(targetNode.position)
        let pos2 = this.node.convertToNodeSpaceAR(pos1)
        let act_3 = cc.moveTo(0.7, pos2.x,pos2.y).easing(cc.easeIn(3.0))//越来越块 
       
        //跃动
        var yy = 30 + Math.random() * 40
        var f_time = 0.1 + Math.random() * 0.3
        var act_1 = cc.moveBy(f_time, 0, yy).easing(cc.easeCubicActionOut())
        var act_2 = cc.moveBy(f_time, 0, -yy + 10 - Math.random() * 40 ).easing(cc.easeBounceOut()) // 
        //
        let callback = cc.callFunc(()=>{
            one.active = false;
            this.moneyNodePool.put(one)})
        let seq = cc.sequence(delatT,act_1,act_2,act_3,callback)
        one.runAction(seq)

    }
    protected start(): void {
        
    }
    
}
