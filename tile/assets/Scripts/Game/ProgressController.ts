import { PauseController } from "../Common/Decorators";
import { GameData, GetAccountInfo } from "../Common/GameData";
import GameSmashStaticData from "../GameStaticData";
import ArchiveSystem from "../System/ArchiveSystem";
import GamingSystem from "../System/GamingSystem";
import SceneManagerSystem from "../System/SceneManagerSystem";
import GamePlayController from "./GamePlayController";
import ProgressStarController from "./ProgressStarController";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ProgressController extends cc.Component {
    public static I: ProgressController;

    @property(cc.Sprite)
    public spriteProgressBar: cc.Sprite = null;

    @property([ProgressStarController])
    public starsController: ProgressStarController[] = [];
    
    drawLibaoIndex = -1;

    public onLoad() {
        ProgressController.I = this;
        this.node.active = false //已废弃直接隐藏
       
    }

    public refresh(): void {
        return
        let lastblock = GamePlayController.I.getBlocksLeftNum() 
        let progress = (GamingSystem.mapBlocksNum-lastblock)/GamingSystem.mapBlocksNum;
        this.spriteProgressBar.fillRange = progress;
        if (GameSmashStaticData.isShenHeModel) {
            this.starsController.forEach((psc: ProgressStarController,index) => {
                psc.lightOff();
            });
           return
        }
        this.starsController.forEach((psc: ProgressStarController,index) => {
            let light: boolean = progress > (psc.node.x + this.node.width / 2) / this.node.width;
            if ((index+1) <= this.drawLibaoIndex){
                psc.lightOff();
                return;
            }else{
                psc.lightUp();
                if (light){
                    this.drawLibaoIndex = index + 1;
                        // SceneManagerSystem.showGetCoinOrLiBaoPanels("libao",index+1,()=>{
                        //     GetAccountInfo();
                        // })
                    psc.lightOff();
                }
            }
        });
    }

    public init(isshow:boolean = true): void {
       return
        this.drawLibaoIndex = GameData.LevelData.boxNum
        this.spriteProgressBar.fillRange = 0;
        if (GameSmashStaticData.isShenHeModel) {
            this.starsController.forEach((psc: ProgressStarController,index) => {
                psc.lightOff();
            });
            return
        }
        this.starsController.forEach((psc: ProgressStarController,index:number) => { 
            psc.node.x = -this.node.width/2 + this.node.width*(index+1)/4
            psc.node.active = isshow;
            if (index +1 > this.drawLibaoIndex){
                psc.lightUp(); 
            }else{
                psc.lightOff();
            }
        });
    }

    public getStarsNum(): number {
        return this.starsController.filter((v: ProgressStarController) => { return v.isLight(); }).length;
    }
}
