const { ccclass, property } = cc._decorator;

@ccclass
export default class UIManager extends cc.Component {

    @property([cc.Node])
    public nodeUI: cc.Node[] = [];

    public onLoad(): void {
        this.nodeUI.forEach((v: cc.Node) => {
            v.active = true;
        });
    }
}
