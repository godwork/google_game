const {ccclass, property,requireComponent} = cc._decorator;

@ccclass
@requireComponent(cc.Animation)
export default class AnimationTs extends cc.Component {
    callback: Function;
    static PlayCenterFrame  = "PlayCenterFrame"

    playAni(playType: cc.WrapMode, cb?:Function){
        this.callback = cb;
        let animstate = this.node.getComponent(cc.Animation).play();
        animstate.wrapMode = playType;
        this.node.getComponent(cc.Animation).off(cc.Animation.EventType.FINISHED)
        this.node.getComponent(cc.Animation).on (cc.Animation.EventType.FINISHED ,()=>{
            if (this.callback){
                this.callback(cc.Animation.EventType.FINISHED);
            }
        })
    }

    onPlayCenterEvent(){
        if (this.callback){
            this.callback(AnimationTs.PlayCenterFrame);
        }
    }

    onDestroy() {
        
    }
}
