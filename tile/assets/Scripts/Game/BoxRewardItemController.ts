import { PROP_BUTTON_TYPE } from "../Common/Constant";
import ArchiveSystem from "../System/ArchiveSystem";
import { BUTTON_TYPE } from "./PropButtonController";

const { ccclass, property } = cc._decorator;

@ccclass
export default class BoxRewardItemController extends cc.Component {
    @property(cc.Label)
    public labelAmount: cc.Label = null;

    @property([cc.Node])
    public nodesRewardTypes: cc.Node[] = [];

    private _collected: boolean = false;
    private _rewardType: PROP_BUTTON_TYPE;
    private _rewardAmount: number;


    public setReward(type: PROP_BUTTON_TYPE, amount: number): void {
        this._collected = false;
        this._rewardType = type;
        this._rewardAmount = amount;
        this.nodesRewardTypes.forEach((v: cc.Node, i: number) => {
            v.active = type === BUTTON_TYPE[i];
        });
        this.labelAmount.string = amount.toString();
    }

    public onCollect(mul: number = 1): void {
        if (this._collected) return;
        this._collected = true;
        let amount = this._rewardAmount * mul;
        switch (this._rewardType) {
            case PROP_BUTTON_TYPE.recall:
                ArchiveSystem.localData.recallNum += amount;
                break;

            case PROP_BUTTON_TYPE.refresh:
                ArchiveSystem.localData.refreshNum += amount;
                break;

            case PROP_BUTTON_TYPE.hint:
                ArchiveSystem.localData.hintNum += amount;
                break;
        }
    }
}
