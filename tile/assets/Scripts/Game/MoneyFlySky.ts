import { AUDIOS_PATH } from "../Common/AudioConfig";
import CocosSmachHelper from "../Common/CocosHelper";
import { CUSTOM_EVENT_NAME } from "../Common/Constant";
import getEventEmiter from "../Libraries/EventEmitter";
import AudioTool from "../System/AudioSystem";

const { ccclass, property } = cc._decorator;

//美钞飞动效
@ccclass
export default class MoneyFlySky extends cc.Component {

    static prefabPath = "panels/MoneyFlySky";

    static nodePool: cc.NodePool = new cc.NodePool();//金币池

    static async initNodePool(count = 15) {
        let money = await CocosSmachHelper.loadResSync<cc.Prefab>('panels/money', cc.Prefab);
        for (let i = 0; i < count; i++) {
            this.nodePool.put(cc.instantiate(money));
        }
        return
    }

    static async popUpCustom(w_stPos: cc.Vec3 = cc.v3(375, cc.view.getVisibleSize().height / 2), w_stEndPos: cc.Vec3 = cc.v3(375, cc.view.getVisibleSize().height / 2), targetNode: cc.Node, count?: number, ShowParent?:cc.Node) {
        let randomScope = 60;
        count = count ? count : 8;

        const poolSize = this.nodePool.size();
        if (poolSize < count) await MoneyFlySky.initNodePool();

        let w_edPos = w_stEndPos;

        let parent =  cc.find("Canvas");
        if (!parent) return;
        let node = parent.getChildByName("MoneyFlySky");
        if (!node) {
            let prefab = await CocosSmachHelper.loadResSync<cc.Prefab>(MoneyFlySky.prefabPath, cc.Prefab);
            node = cc.instantiate(prefab);
            node.name = "MoneyFlySky"
            parent.addChild(node,100000000);
        }
        AudioTool.play(AUDIOS_PATH.flyprop)
        node.getComponent(MoneyFlySky).showAnim3(w_stPos, w_edPos, count, targetNode);
    }

    public showAnim2(w_stPos: cc.Vec3, w_edPos: cc.Vec3, count: number, targetNode: cc.Node) {
        let edPos = this.node.convertToNodeSpaceAR(w_edPos);
        let stPos = this.node.convertToNodeSpaceAR(w_stPos);
        for (let index = 0; index < count; index++) {
            let gold = MoneyFlySky.nodePool.get();
            if (!gold) return;
            gold.setPosition(stPos);
            cc.tween(gold)
                .delay(index * 0.06).call(
                    () => {
                        this.node.addChild(gold);
                    }
                )
                .to(0.7, { position: edPos })
                .call(() => {
                    MoneyFlySky.nodePool.put(gold);
                    cc.tween(targetNode)
                        .to(0.1, { scale: 1.1 })
                        .to(0, { scale: 1 })
                        .start()
                    if (index === count - 1) {
                        getEventEmiter().emit(CUSTOM_EVENT_NAME.onMoneyChange)
                    }
                })
                .start()
        }
        // SoundMgr.inst.playEffect(ResConfig.SoundPath.getRedbagAndGold);
    }

    //花里胡哨
    public showAnim3(w_stPos: cc.Vec3, w_edPos: cc.Vec3, count: number, targetNode: cc.Node) {
        let edPos = this.node.convertToNodeSpaceAR(w_edPos);
        let stPos = this.node.convertToNodeSpaceAR(w_stPos);
        for (let index = 0; index < count; index++) {
            let gold = MoneyFlySky.nodePool.get();
            if (!gold) return;
            gold.setPosition(stPos);
            this.node.addChild(gold);

            this.addOne(gold, edPos, index, count);
        }
        // SoundMgr.inst.playEffect(ResConfig.SoundPath.getRedbagAndGold);
    }

    addOne(node: cc.Node, edPos: cc.Vec3, index: number, count: number) {
        var xx = 5 + Math.random() * 80
        if (Math.random() > 0.5) {
            xx = xx * -1
        }

        var yy = 40 + Math.random() * 50
        var f_time = 0.3 + Math.random() * 0.4

        var act_0 = cc.delayTime(Math.random() * 0.05)
        var act_1 = cc.moveBy(f_time, xx, yy).easing(cc.easeCubicActionOut())
        var act_2 = cc.moveBy(f_time, 0, -yy + 10 - Math.random() * 40).easing(cc.easeBounceOut()) // 
        var act_3 = cc.moveTo(0.7, edPos.x, edPos.y).easing(cc.easeIn(3.0))//越来越块 
        var act_4 = cc.callFunc(() => {
            MoneyFlySky.nodePool.put(node);
            if (index === count - 1) {
                // EventCenter.emit(EventType.addMoney)
            }
        })
        var end = cc.sequence(act_0, act_1, act_2, act_3, act_4)
        node.runAction(end);
    }
}