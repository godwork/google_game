const { ccclass, property } = cc._decorator;

@ccclass
export default class ZIndexController extends cc.Component {
    @property(cc.Integer)
    public zIndex: number = 0;

    public onLoad() {
        this.node.zIndex = this.zIndex;
    }
}
