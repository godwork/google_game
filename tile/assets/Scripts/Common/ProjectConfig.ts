import CCLog from "../CCLog";
import GameSmashStaticData from "../GameStaticData";
import GameSmashStaticJsonData from "../GameStaticJsonData";
import { JsbSmashTileCallMgr } from "./JsbMgr";
import LanguageSmachTile, { Country } from "./Language";


const baseurl = JsbSmashTileCallMgr.handler("getServerIP","()Ljava/lang/String;")
CCLog.log("安卓给的ip",baseurl);
/** 项目配置 */
export const PROJECT_CONFIG = {
    appID: 'x',
    gameName: 'tile-master',
    serverUrl: baseurl== undefined ? "https://leisure.classicsfoo.com/" : baseurl + "/",
    testServerUrl:"http://172.32.10.33:7773/"
}

/**
 *  保留num位小数位, 正负数皆可
 * 
 * @param origin 元数据, 正负数皆可
 * @param num 保留小数的位数, 需要>=1
 * @returns 
 */
export function forMattedMoney(origin: number):string {
    //本地计算了 origin都是美元 getMoneyChangeRate  汇率
    origin = GameSmashStaticJsonData.getTwoPointNum(origin*GameSmashStaticJsonData.getMoneyChangeRate) 
    if ((LanguageSmachTile.getCountry != Country.ID)&&(LanguageSmachTile.getCountry != Country.RU)){
        return origin + ""
    }else{
        //印尼的话 去尾取证
        
        origin = Number(Math.floor(origin))
        if (typeof origin == "number"){
            origin = keepDecimalByNum(origin,2)
            let num = (origin+"").split(".");
            num[0] = num[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return num.join(".")
        }else{
            origin = keepDecimalByNum(parseFloat(origin),2)
            return origin + ""
        }
    }
}

export function keepDecimalByNum(origin: number, num: number):number {
    if (num <= 0) {
        return origin;
    }
    let multiple = 1;
    for (let i = 0; i < num; i++) {
        multiple = multiple * 10
    }
    return Math.floor(origin * multiple) / multiple;
}