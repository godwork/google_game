import { BizTypeEnum, HttpAccount, HttpCloseWindowCount, HttpGetJsonFromServer, HttpLevelInfo, HttpLogin, HttpPostServerAward } from "../net/api";
import { Response } from "../HttpMgr";
import UISmashToast from "./UIToast";
import { Game } from "./Entities";
import getEventEmiter from "../Libraries/EventEmitter";
import { CUSTOM_EVENT_NAME } from "./Constant";
import GameSceneController from "../Scenes/GameSceneController";
import CCLog from "../CCLog";
import GameSmashStaticData, { rewardFlyType } from "../GameStaticData";

import GameSmashStaticJsonData from "../GameStaticJsonData";
import LocalKey from "../LocalKey";
import LanguageSmachTile, { Country } from "./Language";

export const GameData: Game = {
    LoginData: {},
    AccountData: {},
    LevelData: {},
    receiveInfo: {},
    HttpCloseWindowCount : 18
}


/**登陆 */
export const GetHttpLogin = async () => {
    let data:Response = await HttpLogin();
    if (data.resCode === 0) {
        GameData.LoginData = data.data;
       if (CC_JSB) {
        if (data.data.abTestInfo&&data.data.abTestInfo.ab_lists) {
            for (let index = 0; index < data.data.abTestInfo.ab_lists.length; index++) {
                if (data.data.abTestInfo.ab_lists[index].testId== 26||data.data.abTestInfo.ab_lists[index].testId== "26") {
                    if (data.data.abTestInfo.ab_lists[index].groupId== 1||data.data.abTestInfo.ab_lists[index].groupId== "1") {
                            GameSmashStaticData.isShowFlyBox = true
                    }else{
                            GameSmashStaticData.isShowFlyBox = false
                    }
                }
                    
            }
        }
            
       }
        
    }else{
        UISmashToast.popUp(data.msg);
    }
    return data;
}


/**账号 */
export const GetAccountInfo = async (pos?: cc.Vec3) => {
    let data:Response = await HttpAccount();
    if (data.resCode === 0) {
        let coinAdd = 0;
        let moneyAdd = 0;
        GameData.AccountData.moneyLimit = data.data.moneyLimit
        data.data.aclist.forEach(element => {
            if (element.type == "coin"){
                if (GameData.AccountData){
                    if (GameData.AccountData.cointotal){
                        coinAdd =  element.total - GameData.AccountData.cointotal;
                    }
                }
                GameData.AccountData.cointotal = element.total
            }else if (element.type == "money"){
                if (GameData.AccountData){
                    if (GameData.AccountData.moneytotal){
                        moneyAdd =  element.total - GameData.AccountData.moneytotal;
                    }
                }
                GameData.AccountData.moneytotal = element.total
            }
        });
        if (coinAdd > 0){
            //GameSceneController.I && GameSceneController.I.showMoneyFlyAnim(pos);
            GameSmashStaticData.showWithNewFlyAnimation(rewardFlyType.rewardType_coin)
        }
        getEventEmiter().emit(CUSTOM_EVENT_NAME.onCoinChange,coinAdd);
        if (moneyAdd > 0){
            GameSmashStaticData.showWithNewFlyAnimation(rewardFlyType.rewardType_moeny)
            //GameSceneController.I && GameSceneController.I.showRedbagFlyAnim(pos);
        }
        getEventEmiter().emit(CUSTOM_EVENT_NAME.onMoneyChange,moneyAdd);
    }else{
        UISmashToast.popUp(data.msg);
    }
    return data;
}
/**账号 */
export const GetAccountInfo_WithoutAniml = async (addNum) => {
    let data:Response = await HttpAccount();
    if (data.resCode === 0) {
        let coinAdd = 0;
        let moneyAdd = 0;
       
        data.data.aclist.forEach(element => {
            if (element.type == "coin"){
                if (GameData.AccountData){
                    if (GameData.AccountData.moneytotal){
                        coinAdd =  element.total - GameData.AccountData.cointotal;
                    }
                }
                GameData.AccountData.cointotal = element.total
            }else if (element.type == "money"){
                if (GameData.AccountData){
                    if (GameData.AccountData.moneytotal){
                        moneyAdd =  element.total - GameData.AccountData.moneytotal;
                        
                    }
                }
                GameData.AccountData.moneytotal = element.total
            }
        });
        
        getEventEmiter().emit(CUSTOM_EVENT_NAME.onMoneyChange,addNum);
    }else{
        UISmashToast.popUp(data.msg);
    }
    return data;
}
/**账号 */
export const GetAccountSelfAdd = async (pos?: cc.Vec3,coinAdd?:number,moneyAdd?:number,playAni = true) => {
        if (coinAdd > 0){
            //GameSceneController.I && GameSceneController.I.showMoneyFlyAnim(pos);
            GameData.AccountData.cointotal =  GameData.AccountData.cointotal+coinAdd
            GameSmashStaticJsonData.saveLocal_coinMoney(LocalKey.localCoin,GameData.AccountData.cointotal)
            GameSmashStaticData.showWithNewFlyAnimation(rewardFlyType.rewardType_coin)
            getEventEmiter().emit(CUSTOM_EVENT_NAME.onCoinChange,coinAdd);
        }
        
        if (moneyAdd!=0){
            GameData.AccountData.moneytotal =  GameSmashStaticJsonData.getTwoPointNum(GameData.AccountData.moneytotal+moneyAdd) 
            GameSmashStaticJsonData.saveLocal_coinMoney(LocalKey.localMoney,GameData.AccountData.moneytotal)
            if (moneyAdd>0&&playAni==true) {
                GameSmashStaticData.showWithNewFlyAnimation(rewardFlyType.rewardType_moeny)
            }
            //GameSceneController.I && GameSceneController.I.showRedbagFlyAnim(pos);
            getEventEmiter().emit(CUSTOM_EVENT_NAME.onMoneyChange,moneyAdd,playAni);
        }
        
}

/**根据关卡书计算出信息 passNum为当前关*/ 
export const GetLevelInfo = async (passNum?:number) => {
    if (!passNum) {
        passNum =  GameSmashStaticJsonData.getLocalSavePassNum()
    }
    GameData.LevelData.passNum = passNum
    GameData.LevelData.passCoin = GameSmashStaticJsonData.getLevelPassRewardCoin(GameData.AccountData.cointotal)
    GameData.LevelData.passMoney = GameSmashStaticJsonData.getLevelPassRewardMoney(GameData.AccountData.moneytotal)
    GameData.LevelData.coinTile = GameSmashStaticJsonData.getcoinTileAndMoneyTileNum(GameData.LevelData.passNum).coinTile
    GameData.LevelData.moneyTile = GameSmashStaticJsonData.getcoinTileAndMoneyTileNum(GameData.LevelData.passNum).cashTile
    
    // let data:Response = await HttpLevelInfo();
    // if (data.resCode === 0) {
    //     GameData.LevelData = data.data;
    // }else{
    //     UIToast.popUp(data.msg);
    // }
    // return data;
}

/**等级 */
export const GetReceiveAward =  (biztype: BizTypeEnum,AddCoinNum,AddMoney) => {
    // let data:Response = await HttpReceiveAward(biztype,GameData.LevelData.passNum,boxNum,GameData.LevelData.extData,getTpe);
    // if (data.resCode === 0) {
    //     GameData.receiveInfo = data.data;
    // }else{
    //     UIToast.popUp(data.msg);
    // }
    // return data;
    //CCLog.log("++++++上传奖励",biztype,AddCoinNum,AddMoney)
    HttpPostServerAward(biztype,AddCoinNum,AddMoney)

}

/**游戏配置 */
export const GetGameJsonServer = async () => {
   
        if (LanguageSmachTile.getCountry == Country.BR) {
            GameSmashStaticJsonData.getMoneyChangeRate = 5.14
        }else if(LanguageSmachTile.getCountry == Country.ID){
            GameSmashStaticJsonData.getMoneyChangeRate = 15168
        }else if(LanguageSmachTile.getCountry == Country.RU){
            GameSmashStaticJsonData.getMoneyChangeRate = 90
        }else{
            GameSmashStaticJsonData.getMoneyChangeRate = 1
        }
   
    if (CC_JSB) {
        let data:Response = await HttpGetJsonFromServer();
        if (data.resCode === 0) {
           
           
           if (data.data.nc) {
               GameSmashStaticJsonData.newUserAward = data.data.nc
           }
           if (data.data.cc) {
               GameSmashStaticJsonData.levelCoinRule = data.data.cc
           }
           if (data.data.crc) {
               GameSmashStaticJsonData.coinFanbeiRule = data.data.crc
           }
           if (data.data.mc) {
              GameSmashStaticJsonData.levelMoneyRule = data.data.mc
           }
           if (data.data.mrc) {
              GameSmashStaticJsonData.moneyFanBeiRate = data.data.mrc
           }
           if (data.data.psc) {
              GameSmashStaticJsonData.coinTileMoneyTileRule = data.data.psc
           }
           if (data.data.pac) {
              GameSmashStaticJsonData.forceVideoRule = data.data.pac
           }
           if (data.data.dgc) {
              GameSmashStaticJsonData.wabaoRule = data.data.dgc
           }
           if (data.data.frc) {
              GameSmashStaticJsonData.flyBoxRate = data.data.frc
           }
           if (data.data.hdc) {
               GameSmashStaticJsonData.h5Activity = data.data.hdc
           }
           //CCLog.log("+++++++汇率",data.data.rm,data.data.rm.rate)
           if (data.data.rm&&data.data.rm.rate) {
               GameSmashStaticJsonData.getMoneyChangeRate = GameSmashStaticJsonData.getTwoPointNum(Number(data.data.rm.rate)) 
           }
           if (data.data.acc) {
              GameSmashStaticJsonData.InterstitialAd = data.data.acc
           }
           if (data.data.wdc) {
              GameSmashStaticJsonData.tixianParm = data.data.wdc
           }
          




           /*
           CCLog.log("++++++相关的配置1 newUserAward",GameStaticJsonData.newUserAward.coin,GameStaticJsonData.newUserAward.money)
           for (let index = 0; index < GameStaticJsonData.levelCoinRule.length; index++) {
                
                let _a = GameStaticJsonData.levelCoinRule[index]
                CCLog.log("++++++++相关的配置2 levelCoinRule",_a.startCoin,_a.endCoin)
            
           }
           for (let index = 0; index < GameStaticJsonData.coinFanbeiRule.length; index++) {
                let _a = GameStaticJsonData.coinFanbeiRule[index]
                CCLog.log("++++++++相关的配置3 coinFanbeiRule",_a.multiplemin,_a.multiplemaxn)
           }
           for (let index = 0; index < GameStaticJsonData.levelMoneyRule.length; index++) {
            let _a = GameStaticJsonData.levelMoneyRule[index]
            CCLog.log("++++++++相关的配置4 levelMoneyRule",_a.startMoney,_a.endMoney)
           }
           for (let index = 0; index < GameStaticJsonData.moneyFanBeiRate.length; index++) {
            let _a = GameStaticJsonData.moneyFanBeiRate[index]
            CCLog.log("++++++++相关的配置5 moneyFanBeiRate",_a.multiplemin,_a.multiplemaxn)
           }
           for (let index = 0; index < GameStaticJsonData.coinTileMoneyTileRule.length; index++) {
            let _a = GameStaticJsonData.coinTileMoneyTileRule[index]
            CCLog.log("++++++++相关的配置6 coinTileMoneyTileRule",_a.minLevel,_a.maxLevel)
           }
           for (let index = 0; index < GameStaticJsonData.forceVideoRule.length; index++) {
            let _a = GameStaticJsonData.forceVideoRule[index]
            CCLog.log("++++++++相关的配置7 forceVideoRule",_a.lvNum)
           }
           
            CCLog.log("++++++++相关的配置8 wabaoRule",GameStaticJsonData.wabaoRule.startLv,JSON.stringify(GameStaticJsonData.wabaoRule))
            CCLog.log("++++++++相关的配置9 flyBoxRate",GameStaticJsonData.flyBoxRate.coin)
            */
          

        }else{
            UISmashToast.popUp(data.msg);
        }
    }
   
    
}