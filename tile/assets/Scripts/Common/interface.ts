
export interface NativeMethods{
    getServerIP:()=>string;
    decodeMesPack:(msg:string)=>string
    encodeMesPack:(msg:string)=>string
    showVideoAd:()=>void
    coinDrawCash:(jsonStr)=>void;
    moneyDrawCash:(jsonStr)=>void;
    PrivacyPolicy:()=>void
    TermsofUse:()=>void
    getCommonParm:()=>string //获取基础参数
    getLanguage:()=>string
    showInterstitialAd:()=>void
    getDeviceInfo:()=>string //获取设备信息
    getVideoSpace:()=>number //视频时间间隔
    buryPoint:(json:string)=>void
    buryZeusEvent:(EventName:string,Money:string)=>void
    requestIsWhiteBao:()=>string//是不是白包
    isNetworkConnect:()=>string
    showWithdraw:()=>void
    pass:()=>void
    playMergeAudio:()=>void
    sensorsBuryPoint:(EventName:string,jsonStr:string)=>void
    getH5AdTaskReward:(rewardstr:string)=>void
    showH5Ad:()=>void
    requestIsFraudUser:()=>string
}

declare global {
    interface Window {
        onAdShow:(plantFromId?:number)=>void //激励视频开始播放
        onAdClose:(plantFromId?:number,result?:string)=>void //激励视频关闭
        onAdError:(adErrorMsg:string)=>void //激励视频出错 
        
    }
}
