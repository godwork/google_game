import CCLog from "../CCLog";
import { NativeMethods } from "./interface";


export class JsbSmashTileCallMgr{
    private static ANDROIDClassName:string='smash.master.com.zUkzs19mDMhuDW7YRmMPV';

    public static handler<T extends keyof NativeMethods,K extends Parameters<NativeMethods[T]>>(methodName:T,methodSignature:string='()V', ...parameters: K):ReturnType<NativeMethods[T]> {
        let o:ReturnType<NativeMethods[T]>
        if (cc.sys.os == cc.sys.OS_ANDROID){
            //o=jsb.reflection.callStaticMethod(JsbMgr.ANDROIDClassName, "invoke",methodSignature,...parameters);


            let msg = {
                ovl1:methodName
            }
            let obj = {};
            for (let i = 0; i < parameters.length; i++)
            {
                let value = parameters[i];
                let key = "ovl" + (i+2);
                obj[key] = value
               
            }
            CCLog.log("getStringobj====",obj)
            Object.assign(msg,obj)
            let str = JSON.stringify(msg);
            CCLog.log("androidgetString str====",str)
            let res = jsb.reflection.callStaticMethod(JsbSmashTileCallMgr.ANDROIDClassName, "ovl1T24M8pTzp2", "(Ljava/lang/String;)Ljava/lang/String;",str);
            
            CCLog.log("androidgetString res ",res);
            
        
            return res;

        }
        return o;
    }
}

