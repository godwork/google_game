const {ccclass, property} = cc._decorator;

@ccclass
export default class UISmashToast extends cc.Component {
    static prefabPath = "panels/UIToast";

    static async popUp(str: string) {
        let prefab:any = await new Promise((res: Function, rej: Function) => {
            cc.resources.load(UISmashToast.prefabPath, (e: Error, a: cc.Prefab) => {
                if (a){
                    res(a);
                }
            });
        });
        let node = cc.instantiate(prefab);    
        let parent = cc.find("Canvas");
        if(!parent) return;
        parent.addChild(node,cc.macro.MAX_ZINDEX);
        let lab = node.getChildByName("label")?.getComponent(cc.Label);
        if(lab) lab.string = str;
        node.getComponent(UISmashToast).showAnim();
    }

    public showAnim() {
        this.scheduleOnce(()=>{
            this.node.y = 0;
            this.node.opacity = 255;
            //this.node.getChildByName("nodebg").width = this.node.getChildByName("label").width+80
            this.node.getChildByName("nodebg").height = this.node.getChildByName("label").height+20
            cc.tween(this.node)
            .delay(0.8)
            .by(0.5, {position: cc.v3(0, 100, 0)})
            .to(0.5, {opacity: 0})
            .call(() => {
                this.node.destroy();
                this.node.removeFromParent();
            }).start();
        })

    }
}