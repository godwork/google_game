/**方块皮肤路径前缀 */
export const SKIN_PATH_PRE: string = 'BlockSkin';

/**关卡配置文件前缀 */
export const LEVLE_PATH_PRE: string = 'Level';

/**默认皮肤配置组 */
export const SKIN_GROUP_DEFAULT: string = '1';

/**上层比下层碰撞体tag属性增加的值 */
export const BLOCK_TAG_ADDITION: number = 1000;

/**触摸体的碰撞体tag值 */
export const TOUCHER_COLLIDER_TAG: number = 7777;

/**引导层的层级 */
export const GUIDE_LAYER_ZINDEX: number = 100;

/**引导时块的层级 */
export const GUIDE_BLOCK_ZINDEX: number = 110;

/**收集块时的块的层级 */
export const BLOCK_COLLECT_ZINDEX: number = 120;

/**收集区域的层级 */
export const CATCHER_ZINDEX: number = 110;

/**收集块的缓动速度 */
export const BLOCK_COLLECT_SPEED: number = 1200;

/**相同块达到这么数量就消除 */
export const BLOCK_ELIMINATE_LIMIT: number = 3;

/**右上角宝箱奖励的倒计时长 */
export const TIME_BOX_DURATION: number = 300;

/**通关宝箱奖励星星累计数量 */
export const LEVEL_REWARD_SRARS_LIMIT: number = 10;

/**体力上限 */
export const LIFE_MAX: number = 5;

/**一次视频奖励的体力数量 */
export const LIFE_REWARD_ONCE_VIDEO: number = 3;

/**恢复体力时长 */
export const LIFT_RECOVER_DURATION = 600;

/**上下区域动画时间 */
export const AREA_MOVE_DURATION: number = 0.2;

/**没有操作超过这个时间就提示道具使用 */
export const PROP_TIP_SHOW_AFTER: number = 5;

/**没有操作超过这个时间就更换提示道具 */
export const PROP_TIP_CHANGE_AFTER: number = 2;

/**已加载的皮肤的默认值 */
export const SKIN_LOADED_DEFAULT: ISkinLoaded = { blockSkin: {} };

/**对象池名称 */
export const NODE_POOL_NAME = {
    singleBlock: 'singleBlock',
};

/**场景名称 */
export const SCENES_NAME = {
    /** 游戏页面 */
    GameScene: 'Scenes/GameScene',
}

/**预制体名称 */
export const PREFABS_NAME = {

    LevelPanel: 'panels/LevelPanel',
    GetCoinAndLiBaoPanel: 'panels/GetCoinAndLiBaoPanel',
    RedBagPanel: 'panels/RedBagPanel',
    gameSucessPanel: 'panels/gameSucessPanel',
    gameFailPanel: 'panels/gameFailPanel',
    propUsePanel: 'panels/propUsePanel',
    SetPanel: 'panels/SetingPanel',
    gamePhonePanel: 'panels/gamePhonePanel',
    gamephoneTip: 'panels/phoneTip',
}

/** 自定义事件名称 */
export const CUSTOM_EVENT_NAME = {
    localDataUpdate: 'localDataUpdate',
    onSceneChanged: 'onSceneChanged',
    onGameStart: 'onGameStart',
    onGameComplete: 'onGameComplete',
    onSceneChangedComplete: 'onSceneChangedComplete',

    onCoinChange: "onCoinChange",
    onMoneyChange: "onMoneyChange",

    updateRefreshWhiteBao:"updateRefreshWhiteBao",
    videoPlayFailed:"videoPlayFailed"
}

/**道具/体力 的类型， */
export enum PROP_BUTTON_TYPE {
    recall = 0,
    refresh = 1,
    hint = 2,
    life = 3,
}