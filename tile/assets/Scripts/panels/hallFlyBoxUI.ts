

//主页 宝箱


import CCLog from "../CCLog";
import { showVideoAd } from "../Common/AdMethods";
import { CUSTOM_EVENT_NAME } from "../Common/Constant";
import { GameData, GetAccountInfo, GetAccountSelfAdd, GetReceiveAward } from "../Common/GameData";
import { JsbSmashTileCallMgr } from "../Common/JsbMgr";
import LanguageSmachTile, { Country } from "../Common/Language";
import GameSmashStaticData from "../GameStaticData";
import GameSmashStaticJsonData from "../GameStaticJsonData";
import getEventEmiter from "../Libraries/EventEmitter";
import { BizTypeEnum } from "../net/api";



const { ccclass, property } = cc._decorator;

@ccclass
export default class hallFlyBoxUI extends cc.Component {

    @property(cc.Node)
    nodefly:cc.Node= null
    @property(sp.Skeleton)
    spineAnimal: sp.Skeleton = null;
    private haveStartedFly = false
  
   
    onLoad(){
      if (LanguageSmachTile.getCountry == Country.ID) {
         this.spineAnimal.setAnimation(0,"yinni",true)
      }else if(LanguageSmachTile.getCountry == Country.BR){
         this.spineAnimal.setAnimation(0,"baxi",true)
      }else{
         this.spineAnimal.setAnimation(0,"oumei",true)
      }
      getEventEmiter().on(CUSTOM_EVENT_NAME.onSceneChangedComplete, this.startShowBaoXiang, this);
    }
    start() {
        // if (!GameStaticData.isShenHeModel&&GameStaticData.isShowFlyBox == true) {
        //     this.moveBaoXiangAction(3)
        // }
        
       
    }
    startShowBaoXiang(){
        if (GameData.LevelData.passNum>=3) {
            if (this.haveStartedFly) {
                return
            }
            this.haveStartedFly = true
            //CCLog.log("+++++++=宝箱关数",GameData.LevelData.passNum)
            if (!GameSmashStaticData.isShenHeModel&&GameSmashStaticData.isShowFlyBox == true) {
                this.unscheduleAllCallbacks()
                this.moveBaoXiangAction(15)
            }
        }
        
    }
    async onClickBox(){
        let func = ()=>{
            let isRewardCoin = true
            let coinRate = GameSmashStaticJsonData.flyBoxRate.coin*100
            let random = GameSmashStaticJsonData.getRandomNumInter(0,100)
            if (random<coinRate) {
                isRewardCoin = true
            }else{
                isRewardCoin = false
            }
            this.stopFlyBox()
            this.moveBaoXiangAction(this.getRandom(60,70))
           
            let addCoin = 0
            let addMoney = 0
            if (isRewardCoin) {
                addCoin = GameSmashStaticJsonData.getLevelPassRewardCoin(GameData.AccountData.cointotal)
                GetReceiveAward(BizTypeEnum.v2_fly,addCoin,0)
                JsbSmashTileCallMgr.handler("sensorsBuryPoint","(Ljava/lang/String;Ljava/lang/String;)V","balloon_coin","")
                let jsonStr = {event:"balloon_coin",loglevel:2}
                JsbSmashTileCallMgr.handler("buryPoint","(Ljava/lang/String;)V",JSON.stringify(jsonStr))
            }else{
                addMoney = GameSmashStaticJsonData.getLevelPassRewardMoney(GameData.AccountData.moneytotal)
                GetReceiveAward(BizTypeEnum.v2_fly,0,addMoney)
                JsbSmashTileCallMgr.handler("sensorsBuryPoint","(Ljava/lang/String;Ljava/lang/String;)V","balloon_cash","")
                let jsonStr = {event:"balloon_cash",loglevel:2}
                JsbSmashTileCallMgr.handler("buryPoint","(Ljava/lang/String;)V",JSON.stringify(jsonStr))
               
            }
            let pos = this.nodefly.parent.convertToWorldSpaceAR(this.nodefly.position)
            GetAccountSelfAdd(pos,addCoin,addMoney)
        }
        if (CC_JSB) {
            showVideoAd().then(async (res: string) => {
                func()
             }).catch(() => { });
         }else{
            func()
         }
    }
    moveBaoXiangAction(time){
        this.scheduleOnce(()=>{
            cc.Tween.stopAllByTarget(this.nodefly)
            
            let spaceX = cc.view.getVisibleSize().width/4
            let x1 =  -cc.view.getVisibleSize().width/2
            let x2 = cc.view.getVisibleSize().width/2
            let y1 = cc.view.getVisibleSize().height/2-300
            let y2 = cc.view.getVisibleSize().height/2-100


            let startPos = cc.v2(x1-200,y1)
           
            let pos1 = cc.v2(x1+spaceX,y1-50)
            let pos2 = cc.v2(x1+spaceX*2,y1+50)
            let pos3 = cc.v2(x1+spaceX*3,y1-50)
            let endPos = cc.v2(x2+200,y1+20)
            //计算距离
            let length1 = pos1.sub(startPos).len()
            let length2 = pos2.sub(pos1).len()
            let length3 = pos3.sub(pos2).len()
            let length4 = endPos.sub(pos3).len()
            let speed = (length1+length2+length3+length4)/7

            let time1 = length1/speed
            let time2 = length2/speed
            let time3 = length3/speed
            let time4 = length4/speed


            this.nodefly.position = cc.v3(startPos.x,startPos.y)
            this.nodefly.active = true
           
            cc.tween(this.nodefly)
            .to(time1, { position: cc.v3(pos1.x,pos1.y,0) })
            .to(time2, { position: cc.v3(pos2.x,pos2.y,0) })
            .to(time3, { position: cc.v3(pos3.x,pos3.y,0) })
            .to(time4, { position: cc.v3(endPos.x,endPos.y,0) })
            .call(()=>{this.moveBaoXiangAction(this.getRandom(60,70))})
            .start();
        },time)
    }
    stopFlyBox(){
        this.unscheduleAllCallbacks()
        cc.Tween.stopAllByTarget(this.nodefly)
        this.nodefly.active = false
    }
   
    onDestroy(){
        
    }
    closeSelf() {
     
    }
    getRandom(min,max): number {
        // const min = 300000;
        // const max = 700000;
        const random = Math.floor(Math.random() * (max - min + 1) + min);
        return random;
    }
}