// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import CCLog from "../CCLog";
import { showInterstitialAd, showVideoAd } from "../Common/AdMethods";
import { AUDIOS_PATH } from "../Common/AudioConfig";
import { GameData, GetAccountSelfAdd, GetReceiveAward } from "../Common/GameData";
import { JsbSmashTileCallMgr } from "../Common/JsbMgr";
import LanguageSmachTile from "../Common/Language";
import AnimationTs from "../Game/AnimationTs";
import GameSmashStaticJsonData from "../GameStaticJsonData";
import { BizTypeEnum } from "../net/api";
import AudioTool from "../System/AudioSystem";
import SceneManagerSystem from "../System/SceneManagerSystem";

const {ccclass, property} = cc._decorator;

@ccclass
export default class GetCoinAndLiBaoPanel extends cc.Component {

    @property(cc.Label)
    label_title: cc.Label = null;
    
    @property(cc.Label)
    label_yuan: cc.Label = null;

    @property(cc.Label)
    label_des: cc.Label = null;

    @property(cc.Label)
    button_label: cc.Label = null;

    @property(cc.Label)
    label_noThanks: cc.Label = null;

    @property(cc.Node)
    nodeClose: cc.Node = null;

    @property(cc.Node)
    viewad: cc.Node = null;

    @property(cc.Node)
    coin: cc.Node = null;
    
    @property(cc.Node)
    libao: cc.Node = null;

    @property(cc.Node)
    viewAd: cc.Node = null;

    @property(sp.Skeleton)
    jinbispine: sp.Skeleton = null;
    
    @property([AnimationTs])
    yanhuaTs: Array<AnimationTs>= []
   

    private addCoinNormal = 0
    
    type: any 
    callback:Function

    onLoad () {
        this.playerYanHuaAni();
    }

    private playerybInterval;
    playerYanHuaAni(index:number = 0){
        if (cc.isValid(this.yanhuaTs) && cc.isValid(this.yanhuaTs[index])){
            this.yanhuaTs[index].playAni(cc.WrapMode.Normal,(state)=>{
                if (state == cc.Animation.EventType.FINISHED){
                    if (index == 0 || index == 2){
                        this.playerYanHuaAni(index+1);
                    }else if (index == 3){
                        this.playerybInterval = setTimeout(()=>{
                            this.playerybInterval = null;
                            this.playerYanHuaAni();
                        },2000)
                    }
                }else if(state == AnimationTs.PlayCenterFrame){
                    if (index == 1){
                        this.playerYanHuaAni(index+1);
                    }
                }
            })
        }
    }

    show(type="coin",callback?:Function){
        this.callback = callback;
        this.type = type
        
        this.label_title.string = LanguageSmachTile.getWord("CongrAtulations")
        this.label_noThanks.string = LanguageSmachTile.getWord("NoThanks") 
       
        this.libao.active = false
        this.yanhuaTs.forEach(item=>{
            item.node.active = type == "coin"
        })

        if (type == "coin"){
            AudioTool.play(AUDIOS_PATH.coinandmoney);
            this.label_des.string = LanguageSmachTile.getWord("YGACTIR")
            this.jinbispine.node.active = true;
            this.jinbispine.setAnimation(0,"jinbi",false)
            this.coin.active = true;
            this.label_noThanks.node.active = true
            this.nodeClose.active = true
            this.label_yuan.node.active = true;
            
            //this.button_label.string = Language.getWord("Random") + " 1-5"
            this.button_label.string = LanguageSmachTile.getWord("Collect")

            this.addCoinNormal = GameSmashStaticJsonData.getLevelPassRewardCoin(GameData.AccountData.cointotal)
            this.label_yuan.string = this.addCoinNormal + "";
        }else if (type == "libao")
        {
            //废弃了
            // AudioTool.play(AUDIOS_PATH.libaopanel);
            // this.label_des.string = Language.getWord("YHACTGCR")
            // this.jinbispine.node.active = false;
            // this.libao.active = true
            // this.viewad.active = false
            // this.label_noThanks.node.active = false
            // this.label_yuan.node.active = false;
            // this.coin.active = false;
            // this.button_label.string = Language.getWord("Open")
        }else if (type == "newplayerlibao"){
            this.viewAd.active = false;
            this.label_title.string = LanguageSmachTile.getWord("NewbieActivities")
            this.jinbispine.node.active = true;
            this.coin.active = true;
            this.libao.active = false
            this.label_noThanks.node.active = false;
            this.nodeClose.active = false
            this.label_des.node.active = false;
            this.button_label.string = LanguageSmachTile.getWord("SaveInWallet")
            this.label_yuan.string = GameSmashStaticJsonData.newUserAward.coin + "";
        }
    }
    
    async onClickViewAd(){
        if (this.coin.active){
            if (this.type == "newplayerlibao"){
                JsbSmashTileCallMgr.handler("sensorsBuryPoint","(Ljava/lang/String;Ljava/lang/String;)V","lv2_guide4_coin","")
                let jsonStr = {event:"lv2_guide4_coin",loglevel:2}
                JsbSmashTileCallMgr.handler("buryPoint","(Ljava/lang/String;)V",JSON.stringify(jsonStr))
                GetReceiveAward(BizTypeEnum.v2_novice2,GameSmashStaticJsonData.newUserAward.coin,0)
                this.callback && this.callback();
                //更新账户
                GetAccountSelfAdd(null,GameSmashStaticJsonData.newUserAward.coin,0)
                this.node.destroy();
                return;
            }
            let fuc = ()=>{
                // let rate = GameStaticJsonData.getRewardCoinRate(GameData.AccountData.cointotal)
                // let fanbeiNum = Math.floor(this.addCoinNormal*rate)
                // GetReceiveAward(BizTypeEnum.v2_morec,fanbeiNum,0)
                //GetAccountSelfAdd(null,fanbeiNum,0)
                GetReceiveAward(BizTypeEnum.v2_cgcoin,this.addCoinNormal,0)
                GetAccountSelfAdd(null,this.addCoinNormal,0)
                showInterstitialAd()
                
                this.callback && this.callback();
                this.node.destroy();
            }
            if (CC_JSB) {
                showVideoAd().then(async (res: string) => {
                    fuc()         
                }).catch(() => { });
            }else{
                fuc()
            }
           
        }
        //礼包废弃
        // else{ 
        //     if (this.msg.rewardMoney > 0){
        //         showInterstitialAd()
        //         await SceneManagerSystem.showGetRedBagPanels(this.callback,this.msg)
        //         this.node.destroy();
        //     }else{
        //         showInterstitialAd()
        //         this.show(this.msg,"coin",this.callback)
        //     }
        // }
    }

    onClickClose(){
       
        this.callback && this.callback();
        this.node.destroy();
    }
   
}
