// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { showInterstitialAd ,showVideoAd} from "../Common/AdMethods";
import { AUDIOS_PATH } from "../Common/AudioConfig";
import { GameData, GetAccountInfo, GetAccountSelfAdd, GetLevelInfo, GetReceiveAward } from "../Common/GameData";
import LanguageSmachTile from "../Common/Language";
import { forMattedMoney } from "../Common/ProjectConfig";
import GameSceneController from "../Scenes/GameSceneController";
import AudioTool from "../System/AudioSystem";
import GamingSystem from "../System/GamingSystem";
import SceneManagerSystem from "../System/SceneManagerSystem";
import { JsbSmashTileCallMgr } from "../Common/JsbMgr";
import GameSmashStaticData, { videoSceneType } from "../GameStaticData";
import { BizTypeEnum } from "../net/api";
import getEventEmiter from "../Libraries/EventEmitter";
import { CUSTOM_EVENT_NAME } from "../Common/Constant";
import GameSmashStaticJsonData from "../GameStaticJsonData";
import LocalKey from "../LocalKey";
const {ccclass, property} = cc._decorator;

@ccclass
export default class GameSucessPanelTs extends cc.Component {
    static I :GameSucessPanelTs
    @property(cc.Node)
    content: cc.Node = null;

    @property(cc.Label)
    label_title: cc.Label = null;

    @property(cc.Label)
    label_reward: cc.Label = null;

    @property(cc.Label)
    label_coin: cc.Label = null;

    @property(cc.Label)
    label_redbag: cc.Label = null;

    @property(cc.Node)
    nodecoin: cc.Node = null;

    @property(cc.Node)
    nodeRed: cc.Node = null;

    @property(cc.Node)
    nodeNext: cc.Node = null;
    @property(cc.Node)
    guangNode: cc.Node = null;

    @property(cc.Label)
    button_label: cc.Label = null;
   
   
    private clicktime = 0
    private clicktime2 = 0
    private canClickXBtn = false
    private canClickVideoXBtn = false
    private currentLvPassMoney = 0
    onLoad () {
       
       GameSucessPanelTs.I = this;
       getEventEmiter().on(CUSTOM_EVENT_NAME.videoPlayFailed, this.videoPlayFailed, this);
    }
    //视频播放失败
    async videoPlayFailed(){
        if (GameSmashStaticData.videoScene == videoSceneType.videoScene_GameSucc_Btn_force) {
            GameSceneController.I.startGame()
        }
    }
    show(){
        
        this.currentLvPassMoney = GameData.LevelData.passMoney

        let str = {level_number:GameData.LevelData.passNum+""}
        JsbSmashTileCallMgr.handler("sensorsBuryPoint","(Ljava/lang/String;Ljava/lang/String;)V","level",JSON.stringify(str));
        let jsonStr = {event:"level",intvalue:GameData.LevelData.passNum,loglevel:2}
        JsbSmashTileCallMgr.handler("buryPoint","(Ljava/lang/String;)V",JSON.stringify(jsonStr))
        JsbSmashTileCallMgr.handler("pass","()V");
         //本地:passNum+1
         GameData.LevelData.passNum+=1
         GameSmashStaticJsonData.saveLocalPassNum( GameData.LevelData.passNum)

       
       
        JsbSmashTileCallMgr.handler("buryZeusEvent","(Ljava/lang/String;Ljava/lang/String;)V","ext_pass","");
        this.node.active = true;
        this.controlShenHe()
       
        this.canClickXBtn = true
        this.canClickVideoXBtn = true
        AudioTool.play(AUDIOS_PATH.level_complete);
        this.label_title.string = LanguageSmachTile.getWord("GameVictory")
        this.button_label.string = LanguageSmachTile.getWord("Collect")
        this.label_coin.string = GameData.LevelData.passCoin+""
        this.label_redbag.string = LanguageSmachTile.MoneyChar + forMattedMoney(this.currentLvPassMoney);
    }
    controlShenHe(){
        if (GameSmashStaticData.isShenHeModel) {
            this.nodeRed.active = false
            this.nodecoin.active = false
            this.nodeNext.y = -150
            this.label_reward.string = LanguageSmachTile.getWord("GameVictory")
        
            this.guangNode.active =false
            this.label_redbag.node.active = false
            this.node.active = false
            this.NextLevel(false);
        }else{
            this.label_reward.string = LanguageSmachTile.getWord("GetRewards")
            this.nodeRed.active = true
            this.nodecoin.active = false
            
        }
    }
     onClickSeeVideo(){
        if (!this.canClickVideoXBtn) {
            return
        }
        if (this.clicktime2 > Date.now()){   
            return;
        }
        this.clicktime2 = Date.now() + 2000
        if (CC_JSB) {
            showVideoAd().then(async (res: string) => {
                 this.canClickVideoXBtn = false
                 this.canClickXBtn = false
                 GetReceiveAward(BizTypeEnum.v2_pass,0,this.currentLvPassMoney)
                 this.NextLevel(false);
             }).catch(() => { });
         }else{
             GetReceiveAward(BizTypeEnum.v2_pass,0,this.currentLvPassMoney)
             this.NextLevel(false);
         }
         
    }

    async onClickClose(){
        if (!this.canClickXBtn) {
            return
        }
        if (this.clicktime > Date.now()){   
            return;
        }
        this.clicktime = Date.now() + 2000
        this.NextLevel(true);
       
        
    }
    async NextLevel(isNoramlClose){
            let obj1 = { money:this.currentLvPassMoney};
            JsbSmashTileCallMgr.handler("buryZeusEvent","(Ljava/lang/String;Ljava/lang/String;)V","ext_bonus",JSON.stringify(obj1));

            
            this.node.active = false;
             
           

            await GetLevelInfo()
          
            await GamingSystem.loadMap(GameData.LevelData.passNum);
          
          
            let isforceVideo = GameSmashStaticJsonData.isForcePlayVideo(GameData.LevelData.passNum-1)
            if (!isNoramlClose) {
                GetAccountSelfAdd(null,0,this.currentLvPassMoney);
            }
            if(isforceVideo&&isNoramlClose == true){   
                showVideoAd(videoSceneType.videoScene_GameSucc_Btn_force).then(async (res: string) => {
                   
                    GameSceneController.I.startGame()
                }).catch(() => { });
            }else{
               
                GameSceneController.I.startGame()
            }
    }
    protected onDestroy(): void {
        getEventEmiter().off(CUSTOM_EVENT_NAME.videoPlayFailed, this.videoPlayFailed, this);
        GameSucessPanelTs.I = null;
    }
}
