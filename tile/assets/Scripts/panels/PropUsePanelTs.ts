// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import CCLog from "../CCLog";
import { showInterstitialAd, showVideoAd } from "../Common/AdMethods";
import { AUDIOS_PATH } from "../Common/AudioConfig";
import { JsbSmashTileCallMgr } from "../Common/JsbMgr";
import LanguageSmachTile from "../Common/Language";
import ArchiveSystem from "../System/ArchiveSystem";
import AudioTool from "../System/AudioSystem";
import SceneManagerSystem from "../System/SceneManagerSystem";

const {ccclass, property} = cc._decorator;

@ccclass
export default class PropUsePanelTs extends cc.Component {
    static I:PropUsePanelTs

    @property(cc.Node)
    content: cc.Node = null;

    @property(cc.Label)
    label_title: cc.Label = null;
    @property(cc.Label)
    label_btn: cc.Label = null;

    @property(cc.Label)
    label_num: cc.Label = null;

    @property([cc.Node])
    icon: Array<cc.Node> = [];

    index:number
    callback:Function

    onLoad () {
        PropUsePanelTs.I = this;
    }

    start () {

    }

    show(index,callback){
        this.node.active = true;
        this.index = index;
        this.callback = callback;
        this.label_title.string = LanguageSmachTile.getWord("Props")
        this.label_btn.string = LanguageSmachTile.getWord("Free")
        this.label_num.string = "x3";

        this.icon.forEach((item,i)=>{
            item.active = index == i
        })
    }

    onClickClose(){
        this.node.active = false;
        showInterstitialAd()
    }

    onViewAdClick(){
        
        if (this.index == 0) {
            JsbSmashTileCallMgr.handler("sensorsBuryPoint","(Ljava/lang/String;Ljava/lang/String;)V","refresh_ad","")
            let jsonStr = {event:"refresh_ad",loglevel:2}
            JsbSmashTileCallMgr.handler("buryPoint","(Ljava/lang/String;)V",JSON.stringify(jsonStr))
        }else if(this.index == 2){
            JsbSmashTileCallMgr.handler("sensorsBuryPoint","(Ljava/lang/String;Ljava/lang/String;)V","prompt_ad","")
            let jsonStr = {event:"prompt_ad",loglevel:2}
            JsbSmashTileCallMgr.handler("buryPoint","(Ljava/lang/String;)V",JSON.stringify(jsonStr))
        }
        
        if (cc.sys.os == cc.sys.OS_ANDROID&&CC_JSB){
            showVideoAd().then(()=>{
                this.node.active = false;
                this.callback && this.callback();
            })
        }else{
            this.node.active = false;
            this.callback && this.callback();
        }
        
    }
    // update (dt) {}
    protected onDestroy(): void {
        PropUsePanelTs.I = null;
    }
}
