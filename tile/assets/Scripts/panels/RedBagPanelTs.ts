// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import CCLog from "../CCLog";
import { showInterstitialAd, showVideoAd } from "../Common/AdMethods";
import { AUDIOS_PATH } from "../Common/AudioConfig";
import { GameData, GetAccountSelfAdd, GetReceiveAward } from "../Common/GameData";
import { JsbSmashTileCallMgr } from "../Common/JsbMgr";
import LanguageSmachTile, { Country } from "../Common/Language";
import { forMattedMoney } from "../Common/ProjectConfig";
import AnimationTs from "../Game/AnimationTs";
import GameSmashStaticData, { rewardFlyType } from "../GameStaticData";
import GameSmashStaticJsonData from "../GameStaticJsonData";
import { BizTypeEnum } from "../net/api";
import AudioTool from "../System/AudioSystem";

const {ccclass, property} = cc._decorator;

@ccclass
export default class RedBagPanelTs extends cc.Component {
    @property(cc.Label)
    label_title: cc.Label = null;

    @property(cc.Label)
    label_yuan: cc.Label = null;
    
    @property(cc.Label)
    label_des: cc.Label = null;

    @property(cc.Label)
    button_label: cc.Label = null;
    
    @property(cc.Label)
    label_cur: cc.Label = null;

    @property(cc.Label)
    label_total: cc.Label = null;

    @property(sp.Skeleton)
    meichaospine: sp.Skeleton = null;
    @property(sp.Skeleton)
    idmeichaospine: sp.Skeleton = null;
    @property(sp.Skeleton)
    pkmeichaospine: sp.Skeleton = null;

    @property(cc.Sprite)
    ProgressBar: cc.Sprite = null;

    @property(cc.Node)
    money: cc.Node = null;
    
    @property(cc.Node)
    viewAd: cc.Node = null;

    @property(cc.Node)
    button_close: cc.Node = null;
 
    @property([AnimationTs])
    yanhuaTs: Array<AnimationTs>= []
    @property(cc.Node)
    centerNode: cc.Node= null
  
    private addMoney = 0
    
   

  
    callback:Function
    type = ""
    onLoad () {
        this.playerYanHuaAni();
       
    }

    show(type,callback){
        this.type = type
        let country = LanguageSmachTile.getCountry;
        this.meichaospine.node.active = false;
        this.idmeichaospine.node.active = false;
        this.pkmeichaospine.node.active = false;
        if (country == Country.PK){
            this.pkmeichaospine.node.active = true;
            this.pkmeichaospine.setAnimation(0,"huangmeichao",false)
        }else if (country == Country.ID){
            this.idmeichaospine.node.active = true;
            this.idmeichaospine.setAnimation(0,"hongmeichoa",false)
        }else{
            this.meichaospine.node.active = true;
            this.meichaospine.setAnimation(0,"lvmeichao",false)
        }
        AudioTool.play(AUDIOS_PATH.coinandmoney);
        this.label_title.string = LanguageSmachTile.getWord("CongrAtulations")

        let limit = GameData.AccountData.moneyLimit+""
        if (LanguageSmachTile.getCountry == Country.ID) {
            limit = "3000.000"
        }if (LanguageSmachTile.getCountry == Country.RU) {
            limit = "20,000"
        }
        this.label_des.string = LanguageSmachTile.getWord("TRYCWI",limit)
        //this.button_label.string = Language.getWord("Random") + "x1~5";
        this.button_label.string = LanguageSmachTile.getWord("Collect")
        
        this.label_cur.string = LanguageSmachTile.MoneyChar + forMattedMoney(GameData.AccountData.moneytotal)
        this.label_total.string = "/" + LanguageSmachTile.MoneyChar + limit;
        this.ProgressBar.fillRange = (GameSmashStaticJsonData.getTwoPointNum(GameData.AccountData.moneytotal*GameSmashStaticJsonData.getMoneyChangeRate))/ GameData.AccountData.moneyLimit;
        this.viewAd.active= true;
        this.button_close.active = true;

        if (this.type == "newplayerlibao"){
            this.viewAd.active = false;
            this.button_close.active = false;
            this.button_label.string = LanguageSmachTile.getWord("SaveInWallet");
            this.label_title.string = LanguageSmachTile.getWord("NewbieActivities")
            this.label_yuan.string = LanguageSmachTile.MoneyChar + forMattedMoney(GameSmashStaticJsonData.newUserAward.money) + ""
        }else{
            this.addMoney = GameSmashStaticJsonData.getLevelPassRewardMoney(GameData.AccountData.moneytotal)
            this.label_yuan.string = LanguageSmachTile.MoneyChar + forMattedMoney(this.addMoney) + ""
        }
        this.callback = callback
       
    }

    private playerybInterval;
    playerYanHuaAni(index:number = 0){
        if (cc.isValid(this.yanhuaTs) && cc.isValid(this.yanhuaTs[index])){
            this.yanhuaTs[index].playAni(cc.WrapMode.Normal,(state)=>{
                if (state == cc.Animation.EventType.FINISHED){
                    if (index == 0 || index == 2){
                        this.playerYanHuaAni(index+1);
                    }else if (index == 3){
                        this.playerybInterval = setTimeout(()=>{
                            this.playerybInterval = null;
                            this.playerYanHuaAni();
                        },2000)
                    }
                }else if(state == AnimationTs.PlayCenterFrame){
                    if (index == 1){
                        this.playerYanHuaAni(index+1);
                    }
                }
            })
        }
    }

    async onClickViewAd(){
        if (this.type == "newplayerlibao"){
            JsbSmashTileCallMgr.handler("sensorsBuryPoint","(Ljava/lang/String;Ljava/lang/String;)V","lv1_guide4_cash","")
            let jsonStr = {event:"lv1_guide4_cash",loglevel:2}
            JsbSmashTileCallMgr.handler("buryPoint","(Ljava/lang/String;)V",JSON.stringify(jsonStr))
            GetReceiveAward(BizTypeEnum.v2_novice,0,GameSmashStaticJsonData.newUserAward.money)  
            //GameStaticData.showWithNewFlyAnimation(rewardFlyType.rewardType_moeny)
        }
        if (this.viewAd.active == false){
            this.callback && this.callback();
            GetAccountSelfAdd(null,0,GameSmashStaticJsonData.newUserAward.money)
           
            this.node.destroy();
        }else{
            let func = ()=>{
                // let rate = GameStaticJsonData.getRewardMoneyRate(GameData.AccountData.moneytotal)
                // let addNum =  GameStaticJsonData.getTwoPointNum(this.addMoney*rate)
                // GetReceiveAward(BizTypeEnum.v2_morem,0,addNum);

                GetReceiveAward(BizTypeEnum.v2_cgmoney,0,this.addMoney)
                GetAccountSelfAdd(null,0,this.addMoney)
                this.callback && this.callback();
            
                this.node.destroy();
            }
            if (CC_JSB) {
                showVideoAd().then(async (res: string) => {
                    func()
                }).catch(() => { });
            }else{
                func()
            }
        }
    }

    onClickClose(){
     
        this.callback && this.callback();
        showInterstitialAd()
        this.node.destroy();
    }
    protected onDestroy(): void {
    }
    // update (dt) {}
}
