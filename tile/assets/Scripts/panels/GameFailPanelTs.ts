// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { showInterstitialAd, showVideoAd } from "../Common/AdMethods";
import { AUDIOS_PATH } from "../Common/AudioConfig";
import { GameData } from "../Common/GameData";
import LanguageSmachTile from "../Common/Language";
import AudioTool from "../System/AudioSystem";
import SceneManagerSystem from "../System/SceneManagerSystem";

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameFailPanelTs extends cc.Component {
    static I : GameFailPanelTs
    @property(cc.Node)
    content: cc.Node = null;

    @property(cc.Label)
    label_title: cc.Label = null;


    @property(cc.Label)
    button_label: cc.Label = null;

    callback:Function 
    public onLoad() {
        GameFailPanelTs.I = this;
    }

    show (callback) {
        this.node.active = true;
        AudioTool.play(AUDIOS_PATH.level_failed);
        this.callback = callback;
        this.label_title.string = LanguageSmachTile.getWord("GameFailed")
        this.button_label.string = LanguageSmachTile.getWord("continue")
    }

    onClickClose(){
        this.node.active = false;
        showInterstitialAd()
        this.callback && this.callback(0);
        this.callback = null;
    }

    onViewAdClick(){
        if (CC_JSB) {
            showVideoAd().then(async (res: string) => {
                this.node.active = false;
                this.callback && this.callback(1);
            }).catch(() => { });
        }else{
            this.node.active = false;
            this.callback && this.callback(1);
        }
        
    }

    protected onDestroy(): void {
        GameFailPanelTs.I = null
    }
}
