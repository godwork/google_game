// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { AUDIOS_PATH } from "../Common/AudioConfig";
import { GameData } from "../Common/GameData";
import LanguageSmachTile from "../Common/Language";
import { forMattedMoney } from "../Common/ProjectConfig";
import GameSmashStaticData from "../GameStaticData";
import AudioTool from "../System/AudioSystem";
import SceneManagerSystem from "../System/SceneManagerSystem";

const {ccclass, property} = cc._decorator;

@ccclass
export default class LevelPanelTs extends cc.Component {
    static I:LevelPanelTs 

    @property(cc.Node)
    content: cc.Node = null;

    @property(cc.Label)
    label_title: cc.Label = null;

    @property(cc.Label)
    label_reward: cc.Label = null;

    @property(cc.Label)
    label_coin: cc.Label = null;
   
    @property(cc.Label)
    label_redbag: cc.Label = null;

    @property(cc.Node)
    nodeCoin: cc.Node = null;

    @property(cc.Node)
    nodeRed: cc.Node = null;


    callback: Function 
    onLoad () {
        LevelPanelTs.I = this;
    }

    start () {

    }

    show(callback){
        AudioTool.play(AUDIOS_PATH.levelpanel)
        this.controlshenHe()
        this.callback = callback
        this.label_title.string = LanguageSmachTile.getWord("Level").toUpperCase() + ":" + GameData.LevelData.passNum;
        this.label_coin.string = GameData.LevelData.passCoin + "";
        this.label_redbag.string = LanguageSmachTile.MoneyChar + forMattedMoney(GameData.LevelData.passMoney) + "";
        SceneManagerSystem.dialogOnOpenX(this.content,()=>{
            setTimeout(()=>{
               this.hideMyself();
            },1500)
        })
    }
    controlshenHe(){
        if (GameSmashStaticData.isShenHeModel) {
            this.label_reward.string = LanguageSmachTile.getWord("Level").toUpperCase() + ":" + GameData.LevelData.passNum;
            this.nodeCoin.active = false
            this.nodeRed.active = false
        }else{
            this.label_reward.string = LanguageSmachTile.getWord("Rewards");
            this.nodeCoin.active = true
            this.nodeRed.active = true
        }
    }
    hideMyself(){
        SceneManagerSystem.dialogOnCloseX(this.content,()=>{
            this.callback && this.callback();
            this.callback = null;
        })
    }

    protected onDestroy(): void {
        LevelPanelTs.I = null;
    }

    // update (dt) {}
}
