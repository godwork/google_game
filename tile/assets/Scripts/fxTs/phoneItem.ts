// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

import getEventEmiter from "../Libraries/EventEmitter";
import SceneManagerSystem from "../System/SceneManagerSystem";
import CocosSmachHelper from "../Common/CocosHelper"
const {ccclass, property} = cc._decorator;

@ccclass
export default class phoneItem extends cc.Component {



    @property(cc.Node)
    newNode: cc.Node = null;

    @property(cc.Sprite)
    bg: cc.Sprite = null;

    @property(cc.Node)
    lockNode: cc.Node = null;

    @property(cc.Node)
    checkNode: cc.Node = null;

    @property(cc.Node)
    checkRight: cc.Node = null;

    @property(cc.Sprite)
    ProgressBar: cc.Sprite = null;

    @property(cc.Label)
    labelProgress: cc.Label = null;


    @property([cc.SpriteFrame])
    bgList: cc.SpriteFrame[] = [];

    id:number;
    isCurent:boolean = false;
    start () 
    {

    }
    public  newTip(isShow:boolean)
    {
        this.newNode.active = isShow;
    }
    public async setId(id:number)
    {
      
        this.id = id;
        this.newNode.active = false;
      
        //let url = "jsResourc/game2/game/bgIcon/s" + (id+1);
        
        //let res:any = await CocosHelper.loadAssetSync(url,cc.SpriteFrame)
        
        //this.bg.spriteFrame = res;


        this.bg.getComponent("ButtonAudio").enable = false;

    
        this.bg.spriteFrame = this.bgList[id];
    }
    public  initCurent(cur:number,total:number)
    {
        this.isCurent = true;
        this.checkRight.active  = false
        this.lockNode.active = false;
        this.checkNode.active = false;
        
        this.ProgressBar.node.parent.active = true;
        
        this.ProgressBar.fillRange = cur / total;
      
        this.labelProgress.string = cur + "/" + total;
        this.bg.getComponent("ButtonAudio").enable = true;
        
    }

    public  setLock()
    {
        this.lockNode.active = true;
        this.checkNode.active = false;
        this.ProgressBar.node.parent.active = false;
    }
    public  setAlreadFinish(isSelect:boolean)
    {
        
        if(isSelect)
        {
            getEventEmiter().emit("checkClick",{checkNode:this.checkRight,id:this.id});

        }
        this.checkRight.active  = isSelect
        this.lockNode.active = false;
        this.checkNode.active = true;
        this.ProgressBar.node.parent.active = false;
    }
    onClickCheck()
    {
        getEventEmiter().emit("checkClick",{checkNode:this.checkRight,id:this.id});
        getEventEmiter().emit("changeBg",this.id);

    
    }
    onClickCurTask()
    {
        if(this.isCurent)
        {
            SceneManagerSystem.showGamephoneTipsPanels();
        }
        else
        {
            if(this.lockNode.active == false)
            {
                this.onClickCheck();
            }
        }
        
    }
 
    // update (dt) {}
}
