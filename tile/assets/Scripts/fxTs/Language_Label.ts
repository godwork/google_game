// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import LanguageSmachTile from "../Common/Language";
const {ccclass, property} = cc._decorator;

@ccclass
export default class Language_Label extends cc.Label {


    // @property({
       
    //     displayName: "多语言key",
    //     tooltip: "多语言中文key"
    // })
    @property({displayName: "多语言key"})
    Key = "";

    onLoad () 
    {
        if(this.Key != "")
        {
            
            this.string = LanguageSmachTile.getWord(this.Key);
        }
    }

    start () {

    }

    // update (dt) {}
}
