// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
import List from "./List";
const {ccclass, property} = cc._decorator;

import ArchiveSystem from "../System/ArchiveSystem";
import Archive from "../Common/Archive";
import { showInterstitialAd } from "../Common/AdMethods";
import UISmashToast from "../Common/UIToast";
import getEventEmiter from "../Libraries/EventEmitter";
import LanguageSmachTile from "../Common/Language";
import GameSmashStaticData from "../GameStaticData";
@ccclass
export default class PhoneMgr extends cc.Component {
    @property(cc.Sprite)
    ProgressBar: cc.Sprite = null;
    @property(cc.Label)
    labelProgress: cc.Label = null;
    @property(List)
    listview: List = null;

  
    curSelect:cc.Node = null;
    selectId:number = null;

    async onLoad () 
    {
        getEventEmiter().on('checkClick', this.itemCheckClick, this);

        let curTaskId = ArchiveSystem.localData.curTaskId;
        this.selectId = ArchiveSystem.localData.curSelectBg;
        
        this.listview.numItems=30;
        
        this.ProgressBar.fillRange = curTaskId / 30;
        this.labelProgress.string = curTaskId + "/" + 30;
    
    }
   
    start () {
        
    }
    public  async itemRender(node: cc.Node, index: number) 
    {
        let curTaskId = ArchiveSystem.localData.curTaskId;
        let curTaskFinish = ArchiveSystem.localData.curTaskFinish;
        let lastOpenID = ArchiveSystem.localData.lastOpenPhoneTaskId
        let ts = node.getComponent("phoneItem");
      
        await ts.setId(index);
        if(curTaskId == index)
        {
            let taskInfo = Archive.getTask(curTaskId-1);

          
            ts.initCurent(curTaskFinish,taskInfo.count);
        }
        else if(curTaskId < index)
        {
            ts.setLock();
        }
        else
        {
            if(this.selectId != null)
            {
                ts.setAlreadFinish(this.selectId == index);
            }
            else
            {
                ts.setAlreadFinish(false);
            }

            if(index >= lastOpenID)
            {
                ts.newTip(true);
            }

        }
        
    }
    itemCheckClick(data:any)
    {
        if(this.curSelect)
        {
            this.curSelect.active = false;
        }
        this.curSelect = data.checkNode;
        this.curSelect.active = true;
        this.selectId = data.id;
    }
    closeBtn()
    {
        ArchiveSystem.localData.lastOpenPhoneTaskId = ArchiveSystem.localData.curTaskId;

        getEventEmiter().emit("updateNewPhoneIcon");

        showInterstitialAd();
        this.node.destroy();
    }
    Register()
    {
        UISmashToast.popUp(LanguageSmachTile.getWord("taskUnFinish"));
    }
    protected onDestroy(): void {
        getEventEmiter().off('checkClick');
        ArchiveSystem.localData.curSelectBg = this.selectId;
        
    }
}
