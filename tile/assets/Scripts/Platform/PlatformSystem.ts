import PlatformDefault from "./PlatformDefault";


class PlatformSystem {
  
    private _platformDefault = PlatformDefault;
    public get platform(): IPlatform {
        switch (cc.sys.platform) {
           
            // 测试用
            default:
                return this._platformDefault;
                break;
        }
    }

    public initialize() {
        switch (cc.sys.platform) {
           

            // 测试用
            default:
                this._platformDefault.initialize();
                break;
        }
    }
}

export default new PlatformSystem();