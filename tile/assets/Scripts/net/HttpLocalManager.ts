import CCLog from "../CCLog";

//编造假数据
export class HttpLocalManager {
    
    public static getResponseStr(type :string, callback: Function) {
        HttpLocalManager.interrupt(type, callback);
    }

    //根据url分发数据, 可以自己创建实体, 也可以使用现有的json数据, 下面有举例说明
    private static interrupt(type:string, callback: Function) {
        if (type == "J_V2_UL"){ //游戏登陆
            let msg = {
                "data": {
                    "userId": "e6dff1a78f194c9725579e4a14ab81c0",
                    "registerTime": 600098855167,
                    "isAudit": true,
                    "passNum": 1,
                    "extData": "ASHDHQHYQWYYE"
                },
                "resCode": 0,
                "msg": "success"
            }
            CCLog.log(msg)
            callback && callback(JSON.stringify(msg));
        }else if (type == "J_V2_ACI"){
            let msg = {
                "data": {
                    "aclist": [
                        {
                            "type": "coin",
                            "total": 0,
                            "rem": 29
                        },
                        {
                            "type": "money",
                            "total": 0,
                            "rem": 76
                        }
                    ],
                    "moneyLimit":500,
                },
                "resCode": 0,
                "msg": "success"
            }
            CCLog.log(msg)
            callback && callback(JSON.stringify(msg));
        }else if (type == "J_V2_PSI"){
            let msg = {
                "data": {
                    "passNum": 1,
                    "passCoin": 76,
                    "passMoney": 8,
                    "extData": "AGDYADDNNQIWIOQWE",
                    "coinTile": 2,
                    "moneyTile": 2,
                    "novice": 0,
                    "novice2": 0,
                    "boxnum":0,
                    "noviceReward" : 12,
                    "novice2Reward":32
                },
                "resCode": 0,
                "msg": "success"
            }
            CCLog.log(msg)
            callback && callback(JSON.stringify(msg));
        }else if (type == "J_V2_PR"){
            let msg = {
                "data": {
                    "rewardCoin": 57,
                    "accountCoin": 6,
                    "rewardMoney": 94,
                    "accountMoney": 91,
                    "extData": "qweqweqwrqwrr",
                    "biztype": "pass"
                },
                "resCode": 0,
                "msg": "success"
            }
            CCLog.log(msg)
            callback && callback(JSON.stringify(msg));
        }
    }
}